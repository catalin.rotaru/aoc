import fs = require("fs")

const print = console.log.bind(console)

type Edges = Map<string, Nodes>
type Nodes = Set<string>
type Path = Array<string>

function readGraph(): Edges {
  let res = new Map()

  fs.readFileSync('day12.txt','utf8').split(/\r?\n/).forEach(l => {
    const [a, b] = l.split('-')

    const na: Nodes = res.get(a) || new Set()
    na.add(b)
    res.set(a, na)

    const nb: Nodes = res.get(b) || new Set()
    nb.add(a)
    res.set(b, nb)
  })

  return res
}

function traverse(e: Edges, v: Path = [], s: string = "start"): number {
  const nv = v.concat(s)

  if (s === 'end')
    return 1

  return [...e.get(s)].filter(n => (!nv.includes(n)) || n == n.toUpperCase()).
    reduce((acc, cur) => acc + traverse(e, nv, cur), 0)
}

function traverse2(e: Edges, v: Path = [], s: string = "start"): number {
  const nv = (s == s.toUpperCase()) ? v : v.concat(s)

  if (s === 'end')
    return 1

  return [...e.get(s)].filter(n =>
    (!nv.includes(n)) || ((n !== "start") && (new Set(nv).size == nv.length))).
    reduce((acc, cur) => acc + traverse2(e, nv, cur), 0)
}

const edges = readGraph()
print(edges)

print("A1:", traverse(edges))

print("------------------")

print("A2:", traverse2(edges))