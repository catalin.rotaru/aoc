import fs = require("fs")

const print = console.log.bind(console)

type Crabs = Array<number>
type Cost = (a: number, b: number) => number

function align(c: Crabs, cost: Cost = (a, b) => Math.abs(b - a)): number {
  let min = Math.min(...c)
  let max = Math.max(...c)
  let res: Array<number> = new Array()

  for (let i = min; i <= max; i++)
    res.push(c.reduce((acc, cur) => acc + cost(i, cur), 0))

  return Math.min(...res)
}

const cost2: Cost = (a, b) => {
  const n = Math.abs(b - a)
  return n * (n + 1) / 2
}

function readCrabs(): Crabs {
  return fs.readFileSync('day7.txt','utf8').split(',').map(v => +v)
}

const c1 = "16,1,2,0,4,2,7,1,2,14".split(',').map(v => +v)
print(align(c1))

const crabs = readCrabs()
print("A1:", align(crabs))

print("------------------")

print(align(c1, cost2))
print("A2:", align(crabs, cost2))
