import fs = require("fs")

const print = console.log.bind(console)

type Signals = Array<string>
type Values = Array<string>
type Entries = Array<[Signals, Values]>
type Code = Array<string>

const UNIQ = new Set([2, 4, 3, 7])

const DISPLAY = {
  'abcefg': 0, 
  'cf': 1, 
  'acdeg': 2, 
  'acdfg': 3, 
  'bcdf': 4, 
  'abdfg': 5, 
  'abdefg': 6,
  'acf': 7, 
  'abcdefg': 8, 
  'abcdfg': 9
}

function readEntries(): Entries {
  return fs.readFileSync('day8.txt','utf8').split(/\r?\n/).map(l => {
    const [signal, output] = l.split(' | ')
    return [signal.split(' '), output.split(' ')]
  })
}

const countUniques = (ent: Entries): number =>
  ent.reduce((acc, e) => acc + e[1].reduce((a, v) => a + +UNIQ.has(v.length), 
    0), 0)

function del<Type>(a: Array<Type>, b: Array<Type>): Array<Type> {
  let res = a.slice()

  b.forEach(v => {
    const i = res.indexOf(v)
    if (i > -1)
      res.splice(i, 1)
  })

  return res
}

function remap(signals: Signals): Code {
  const sig = signals.map(s => s.split(''))
  //print(sig)

  const d1 = sig.find(s => s.length == 2) // digit 1 has 2 segments: CF
  const d7 = sig.find(s => s.length == 3) // digit 7 has 3 segments: ACF
  const [lA] = del(d7, d1)  // line that should be A
  //print("line A is", lA)

  const d4 = sig.find(s => s.length == 4) // digit 4 has 4 segments: BCDF
  const lBD = del(d4, d1) // lines that should be BD
  //print(d4, d1, lBD)

  const d8 = sig.find(s => s.length == 7) // digit 8 has 7 segments: ABCDEFG
  const lEG = del(del(d8, d7), lBD) //lines that should be EG
  //print(d8, lEG)

  const d8x3 = [d8, d8, d8].flat() // digit 8 taken 3 times

  // digits 2,3 & 5 have 5 segments each
  const d235 = sig.filter(s => s.length == 5).flat()
  const lBE = [...new Set(del(del(d8x3, d235), d1))]
  //print(lBE)

  const [lB] = lBD.filter(v => lBE.includes(v)) // line that should be B
  //print("line B is", lB)

  const [lD] = del(lBD, [lB]) // line that should be D
  //print("line D is", lD)
  
  const [lE] = del(lBE, [lB]) // line that should be E
  //print("line E is", lE)
  
  const [lG] = del(lEG, [lE]) // line that should be G
  //print("line G is", lG)
  
  // digits 0,6 & 9 have 6 segments each
  const d069 = sig.filter(s => s.length == 6).flat()
  const lCDE = del(d8x3, d069)
  //print(lCDE)

  const [lC] = del(del(lCDE, [lD]), [lE]) // line that should be C
  //print("line C is", lC)
  
  const [lF] = del(d1, [lC]) // line that should be F
  //print("line F is", lF)

  return [lA, lB, lC, lD, lE, lF, lG]
}

const decode = (str: string, code: Code): String =>
  DISPLAY[str.split('').map(v => String.fromCharCode('a'.charCodeAt(0) + 
    code.indexOf(v))).sort().join('')]

function compute(ent: Entries) {
  return ent.map(e => {
    const code = remap(e[0])
    return e[1].map(v => decode(v, code)).join('')
  }).reduce((acc, cur) => acc + +cur, 0)
}

const entries = readEntries()
//print(entries)

print("A1: ", countUniques(entries))

print("------------------")

const cd1 = 
  remap("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab".split(' '))

print(decode("cdfeb", cd1), decode("fcadb", cd1))

print("A2: ", compute(entries))