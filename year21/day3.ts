import fs = require("fs")

const print = console.log.bind(console)

type Report = Array<number>
type Diagnostics = Array<Report>

function readReport(): Diagnostics {
  return fs.readFileSync('day3.txt','utf8').split(/\r?\n/).
    map(l => l.split('').map(s => +s))
}

function frequency(d: Diagnostics, pos: number = 0): [number, number] {
  let zeroes = 0
  let ones = 0

  d.forEach(e => e[pos] ? ones++ : zeroes++)

  return [zeroes, ones]
}

const criteria = (z: number, o: number, most: boolean = true): number => 
  +(most ? (o >= z) : (z > o))

function power(d: Diagnostics): number {
  let gamma: number = 0
  let epsilon: number = 0

  for (let col = 0; col < d[0].length; col++) {
    const mc = criteria(...frequency(d, col))
    gamma = gamma * 2 + mc
    epsilon = epsilon * 2 + 1 - mc
  }

  return gamma * epsilon
}

function rating(diag: Diagnostics, most: boolean = true) {
  let d = [...diag]

  for (let i = 0; d.length > 1; i++)
    d = d.filter(e => e[i] == criteria(...frequency(d, i), most))

  //print(d)
  return d[0].reduce((acc, cur) => acc * 2 + cur)
}

const report = readReport()
//print(report)
print(frequency(report))
print("A1:", power(report))

print("------------------")

print("oxy", rating(report))
print("co2", rating(report, false))
print("A2:", rating(report) * rating(report, false))