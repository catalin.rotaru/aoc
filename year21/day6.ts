import fs = require("fs")

const print = console.log.bind(console)

type Fish = Array<number> // count of fish in index state

function readFish(): Fish {
  return fs.readFileSync('day6.txt','utf8').split(',').map(v => +v)
}

function init(fish: Array<number>): Fish {
  let res = new Array<number>(9).fill(0)
  fish.forEach(f => res[f]++)
  return res
}

function day(fish: Fish): Fish {
  let n = fish[0]
  let res = fish.slice(1).concat(n)
  res[6] += n
  return res
}

function iterate(fish: Fish, days: number = 256): Fish {
  let f = fish
  for (let i = 0; i < days; i++)
    f = day(f)

  return f
}

const count = (fish: Fish) => fish.reduce((acc, cur) => acc + cur)

print(day([1, 1, 2, 1, 0, 0, 0, 0, 0]))

const f1 = init("3,4,3,1,2".split(',').map(v => +v))
print(day(f1))
print(count(iterate(f1, 18)))
print(count(iterate(f1, 80)))

const fish = init(readFish())
print(fish)
print("A1:", count(iterate(fish, 80)))

print("------------------")

print(count(iterate(f1)))
print("A2:", count(iterate(fish)))
