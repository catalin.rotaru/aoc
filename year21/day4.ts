import fs = require("fs")

const print = console.log.bind(console)

type Moves = Array<number>
type Board = Array<Array<number>>
type Boards = Array<Board>

function readGame(): [Moves, Boards] {
  const file = fs.readFileSync('day4.txt','utf8').split(/\r?\n\r?\n/)
  const moves = file[0].split(',').map(v => +v)
  const boards = file.slice(1).map(b => b.split(/\r?\n/)
    .map(l => l.trim().split(/ +/).map(v => +v)))

  return [moves, boards]
}

const bingo = (b: Array<Array<boolean>>): boolean =>
  (b.some(l => l.every(v => v))) || 
    (b.some((_, j) => b.map(l => l[j]).every(v => v)))

const score = (b: Board, h: Array<Array<boolean>>): number => b.reduce(
  (acc, l, j) => acc + l.reduce((a, v, i) => a + (h[j][i] ? 0 : v), 0), 0)

function play(boards: Boards, moves: Moves): void {
  let hit = boards.map(b => b.map(l => l.map(v => false)))
  let win: Array<number> = new Array() // score
  let won: Set<number> = new Set() // boards

  moves.forEach(m => {
    boards.forEach((b, k) => b.forEach((l, j) => l.forEach((v, i) => {
      if (v == m)
        hit[k][j][i] = true
    })))

    hit.forEach((h, i) => {
      if (!won.has(i) && bingo(h)) {
        win.push(m * score(boards[i], h))
        won.add(i)
      }
    })
  })

  print("A1:", win[0])
  print("------------------")
  print("A2:", win[win.length - 1])
}

const [moves, boards] = readGame()
//print(moves)
//print(boards)
print(bingo([[false, true], [true, false]]))
print(bingo([[false, false], [true, true]]))
print(bingo([[false, true], [false, true]]))

play(boards, moves)
