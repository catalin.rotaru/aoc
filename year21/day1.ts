import fs = require("fs")

const print = console.log.bind(console)

type Depths = Array<number>

function readDepths(): Depths {
  return fs.readFileSync('day1.txt','utf8').split(/\r?\n/).map(l => +l)
}

function countIncreases(d: Depths): number {
  let res = 0
  for (let i: number = 1; i < d.length; i++)
    if (d[i - 1] < d [i])
      res++

  return res
}

function slidingSum(d: Depths): Depths {
  let res: Depths = new Array()
  for (let i: number = 2; i < d.length; i++)
    res.push(d[i] + d[i - 1] + d[i - 2])
  
  return res
}

const depths = readDepths()

//print (depths)
print("A1:", countIncreases(depths))

print("------------------")

//print(slidingSum(depths))
print("A2:", countIncreases(slidingSum(depths)))
