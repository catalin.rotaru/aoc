import fs = require("fs")

const print = console.log.bind(console)

const pair = new Map([['[', ']'], ['{', '}'], ['<', '>'], ['(', ')']])
const score = {')': 3, ']': 57, '}': 1197, '>': 25137}
const score2 = {')': 1, ']': 2, '}': 3, '>': 4}

// returns [idx of next char to parse, error]
function parseOne(str: string): [number, string] {
  const begin = str.charAt(0)
  const end = pair.get(begin)

  if (pair.has(begin)) {
    let [n, err] = parse(str.slice(1))
    n++
    if (err != undefined)
      return [n, err + end]

    const ch = str.charAt(n)

    if (!ch.length) // if incomplete, return begin char's pair
      return [n, end]

    if (ch === end) 
      return [n + 1, undefined]
    
    // end doesn't match begin - corruption
    return  [n, '']
  }
  
  return [0, undefined]
}

function parse(str: string): [number, string] {
  for (let i = 0; i < str.length;) {
    if (!pair.has(str.charAt(i)))
      return [i, undefined]
    
    const [n, err] = parseOne(str.slice(i))
    i += n
    if (err != undefined)
      return [i, err]
  }

  return [str.length, undefined]
}

function process(str: string): number {
  const [n, err] = parse(str)
  return ((err != undefined) && (n < str.length)) ? score[str.charAt(n)] : 0
}

const scoreComp = (str: string): number =>
  str.split('').reduce((acc, cur) => 5 * acc + score2[cur], 0)

function process2(str: string): number {
  const [n, err] = parse(str)
  return ((err != undefined) && (n == str.length)) ? scoreComp(err) : 0
}

function readLines(): Array<string> {
  return fs.readFileSync('day10.txt','utf8').split(/\r?\n/)
}

print(parse("{<[]>}{<[]>}{<[]>}"))
print(parse("{<["))


const lines = readLines()
//print (lines)
//print(lines.map(l => process(l)))
print("A1:", lines.map(l => process(l)).reduce((acc, cur) => acc + cur))

print("------------------")

//print(lines.map(l => process2(l)))
const ans2 = lines.map(l => process2(l)).filter(v => v).sort((a, b) => a - b)
print("A2:", ans2[(ans2.length - 1) / 2])
