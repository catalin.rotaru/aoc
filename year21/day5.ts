import fs = require("fs")

const print = console.log.bind(console)
const printFloor = (f: Floor) => f.forEach(f => print(f.join('')))

const SIZE = 1000
type Floor = Array<Array<number>>
type Vents = Array<Array<Array<number>>>

function readVents(): Vents {
  return fs.readFileSync('day5.txt','utf8').split(/\r?\n/).
    map(l => l.split(' -> ').map(s => s.split(',').map(v => +v)))
}

function buildFloor(vents: Vents, right: boolean = true): Floor {
  let res: Floor = Array.from({length: SIZE}, () => new Array(SIZE).fill(0))

  vents.forEach(v => {
    const x1 = v[0][0]
    const y1 = v[0][1]
    const x2 = v[1][0]
    const y2 = v[1][1]
    const dx = Math.sign(x2 - x1)
    const dy = Math.sign(y2 - y1)

    if (right && dx && dy)
      return

    for (let x = x1, y = y1; (x != x2 + dx) || (y != y2 + dy); x += dx, y += dy)
      res[y][x]++
  })

  return res
}

const dangerous = (f: Floor): number => 
  f.reduce((acc, cur) => acc + cur.filter(v => v > 1).length, 0)

const vents = readVents()
const floor = buildFloor(vents)
//printFloor(floor)
print("A1:", dangerous(floor))

print("------------------")

print("A2:", dangerous(buildFloor(vents, false)))