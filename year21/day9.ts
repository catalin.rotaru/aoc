import fs = require("fs")

const print = console.log.bind(console)

type Grid = Array<Array<number>> // y, x
type Cell = [number, number] // x, y
type Cells = Array<Cell>
type Flags = Array<Array<boolean>>

const neighbor = [[0, 1], [0, -1], [1, 0], [-1, 0]] // array of [x, y]

function readGrid(): Grid {
  return fs.readFileSync('day9.txt','utf8').split(/\r?\n/).
    map(l => l.split('').map(v => +v))
}

const onGrid = (g: Grid, x: number, y: number): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, x: number, y: number): Cells =>
  neighbor.map(([dx, dy]) => <Cell>[x + dx, y + dy]).
    filter(([x, y]) => onGrid(g, x, y))

const low = (g: Grid, x: number, y: number): boolean =>
  neighbors(g, x, y).map(([c, l]) => g[l][c]).every(v => v > g[y][x])

const risk = (g: Grid): number =>
  g.reduce((acc, l, y) => acc + l.reduce((a, v, x) => 
    a + (low(g, x, y) ? v + 1 : 0), 0), 0)

function flow(g: Grid, d: Flags, x: number, y: number) {
  d[y][x] = true
  const v = g[y][x]
  neighbors(g, x, y).forEach(([nx, ny]) => {
    const nv = g[ny][nx]
    if ((!d[ny][nx]) && (nv >= v) && (nv < 9))
      flow(g, d, nx, ny)
  })
}

function basin(g: Grid, x: number, y: number) {
  let res: Flags = g.map(l => l.map(_ => false))
  flow(g, res, x, y)

  return res.reduce((acc, l) => acc + l.filter(v => v).length, 0)
}

function basins (g: Grid): number {
  let res = g.map((l, y) => l.map((_, x) => low(g, x, y) ? basin(g, x, y) : 0)).
    flat().sort((a, b) => b - a)

    return res[0] * res[1] * res[2]
}

const grid = readGrid()
//print(grid)
print(onGrid(grid, -1, 5), onGrid(grid, 2, 2), onGrid(grid, 0, 10))
print(low(grid, 0, 0), low(grid, 1, 0), low(grid, 8, 0), low(grid, 6, 4))

print("A1:", risk(grid))

print("------------------")

print(basin(grid, 1, 0), basin(grid, 9, 0))
print("A2:", basins(grid))