import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => 
  grid.forEach(g => print(g.map(v => v ? '█' : ' ').join('')))

type Grid = Array<Array<number>>  // Array[y][x]
type Cell = [number, number]      // x, y
type Cells = Array<Cell>
type Folds = Array<[string, number]>  // Array<[direction, line]
const SIZE = 2000

function readPaper(): [Cells, Folds] {
  const [dots, folds] = fs.readFileSync('day13.txt','utf8').split(/\r?\n\r?\n/)
  const d = dots.split(/\r?\n/).map(l => l.split(',').map(v => +v))
  const f: Folds = folds.split(/\r?\n/).map(l => l.slice(11).split('=')).
    map((l => [l[0], +l[1]]))

  return [<Cells>d, f]
}

const fold = (cells: Cells, d: number, ony: boolean = true): Cells =>
  ony ? cells.map(([x, y]) => [x, (y < d) ? y : 2 * d - y]) :
    cells.map(([x, y]) => [(x < d) ? x : 2 * d - x, y])

function process(cells: Cells, f: Folds): number {
  const cl = f.reduce((acc, cur) => fold(acc, cur[1], cur[0] == 'y'), cells)

  let grid: Grid = Array.from({length: SIZE}, _ => new Array(SIZE).fill(0))
  let xm = 0
  let ym = 0
  cl.forEach((c => {
    const y = c[1]
    const x = c[0]
    grid[y][x] = 1
    ym = y > ym ? y : ym
    xm = x > xm ? x : xm
  }))

  const g = grid.slice(0, ym + 1).map(l => l.slice(0, xm + 1))
  if (g.length < 10)
    printGrid(g)
  return g.reduce((acc, l) => acc + l.filter(v => v).length, 0)
}

const [cells, folds] = readPaper()
//print(cells)
print(folds)

print(fold([[0, 14]], 7))
print(fold([[0, 8]], 7))
print(fold([[9, 0]], 5, false))

print("A1:", process(cells, [folds[0]]))

print("------------------")

print("A2:", process(cells, folds))
