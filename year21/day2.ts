import fs = require("fs")

const print = console.log.bind(console)

type Move = [number, number]  // horizontal, vertical
type Moves = Array<Move>

enum DIR {FWD = "forward", DWN = "down", UP = "up"}

// command => [horizontal, vertical]
const move = {
  [DIR.FWD]: [1, 0],
  [DIR.DWN]: [0, 1],
  [DIR.UP]: [0, -1]
}

function readCommands(): Moves {
  return fs.readFileSync('day2.txt','utf8').split(/\r?\n/).map(l => {
    const [dir, steps] = l.split(' ')
    const s = +steps
    const [h, v] = move[dir]
    return [h * s, v * s]
  })
}

// returns [horizontal, depth]
const execute = (m: Moves): [number, number] =>
  m.reduce((acc, cur) => [acc[0] + cur[0], acc[1] + cur[1]], [0, 0])

// returns [horizontal, depth, aim]
const execute2 = (m: Moves): [number, number, number] =>
  m.reduce((acc, cur) => 
    [acc[0] + cur[0], acc[1] + cur[0] * acc[2], acc[2] + cur[1]], [0, 0, 0])

const commands = readCommands()
//print(commands)
const [horizontal, vertical] = execute(commands)
print("A1:", horizontal * vertical)

print("------------------")

const [horizontal2, vertical2] = execute2(commands)
print("A2:", horizontal2 * vertical2)