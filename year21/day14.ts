import fs = require("fs")

const print = console.log.bind(console)

type Template = Array<string>
type Rules = Map<string, string>  // pair -> insert
type Pairs = Map<string, number>  // pair -> count

function readPolymer(): [Template, Rules] {
  const [tpl, rules] = fs.readFileSync('day14.txt','utf8').split(/\r?\n\r?\n/)
  const rls = rules.split(/\r?\n/).map(l => <[string, string]>l.split(' -> '))

  return [tpl.split(''), new Map(rls)]
}

const step = (t: Template, r: Rules): Template =>
  t.map((ch, i) => {
    const s = ch + t[i + 1]
    return (i < t.length - 1) ? (r.has(s) ? ch + r.get(s) : ch) : ch
  }).join('').split('')

function inc<Type>(m: Map<Type, number>, k: Type, d: number = 1): void {
  m.set(k, m.has(k) ? m.get(k) + d : d)
}

function dec<Type>(m: Map<Type, number>, k: Type, d: number = 1): void {
  const count = m.get(k) - d
  if (count)
    m.set(k, count)
  else 
    m.delete(k)
}

function process(t: Template, r: Rules, steps: number = 10): number {
  let res = t
  for (let i = 0; i < steps; i++)
    res = step(res, r)

  let count: Map<string, number> = new Map()
  res.forEach(v => inc(count, v))

  return Math.max(...count.values()) - Math.min(...count.values())
}

function process2(t: Template, r: Rules, steps: number = 10) {
  let pairs: Pairs = new Map(t.map((ch, i) => ch + t[i + 1]).
    filter(v => v.length == 2).map(p => [p, 1]))

  for (let i = 0; i < steps; i++) 
    new Map(pairs).forEach((v, p) => {
      if (r.has(p)) {
        const ch = r.get(p)
        inc(pairs, p[0] + ch, v)
        inc(pairs, ch + p[1], v)
        dec(pairs, p, v)
      }
    })

  let count: Map<string, number> = new Map();
  pairs.forEach((v, p) => p.split('').map(ch => inc(count, ch, v)))
  inc(count, t[0])
  inc(count, t[t.length - 1])
  //print(count)

  //return pairs
  return (Math.max(...count.values()) - Math.min(...count.values())) / 2
}

const [template, rules] = readPolymer()
//print(template, rules)
print(step(template, rules).join(''))

print("A1:", process(template, rules))

print("------------------")

print(process2(template, rules, 2))
print("A1:", process2(template, rules))
print("A2:", process2(template, rules, 40))
