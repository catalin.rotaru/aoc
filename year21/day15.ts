import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Grid = Array<Array<number>>  // Array[y][x]
type Cell = [number, number]      // x, y
type Cells = Array<Cell>
type Done = Set<string>

const neighbor = [[0, 1], [0, -1], [1, 0], [-1, 0]]  // Cells
const MAX = 100 // Number.MAX_SAFE_INTEGER

function readGrid(): Grid {
  return fs.readFileSync('day15.txt','utf8').split(/\r?\n/).
    map(l => l.split('').map(v => +v))
}

const onGrid = (g: Grid, x: number, y: number): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, x: number, y: number): Cells =>
  neighbor.map(([dx, dy]) => <Cell>[x + dx, y + dy]).
    filter(([x, y]) => onGrid(g, x, y))

const hash = (x: number, y: number): string => x + ',' + y

function minimum(g: Grid, d: Done): Cell {
  let rx = 0
  let ry = 0

  g.forEach((l, y) => l.forEach((v, x) => {
    if ((v < g[ry][rx]) && (!d.has(hash(x, y)))) {
      rx = x
      ry = y
    }
  }))

  return [rx, ry]
}

function find(g: Grid): number {
  let d: Done = new Set()
  let r: Grid = 
    Array.from({length: g.length}, (_, i) => new Array(g[i].length).fill(MAX))
  r[0][0] = 0  // starting point

  while (d.size < g.length * g[0].length) {
    const [x, y] = minimum(r, d)
    d.add(hash(x, y))

    const n = neighbors(g, x, y)
    n.forEach(([l, c]) => {
      const dist = r[y][x] + g[l][c]
      if (dist < r[l][c]) 
        r[l][c] = r[y][x] + g[l][c]
    })
  }

  return r[r.length - 1][r[0].length - 1]
}

const grid = readGrid()
printGrid(grid)
print(minimum([[0, 3], [2, 1]], new Set(['0,0'])))

//print("A1:", find(grid))

print("------------------")