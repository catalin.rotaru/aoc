import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Grid = Array<Array<number>>  // Array[y][x]
type Cell = [number, number]      // x, y
type Cells = Array<Cell>

const neighbor = [[0, 1], [0, -1], [1, 0], [-1, 0], [1, 1], [-1, -1], [1, -1], 
  [-1, 1]]  // array of [x, y]

function readGrid(): Grid {
  return fs.readFileSync('day11.txt','utf8').split(/\r?\n/).
    map(l => l.split('').map(v => +v))
}

const onGrid = (g: Grid, x: number, y: number): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, x: number, y: number): Cells =>
  neighbor.map(([dx, dy]) => <Cell>[x + dx, y + dy]).
    filter(([x, y]) => onGrid(g, x, y))

function flash(g: Grid, cells: Cells): Cells {
  let res = cells.slice()
  
  cells.forEach(c => {
    const next = neighbors(g, ...c).filter(([x, y]) => {
      g[y][x]++
      return (g[y][x] == 10)
    })
    res.push(...flash(g, next))
  })

  return res
}

function step(g: Grid): number {
  let f: Cells = new Array()
  
  g.forEach((l, y) => l.forEach((v, x) => {
    v++
    g[y][x] = v
    if (v == 10)
      f.push([x, y])
  }))

  const res = flash(g, f)

  g.forEach((l, y) => l.forEach((v, x) => {
    if (v > 9)
      g[y][x] = 0
  }))

  return res.length
}

function process(g: Grid, steps: number = 100): number {
  let grid: Grid = g.map(l => l.slice())
  let res = 0

  for (let i = 0; i < steps; i++)
    res += step(grid)

  return res
}

function allSync(g: Grid): number {
  let grid: Grid = g.map(l => l.slice())
  let sz = grid.reduce((acc, cur) => acc + cur.length, 0)

  let res = 1
  for (; step(g) < sz; res++) {}

  return res
}

const grid = readGrid()
printGrid(grid)
print(onGrid(grid, -1, 5), onGrid(grid, 2, 2), onGrid(grid, 0, 10))
//print(step(grid))
//print(step(grid))
//printGrid(grid)

print("A1:", process(grid))

print("------------------")

print("A2:", allSync(grid))