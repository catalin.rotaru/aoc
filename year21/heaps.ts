const print = console.log.bind(console)

export interface BinaryHeap {
  
}

class MinHeap implements BinaryHeap {
  items: Array<number>
  
  constructor() {
    this.items = new Array()
  }
}

const heap = new MinHeap()

print(heap)