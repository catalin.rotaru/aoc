import fs = require("fs")

const print = console.log.bind(console)

type Row = Array<number>          // [x]
type Grid = Array<Row>            // [y] [x]
type Cell = [number, number ]     // [x, y]
type Cells = Array<Cell>

const neighbor = [[0, -1], [1, 0], [0, 1], [-1, 0]] // array of [x, y]

const onGrid = (g: Grid, [x, y]: Cell): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, [x, y]: Cell): Cells =>
  neighbor.map(([dx, dy]) => <Cell> [x + dx, y + dy]).filter(c => onGrid(g, c))

const readDrops=(): Cells =>
  fs.readFileSync('day18.txt','utf8').split(/\r?\n/)
  .map(l => l.split(',')).map(([x, y]) => [+x, +y])

const buildGrid = (sz: number = 71): Grid => 
  Array.from({length: sz}, (_) => new Array(sz).fill(Number.MAX_SAFE_INTEGER))

const drop = (g: Grid, c: Cells) =>
  c.forEach(([x, y]) => g[y][x] = -1)

function flood(g: Grid) {
  g[0][0] = 0
  let next: Cells = [[0, 0]]

  do {
    const c = next.pop()
    const l = g[c[1]][c[0]] + 1
    neighbors(g, c).filter(([x, y]) => g[y][x] > l).forEach(([x, y]) => {
      g[y][x] = l
      next.push([x, y])
    })
  } while (next.length > 0)
}

const exitCost = (g: Grid): number => g[g.length - 1][g[0].length - 1]

const exitAble = (g: Grid): boolean => exitCost(g) < Number.MAX_SAFE_INTEGER

function search(g0: Grid, d: Cells) {
  let a: number = 1023          // exit-able
  let b: number = d.length - 1  // non-exit-able

  do {
    const g: Grid = g0.map(row => row.slice())
    const m = Math.floor((a + b) / 2)

    drop(g, d.slice(1024, m + 1))
    flood(g)

    if (exitAble(g))
      a = m
    else
      b = m
  } while (b - a > 1)

  return d[b].join()
}

const drops = readDrops()
const grid = buildGrid()
drop(grid, drops.slice(0, 1024))
//print(grid)

flood(grid)
//print(grid)

print("A1:", exitCost(grid))

print("------------------")

const grid2 = buildGrid()
drop(grid2, drops.slice(0, 1024))
print("A2:", search(grid2, drops))