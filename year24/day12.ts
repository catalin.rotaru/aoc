import fs = require("fs")

const print = console.log.bind(console)

type Cell = [number, number]    // [x, y]
type Cells = Array<Cell>
type Row = Array<number>        // [x]
type Grid = Array<Row>          // [y] [x]
type Cost = [number, number]    // [area, perimeter]

const neighbor = [[0, -1], [1, 0], [0, 1], [-1, 0]] // array of [x, y]

const onGrid = (g: Grid, [x, y]: Cell): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, [x, y]: Cell): Cells =>
  neighbor.map(([dx, dy]) => <Cell> [x + dx, y + dy]).filter(c => onGrid(g, c))

const readGrid = (): Grid =>
  fs.readFileSync('day12.txt','utf8').split(/\r?\n/).map(l => 
    l.split('').map(s => s.charCodeAt(0)))

const totalCost = (c: Cost): number =>
  c[0] * c[1]

const addCost = (c1: Cost, c2: Cost): Cost =>
  c1.map((v, idx) => v + c2[idx]) as Cost

function flood(g: Grid, reg: Grid, o: Cell, m: number): Cost {
  const [ox, oy] = o
  if (reg[oy][ox] >= 0)
    return [0, 0]

  reg[oy][ox] = m
  const s = g[oy][ox]
  
  const n = neighbors(g, o).filter(([x, y]) => g[y][x] == s)
  const cost = n.reduce((acc, cur) =>
    addCost(acc, flood(g, reg, cur, m)), [0, 0])

  return [cost[0] + 1, cost[1] + 4 - n.length]
}

const value = (g: Grid, o: Cell): number =>
  onGrid(g, o) ? g[o[1]][o[0]] : -1

const around = ([x, y]: Cell) =>
  neighbor.map((n, idx, a) => [n, a[(idx + 1) % 4]])
  .map(([c1, c2]) => [c1, [c1[0] + c2[0], c1[1] + c2[1]], c2]
  .map(([dx, dy]) => <Cell> [x + dx, y + dy]))

const corner=([l, m, r]: Row, o: number): boolean =>
  ((l != o) && (r != o)) || ((l == o) && (r == o) && (m != o))

const corners=(reg: Grid, o: Cell): number =>
  around(o).map(t => corner(t.map(c => value(reg, c)), value(reg, o)))
  .filter(v => v).length

const sides=(reg: Grid, m: number): number =>
  reg.reduce((acc, row, y) => acc + 
    row.reduce((a, v, x) => a + ((v == m) ? corners(reg, [x, y]) : 0), 0), 0)

function regions(g: Grid) {
  const reg: Grid = g.map(row => new Array(row.length).fill(-1))
  const rgs: Array<[number, number]> = new Array()  // [region mark, area]
  let m = 0

  const res1 = reg.reduce((acc, row, y) => acc + row.reduce((a, v, x) => {
    let tc = 0 
    if (v < 0) {
      const cost = flood(g, reg, [x, y], m)
      rgs.push([m, cost[0]])
      tc += totalCost(cost)
      m++
    }
    return a + tc
  }, 0), 0)
  //print(reg)
  //print(rgs)

  print("A1:", res1)

  const res2 = rgs.reduce((acc, [m, p]) => acc + p * sides(reg, m), 0)

  print("A2:", res2)
}

const grid = readGrid()
//printGrid(grid)

regions(grid)

print("------------------")
