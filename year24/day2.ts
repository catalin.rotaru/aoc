import fs = require("fs")

const print = console.log.bind(console)

type Report = Array<number>

function safe(r: Report): boolean {
  let sign: number
  let prev: number

  return !r.some(v => {
    if (prev != undefined) {
      const diff = v - prev
      const delta = Math.abs(v - prev)
      const ns = Math.sign(diff)
      if (sign == undefined)
        sign = ns
      if ((ns != sign) || (delta == 0) || (delta > 3))
        return true
    }

    prev = v
    return false
  })
}

function safe2(r: Report): boolean {
  return r.map((_, i) => r.slice(0, i).concat(r.slice(i + 1))).some(safe)
}

const readReports = (): Array<Report> =>
  fs.readFileSync('day2.txt','utf8').split(/\r?\n/).map(l => 
    l.split(/\s+/).map(v => +v))

print(safe([7, 6, 4, 2, 1]))
print(safe([1, 2, 7, 8, 9]))
print(safe([8, 6, 4, 4, 1]))
print(safe([1, 3, 6, 7, 9]))
print(safe([9, 9, 8, 7, 6]))

const rep = readReports()

print("A1:", rep.reduce((acc, cur) => acc + +safe(cur), 0))

print("------------------")

print(safe2([1, 2, 7, 8, 9]))
print(safe2([1, 3, 2, 4, 5]))

print("A2:", rep.reduce((acc, cur) => acc + +safe2(cur), 0))
