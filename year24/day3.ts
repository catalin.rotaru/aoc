import fs = require("fs")

const print = console.log.bind(console)

type Result = [number, number] // offset, result

const readProg = (): string =>
  fs.readFileSync('day3.txt','utf8')

function parseNumber(s: string): Result {
  let res = 0
  let off: number

  for (off = 0; off <= Math.min(s.length, 3); off++) {
    const ch = s.charAt(off)

    if ((ch < '0') || ch > '9')
      break

    res = 10 * res + +ch
  }

  return [off, res]
}

function parseMul(s: string): Result {
  if (!s.startsWith('mul('))
    return [1, -1]

  let off = 4

  const a = parseNumber(s.substring(off))
  if (a[0] == 0)
    return [off, -1]
  off += a[0]

  if (s.charAt(off) != ',')
    return [off, -1]
  off++

  const b = parseNumber(s.substring(off))
  if (b[0] == 0)
    return [off, -1]
  off += b[0]

  if (s.charAt(off) != ')')
    return [off, -1]
  off++
  
  return [off, a[1] * b[1]]
}

function parseDo(s: string): Result {
  if (s.startsWith('do()'))
    return [4, 1]
  if (s.startsWith("don't()"))
    return [7, 0]
  
  return [0, -1]
}

function parse(s: string): number {
  let res = 0
  let off = 0

  while (off < s.length) {
    const r = parseMul(s.substring(off))
    off += r[0]
    if (r[1] >= 0)
      res += r[1]
  }

  return res
}

function parse2(s: string): number {
  let res = 0
  let off = 0
  let en = 1

  while (off < s.length) {
    let r = parseDo(s.substring(off))
    if (r[1] >= 0) {
      en = r[1]
    } else {
      r = parseMul(s.substring(off))
      if (r[1] >= 0)
        res += en * r[1]
    }
    off += r[0]
  }

  return res
}

print(parseNumber("a"))
print(parseNumber("1"))
print(parseNumber("12b"))
print(parseNumber("12342mul"))

print(parseMul("xm"))
print(parseMul("mul(4*"))
print(parseMul("mul(44,46)"))

print(parse("xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"))

const prog = readProg()

print("A1:", parse(prog))

print("------------------")

print("A2:", parse2(prog))
