import fs = require("fs")

const print = console.log.bind(console)

type Pad = Array<Array<string>> // [y] [x]
type Pads = Array<Pad>
type Cell = [number, number ]   // [x, y]
type Paths = Array<string>
type Costs = Array<number>
type Cache = Map<string, number>

const nPad: Pad = 
  [['7', '8', '9'], ['4', '5', '6'], ['1', '2', '3'], ['', '0', 'A']]
const dPad: Pad = [['', '^', 'A'], ['<', 'v', '>']]
const pads = [nPad, dPad]

function find(p: Pad, k: string): Cell {
  let x: number
  const y = p.findIndex(r => (x = r.findIndex(v => v == k)) >= 0)
  return [x, y]
}

// return best path(s) from o to t on the given pad, avoiding gaps
function navigate(p: Pad, o: string, t: string): Paths {
  const [ox, oy] = find(p, o)
  const [tx, ty] = find(p, t)
  const dx = tx - ox
  const dy = ty - oy

  const h = (dx < 0 ? '<' : '>').repeat(Math.abs(dx))
  const v = (dy < 0 ? '^' : 'v').repeat(Math.abs(dy))
  const ph = h + v
  const pv = v + h

  if ((tx == 0) && p[oy].includes('')) 
    return [pv]

  if ((ox == 0) && p[ty].includes('')) 
    return [ph]

  if ((ox == tx) || (oy == ty))
    return [ph]

  return [ph, pv]
}

// return total costs of typing s paths on the l level pad
function type(ps: Pads, s: Paths, l: number = 0, lim: number = 2, 
  cache: Cache = new Map()): Costs {
  const p = ps[ l ? 1 : 0]

  // for each path
  return s.map(e => {
    const k = e + l
    let cost: number = cache.get(k)
    if (cost != undefined)
        return cost

    if (l == lim + 1)
      cost = e.length
    else {
      cost = 0

      // for each keys pair
      e.split('').reduce((acc, cur) => {
        const r = navigate(p, acc, cur).map(v => v + 'A')
        const c = type(ps, r, l + 1, lim, cache)
        cost += Math.min(...c)
        return cur
      }, 'A')
    }

    cache.set(k, cost)
    return cost
  })
}

const complex = (ps: Pads, s: Paths, lim: number = 2): number =>
  type(ps, s, 0, lim).reduce((acc, cur, idx) => acc + 
    +s[idx].substring(0, s[idx].length - 1) * cur, 0)

const testSeq = ['029A', '980A', '179A', '456A', '379A']
const mySeq =   ['208A', '540A', '685A', '879A', '826A']

print("find", find(nPad, '7'), find(nPad, 'A'))
print("find", find(dPad, 'v'), find(dPad, 'A'))

print("nav", navigate(nPad, 'A', '4'), navigate(nPad, '5', '3'))
print("nav", navigate(nPad, '0', '0'))

print("type 0", type(pads, ['029A'], 0))
print("type 1", type(pads, ['029A'], 1))
print("type 2", type(pads, ['029A'], 2))

print("c", complex([nPad, dPad], ['029A']))
print("c", complex([nPad, dPad], testSeq))

print("A1:", complex([nPad, dPad], mySeq))

print("------------------")

print(type([nPad, dPad], testSeq))

print("A2:", complex([nPad, dPad], mySeq, 25))
