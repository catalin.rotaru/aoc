import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

type Cell = [number, number]  // [x, y]
type Cells = Array<Cell>
type Row = Array<string>      // [x]
type Grid = Array<Row>        // [y] [x]
type Antennas = Map<string, Cells>

const onGrid = (g: Grid, [x, y]: Cell): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const readGrid = (): Grid =>
  fs.readFileSync('day8.txt','utf8').split(/\r?\n/).map(l => l.split(''))

function antennas(g: Grid): Antennas {
  let res: Antennas = new Map()
  g.forEach((row, y) => row.forEach((v, x) => {
    if (v != '.') {
      let c = res.get(v) 
      if (!c) {
        c = new Array()
        res.set(v, c)
      }
      c.push([x, y])
    }
  }))

  return res
}

const nodes = ([x1, y1]: Cell, [x2, y2]: Cell): Cells =>
  [[2 * x1 - x2, 2 * y1 - y2], [2 * x2 - x1, 2 * y2 - y1]]

function* pairs(c: Cells) {
  for (const [i, a] of c.entries())
    for (const [j, b] of c.entries())
      if (i != j)
        yield [a, b]
}

function countNodes(g: Grid, ax: Antennas): number {
  let res: Set<string> = new Set()

  for (const a of ax.values())
    for (const p of pairs(a))
      nodes(p[0], p[1]).filter(e => onGrid(g, e)).forEach(e => 
        res.add(e[0] + ':' + e[1]))
  
  return res.size
}

function* nodes2(g: Grid, [x1, y1]: Cell, [x2, y2]: Cell) {
  const dx = x2 - x1
  const dy = y2 - y1

  for (let x = x1, y = y1; onGrid(g, [x, y]); x +=dx, y += dy)
    yield [x, y]
  for (let x = x1 - dx, y = y1 - dy; onGrid(g, [x, y]); x -=dx, y -= dy)
    yield [x, y]
}

function countNodes2(g: Grid, ax: Antennas): number {
  let res: Set<string> = new Set()

  for (const a of ax.values())
    for (const p of pairs(a))
      for (const n of nodes2(g, p[0], p[1])) 
        res.add(n[0] + ':' + n[1])
  
  return res.size
}

print("nodes", nodes([1, 1], [0, 0]))
print("pairs", [...pairs([[1, 1], [2, 2]])])

const grid = readGrid()
//printGrid(grid)

const ant = antennas(grid)
//print(ant)

print("A1:", countNodes(grid, ant))

print("------------------")

print("A2:", countNodes2(grid, ant))
