import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

type Row = Array<string>          // [x]
type Grid = Array<Row>            // [y] [x]
type Cell = [number, number ]     // [x, y]
type Cells = Array<Cell>
type Cost = Array<Array<number>>  // [y] [x]
type Cheats = Map<number, number> // saving => no. of cheats

const neighbor = [[0, -1], [1, 0], [0, 1], [-1, 0]] // array of [x, y]

const onGrid = (g: Grid, [x, y]: Cell): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, [x, y]: Cell, wall: boolean = false): Cells =>
  neighbor.map(([dx, dy]) => <Cell> [x + dx, y + dy])
  .filter(([nx, ny]) => (g[ny][nx] == '#') == wall)

const readGrid=(): Grid =>
  fs.readFileSync('day20.txt','utf8').split(/\r?\n/).map(l => l.split(''))

function origin(g: Grid, m: string = 'S'): Cell {
  let x: number
  const y = g.findIndex(row => (x = row.findIndex(v => v == m)) > 0)
  return [x, y]
}

function flood(g: Grid) {
  let res: Cost = g.map(r => new Array(r.length).fill(Number.MAX_SAFE_INTEGER))
  const s: Cell = origin(g)
  let next: Cells = [s]
  res[s[1]][s[0]] = 0

  do {
    const cur = next.pop()
    const l = res[cur[1]][cur[0]] + 1

    neighbors(g, cur).filter(([x, y]) => res[y][x] > l).forEach(([x, y]) => {
      res[y][x] = l
      next.push([x, y])
    })
  } while (next.length > 0)

  return res
}

function minIdx(a: Array<number>): number {
  let res: number
  a.reduce((acc, cur, idx) => {
    if (cur < acc) {
      res = idx
      return cur
    }
    return acc
  }, Number.MAX_SAFE_INTEGER)
  return res
}

function cheats(g: Grid, c: Cost, [ox, oy]: Cell, d: number): Array<number> {
  let res: Array<number> = []

  for (let i = - d ; i <= d ; i++)
    for (let j =  - d ; j <= d ; j++) {
      const dist = Math.abs(i) + Math.abs(j)
      if (dist <= d) {
        const nx = ox + i
        const ny = oy + j

        if (onGrid(g, [nx, ny]) && (g[ny][nx] != '#')) {
          const gain = c[oy][ox] - c[ny][nx]
          if (gain > d)
            res.push(gain - dist)
        }
      }
    }

  return res
}

function walk(g: Grid, c: Cost, d = 2): Cheats {
  let res: Cheats = new Map()

  for (let l: Cell = origin(g, 'E'); ; ) {
    //print(l)
    cheats(g, c, l, d).forEach(v => res.set(v, (res.get(v) || 0) + 1))

    if (g[l[1]][l[0]] == 'S')
      break

    const n = neighbors(g, l)
    const m = minIdx(n.map(([x, y]) => c[y][x]))
    l = n[m]
  }

  return res
}

print(minIdx([3, 1, -4, 8, -3, 0, 4]))
print(minIdx([84, 82]))

const grid = readGrid()
//printGrid(grid)
//const start = origin(grid)
//print("S", start)

const cost = flood(grid)
//print(cost)

const cheat = [...walk(grid, cost)]
//print(cheat)
print("A1:", cheat.reduce((acc, [k, v]) => (k >= 100) ? acc + v : acc, 0))

print("------------------")

const cheat2 = [...walk(grid, cost, 20)]
//print(cheat2)
print("A2:", cheat2.reduce((acc, [k, v]) => (k >= 100) ? acc + v : acc, 0))
