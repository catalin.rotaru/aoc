import fs = require("fs")

const print = console.log.bind(console)

type Locations = [Array<number>, Array<number>]

function readLocations(): Locations {
  const loc = fs.readFileSync('day1.txt','utf8').split(/\r?\n/).map(l  => 
    l.split(/\s+/).map(v => +v))

  let res: Locations = [new Array(), new Array()]

  loc.forEach(p => p.forEach((v, i) => res[i % 2].push(v)))
  res.forEach(v => v.sort())

  return res
}

const distance = (l: Locations): number =>
  l[0].reduce((acc, cur, i) => acc + Math.abs(cur - l[1][i]), 0)

function similarity(l: Locations): number {
  let f: Map<number, number> = new Map()

  l[1].forEach(v => f.set(v, 1 + (f.get(v) || 0)))

  return l[0].reduce((acc, cur) => acc  + cur * (f.get(cur) || 0), 0)
}

const loc = readLocations()

print("A1:", distance(loc))

print("------------------")

print("A2:", similarity(loc))
