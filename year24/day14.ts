import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

type Cell = [number, number ] // [x, y]
type Cells = Array<Cell>
type Bot = [Cell, Cell]       // [position, velocity]
type Bots = Array<Bot>
type Row = Array<string>      // [x]
type Grid = Array<Row>        // [y] [x]

const readBots = (): Bots =>
  <Bots> fs.readFileSync('day14.txt','utf8').split(/\r?\n/)
    .map(l => l.slice(2).split(' v=').map(s => s.split(',').map(v => +v)))

const modulo = (n: number, d: number): number => ((n % d) + d) %d

const move = (o: Cell, v: Cell, d: Cell, t: number): Cell =>
  [modulo(o[0] + v[0] * t, d[0]), modulo(o[1] + v[1] * t, d[1])]

function moveBots(b: Bots, d: Cell = [11, 7], t: number = 100): Cells {
  const o = b.map(e => e[0])
  const v = b.map(e => e[1])

  return o.map((o, idx) => move(o, v[idx], d, t))
}

const quadrant = ([x, y]: Cell, [w, h]: Cell) =>
  (x == (w - 1) / 2) || (y == (h - 1) / 2) ? -1 :
    ((x < (w - 1) / 2) ? 0 : 1) +
    2 * ((y < (h - 1) / 2) ? 0 : 1)

function safety (b: Cells, d: Cell = [11, 7]) {
  let q = [0, 0, 0, 0]
  b.forEach(e => {
    const nq = quadrant(e, d)
    if (nq >= 0)
      q[nq]++
  })
  return q.reduce((acc, cur) => acc * cur, 1)
}

function printBots(b: Cells, [w, h]: Cell) {
  let g: Grid = Array.from({ length: h }, (_) => new Array(w).fill(' '))
  b.forEach(([x, y]) => g[y][x] = '#')
  printGrid(g)
}

function search(b: Bots, d: Cell = [101, 103]): number {
  let t = 0
  let s = Number.MAX_SAFE_INTEGER

  for (let i = 0; i < d[0] * d[1]; i++) {
    const ns = safety(moveBots(bots, d, i), d)
    if (ns < s) {
      s = ns
      t = i
    }
  }

  printBots(moveBots(b, d, t), d)
  return t
}

print('m', move([2, 4], [2, -3], [11, 7], 1), move([2, 4], [2, -3], [11, 7], 2))
print('q', quadrant([1, 3], [5, 5]))

const bots = readBots()
//print(bots)

//print(safety(moveBots(bots)))

print("A1:", safety(moveBots(bots, [101, 103]), [101, 103]))

print("------------------")

print("A2:", search(bots))
