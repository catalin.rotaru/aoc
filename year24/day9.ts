import fs = require("fs")

const print = console.log.bind(console)

const FREE = -1

type Space = [number, number, number]  // id (-1 means FREE), begin, length
type Disk = Array<Space>

function parseMap(m: string): Disk {
  let begin = 0
  
  return m.split('').map((v, idx) => {
    const len = +v
    const b = begin
    begin += len
    return [(idx % 2 == 1) ? -1 : idx / 2, b, len]
  })
}

function trim(d: Disk) {
  while ((d[d.length - 1][0] == FREE) || (d[d.length - 1][2] <= 0))
    d.pop()
}

function compact(d: Disk) {
  for (let sp = 0; (sp = d.findIndex(v => v[0] == FREE)) != -1;) {
    const space = disk[sp]
    const file = disk[disk.length - 1]
    //print(space, file)

    if (space[2] > file[2]) {
      d.splice(sp + 1, 0, [FREE, space[1] + file[2], space[2] - file[2]])
      space[2] = file[2]
    }

    space[0] = file[0]
    file[2] -= space[2]

    if (sp == disk.length - 2) {
      space[2] += file[2]
      file[2] = 0
    }

    trim(d)
  }
}

const checksum = (d: Disk): number =>
  d.reduce((acc, cur) => acc + 
    (cur[0] == FREE ? 0 : cur[0]) * cur[2] * (cur[1] + (cur[2] - 1) / 2), 0)

function compact2(d: Disk) {
  for (let id = d[d.length - 1][0]; id >= 0; id--) {
    const fl = d.findLastIndex(v => v[0] == id)
    const file = d[fl]
    const sp = d.slice(0, fl).findIndex(v => 
      ((v[0] == FREE) && (v[2] >= file[2])))

    if (sp >= 0) {
      const space = d[sp]
      //print(file, space)
    
      if (space[2] > file[2]) {
        d.splice(sp + 1, 0, [FREE, space[1] + file[2], space[2] - file[2]])
        space[2] = file[2]
      }
  
      space[0] = file[0]
      file[0] = FREE
    }
  }
}

//print(parseMap("12345"))
//print(parseMap("2333133121414131402"))

const map = fs.readFileSync('day9.txt','utf8')
const disk = parseMap(map)
//print(disk)
compact(disk)
//print(disk)
print("A1:", checksum(disk))

print("------------------")

const disk2 = parseMap(map)
//print(disk2)
compact2(disk2)
//print(disk2)
print("A2:", checksum(disk2))
