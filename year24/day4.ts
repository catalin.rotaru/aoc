import fs = require("fs")

const print = console.log.bind(console)

type Row = Array<string>  // [x]
type Grid = Array<Row>    // [y] [x]

const direction = [[-1, -1], [0, -1], [1, -1], [-1, 0], // array of [x, y]
  [1, 0], [-1, 1], [0, 1], [1, 1]]

const onGrid = (g: Grid, x: number, y: number): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const readGrid = (): Grid =>
  fs.readFileSync('day4.txt','utf8').split(/\r?\n/).map(l => l.split(''))

const checkWord = 
(g: Grid, x: number, y: number, dx: number, dy: number): boolean =>
  'MAS'.split('').every((v, idx) => {
    const nx = x + (idx + 1) * dx
    const ny = y + (idx + 1) * dy
    return onGrid(g, nx, ny) && (v == g[ny][nx])
  })

const findWords = (g: Grid, x: number, y: number): number =>
  g[y][x] != 'X' ? 0 :
    direction.reduce((acc, cur) => acc + +checkWord(g, x, y, cur[0], cur[1]), 0)

const findWords2 = (g: Grid, x: number, y: number): number =>
  g[y][x] != 'A' ? 0 : +(
    (((g[y - 1][x - 1] == 'M') && (g[y + 1][x + 1] == 'S')) || 
      ((g[y - 1][x - 1] == 'S') && (g[y + 1][x + 1] == 'M'))) 
    &&
    (((g[y + 1][x - 1] == 'M') && (g[y - 1][x + 1] == 'S')) || 
      ((g[y + 1][x - 1] == 'S') && (g[y - 1][x + 1] == 'M'))) 
  )

const find = (g: Grid) =>
  g.reduce((acc, row, y) => acc + 
    row.reduce((all, _, x) => all + findWords(g, x, y), 0), 0)

const find2 = (g: Grid) =>
  g.slice(1, -1).reduce((acc, row, y) => acc + 
    row.slice(1, -1).reduce((all, _, x) => all + 
      findWords2(g, x + 1, y + 1), 0), 0)

const grid = readGrid()

//print(grid)
print("xmas word", checkWord(grid, 2, 0, 1, 1))
print("found", findWords(grid, 9, 9))

print("A1:", find(grid))

print("------------------")

print("A2:", find2(grid))

