import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

type Row = Array<string>      // [x]
type Grid = Array<Row>        // [y] [x]
type Cell = [number, number ] // [x, y]

const direction = {'^': [0, -1], '>': [1, 0], 'v': [0, 1], '<': [-1, 0]}
const replace = {'#': ['#', '#'], 'O': ['[', ']'], '.': ['.', '.'], 
    '@': ['@', '.']}

function readGrid(): [Grid, string] {
  const [g, m] = fs.readFileSync('day15.txt','utf8').split(/\r?\n\r?\n/)
  return [g.split(/\r?\n/).map(l => l.split('')), m.split(/\r?\n/).join('')]
}

function origin(g: Grid): Cell {
  let x: number
  const y = g.findIndex(row => (x = row.findIndex(v => v == '@')) > 0)
  return [x, y]
}

function push(g: Grid, [x, y]: Cell, d: Cell): boolean {
  const nx = x + d[0]
  const ny = y + d[1]
  const v = g[ny][nx]

  if (v == '#')
    return false

  if ((v == 'O') && (!push(g, [nx, ny], d)))
    return false
      
  g[ny][nx] = g[y][x]
  return true
}

function move(g: Grid, o: Cell, m: string): Cell {
  const d = direction[m]

  if (!push(g, o, d)) 
    return o

  g[o[1]][o[0]] = '.'
  return o.map((v, idx) => v + d[idx]) as Cell
}

const walk = (g: Grid, m: string) =>
  m.split('').reduce((acc, cur) => move(g, acc, cur), origin(g))

const gps = (g: Grid): number => g.reduce((acc, row, y) => 
  acc + row.reduce((a, v, x) => 
    a + ((v == 'O') || (v == '[') ? 100 * y + x : 0), 0), 0)

const expand = (g: Grid): Grid =>
  g.map(row => row.map(v => replace[v]).flat())

function push2(g: Grid, [x, y]: Cell, d: Cell, t: boolean = false): 
boolean {
  const nx = x + d[0]
  const ny = y + d[1]
  const v = g[ny][nx]

  if (v == '#')
    return false

  if ((v == '[') || (v == ']')) {
    if (d[1] == 0) {
      if (!push2(g, [nx, ny], d, t))
        return false
    } else {
      const nnx = nx + ((v == '[') ? 1 : -1)
      if (!push2(g, [nx, ny], d, t) || !push2(g, [nnx, ny], d, t))
        return false
    }
  }

  if (!t) {
    g[ny][nx] = g[y][x]
    g[y][x] = '.'
  }
  
  return true
}

function move2(g: Grid, o: Cell, m: string): Cell {
  const d = direction[m]

  if (!push2(g, o, d, true)) 
    return o

  push2(g, o, d)
  return o.map((v, idx) => v + d[idx]) as Cell
}

const walk2 = (g: Grid, m: string) =>
  m.split('').reduce((acc, cur) => move2(g, acc, cur), origin(g))

const [grid, moves] = readGrid()
const grid2 = expand(grid)
//printGrid(grid)
//print(moves)
const start = origin(grid)
print("S", start)

walk(grid, moves)
//printGrid(grid)

print("A1:", gps(grid))

print("------------------")

//printGrid(grid2)

walk2(grid2, moves)
//printGrid(grid2)

print("A2:", gps(grid2))
