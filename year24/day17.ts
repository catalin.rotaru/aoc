import fs = require("fs")

const print = console.log.bind(console)

type Registers = [number, number, number] // A, B, C

const combo = (r: Registers, op: number): number =>
  (op <= 3) ? op : r[op - 4]

const execute = [
  (r: Registers, op: number): any => 
    {r[0] = Math.floor(r[0] / 2 ** combo(r, op))},
  (r: Registers, op: number): any => {r[1] ^= op},
  (r: Registers, op: number): any => {r[1] = combo(r, op) % 8},
  (r: Registers, op: number): any => r[0] ? op : undefined,
  (r: Registers, _: number): any => {r[1] ^= r[2]},
  (r: Registers, op: number): any => (combo(r, op) % 8).toString(),
  (r: Registers, op: number): any => 
    {r[1] = Math.floor(r[0] / 2 ** combo(r, op))},
  (r: Registers, op: number): any => 
    {r[2] = Math.floor(r[0] / 2 ** combo(r, op))}
]

function run(r: Registers, s: string, once: boolean = false): string {
  const ac = s.split(',').map(v => +v)
  const code = once ? ac.slice(0, -2) : ac
  let out = new Array()

  let ip = 0
  while ((ip >= 0) && (ip < code.length)) {
    const res = execute[code[ip]](r, code[ip + 1])
    //print('IP ' + ip/2 + ':', ...r.map(v => v.toString(2) + '(' + v + ')'))
    if (typeof res == 'string')
      out.push(res)
    ip = (typeof res != 'number') ? ip + 2 : res
  }

  return out.join(',')
}

/*
do {
0  B = A % 8         // B = last 3 bits of A
1  B = B xor 5       // in B, inverse #0 and #3 bits
2  C = A / (2 ^ B)   // C = A without last B bits
3  B = B xor C 
4  B = B xor 6       // in B, inverse #1 and #2 bits
5  A = A / 8         // in A, lose the last 3 bits
6  out B % 8         // out last 3 bits of B
} while (A != 0)
*/

function digit(a: bigint): bigint {
  let b = (a % 8n) ^ 5n
  const c = a >> b
  b = (c ^ b) ^ 6n

  return b % 8n
}

const digit2 = (a: number, s: string): number => +run([a, 0, 0], s, true)

function compute(a: bigint): string {
  let res = []

  do {
    res.push(digit(a))
    a = a >> 3n
  } while (a > 0)

  return res.join('')
}

function find(r: Registers, s: string): number {
  let i = 0
  for (; run([i, r[1], r[2]], s) != s; i++) {}
  return i
}

function findDigit(a: bigint, d: bigint): Array<bigint> {
  let res: Array<bigint> = []
  for (let i: bigint = 0n; i < 8; i++)
    if (digit(a * 8n + i) == d)
      res.push(i)
  return res
}

const find2 = (s: string): Array<bigint> =>
  s.split(',').map(v => BigInt(v))
  .reduceRight((acc, cur) => 
    acc.map(a => findDigit(a, cur).map(v => a * 8n + v)).flat(), [0n])

const reg = (idx: number): string =>
  idx == 0 ? 'A' : (idx == 1) ? 'B' : 'C'

const strCombo = (op: number): string =>
  (op <= 3) ? op.toString() : reg(op - 4)

const strInstruction = [
  (op: number) => 'A = A / 2 ^ ' + strCombo(op),
  (op: number) => 'B = B xor ' + op,
  (op: number) => 'B = ' + strCombo(op) + ' % 8   // last 3 bits',
  (op: number) => 'if (A != 0) jmp ' + op,
  (_: number) => 'B = B xor C',
  (op: number) => 'out ' + strCombo(op) + ' % 8   // last 3 bits',
  (op: number) => 'B = A / 2 ^ ' + strCombo(op),
  (op: number) => 'C = A / 2 ^ ' + strCombo(op),
]

const printCode = (s: string) =>
  s.split(',').forEach((v, idx, c) => {
    if (idx % 2 == 0)
      print(strInstruction[+v](+c[idx + 1]))
  })

const minBig = (a: Array<bigint>) =>
  a.reduce((acc, cur) => cur < acc ? cur : acc)

let rgs: Registers
print('run', run(rgs = [0, 0, 9], "2,6"), rgs)
print('run', run(rgs = [10, 0, 0], "5,0,5,1,5,4"))
print('run', run(rgs = [2024, 0, 0], "0,1,5,4,3,0"), rgs)
print('run', run(rgs = [0, 29, 0], "1,7"), rgs)
print('run', run(rgs = [0, 2024, 43690], "4,0"), rgs)

print('run', run([729, 0, 0], "0,1,5,4,3,0"))

print("A1:", run([61156655, 0, 0], "2,4,1,5,7,5,4,3,1,6,0,3,5,5,3,0"))

print("------------------")

print('find', find([2024, 0, 0], "0,3,5,4,3,0"))

//print("------CODE--------")
//printCode("2,4,1,5,7,5,4,3,1,6,0,3,5,5,3,0")
//run([61156655, 0, 0], "2,4,1,5,7,5,4,3,1,6,0,3,5,5,3,0")
//print("------END---------")

print('C', compute(61156655n))
print('C', compute(98434n))
print('C cmp', compute(12304n), compute(12309n))

print('run', run([105734774294938, 0, 0], "2,4,1,5,7,5,4,3,1,6,0,3,5,5,3,0"))
print('C', compute(105734774294938n))

//print("A2:", find([61156655, 0, 0], "2,4,1,5,7,5,4,3,1,6,0,3,5,5,3,0"))
print("A2:", minBig(find2("2,4,1,5,7,5,4,3,1,6,0,3,5,5,3,0")))
