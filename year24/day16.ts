import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

type Row = Array<string>        // [x]
type Grid = Array<Row>          // [y] [x]
type Cell = [number, number ]   // [x, y]
type Costs = Array<Array<Cost>> // [y] [x]
type Cost = [number, number, number, number]  // for each heading

const neighbor = [[0, -1], [1, 0], [0, 1], [-1, 0]] // array of [x, y]

const readGrid=(): Grid =>
  fs.readFileSync('day16.txt','utf8').split(/\r?\n/).map(l => l.split(''))

function origin(g: Grid, m: string = 'S'): Cell {
  let x: number
  const y = g.findIndex(row => (x = row.findIndex(v => v == m)) > 0)
  return [x, y]
}

const turn = (h: number, nh: number): number =>
  (h == nh) ? 0 : (h == -nh) ? 2000 : 1000

function explore(g: Grid, c: Costs, [x, y]: Cell = origin(g), h: number = 1,
m: number = 0) {
  if ((g[y][x] == '#') || (c[y][x][h] <= m))
    return

  c[y][x][h] = m

  neighbor.forEach(([nx, ny], idx) => 
    explore(g, c, [x + nx, y + ny], idx, m + 1 +  turn(h, idx)))
}

function reverse(c: Costs, [x, y]: Cell, m: number, p: Set<string>, 
h: number = -1) {
  if (m < 0)
    return

  p.add(x + ':' + y)

  c[y][x].forEach((v, idx) => {
    const nc = m - (h >= 0 ? turn(idx, h) : 0)
    if (v == nc) {
      const nd = (idx + 2) % 4
      const n: Cell = [x + neighbor[nd][0], y + neighbor[nd][1]]
      reverse(c, n, nc - 1, p, idx)
    }
  })
}

function walk(g: Grid): [number, number] {
  let c: Costs = g.map(row => row.map(_ => 
    new Array(4).fill(Number.MAX_SAFE_INTEGER) as Cost))

  explore(g, c)

  const t = origin(g, 'E')
  const res = Math.min(...c[t[1]][t[0]])

  let p: Set<string> = new Set()
  reverse(c, t, res, p)

  return [res, p.size]
}

const grid = readGrid()
//printGrid(grid)
const start = origin(grid)
//print("S", start)

print("[A1, A2]:", walk(grid))

print("------------------")
