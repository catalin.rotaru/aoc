import fs = require("fs")

const print = console.log.bind(console)

type Computers = Set<string>
type Network = Map<string, Computers>

function readNetwork(): Network {
  let res = new Map()

  fs.readFileSync('day23.txt','utf8').split(/\r?\n/).forEach(l => {
    const [f, t] = l.split('-')

    let c1 = res.get(f)
    if (c1 == undefined) {
      c1 = new Set()
      res.set(f, c1)
    }
    c1.add(t)

    let c2 = res.get(t)
    if (c2 == undefined) {
      c2 = new Set()
      res.set(t, c2)
    }
    c2.add(f)
  })

  return res
}

const connected=(n: Network, a: string, b: string, c: string) =>
  n.get(a).has(b) && n.get(a).has(c) && n.get(b).has(c)

function find(n: Network) {
  let res = 0
  const c = [...n.keys()]

  for (let i = 0; i < c.length - 2; i++)
    for (let j = i + 1; j < c.length - 1; j++)
      for (let k = j + 1; k < c.length; k++)
        if (connected(n, c[i], c[j], c[k]) && 
        (c[i].startsWith('t') || c[j].startsWith('t') || c[k].startsWith('t')))
          res++ 
  
  return res
}

const intersect = (s1: Set<any>, s2: Set<any>): Set<any> =>
  new Set([...s1].filter(x => s2.has(x)))

let result: Computers = new Set()

function grow(n: Network, lan: Computers = new Set(), 
can: Computers = new Set(n.keys())) {
  if (can.size == 0) {
    //print(lan)
    if (lan.size > result.size)
      result = lan
  } else
    for (const c of can) {
      const v = net.get(c)
      grow(n, new Set([...lan, c]), intersect(can, v))
      can.delete(c)
    }
}

const net = readNetwork()
//print(net)

print("A1:", find(net))

print("------------------")

grow(net)
print("A2:", [...result.values()].sort().join(','))
