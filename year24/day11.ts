import fs = require("fs")

const print = console.log.bind(console)

type Stones = Map<number, number> // value => count

function change(n: number): Array<number> {
  if (n == 0) 
    return [1]

  const s = n.toString()
  if (s.length % 2 == 0) {
    const h = 10 ** (s.length / 2)
    return [Math.floor(n / h), n % h]
  }

  return [n * 2024]
}

const blink = (a: Array<number>): Array<number> =>
  a.reduce((acc, cur) => {
    acc.push(...change(cur))
    return acc
  }, [])

function process(s: string): number {
  let a = s.split(' ').map(v => +v)

  for (let i = 0; i < 25; i++)
    a = blink(a)

  return a.length
}

function blink2 (s: Stones): Stones {
  let res: Stones = new Map()

  s.forEach((count, val) => change(val).forEach(v =>
    res.set(v, count + (res.get(v) || 0))))

  //print(res)
  return res
}

function process2(s: string, steps: number = 75): number {
  let m: Stones = new Map()
  s.split(' ').map(v => +v).forEach(v => m.set(v, 1))
  //print(m)

  for (let i = 0; i < steps; i++)
    m = blink2(m)

  return [...m.values()].reduce((acc, cur) => acc + cur, 0)
}

print("ch", change(0))
print("ch", change(9999))
print("ch", change(1))
print(process("125 17"))

print("A1:", process("4189 413 82070 61 655813 7478611 0 8"))

print("------------------")

print(process2("125 17", 25))

print("A2:", process2("4189 413 82070 61 655813 7478611 0 8"))
