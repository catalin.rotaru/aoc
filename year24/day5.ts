import fs = require("fs")

const print = console.log.bind(console)

type Order = [number, number] // [first, second]
type Update = Array<number>
type Rules = [Array<Order>, Array<Update>] // [[order], [update]]

type Pages = Map<number, Page>

class Page {
  readonly id: number
  level: number
  next: Array<number>

  constructor(id: number) {
    this.id = id
    this.level = 0
    this.next = new Array()
  }

  addNext(id: number) {
    this.next.push(id)
  }
}

const readRules = (): Rules =>
  fs.readFileSync('day5.txt','utf8').split(/\r?\n\r?\n/).map((p, idx) => 
    p.split(/\r?\n/).map(l => l.split((idx == 0) ? '|' : ',').map(v => +v))) as 
      Rules

function page(ps: Pages, id: number): Page {
  let p = ps.get(id)
  if (!p) {
    p = new Page(id)
    ps.set(id, p)
  }
  return p
}

function processOrder(ord: Array<Order>): Pages {
  const res: Pages = new Map()
  ord.forEach(o => {
    page(res, o[0]).addNext(o[1])
    page(res, o[1]).level = 1
  })
  return res
}

function processLevels(ps: Pages) {
  let np = [...ps.values()].filter(p => p.level == 0).map(p => p.id)
  for (let l = 0; np.length > 0; l++) {
    np.forEach(p => page(ps, p).level = l)
    np = [...new Set([...np.values()].reduce((acc, cur) => 
      acc.concat(page(ps, cur).next), []))]
  }
}

function processPages(ord: Array<Order>, up: Update) {
  const incl = new Set<number>(up)
  const ps = processOrder(ord.filter(o => incl.has(o[0]) && incl.has(o[1])))
  processLevels(ps)
  return ps
}

const goodUpdate = (ps: Pages, up: Update): boolean => 
  up.slice(1).every((u, i) => page(ps, u).level >= page(ps, up[i]).level)

function checkUpdate(ord: Array<Order>, up: Update): number {
  const ps = processPages(ord, up)
  return goodUpdate(ps, up) ? up[Math.floor(up.length / 2)] : 0
}

function checkUpdate2(ord: Array<Order>, up: Update): number {
  const ps = processPages(ord, up)
  return goodUpdate(ps, up) ? 0 : up.sort((a, b) => 
    page(ps, a).level - page(ps, b).level)[Math.floor(up.length / 2)]
}

const rules = readRules()

print("A1:", rules[1].reduce((acc, cur) => acc + checkUpdate(rules[0], cur), 0))

print("------------------")

print("A2:", 
  rules[1].reduce((acc, cur) => acc + checkUpdate2(rules[0], cur), 0))
