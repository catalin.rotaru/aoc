import fs = require("fs")

const print = console.log.bind(console)

type Cell = [number , number ]      // [x, y]
type Machine = [Cell, Cell, Cell] // [button 1, button 2, prize]
type Machines = Array<Machine>

const readButton = (s: string): Cell =>
  <Cell> s.slice(11).split(', Y+').map(v => +v)

const readPrize = (s: string): Cell =>
  <Cell> s.slice(9).split(', Y=').map(v => +v)

const readMachines = (): Machines =>
  fs.readFileSync('day13.txt','utf8').split(/\r?\n\r?\n/).map((m) => {
    const [b1, b2, p] = m.split(/\r?\n/).map(l => l)
    return [readButton(b1), readButton(b2), readPrize(p)]
  })

function press([x1, y1]: Cell, [x2, y2]: Cell, [x, y]: Cell): Cell {
  const n =  (x * y2 - y * x2) / (x1 * y2 - x2 * y1)
  const m =  (y * x1 - x * y1) / (x1 * y2 - x2 * y1)
  return Number.isInteger(n) && Number.isInteger(m) ? [n, m] : [0, 0]
}

const cost = (m: Machines): number =>
  m.map(e => press(...e)).reduce((acc, [p1, p2]) => acc + p1 * 3 + p2, 0)

const mach = readMachines()
//print(mach)

print("A1:", cost(mach))

print("------------------")

print("A2:", cost(mach.map(([b1, b2, p]) => 
  [b1, b2, [p[0] + 10000000000000, p[1] + 10000000000000]])))
