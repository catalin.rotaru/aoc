import fs = require("fs")

const print = console.log.bind(console)

type Operation = (a: number, b: number) => number
type Wires = Map<string, number>
type Gates = Map<string, [Operation, string, string]>
type Outputs = Array<string>

const Ops = {
  'AND': (a: number, b: number) => a & b,
  'OR': (a: number, b: number) => a | b,
  'XOR': (a: number, b: number) => a ^ b
}

function readGates(): [Wires, Gates, Outputs] {
  const [s1, s2] = fs.readFileSync('day24.txt','utf8').split(/\r?\n\r?\n/)
  const w: Array<[string, number]> = s1.split(/\r?\n/).map(l => {
    const [n, v] = l.split(': ')
    return [n, +v]
  })
  const os: Outputs = []
  const g: Array<[string, [Operation, string, string]]>  = 
  s2.split(/\r?\n/).map(l => {
    const [s, o] = l.split(' -> ')
    const [w1, op, w2] = s.split(' ')
    if (o.startsWith('z'))
      os.push(o)

    return [o, [Ops[op], w1, w2]]
  })

  return [new Map(w), new Map(g), os]
}

function getWire(w: Wires, g: Gates, n: string): number {
  let res = w.get(n)

  if (res == undefined) {
    const [op, a , b] = g.get(n)
    res = op(getWire(w, g, a), getWire(w, g, b))
    w.set(n, res)
  }

  return res
}

const compute = (w: Wires, g: Gates, o: Outputs): number =>
  o.sort().reduceRight((acc, cur) => acc * 2 + getWire(w, g, cur), 0)

const [wire, gate, outs] = readGates()
//print(wire, gate, outs)
//print(getWire(wire, gate, "z01"), getWire(wire, gate, "z02"))

print("A1:", compute(wire, gate, outs))

print("------------------")
