import fs = require("fs")

const print = console.log.bind(console)

type Operands = Array<number>
type Test = [number, Operands]

const evaluate = (a: Operands, o: Operands): number =>
  a.reduce((acc, cur, idx) => {
    if (idx == 0)
      return cur
    
    switch (o[idx - 1]) {
      case 0: return acc + cur 
      case 1: return acc * cur
      case 2: return +("" + acc + cur)
    }
  }, 0)

function* generateOps(n: number, r: number) {
  if (n == 1)
    for (let i = 0; i < r; i++)
      yield [i]
  else
    for (const etc of generateOps(n - 1, r))
      for (let i = 0; i < r; i++)
        yield [i, ...etc]
}

function test(value: number, a: Operands, r: number = 2): boolean {
  for (const o of generateOps(a.length - 1, r)) 
    if (value == evaluate(a, o))
      return true

  return false
}

const readTests = (): Array<Test> =>
  fs.readFileSync('day7.txt','utf8').split(/\r?\n/).map(l => {
    const [v, a] = l.split(': ')
    return [+v, a.split(' ').map(v => +v)]
  })

print("eval", evaluate([10, 2, 3], [0, 0]))
print("eval", evaluate([10, 2, 3], [1, 0]))
print("eval", evaluate([10, 2, 3], [0, 1]))
print("eval", evaluate([10, 2, 3], [1, 1]))
print("eval2", evaluate([6, 8, 6, 15], [1, 2, 1]))


//print("gen", [...generateOps(3)])
print("test", test(190, [10, 19]))
print("test", test(3267, [81, 40, 27]))
print("test", test(7290, [6, 8, 6, 15]))

const tests = readTests()
//print(tests)

print("A1:", tests.reduce((acc, [v, a]) => acc + (test(v, a) ? v : 0), 0))

print("------------------")

print("A2:", tests.reduce((acc, [v, a]) => acc + (test(v, a, 3) ? v : 0), 0))
