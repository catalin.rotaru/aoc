import fs = require("fs")

const print = console.log.bind(console)

type Pattern = Map<string, Pattern> // stripe => next
                                    // if (next has #) then pattern end!
type Design = Array<string>
type Cache = Map<string, number>   // design => possible

function buildPatterns(s: string): Pattern {
  let res = new Map()

  s.split(', ').forEach(s => {
    let m = res
    s.split('').forEach(ch => {
      let n = m.get(ch) 
      if (!n) {
        n = new Map()
        m.set(ch, n)
      }
      m = n
    })
    m.set('#', undefined)
  })

  return res
}

const readPatterns = (): [Pattern, Design] => {
  const l = fs.readFileSync('day19.txt','utf8').split(/\r?\n/)
  return [buildPatterns(l[0]), l.slice(2)]
}

function word(p: Pattern, s: string): Array<string> {
  let res = []
  let cur = ''
  for (let i = 0; p; i++) {
    if (p.has('#'))
      res.push(cur)

    if (i >= s.length)
      break

    const ch = s.charAt(i)
    cur += ch
    p = p.get(ch)
  }

  return res
}

function doParse(p: Pattern, s: string): number {
  if (s.length == 0)
    return 1

  const w = word(p, s)
  if (w.length == 0)
    return 0

  return w.reduce((acc, cur) => acc + parse(p, s.slice(cur.length)), 0)
}

function parse(p: Pattern, s: string): number {
  let res: number = cache.get(s)

  if (res == undefined) {
    res = doParse(p, s)
    cache.set(s, res)
  }

  return res
}

const possibles = (p: Pattern, d: Design): number =>
  d.filter(e => parse(p, e) > 0).length

const count = (p: Pattern, d: Design): number =>
  d.reduce((acc, cur) => acc + parse(p, cur), 0)

const [pattern, design] = readPatterns()
let cache: Cache = new Map()
//print([pattern, design])

//print(word(pattern, "r"))
//print(doParse(pattern, design[0]))

print("A1:", possibles(pattern, design))

print("------------------")

print("A2:", count(pattern, design))
