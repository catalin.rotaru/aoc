import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

type Cell = [number, number]  // [x, y]
type Cells = Array<Cell>
type Row = Array<number>      // [x]
type Grid = Array<Row>        // [y] [x]
type Peaks = Set<string>

const neighbor = [[0, -1], [1, 0], [0, 1], [-1, 0]] // array of [x, y]

const onGrid = (g: Grid, [x, y]: Cell): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, [x, y]: Cell): Cells =>
  neighbor.map(([dx, dy]) => <Cell>[x + dx, y + dy]).filter(c => onGrid(g, c))

const readGrid = (): Grid =>
  fs.readFileSync('day10.txt','utf8').split(/\r?\n/).map(l => 
    l.split('').map(v => +v))

function score(g: Grid, t: Cell, bag: Peaks = new Set(), l: number = 0)
: number {
  const [x, y] = t
  const nl = l + 1

  if (g[y][x] == 9)
    bag.add(y + ':' + x)
  else
    neighbors(g, t).filter(([nx, ny]) => g[ny][nx] == nl).forEach(c => 
      score(g, c, bag, nl))

  return bag.size
}

function rate(g: Grid, t: Cell, l: number = 0): number {
  const [x, y] = t

  if (g[y][x] == 9)
    return 1

  const nl = l + 1
  const n = neighbors(g, t).filter(([nx, ny]) => g[ny][nx] == nl)

  return n.reduce((acc, cur) => acc + rate(g, cur, nl), 0)
}

function search(g: Grid, r: boolean = false) {
  let head = new Array()
  g.forEach((row, y) => row.forEach((v, x) => (v == 0) && head.push([x, y])))

  const sc = head.map(c => r ? rate(g, c) : score(g, c))
  //print(sc)

  return sc.reduce((acc, cur) => acc + cur, 0)
}

const grid = readGrid()
//printGrid(grid)

print("A1:", search(grid))

print("------------------")

print("A2:", search(grid, true))
