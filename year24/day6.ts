import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

type Cell = [number, number]  // [x, y]
type Cells = Array<Cell>
type Row = Array<string>      // [x]
type Grid = Array<Row>        // [y] [x]

const DIR = [
  1 << 0,
  1 << 1,
  1 << 2,
  1 << 3
]

const direction = [[0, -1], [1, 0], [0, 1], [-1, 0]] // array of [x, y]

const readGrid = (): Grid =>
  fs.readFileSync('day6.txt','utf8').split(/\r?\n/).map(l => l.split(''))

const onGrid = (g: Grid, x: number, y: number): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

function start(g: Grid): Cell {
  let x: number
  const y = g.findIndex(r => (x = r.indexOf('^')) != -1)

  return [x, y]
}

// returns true if loop found, false otherwise
function navigate(g: Grid, o: Cell, mark: boolean = true): boolean {
  let dg: Array<Array<number>> = g.map(r => new Array(r.length).fill(0))
  let [x, y] = o
  //print("origin", x, y)
  let d = 0

  do {
    if ((dg[y][x] & DIR[d]) > 0)
      return true

    dg[y][x] |= DIR[d]
    if (mark)
      g[y][x] = 'X'

    let nx: number
    let ny: number
    for (;;) {
      nx = x + direction[d][0]
      ny = y + direction[d][1]

      if (!onGrid(g, nx, ny) || g[ny][nx] != '#')
        break

      d = (d + 1) % 4 // turn right
    }

    x = nx
    y = ny
  } while (onGrid(g, x, y))
  
  return false
}

function search(g: Grid, o: Cell): number {
  const path: Cells = new Array()
  grid.forEach((row, y) => 
    row.forEach((v, x) => (v == 'X') && path.push([x, y])))

  return path.reduce((acc, [x, y]) => {
    g[y][x] = '#'
    const loop = navigate(g, o, false)
    g[y][x] = 'X'
    return acc + +loop
  }, 0)
}

const grid = readGrid()
const origin = start(grid)

//printGrid(grid)

navigate(grid, origin)

//printGrid(grid)

print("A1:", 
  grid.reduce((acc, row) => row.reduce((a, v) => a + +(v == 'X'), acc), 0))

print("------------------")

print("A2:", search(grid, origin))