import fs = require("fs")

const print = console.log.bind(console)

const mix = (sec: number, n: number) =>
  ((sec ^ n) % 16777216 + 16777216) % 16777216

function next(sec: number): number {
  const a = sec * 64
  const s1 = mix(sec, a)
  const b = Math.floor(s1 / 32)
  const s2 = mix (s1, b)
  const c = s2 * 2048
  return mix(s2, c)
}

function compute(sec: number, steps = 1): number {
  let res = sec
  for (let i = 0; i < steps; i++)
    res = next(res)
  return res
}

function prices(sec: number, steps = 10): Array<number> {
  let res: Array<number> = []
  for (let i = 0, s = sec; i < steps; i++, s = next(s))
    res.push(s % 10)
  return res
}

function sequences(sec: number, steps = 2000): Map<string, number> {
  const ps = prices(sec, steps)

  const ch = []
  ps.reduce((acc, cur) => (ch.push(cur - acc), cur))
  //print(ch)

  let sq = new Map()
  for (let i = 0; i < ch.length - 3; i++) {
    const k = (ch[i] + 9) + (ch[i + 1] + 9) * 20 + (ch[i + 2] + 9) * 400 + 
      (ch[i + 3] + 9) * 8000
    if (!sq.has(k))
      sq.set(k, ps[i + 4])
  }

  return sq
}

function buy(all: Array<number>) {
  const sq = all.map(v => sequences(v))

  let keys: Set<string> = new Set()
  sq.forEach(b => {
    for (const k of b.keys())
      keys.add(k)
  })
  //print(keys)

  let max = 0
  for (const k of keys) {
    const sum = sq.map(s => s.get(k) ?? 0).reduce((acc, cur) => acc + cur, 0)
    if (sum > max)
      max = sum
  }

  return max
}

const total = (all: Array<number>): number =>
  all.map(v => compute(v, 2000)).reduce((a, c) => a + c, 0)

const readSecrets=(): Array<number> =>
  fs.readFileSync('day22.txt','utf8').split(/\r?\n/).map(l => +l)

const secrets = readSecrets()

print('mix', mix(42, 15))
print('123', next(123))
print('123', compute(123, 10))
print("test", total([1, 10, 100, 2024]))

print("A1:", total(secrets))

print("------------------")

//print("prices", prices(123))
//print(sequences(123, 10))

const time = performance.now()
buy([1, 2, 3, 2024])
print("A2:", buy(secrets))
print("it took", (performance.now() - time) / 1000, 'seconds')
