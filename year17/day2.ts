import fs = require('fs');

function readSpreadSheet(): number[][] {
  let spreadsheet: string = fs.readFileSync('day2.txt','utf8').toString();
  let rows: string[] = spreadsheet.split(/\r?\n/)
  let data: number[][] = rows.map(function(row: string) {
    return row.split(/[ \t]+/).map((val) => +val)
  })

  return data;
}

type RowCheckFunction = (row: number[]) => number;

let rowCheckSum: RowCheckFunction =
  function (row: number[]): number {
    return Math.max(...row) - Math.min(...row);
  }

let rowCheckDivide: RowCheckFunction =
  function (row: number[]): number {
    for (let i: number = 0; i < row.length; i++) {
      for (let j: number = 0; j < row.length; j++) {
        if ((i != j) && (row[i] % row[j] === 0 ))
          return row[i] / row[j] 
    }
  }

    throw Error("Cannot find evenly divisible members!")
  }

function checksum(data: number[][], rowCheck: RowCheckFunction): number {
  let checksums: number[] = data.map(rowCheck)

  return checksums.reduce((a, b) => a + b, 0)
}


let data: number[][] = readSpreadSheet()
//console.log(data)

console.log(checksum(data, rowCheckSum))
console.log(checksum(data, rowCheckDivide))