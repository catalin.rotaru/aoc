function insert(buffer: number[], step: number, value: number = 2017): number {
  let curr: number = 0
  //console.log(buffer)

  for (let i = 1; i <= value; i++) {
    let loc = (curr + step) % buffer.length + 1
    //console.log("step from", curr, "with", step, "to", loc)

    buffer.splice(loc, 0, i)
    //console.log(buffer)

    curr = loc
  }

  return buffer[(curr + 1) % buffer.length]
}

function insert2(buffer: number[], step: number, value: number = 2017): number {
  let curr: number = 0
  let length = buffer.length
  let last = 0

  for (let i = 1; i <= value; i++) {
    let loc = (curr + step) % length + 1
    if (loc == 1) {
      last = i
      //console.log("in position 1 inserted", i)
    }

    length++
    curr = loc
  }

  return last
}

console.log(insert([0], 3))
console.log(insert([0], 312))

console.log(insert2([0], 312, 50000000))