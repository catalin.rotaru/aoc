import fs = require('fs')

const INFECTION_MARK = "#"

enum State {CLEAN, WEAKENED, INFECTED, FLAGGED}

type Vector = [number, number]

const direction: Vector[] = [[-1, 0], [0, 1], [1, 0], [0, -1]]

type Infection = Map<string, number>

const changeDirection = (dir: number, right: boolean = true): number =>
  (dir + (right ? 1 : -1) + direction.length) % direction.length

const toString = (vec: Vector) => "" + vec[0] + "/" + vec[1]

function readMap(map:Infection): Vector {
  let lines = fs.readFileSync('day22.txt','utf8').split(/\r?\n/)
  lines.map((l, i) => {
    l.split("").map((c, j) => {
      //console.log([i, j], c)
      if (c === INFECTION_MARK)
        map.set(toString([i, j]), State.INFECTED)
    })
  })

  return [(lines[0].length - 1)/2, (lines.length - 1)/2]
}

function virus(map: Infection, loc: Vector, bursts: number, dir: number = 0) {
  let infection: Infection = new Map()
  for (let [k, v] of map) infection.set(k, v)
  //console.log(map, infection)

  let count = 0

  for (let i = 0; i < bursts; i++) {
    //console.log(infection)

    let label = toString(loc)
    let infected:boolean = infection.has(label)
    //console.log(label, infected)

    dir = changeDirection(dir, infected)
    
    if (infected)
      infection.delete(label)
    else {
      infection.set(label, State.INFECTED)
      count++
    }

    loc[0] += direction[dir][0]
    loc[1] += direction[dir][1]
  }

  return count
}

function virus2(map: Infection, loc: Vector, bursts: number, dir: number = 0) {
  let infection: Infection = new Map()
  for (let [k, v] of map) infection.set(k, v)
  //console.log(map, infection)

  let count = 0

  for (let i = 0; i < bursts; i++) {
    //console.log(i, infection)

    let label = toString(loc)
    let state:State = infection.has(label) ? infection.get(label) : State.CLEAN
    //console.log(label, state, "dir:", dir)

    switch (state) {
      case State.WEAKENED:
        state = State.INFECTED
        count++
        break;
        
      case State.INFECTED:
        state = State.FLAGGED
        dir = changeDirection(dir)
        break;

      case State.FLAGGED:
        state = State.CLEAN
        dir = changeDirection(changeDirection(dir))
        break;

      default:
        state = State.WEAKENED
        dir = changeDirection(dir, false)
        break;
    }
    
    if (state === State.CLEAN)
      infection.delete(label)
    else
      infection.set(label, state)

    loc[0] += direction[dir][0]
    loc[1] += direction[dir][1]

    //console.log("new state:", state, "dir:", dir, "loc: ", loc)
  }

  return count
}

//console.log(changeDirection(3, false))

let startMap: Infection = new Map()
let start: Vector = readMap(startMap)
//console.log(startMap, start)
console.log(virus(startMap, [start[0], start[1]], 10000))
//console.log(startMap, start)
console.log(virus2(startMap, [start[0], start[1]], 10000000))