import fs = require('fs')

type RegistersType = Map<string, number>
type PortType = number[]

let ops = {
  set: (reg: RegistersType, x:string, y:string): number => {
    reg.set(x, regVal(reg, y))
    return 1
  },

  sub: (reg: RegistersType, x:string, y:string): number => {
    reg.set(x, regVal(reg, x) - regVal(reg, y))
    return 1
  },
  
  mul: (reg: RegistersType, x:string, y:string): number => {
    reg.set(x, regVal(reg, x) * regVal(reg, y))
    return 1
  },
  
  jnz: (reg: RegistersType, x:string, y:string): number => {
    if (regVal(reg, x) === 0)
      return 1 
    return regVal(reg, y)
  },
}

const regVal = (reg: RegistersType, x:string) => isNaN(+x) ? (reg.has(x) ? reg.get(x) : 0) : +x

function operation(reg: RegistersType, instruction: string): number {
  //console.log("op:", instruction)

  let [op, x, y] = instruction.split(" ")
  return ops[op](reg, x, y)
}

function execute(ops: string[], debug: boolean = true) {
  //console.log(ops)
  let reg: RegistersType = new Map()
  if (!debug)
    reg.set("a", 1)

  let mulCount = 0

  let pc = 0
  for (; (pc >= 0) && (pc < ops.length); ) {
    let op = ops[pc]

    pc += operation(reg, op)

    if (op.startsWith("mul"))
      mulCount++
    //console.log("next:", i)
  }

  console.log("mul count:", mulCount)
  console.log("register h:", reg.get("h"))
  //console.log("exit:", i, reg)
}

let operations = fs.readFileSync('day23.txt','utf8').split(/\r?\n/)

function isPrime(n: number): boolean {
  for (let i = 2; i < n; i++)
    if (n % i === 0)
      return false

  return true
} 

function program() {
  let h = 0

  for (let b = 109300; b !== 126300; b += 17) {
    let f = 1

    for (let d = 2; d !== b; d++)
      for (let e = 2; e !== b; e++)
        if (d * e === b)
          f = 0

    if (f === 0)
      h++
  }

  console.log("register h:", h)
}

function programMine() {
  let h = 0

  for (let b = 109300; b !== 126300 + 17; b += 17) {

    if (!isPrime(b))
      h++
  }

  console.log("register h:", h)
}

execute(operations)
//execute(operations, false)

//console.log(isPrime(5), isPrime(7), isPrime(9), isPrime(11))
programMine()