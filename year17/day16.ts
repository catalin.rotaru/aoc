import fs = require('fs')

type ProgramsType = string[]

function spin(programs: ProgramsType, length: number) {
  let v: string[] = programs.splice(programs.length - length)
  programs.splice(0, 0, ...v)
}

function exchange(programs: ProgramsType, pos1: number, pos2: number) {
  let val = programs[pos1]
  programs[pos1] = programs[pos2]
  programs[pos2] = val
}

function partner(programs: ProgramsType, prog1: string, prog2: string) {
  exchange(programs, programs.indexOf(prog1), programs.indexOf(prog2))
}

function danceMove(programs: ProgramsType, move: string) {
  let op = move.charAt(0)
  let rest = move.substr(1)

  if (op === "s") 
    spin(programs, +rest)
  else {
    let [p1, p2] = rest.split("/")
    //console.log(rest, p1, p2)

    if (op === "p")
      partner(programs, p1, p2)
    else
      exchange(programs, +p1, +p2)
  }
}

function danceMoves(programs: ProgramsType, moves: string, times: number = 1) {
  let configs: string[] = [programsConcat(programs)]
  let config
  let m: string[] = moves.split(",")

  for (let i = 0; i < times; i++) {
    for (let move of m)
      danceMove(programs, move)

    config = programsConcat(programs)
    let idx = configs.indexOf(config)

    if (idx != -1) {
      //console.log(configs)
      console.log("found", config ,"at idx", idx, "step", i, "of", times)
      return configs[(times - idx) % (i - idx + 1)]
    }

    configs.push(config)
  }
  
  return config
}

function initPrograms(size:number = 16): ProgramsType {
  let res: ProgramsType = new Array<string>(size)
  let start = "a".charCodeAt(0)
  
  for (let i = 0; i < size; i++)
    res[i] = String.fromCharCode(start + i)

  return res;
}

const programsConcat = (programs: ProgramsType) => programs.reduce((acc, curr) => acc += curr)

let progs: ProgramsType = initPrograms(5)
console.log(danceMoves(progs, "s1,x3/4,pe/b", 2))

let progs2: ProgramsType = initPrograms(16)
let moves = fs.readFileSync('day16.txt','utf8')
console.log(danceMoves(progs2, moves, 1000000000))
