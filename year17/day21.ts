import fs = require('fs')

function rotate<T> (array: T[][]): T[][] {
  let res: T[][] = []

  for (let c = 0; c < array[0].length; c++) {
    let row = []
    for (let r = 0; r < array.length; r++)
      row[array.length - r - 1] = array[r][c]
    
    res[c] = row
  }

  return res
}

const flip = <T>(array: T[][], vert: boolean = false): T[][] =>
  vert ? array.slice().reverse() : array.map(a => a.slice().reverse())

function readRules() {
  let res: Map<string, string[][]> = new Map()
  let lines = fs.readFileSync('day21.txt','utf8').split(/\r?\n/)

  for (let l of lines) {
    let [pattern, produce] = l.split(" => ").map(a => a.split("/").map(a => a.split("")))
    //console.log(pattern, produce)

    for (let i = 0; i < 4; i++) {
      res.set(toString(pattern), produce)
      res.set(toString(flip(pattern, true)), produce)
      res.set(toString(flip(pattern, false)), produce)
      //console.log(i, pattern, res.keys())

      pattern = rotate(pattern)
    }
  }

  return res
}

const toString = (array: string[][]) => 
  array.reduce((acc, curr) => acc += curr.reduce((a, c) => a += c), "")

type Square = string[]

function divideGrid(grid: Square): string[][] {
  let factor = ((grid.length % 2) === 0) ? 2 : 3
  let g: Square[] = grid.map(a => a.match(new RegExp('.{1,' + factor + '}', 'g')))
  //console.log(factor, g)

  let res: string[][] = []

  for (let row = 0; row < g.length; row += factor) {
    let rowres: string[] = []

    for (let col = 0; col < g[0].length; col++) {
      let pattern = ""
      for (let r = 0; r < factor; r++) {
        pattern += g[row + r][col]
      }

      rowres.push(pattern)
      //console.log(pattern)
    }

    res.push(rowres)
  }

  return res
}

const stringify = (array: string[][]): string[] =>
  array.map(a => a.reduce((acc, curr) => acc + curr))

function enhanceGrid(grid: string[][]): string[] {
  let eg: string[][][] = grid.map(a => a.map(a => stringify(rules.get(a))))

  let res: string[] = []

  for (let row of eg) {
    for (let r = 0; r < row[0].length; r++) {
      let rowres = ""
      for (let c = 0; c < eg.length; c++)
        rowres += row[c][r]
      
      res.push(rowres)
    }
  }

  return res
}

function fractal(grid: Square, steps: number): Square {
  for (let i = 0; i < steps; i++) 
    grid = enhanceGrid(divideGrid(grid))
  
    return grid
}

const countOnPixels = (grid: Square) =>
  grid.reduce((acc, curr) => acc + curr.match(/#/g).length, 0)

/*
let m1 = [["1","2"], ["3","4"]]
console.log(rotate(m1), toString(rotate(m1)))
console.log(rotate(rotate(m1)), toString(rotate(rotate(m1))))
console.log(rotate(rotate(rotate(m1))), toString(rotate(rotate(rotate(m1)))))
let m2 = [[1,2,3], [4,5,6], [7,8,9]]
console.log(rotate(m2))
console.log(rotate(rotate(m2)))
console.log(rotate(rotate(rotate(m2))))
let m3 = [[1,2,3], [4,5,6], [7,8,9]]
console.log(flip(m3))
console.log(flip(m3, true))
*/

let rules = readRules()
//console.log(rules.keys())

//let grid = divideGrid([".#.", "..#", "###"])
//console.log("divided:", grid)
//console.log("enhanced:", enhanceGrid(grid))
//console.log("enhanced:", enhanceGrid([["#...", ".#.."], ["..#.", "...#"]]))

console.log(countOnPixels(fractal([".#.", "..#", "###"], 18)))