import fs = require('fs')

/*  Parsed Language

  anyChar: [a-z] | [0-9]

  garbageChar: "!" anyChar | anyChar

  garbage: "<" garbageChar* ">""

  term: group | garbage

  group: "{" "" | (term ",")* term "}"
*/

class Group {
  children: Group[]

  addChild(group: Group) {
    if (!this.children)
      this.children = new Array()
    
    this.children.push(group)
  }

  getScoreSum(level: number = 1): number {
    let res = level

    if (this.children)
      for (let group of this.children)
        res += group.getScoreSum(level + 1)

    return res
  }
}

class Parser {
  buffer: string
  index: number
  garbageCount:number

  getChar(): string {
    if (this.index >= this.buffer.length)
      throw("Premature end")

    let char = this.buffer.charAt(this.index)
    this.index++

    return char
  }

  peekChar(): string {
    if (this.index >= this.buffer.length)
      throw("Premature end")

    return this.buffer.charAt(this.index)
  }  

  parseGarbageChar() {
    if (this.getChar() === "!")
      this.getChar()
    else
      this.garbageCount++
  }

  parseGarbage() {
    if (this.getChar() !== "<")
      throw("Invalid garbage")

    while (this.peekChar() !== ">")
      this.parseGarbageChar()

    if (this.getChar() !== ">")
      throw("Invalid garbage")
  }

  parseGroup(): Group {
    if (this.getChar() !== "{")
      throw("Invalid group")

    let group = new Group()

    while (this.peekChar() !== "}") {
      switch (this.peekChar()) {
        case "{":
          group.addChild(this.parseGroup())
          break
        case "<":
          this.parseGarbage()
          break
        case ",":
          this.getChar()
          break
        default:
          throw("Malformed group")
      }
    }

    if (this.getChar() !== "}")
      throw("Invalid group")

    return group
  }

  parse() {
    if (this.peekChar() === "<")
      this.parseGarbage()
    else
      console.log("Groups: " + this.parseGroup().getScoreSum())

    console.log("Garbage: " + this.garbageCount)

    return this.index === this.buffer.length
  }

  constructor(buffer: string) {
    this.buffer = buffer
    this.index = 0
    this.garbageCount = 0
  }
}

function readStreams() {
  for (let stream of fs.readFileSync('day9.txt','utf8').split(/\r?\n/))
    console.log(new Parser(stream).parse())
}

readStreams()