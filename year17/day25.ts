import fs = require('fs')

type Action = [number, number, string]

type Blueprints = {[state: string]: [Action, Action]}

const blueprints1: Blueprints = {
  A: [[1, 1, "B"], [0, -1, "B"]],
  B: [[1, -1, "A"], [1, 1, "A"]]
}

const blueprints2: Blueprints = {
  A: [[1, 1, "B"], [1, -1, "E"]],
  B: [[1, 1, "C"], [1, 1, "F"]],
  C: [[1, -1, "D"], [0, 1, "B"]],
  D: [[1, 1, "E"], [0, -1, "C"]],
  E: [[1, -1, "A"], [0, 1, "D"]],
  F: [[1, 1, "A"], [1, 1, "C"]],
}
function execute(bp: Blueprints, steps: number) {
  let tape: number[] = [steps]
  for (let i = 0; i < steps; i++) tape[i] = 0

  let pc = Math.floor(steps/2)
  let state = "A"

  for (let i = 0; i < steps; i++) {
    let curr: number = tape[pc]
    let act: Action = bp[state][curr]
    //print(state, curr, act)

    tape[pc] = act[0]
    pc += act[1]
    state = act[2]

    //print(tape)
  }

  return tape.reduce((acc, curr) => acc + curr)
}

const print = console.log.bind(console)
//print(blueprints["A"][0])

print(execute(blueprints1, 6))
print(execute(blueprints2, 12459852))