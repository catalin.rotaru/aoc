import fs = require('fs')

type Hexagon = [number, number, number]

let moves = {"n": [0, 1, -1], "ne": [1, 0, -1], "se": [1, -1, 0], "s": [0, -1, 1], "sw": [-1, 0, 1], "nw": [-1, 1, 0]}

function move(hex: Hexagon, dir: string): Hexagon {
  return <Hexagon> hex.map((curr, idx) => curr += moves[dir][idx])
}

function getDistance(origin: Hexagon, final: Hexagon): number {
  return Math.max(Math.abs(origin[0] - final[0]), 
    Math.abs(origin[1] - final[1]), Math.abs(origin[2] - final[2]))
}

function moveSteps(steps: string, origin: Hexagon = [0, 0, 0]): number {
  let dist: number[] = []
  
  let final =  steps.split(",").reduce(function(acc: Hexagon, curr: string): Hexagon {
    let res = move(acc, curr)
    dist.push(getDistance(origin, res))
    return res
  }, origin)

  console.log("Max:", Math.max(...dist))

  return dist[dist.length - 1]
}

//console.log(move([5, 5, 5], "n"))
console.log(moveSteps("ne,ne,ne"))
console.log(moveSteps("ne,ne,sw,sw"))
console.log(moveSteps("ne,ne,s,s"))
console.log(moveSteps("se,sw,se,sw,sw"))

console.log(moveSteps(fs.readFileSync('day11.txt','utf8')))