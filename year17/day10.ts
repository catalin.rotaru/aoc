function createList(length: number): number[] {
  let list = new Array<number>(length)
  for (let i = 0; i < length; i++)
    list[i] = i
  return list
}

function getItemCircular(list: number[], index: number): number {
  return list[index % list.length]
}

function setItemCircular(list: number[], index: number, value:number) {
  list[index %list.length] = value
}

function swapItemCircular(list: number[], first: number, last: number) {
  let a = first % list.length
  let b = last % list.length
  //console.log("Swap " + first + " with " + last)

  if (a === b) 
    return

  let v = list[a]
  list[a] = list[b]
  list[b] = v
}

function twist(list: number[], first: number, length: number) {
  if (length > list.length)
    throw("Twist length larger than list")

  for (let i = 0; i < Math.floor(length/2); i++)
    swapItemCircular(list, first + i, first + length -i - 1)
}

function hashKnot(list: number[], lengths: number[], rounds: number = 1): number {
  //console.log(len)
  //console.log(list)

  let current = 0
  let skip = 0

  for (let r = 0; r < rounds; r++)
    for (let len of lengths) {
      twist(list, current, len)
      //console.log(list)
      current += len + skip
      skip++
    }

  return list[0] * list[1]
}

type RepresentFunction = (hash: number) => string

function representHash(hash: number): string {
  return ("00" + hash.toString(16)).substr(-2)
}

function denseHash(list: number[], represent: RepresentFunction, size: number = 16): string {
  //console.log(list)
  let res = list.reduce((acc, curr, i, self) => {
    if (!(i % size))
      acc.push(list.slice(i, i + size).reduce((a, c) => a ^ c));
    
    return acc;
  }, [])

  //console.log(res)
  return res.reduce((acc, curr) => acc + represent(curr), "")
}

function readLengths(lengths: string): number[] {
  return lengths.split(",").map((a) => +a)
}

function readLengths2(lengths: string): number[] {
  let res = lengths.split("").map((a) => +a.charCodeAt(0))

  let extra = [17, 31, 73, 47, 23]
  return res.concat(extra)
}

export default function computeHash(input: string, represent: RepresentFunction = representHash): string {
  list = createList(256)
  hashKnot(list, readLengths2(input), 64)
  return denseHash(list, represent)
}

let list
//list = createList(5)
//console.log(list)
//twist(list, 2, 5)
//console.log(list)

list = createList(5)
console.log(hashKnot(list, readLengths("3,4,1,5")))

list = createList(256)
console.log(hashKnot(list, readLengths("18,1,0,161,255,137,254,252,14,95,165,33,181,168,2,188")))

//console.log(readLengths2("1,2,3"))
//console.log(denseHash(createList(10), 2))

console.log(computeHash(""))
console.log(computeHash("AoC 2017"))
console.log(computeHash("1,2,3"))
console.log(computeHash("1,2,4"))

console.log(computeHash("18,1,0,161,255,137,254,252,14,95,165,33,181,168,2,188"))