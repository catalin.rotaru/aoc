import fs = require('fs');

type OffsetFunction = (command: number) => number

function readCommands(): number[] {
  return fs.readFileSync('day5.txt','utf8').split(/\r?\n/).map((a)=>+a);
}

function offset1(command: number): number {
  return 1
}

function offset2(command: number): number {
  return command >= 3 ? -1 : 1
}

function execute(commands: number[], offset: OffsetFunction): number {
  let current:number = 0;
  for (let steps = 0; ; steps++) {
    //console.log(current, commands) 
    let next:number = current + commands[current]
    commands[current] += offset(commands[current])

    if ((next < 0) || (next >= commands.length))
      return steps + 1

    current = next
  }
}

//console.log(execute([0, 3, 0, 1, -3], offset1))
console.log(execute(readCommands(), offset1))
//console.log(execute([0, 3, 0, 1, -3], offset1))
console.log(execute(readCommands(), offset2))