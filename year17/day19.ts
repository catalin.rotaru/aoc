import fs = require('fs')

type MaizeType = string[][]

const VERT = "|"
const HOR = "-"
const TURN = "+"
const END = " "

function navigate(maize: MaizeType): [string, number] {
  let row = 0
  let col = maize[row].indexOf(VERT)

  let dr = 1
  let dc = 0

  let trace = ""
  let steps = 0

  do {
    let ch = maize[row][col]

    if (ch === TURN) {
      let [nr, nc] = findExit(maize, row, col, dr, dc)
      dr = nr - row
      dc = nc - col
      row = nr
      col = nc
    } else {
      if (ch.match(/[A-Z]/))
        trace += ch
      row += dr
      col += dc
    }

    steps ++
    
  } while (inMaize(maize, row, col) && (maize[row][col] !== END))

  return [trace, steps]
}

const inMaize = (maize: MaizeType, row: number, col: number): boolean => 
  (row >= 0) && (row < maize.length) && (col >= 0) && (col < maize[row].length)

const path = (dr: number): string => (dr === 0) ? HOR : VERT

function isExit(maize: MaizeType, row: number, col: number, path: string): boolean {
  if (!inMaize(maize, row, col))
    return false
  
  let ch = maize[row][col]
  //console.log("is:", [row, col], ch)

  if ((ch === path) || (ch === TURN) || ch.match(/[A-Z]/))
    return true
}

function findExit(maize: MaizeType, row: number, col: number, dr: number, dc: number): [number, number] {
  let p = path(dr)
  //console.log("find:", [row, col], maize[row][col], p)

  if (isExit(maize, row + dr, col + dc, p))
    return [row + dr, col + dc]

  if (isExit(maize, row - dc, col + dr, path(dc)))
    return [row - dc, col + dr]

  if (isExit(maize, row + dc, col - dr, path(dc)))
    return [row + dc, col - dr]

  return [row, col]
}

let maize: MaizeType
  = fs.readFileSync('day19.txt','utf8').split(/\r?\n/).map((a)=>a.split(""))
console.log(maize.length, maize[0].length)
console.log(navigate(maize))

//let pos = findExit(maize, 3, 14, -1, 0)
//console.log(pos, maize[pos[0]][pos[1]])