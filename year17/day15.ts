function generator(factor: number, past: number, criteria: number = 1): number {
  let val = past;

  do {
    val = val*factor % 2147483647
  } while (val % criteria != 0)

  return val
}

function generate(start1: number, start2: number, [criteria1, criteria2]: [number, number] = [1, 1], steps: number = 40000000):number {
  let val1 = start1
  let val2 = start2

  const factor1 = 16807
  const factor2 = 48271

  let matches = 0

  for (let i = 0; i < steps; i++) {
    val1 = generator(factor1, val1, criteria1)
    val2 = generator(factor2, val2, criteria2)

    if ((val1 & 0xffff) === (val2 & 0xffff))
      matches++

    //console.log("values: " + val1 + "\t" + val2)
  }

  return matches
}

console.log(generate(65, 8921))
console.log(generate(289, 629))

console.log(generate(65, 8921, [4, 8], 5000000))
console.log(generate(289, 629, [4, 8], 5000000))
