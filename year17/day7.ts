import fs = require('fs')

type TreeMap = Map<string, Tree>

class Tree {
  name: string
  weight: number
  totalWeight: number
  parent: Tree
  children: TreeMap

  constructor(name: string) {
    this.name = name
    this.weight = 0
    this.totalWeight = 0
  }

  addChild(name: string): Tree {
    if (!this.children) {
      this.children = new Map()
    }

    let child:Tree = getTree(name);
    this.children.set(name, child)
    child.parent = this
    return child
  }

  
  computeWeight() {
    if (this.children) {
      let weights: Set<number> = new Set()

      for (let child of this.children.values()) {
        child.computeWeight()
        
        weights.add(child.totalWeight)

        this.totalWeight += child.totalWeight
      }
      
      if (weights.size > 1)
        console.log("Unbalanced: ", 
          [...this.children.values()].map((t) => t.totalWeight),
          [...this.children.values()].map((t) => t.weight))
    }

    this.totalWeight += this.weight
  }
}

let trees: TreeMap = new Map()

function getTree(name: string) {
  let tree = trees.get(name)
  if (!tree) {
    tree = new Tree(name)
    trees.set(name, tree)
  }
  return tree
}

function parseTree(line: string): Tree {
  //console.log(line)
  let [node, subs] = line.split("->")
  let [name, weightString] = line.split(" ")
  let weight = +weightString.substring(1, weightString.length - 1)

  let tree = getTree(name)
  tree.weight = weight

  if (subs) {
    for (let name of subs.trim().split(", "))
      tree.addChild(name)
  }

  return tree
}

function readTrees() {
  for (let line of fs.readFileSync('day7.txt','utf8').split(/\r?\n/))
    parseTree(line)
}

readTrees()

let root:Tree = [...trees.values()].find((t) => !t.parent)
console.log(root.name)
root.computeWeight()
console.log(root.totalWeight)
