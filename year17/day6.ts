type Memory = number[]

function balance(memory: Memory): [number, number] {
  let configs = new Set<String> ()

  for (let steps = 0; ; ) {
    //console.log(memory)
    let memoryConfig = memory.toString()
    let size = configs.size
    configs.add(memoryConfig)
    //console.log(configs)
    if (size === configs.size) {
      return [steps, steps - [...configs].indexOf(memoryConfig)]
    }

    steps++

    let maxBank = memory.reduce((max, x, i, mem) => x > mem[max] ? i : max, 0)
    //console.log(maxBank)
    let blocks = memory[maxBank]
    memory[maxBank] = 0
    let extra = Math.floor(blocks/memory.length)
    for (let i = 0; i < memory.length; i++) 
      memory[i] += extra
    for (let i = 1; i <= blocks % memory.length; i++)
      memory[(maxBank + i) % memory.length]++;
  }
}

console.log(balance([0, 2, 7, 0]))

let input: String = "5	1	10	0	1	7	13	14	3	12	8	10	7	12	0	6"
console.log(balance(input.split(/[ \t]+/).map((a)=>+a)))