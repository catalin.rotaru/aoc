import fs = require('fs');

type NormalizationFunction = (word: string) => string

function readPassphrases(): string[] {
  return fs.readFileSync('day4.txt','utf8').split(/\r?\n/);
}

function isValid(passphrase: string, normalize: NormalizationFunction): boolean {
  let words = new Set<string>()
  return passphrase.split(" ").every(function(word: string): boolean {
    //console.log(word)
    let size = words.size
    words.add(normalize(word));
    return size !== words.size;
  })
}

function countValidPassphrases(phrases: string[], normalize: NormalizationFunction): number {
  return phrases.reduce((total, phrase) => total + +isValid(phrase, normalize), 0)
}

function normalizeWord1(word: string): string {
  return word;
}

function normalizeWord2(word: string): string {
  return [...word].sort().join("");
}

//console.log(isValid("aa bb cc dd ee", normalizeWord1))
//console.log(isValid("aa bb cc dd aa", normalizeWord1))
//console.log(isValid("aa bb cc dd aaa", normalizeWord1))

let phrases = readPassphrases()
console.log(countValidPassphrases(phrases, normalizeWord1))
//console.log(normalizeWord2("creatives"))
console.log(countValidPassphrases(phrases, normalizeWord2))