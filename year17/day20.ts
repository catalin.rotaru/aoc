import fs = require('fs')

type Vector = [number, number, number]
type Particle = [Vector, Vector, Vector]

const travel = (x: number, speed: number, acc: number, time: number): number =>
  x + speed * time + acc * time * (time + 1) / 2

function travelVector(point: Vector, speed: Vector, acc: Vector, time: number = 1): Vector {
  return [travel(point[0], speed[0], acc[0], time), 
    travel(point[1], speed[1], acc[1], time), 
    travel(point[2], speed[2], acc[2], time)]
}

const travelParticles = (parts: Particle[], time: number = 1): Vector[] =>
  parts.map((a) => travelVector(a[0], a[1], a[2], time))

function distance(orig: Vector, dest: Vector = [0, 0, 0]): number {
  return Math.abs(dest[0] - orig[0]) + Math.abs(dest[1] - orig[1]) + Math.abs(dest[2] - orig[2]) 
}

function readVector(serial: string): Vector {
  return <Vector> serial.slice(3, serial.length - 1).split(",").map((a) => +a)
}

function readParticles(): Particle[] {
  let lines = fs.readFileSync('day20.txt','utf8').split(/\r?\n/)
  let part = lines.map((a) => a.split(", ").map((a) => readVector(a)))

  return <Particle[]> part
}

const toString = (point: Vector): string =>
  "" + point[0] + point[1] + point[2]

function getCollided(point: Vector[]): boolean[] {
  let count: Map<string, number> = new Map()

  point.map((a, i) => count.set(toString(a), (count.get(toString(a)) || 0) + 1))
  //console.log(count)

  return point.map((a) => count.get(toString(a)) != 1)
}

function collide(part: Particle[], length: number): number {
  for (let i = 0; i < length; i++) {
    let p = travelParticles(part, i)
    //console.log(pt, v, acc)
    let dead: boolean[] = getCollided(p)
    //console.log(dead)

    part = part.filter((a, idx) => !dead[idx])
  }

  return part.length
}

//console.log(distance(travelVector(readVector("a=<3,0,0>"), [2,0,0], [-1,0,0], 4)))
//console.log(getCollided([[0,0,0], [1,1,1], [0,0,0]]))

let orig = readParticles()
let parts = travelParticles(orig, 1000000000)
let dist = parts.map((a) => distance(a))
console.log(dist.indexOf(Math.min(...dist)))

console.log(collide(orig, 1000))
