import fs = require('fs');

type NextIndexFunction = (captcha: string, current: number) => number;

let nextIndex1: NextIndexFunction =
  function (captcha: string, current: number): number {
    return (current + 1) % captcha.length
  }

let nextIndex2: NextIndexFunction =
  function (captcha: string, current: number): number {
    return (current + captcha.length / 2) % captcha.length
  }

function getNextChar(captcha: string, current: number, nextIndex: NextIndexFunction): string {
  let index = nextIndex(captcha, current);
  return captcha.charAt(index);
}

function sumMatchers(captcha: string, nextIndex: NextIndexFunction): number {
  let res = 0;
  for (let i = 0, len = captcha.length; i < len; i++) {
    let ch = captcha.charAt(i)
    if (ch === getNextChar(captcha, i, nextIndex)) {
      res += +ch;
    }
  }

  return res;
}

let puzzle: string = fs.readFileSync('day1.txt','utf8').toString();
//console.log(puzzle)

console.log(sumMatchers(puzzle, nextIndex1));
console.log(sumMatchers(puzzle, nextIndex2));