import fs = require('fs')

class Scanner {
  range: number

  constructor(range: number) {
    this.range = range
  }

  computeLocation(steps: number): number {
    let loc = steps % (2 * (this.range - 1))
    
    if (loc < this.range)
      return loc
    else
      return 2 * this.range - loc - 2
  }
}

let firewall: Map<number, Scanner> = new Map()

function addScanner(depth: number, range: number) {
  firewall.set(depth, new Scanner(range))
}

function readFirewall() {
  for (let line of fs.readFileSync('day13.txt','utf8').split(/\r?\n/)) {
    let [depth, range] = line.split(": ")
    addScanner(+depth, +range)
  }
}

function traverseFirewall(): number {
  return Array.from(firewall).reduce((acc: number, [d, s]) =>
    acc + ((s.computeLocation(d) == 0) ? d * s.range : 0), 0)
}

function probeFirewall() {
  for (let delay = 0; ; delay++) {
    //console.log(Array.from(firewall).map(([d, s]) => s.computeLocation(delay + d)))

    let open = Array.from(firewall).every(([d, s]) => s.computeLocation(delay + d) != 0)

    if (open) {
      console.log("delay " + delay + " open is " + open)
      break
    }
  }
}

readFirewall()
//console.log(firewall)
console.log(traverseFirewall())
probeFirewall()