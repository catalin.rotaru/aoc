import fs = require('fs')

class Instruction {
  register: string
  operation: string
  amount: number

  conditionReg: string
  condition: string
  conditionAmount: number

  constructor (register: string, operation: string, amount: number, 
    conditionReg: string, condition: string, conditionAmount: number) {
    this.register = register
    this.operation = operation
    this.amount = amount

    this.conditionReg = conditionReg
    this.condition = condition
    this.conditionAmount = conditionAmount
  }
}

function getRegisterValue(name: string): number {
  let value: number = registers.get(name)
  return value ? value : 0
}

function parseInstruction(line: string): Instruction {
  //console.log(line)

  let [op, cnd] = line.split("if")

  let [reg, o, oa] = op.trim().split(" ")
  let [cr, c, ca] = cnd.trim().split(" ")

  return new Instruction(reg, o, +oa, cr, c, +ca)
}

function executeInstruction(ins: Instruction): number {
  let expression = getRegisterValue(ins.conditionReg) + " " + ins.condition + " " + ins.conditionAmount
  //console.log(expression)
  if (! eval(expression))
    return

  let amount = ins.operation === "inc" ?  ins.amount : -  ins.amount
  //console.log(amount)
  let val = getRegisterValue(ins.register) + amount
  registers.set(ins.register, val)

  return val
}

function readInstructions() {
  let max = 0

  for (let line of fs.readFileSync('day8.txt','utf8').split(/\r?\n/)) {
    let val = executeInstruction(parseInstruction(line))

    if (val) {
      max = Math.max(max, val)
      //console.log([max, val])
    }
  }

  return max
}

let registers: Map<string, number> = new Map()

//console.log(getRegisterValue("xxx"))
//executeInstruction(parseInstruction("b dec -5 if a > -1"))
let max = readInstructions()
console.log(registers)
console.log("Max is: " + Math.max(...registers.values()))
console.log("Absolute max was: " + max)