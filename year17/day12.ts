import fs = require('fs')

type GraphMap = Map<number, Graph>

class Graph {
  id: number
  group: number
  adjacent: GraphMap
  
  constructor (id: number) {
    this.id = id
    this.group = -1
  }

  addRelation(id: number): Graph {
    let graph:Graph = getGraph(id)

    this.setAdjacentGraph(id, graph)
    graph.setAdjacentGraph(this.id, this)

    return graph
  } 

  addRelations(ids: number[]) {
    for (let id of ids)
      this.addRelation(id)
  }

  setAdjacentGraph(id: number, graph: Graph) {
    if (!this.adjacent) {
      this.adjacent = new Map()
    }

    this.adjacent.set(id, graph)
  }  

  setGroup(group: number) {
    if (this.group !== -1)
      return
    
    this.group = group
    for (let a of this.adjacent.values())
      a.setGroup(group)
  }
}

let graphs: GraphMap = new Map()

function getGraph(id: number) {
  let graph = graphs.get(id)
  if (!graph) {
    graph = new Graph(id)
    graphs.set(id, graph)
  }
  return graph
}

function readGraphs() {
  for (let line of fs.readFileSync('day12.txt','utf8').split(/\r?\n/)) {
    let [graph, adjacent] = line.split("<->")
    let id = +graph.trim()
    let rel = adjacent.trim().split(", ").map((a) => +a.trim())

    //console.log("Graph " + id + "<->" + rel)
    getGraph(id).addRelations(rel)
  }
}

function countGroup(group: number) {
  return [...graphs.values()].reduce(
    (acc, curr) => acc + (curr.group === group ? 1 : 0), 0)
}

function setGroups() {
  for (let g of graphs.values())
    g.setGroup(g.id)
}

function countGroups():number {
  let groups: Set<number> = new Set()
  for (let g of graphs.values())
    groups.add(g.group)

  return groups.size
}

//getGraph(1).addRelations([3, 5])

readGraphs()
setGroups()
//console.log(graphs)
console.log(countGroup(0))
console.log(countGroups())
