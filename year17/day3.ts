type Coordinate = [number, number];
type SquareVal = [number, number, number];

let vector: Coordinate[] = [[0, 1], [1, 0], [0, -1], [-1, 0]];

function distance(loc: Coordinate) {
  return Math.abs(loc[0]) + Math.abs(loc[1])
}
function turn(direction: number, side: string): number {
  return ((direction + ((side === "R") ? 1 : -1) % vector.length) + vector.length) % vector.length
}

function travel(dest: number): Coordinate {
  let x = 0;
  let y = 0;
  let d = 1;

  let step = 1;
  let loc = 1;
  
  do {
    //console.log(loc, "=>", [x, y])
    
    let steps = Math.min(step, dest - loc)
    if (steps === 0)
      break;
      
    x += steps*vector[d][0]
    y += steps*vector[d][1]

    loc += steps;
    //console.log("loc:", loc)
    if (d%2 === 0) {
      step++;
      //console.log("step:", step)
    }
    d = turn(d, "L");

  } while (loc <= dest)

  return [x, y];
}
function traceFill(maxVal: number):number {
  let squares: SquareVal[] = [];

  let x = 0;
  let y = 0;
  let d = 1;

  let step = 1;
  let loc = 1;

  do {
    let i = 0;
    do {
      let val = computeValInSquare(squares, x, y);
      squares.push([x, y, val]);
      //console.log(loc + i, "=>", [x, y, val])      
      if (val > maxVal)
        return val;
      
      x += vector[d][0]
      y += vector[d][1]
      i++
    } while (i < step)

    loc += step;
    if (d%2 === 0) {
      step++;
      //console.log("step:", step)
    }
    d = turn(d, "L");

  } while (true)
}

function findValInSquare(squares: SquareVal[], x:number, y:number): number {
  for (let sq of squares) {
    if ((sq[0] === x) && (sq[1] === y))
      return sq[2]
  }

  return 0;
}

function computeValInSquare(squares: SquareVal[], x:number, y:number): number {
  let res = 0;

  for (let i = -1; i <= 1; i++)
    for (let j = -1; j <= 1; j++)
      res += findValInSquare(squares, x + i, y + j)

  return res > 0 ? res : 1;
}

console.log(distance(travel(312051)));
console.log(traceFill(312051));

//let squares: SquareVal[] = [[1, 1, 5], [-1, -1, 10]]
//console.log(computeValInSquare(squares, 0, 0))