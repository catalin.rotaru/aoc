import fs = require('fs')

type Component = [number, number]
type Bridge = Component[]

const print = console.log.bind(console)

function build(comps: Component[], res: Bridge[], head: number = 0, bridge: Bridge = []) {
  //print("build:", comps, bridge)

  if (bridge.length > 0)
    res.push(bridge)

  let match = comps.filter(a => (a[0] === head) || (a[1] === head))
  if (match.length <= 0)
    return
  
  for (let comp of match) {
    let next = comps.indexOf(comp)

    let nc = comps.slice()
    nc.splice(next, 1)
    
    let nb = bridge.concat([comps[next]])
    //print("next", nc, nb)

    build(nc, res, (head === comp[0]) ? comp[1] : comp[0], nb)
  }
}

const stengthComp = (b: Component) => b[0] + b[1]

const strengthBridge = (bridge: Bridge) =>
  bridge.reduce((acc, curr) => acc + stengthComp(curr), 0)

const max = <T>(array: T[]) =>
  array.reduce((acc, curr) => (curr > acc) ? curr : acc)

let bridges: Bridge[] = []
let comps: Component[] = fs.readFileSync('day24.txt','utf8').split(/\r?\n/).map(l => {
  let [c1, c2] = l.split("/")
  return <Component> [+c1, +c2]
})

build(comps, bridges)

let strengths = bridges.map(strengthBridge)
print("strongest:", max(strengths))

let lengths = bridges.map(a => a.length)
let longest = max(lengths)
print("longest:", longest)

print(max(bridges.filter(a => a.length === longest).map(strengthBridge)))