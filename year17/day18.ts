import fs = require('fs')

type RegistersType = Map<string, number>
type PortType = number[]

let ops = {
  snd: (reg: RegistersType, port: PortType, x:string): number => {
    port.push(regVal(reg, x))
    reg.set("sent", regVal(reg, "sent") + 1)
    return 1
  },

  set: (reg: RegistersType, port: PortType, x:string, y:string): number => {
    reg.set(x, regVal(reg, y))
    return 1
  },

  add: (reg: RegistersType, port: PortType, x:string, y:string): number => {
    reg.set(x, regVal(reg, x) + regVal(reg, y))
    return 1
  },
  
  mul: (reg: RegistersType, port: PortType, x:string, y:string): number => {
    reg.set(x, regVal(reg, x) * regVal(reg, y))
    return 1
  },
  
  mod: (reg: RegistersType, port: PortType, x:string, y:string): number => {
    reg.set(x, regVal(reg, x) % regVal(reg, y))
    return 1
  },

  rcv: (reg: RegistersType, port: PortType, x:string): number => {
    if (regVal(reg, x) === 0) 
      return 1

    console.log("recover: ", port.pop())
    return 1000
  },
  
  jgz: (reg: RegistersType, port: PortType, x:string, y:string): number => {
    if (regVal(reg, x) <= 0)
      return 1 
    return regVal(reg, y)
  },
}

const regVal = (reg: RegistersType, x:string) => isNaN(+x) ? (reg.has(x) ? reg.get(x) : 0) : +x

function operation(reg: RegistersType, port: PortType, instruction: string): number {
  //console.log("op:", instruction)

  let [op, x, y] = instruction.split(" ")
  return ops[op](reg, port, x, y)
}

function execute(ops: string[]) {
  //console.log(ops)
  let reg: RegistersType = new Map()
  let port: PortType = []

  let i = 0
  for (; (i >= 0) && (i < ops.length); ) {
    i += operation(reg, port, ops[i])
    //console.log("next:", i)
  }

  //console.log("exit:", i, reg)
}

function executeConcurrent(ops: string[]) {
  let reg: RegistersType[] = [new Map(), new Map()]
  let port: PortType[] = [[], []]
  let pc: number[] = [0, 0]

  for (let i = 0; i < reg.length; i++)
    reg[i].set("p", i)

  for (let dead: number = 0; dead < 2; ) {
    dead = 0

    for (let i = 0; i < reg.length; i++) {
      if ((pc[i] >= 0) && (pc[i] < ops.length)) {
        let op = ops[pc[i]]
        let p = port[op.startsWith("rcv") ? 1 - i : i]

        //process.stdout.write("T" + i + ": ");
        let inc = operation(reg[i], p, op)
        pc[i] += inc
        //console.log("next:", pc[i])

        if (inc === 0)
          dead++
      } else
        dead++
    }
  }

  console.log(reg)
  console.log(port)
  console.log(pc)
}

let operations = fs.readFileSync('day18.txt','utf8').split(/\r?\n/)

execute(operations)

ops.rcv = (reg: RegistersType, port: PortType, x:string): number => {
  if (port.length <= 0)
    return 0

  reg.set(x, port.shift())
  return 1
}

executeConcurrent(operations)
