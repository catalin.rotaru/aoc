import computeHash from "./day10"

type MapType = number[][]

function representHash2(hash: number): string {
  return ("00000000" + hash.toString(2)).substr(-8)
}

function computeUsage(hashes: string[]): number {
  return hashes.reduce((acc, curr) => acc + (curr.match(/1/g) || []).length, 0)
}

function generateInput(input: string, length: number = 128): string[] {
  let list = new Array<string>(length)
  for (let i = 0; i < length; i++)
    list[i] = input + "-" + i
  return list
}

function computeHashes(input: string[]): string[] {
  return input.map((a) => computeHash(a, representHash2))
}

function computeMap(hashes: string[]): MapType {
  let map: MapType = new Array()

  for (let hash of hashes)
    map.push(hash.split("").map((a) => -a))
  
  //console.log(map[0])
  return map
}

function markRegion(map: MapType, row: number, col: number, region: number): number {
  if (map[row][col] >= 0) 
    return 0

  let size = 1
  map[row][col] = region

  if (col < map[row].length - 1)
    size += markRegion(map, row, col + 1, region)
  if (col > 0)
    size += markRegion(map, row, col - 1, region)
  if (row < map.length - 1)
    size += markRegion(map, row + 1, col, region)
  if (row > 0)
    size += markRegion(map, row - 1, col, region)

  return size
}

function printMap(map: MapType, size: number = 20) {
  for (let row = 0; row < size; row++) {
    console.log(map[row].slice(0, size).reduce((acc, curr) => acc += (" " + curr), ""))
  }
}

function regionsMap(map: MapType) {
  let region = 1

  for (let row = 0; row < map.length; row++)
    for (let col = 0; col < map[row].length; col++) {
      let size = markRegion(map, row, col, region)
      if (size > 0) {
        //console.log("Region " + region + " size: " + size)
        region++
      }
    }

  console.log("Regions marked: " + (region - 1))
  //printMap(map, 4)
}

console.log("--------------------------------")
//console.log(representHash2(0))
//console.log(representHash2(1))
//console.log(representHash2(128))
//console.log(representHash2(255))

//console.log(generateInput("home", 16))
//console.log((computeHash("flqrgnkx-0", representHash2).match(/1/g) || []).length)
let hashes: string[]

hashes = computeHashes(generateInput("flqrgnkx"))
console.log(computeUsage(hashes))
regionsMap(computeMap(hashes))


hashes = computeHashes(generateInput("nbysizxe"))
console.log(computeUsage(hashes))
regionsMap(computeMap(hashes))
