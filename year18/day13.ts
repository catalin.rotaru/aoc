import fs = require('fs')

const print = console.log.bind(console)

// direction is a number! 0:up, 1:right, 2:down, 3:left
const directionDelta: [number, number][] = [[0, -1], [1, 0], [0, 1], [-1, 0]]
const cartDirection = {'^':0, '>':1, 'v':2, '<':3}
const directionSize = 4

// spin is a number! 0:left, 1:straight, 2:right
const spinSize = 3

type Position = [number, number] // X, Y

class Cart {
  position: Position
  direction: number
  spin: number
  removed: boolean

  constructor(x: number, y: number, cart: string) {
    this.position = [x,y]
    this.direction = cartDirection[cart]
    this.spin = 0
    this.removed = false
  }
}

type Carts = Array<Cart>
type TheMap = Array<Array<string>>

function rotate(original: number, direction: number): number {
  return (directionSize + original + direction) % directionSize
}

function move(x: number, y:number, direction: number): [number, number] {
  const [dx, dy] = directionDelta[direction]
  return [x + dx, y + dy]
}

function computeSpin(original: number, spin: number): [number, number] {
  return [rotate(original, spin - 1), (spinSize + spin + 1) % spinSize]
}

function computeDirection(original: number, spin: number, track: string): [number, number] {
  const [dx, dy] = directionDelta[original]

  switch (track) {
    case '+':
      return computeSpin(original, spin)
    case '/':
      return [rotate(original, 2*Math.abs(dy) - 1), spin]
    case '\\':
      return [rotate(original, 2*Math.abs(dx) - 1), spin]
    case '-':
    case '|':
      return [original, spin]
    default:
      print("ERROR!")
  }
}

function readMap(): [TheMap, Carts] {
  const ct = {'^':'|', '>':'-', 'v':'|', '<':'-'}
  const map = fs.readFileSync('day13.txt','utf8').split(/\r?\n/).map(s => s.split(''))

  let carts: Carts = new Array()
  map.forEach((row, y) => row.forEach((s, x) => {
    if (ct[s])
      carts.push(new Cart(x, y, s))
  }))

  return [map.map(row => row.map(s => ct[s] || s)), carts]
}

function printMap(map: TheMap) {
  map.forEach(s => print(s.join('')))
}

function crashed(carts: Carts, index: number) {
  for (let i = 0; i < carts.length; i++)
    if ((i != index) && (!carts[i].removed) && (carts[i].position[0] == carts[index].position[0]) && (carts[i].position[1] == carts[index].position[1])) {
      //print("removing", i, index)
      return carts[i].removed = carts[index].removed = true
    }
  
  return false
}

function moveCarts(carts: Carts, map: TheMap, remove: boolean = false): boolean {
  carts = carts.sort((a, b) => (a.position[1] == b.position[1]) ? a.position[0] - b.position[0]: a.position[1] - b.position[1])

  for (let i = 0; i < carts.length; i++) {
    let c = carts[i]
    if (!c.removed) {
      c.position = move(c.position[0], c.position[1], c.direction)
      if (crashed(carts, i)) 
        if (!remove) {
          print("CRASH: ", c.position)
          return false
        }

      let track = map[c.position[1]][c.position[0]]
      let [d, s] = computeDirection(c.direction, c.spin, track)
      c.direction = d
      c.spin = s
    }
  }

  let left = carts.filter(c => !c.removed)
  if (left.length <= 1) {
    print("LEFT:", left)
    return false
  }

  return true
}

function drive(carts: Carts, map: TheMap, remove: boolean = false) {
  do {
    //print(carts)
  } while (moveCarts(carts, map, remove))
}

print("rotate", rotate(1, 1))
print("rotate", rotate(2, -1))
print("rotate", rotate(0, -1))
print("rotate", rotate(3, 1))

print("spin", computeSpin(0, 0))
print("spin", computeSpin(3, 1))
print("spin", computeSpin(3, 2))

print("move", move(2, 2, 0))

print("dir", computeDirection(0, 0, '/'))
print("dir", computeDirection(0, 0, '\\'))
print("dir", computeDirection(3, 0, '/'))
print("dir", computeDirection(3, 0, '\\'))
print("dirs", computeDirection(0, 0, '+'))

let [map, carts] = readMap()
//printMap(map)
drive(carts, map)

let [map1, carts1] = readMap()
//printMap(map1)
drive(carts1, map1, true)
