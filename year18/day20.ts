import fs = require('fs')

const print = console.log.bind(console)

enum Tile {ROOM = '.', WALL = '#', DOOR = 'o', UNKNOWN = '?', ORIGIN = 'X'}
enum Branch {OPEN = '(', CLOSE = ')', ALT = '|'}
const DELTA = {N: [0, -1], E: [1, 0], S: [0, 1], W: [-1, 0]} // dir => [x, y]

type Cell = [number, number] // x, y
type Cells = Map<string, Cell> // hash => [x, y]
type Doors = Set<string> // Cell:Cell
const DOOR = ':'

const addDoor = (d: Doors, c1: Cell, c2: Cell) => {
  const s1 = c1.toString()
  const s2 = c2.toString()
  d.add(s1 + DOOR + s2) 
  d.add(s2 + DOOR + s1)
}
const hasDoor = (d: Doors, c1: Cell, c2: Cell, o: Cell) => {
  const s1 = [c1[0] - o[0], c1[1] - o[1]].toString()
  const s2 = [c2[0] - o[0], c2[1] - o[1]].toString()
  return d.has(s1 + DOOR + s2)
}

const addCell = (cells: Cells, c: Cell): Cells => cells.set(c.toString(), c)
const uniteCells = (c1: Cells, c2: Cells) => 
  [...c2.values()].reduce(addCell, c1)
const cellsFrom = (cells: Cell[]): Cells => cells.reduce(addCell, new Map())
const moveCell = (c: Cell, s: string, d: Doors): Cell => {
  const [dx, dy] = DELTA[s]
  const next: Cell = [c[0] + dx, c[1] + dy]
  if (d)
    addDoor(d, c, next)
  return next
}
const moveCells = (cells: Cells, s: string, d: Doors): Cells =>
  cellsFrom([...cells.values()].map(c => moveCell(c, s, d)))
const SIZE = 5

type Distances = Array<Array<number>>
const FAR = -1
const NEIGHBORS = [[0, 1], [0, -1], [1, 0], [-1, 0]] // Cells

class Maze {
  map: Array<Array<string>>
  ox: number
  oy: number
  doors: Doors

  constructor (size: number = SIZE) {
    this.map = Array.from({length: size}, () => new Array(size).fill(Tile.UNKNOWN))
    this.ox = this.oy = Math.floor(size / 2)

    this.map[this.oy][this.ox] = Tile.ORIGIN

    this.doors = new Set()
  }

  set(x: number, y: number, value: string) {
    let nx = x + this.ox
    let ny = y + this.oy

    if (nx < 0) {
      this.map.forEach(r => r.splice(0, 0, ...new Array(-nx).fill(Tile.UNKNOWN)))
      this.ox -= nx
      nx = 0
    } else if (nx >= this.map[0].length)
      this.map.forEach(r => r.push(...new Array(nx - r.length + 1).fill(Tile.UNKNOWN)))
      
    if (ny < 0) {
      const len = this.map[0].length
      this.map.splice(0, 0, ...Array.from({length: -ny}, () => new Array(len).fill(Tile.UNKNOWN)))
      this.oy -= ny
      ny = 0
    } else if (ny >= this.map.length) {
      const len = this.map[0].length
      this.map.push(...Array.from({length: ny - this.map.length + 1}, () => new Array(len).fill(Tile.UNKNOWN)))
    }

    this.map[ny][nx] = value
  }

  setCells(cells: Cells, value: string = Tile.ROOM) {
    for (const c of cells.values())
      this.set(c[0], c[1], value)
  }
  
  get(x: number, y: number): string {
    return this.map[y + this.oy][x + this.ox]
  }

  print() {
    this.map.forEach(r => print(r.join('')))
  }

  // on the map and accessible through doors
  neighbors(x: number, y: number): Cell[] {
    let res: Cell[] = NEIGHBORS.map(([a, b]) => [x + a, y + b])
    res = res.filter(([a, b]) => (a >= 0) && (a < this.map[0].length) && 
      (b >= 0) && (b < this.map.length))
    res = res.filter(v => hasDoor(this.doors, [x, y], v, [this.ox, this.oy]))

    return res
  }

  farthest(): [number, number] {
    let dist: Distances = Array.from({length: this.map.length}, () => 
      new Array<number>(this.map[0].length).fill(FAR))
    let next: Cell[] = [[this.ox, this.oy]]
    dist[this.oy][this.ox] = 0

    while (next.length > 0) {
      const [x, y]: Cell = next.shift()
      const d = dist[y][x] + 1
      const n = this.neighbors(x, y).filter(([a, b]) => dist[b][a] == FAR)

      n.forEach(([a, b]) => dist[b][a] = d)
      next.push(...n)
    }

    const farthest = dist.reduce((acc, cur) => Math.max(acc, ...cur), FAR)
    const far = dist.reduce((acc, r) => r.reduce((a, c) => (c >= 1000) ? a + 1: a, acc), 0)
    return [farthest, far]
  }
}

function parseBranch(path: string, maze: Maze = null, idx: number = 0, heads: Cells = new Map()): [number, Cells] {
  let res: Cells = new Map()
  let ch: string

  do {
    idx++

    let [i, h] = parsePath(path, maze, idx, heads)
    uniteCells(res, h)
    idx = i

    ch = path.charAt(idx)
  } while (ch != Branch.CLOSE)

  return [idx, res] // idx is index of CLOSE char ')'
}

function parsePath(path: string, maze: Maze = null, idx: number = 0, heads: Cells = new Map()): [number, Cells] {
  if (heads.size == 0)
    addCell(heads, [0,0])

  for (; idx < path.length; idx++) {
    let ch = path.charAt(idx)

    if ((ch == Branch.CLOSE) || (ch == Branch.ALT)) // end of parsing this path?
      break

    if (ch == Branch.OPEN) // branches?
      [idx, heads] = parseBranch(path, maze, idx, heads)
    else {
      heads = moveCells(heads, ch, maze ? maze.doors : null)
      if (maze)
        maze.setCells(heads)
    }
  }

  return [idx, heads]
}

let el = new Maze()
el.set(-2, -2, '2')
el.set(-4, -4, '4')
el.set(2, 2, '2')
el.set(4, 4, '4')
el.print()
print("origin:", el.get(0, 0))

print("------------------")

print("tuple", [1, 2], [3, 4].toString())
print("move", moveCells(cellsFrom([[0, 0], [1, 1], [2, 2]]), 'W', null))

print(parsePath("NNN")[1])
print(parsePath("N(E|W)N")[1])

let m1 = new Maze(3)
print(parsePath("ENWWW(NEEE|SSE(EE|N))", m1)[1].keys())
//m1.print()
//print(m1.doors)
print("farthest", m1.farthest())

let m2 = new Maze()
print(parsePath("ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN", m2)[1])
//m2.print()
print("farthest", m2.farthest())

let m3 = new Maze()
print(parsePath("WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))", m3)[1].size)
//m3.print()
print("farthest", m3.farthest())

print("------------------")

let maze = new Maze()
print("heads", parsePath(fs.readFileSync('day20.txt','utf8'), maze)[1].size)
//maze.print()
print("Farthest", maze.farthest())
