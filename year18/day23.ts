import fs = require('fs')

const print = console.log.bind(console)

type Bot = [number, number, number, number] // x, y, z, radius
type Bots = Array<Bot>
type Box = [number, number, number, number, number, number] // x1, x2, y1, y2, z1, z2
type Cube = [number, number, number, number] // x, y, z, size

function readBots(): Bots {
  return fs.readFileSync('day23.txt','utf8').split(/\r?\n/).map(l => {
    const [pos, rad] = l.split('>, r=')
    const [x, y, z] = pos.replace('pos=<', '').split(',')

    return <Bot>[+x, +y, +z, +rad]
  })
}

function distance(b1: Bot, b2: Bot): number {
  return Math.abs(b1[0] - b2[0]) + Math.abs(b1[1] - b2[1]) + 
    Math.abs(b1[2] - b2[2])
}

function strongestBotRange(bots: Bots) {
  const sb: number = bots.reduce((acc, cur, idx, arr) => 
    cur[3] > arr[acc][3] ? idx : acc , 0)

  const strongest = bots[sb]
  
  const inRange = bots.reduce((acc, cur) => 
    distance(strongest, cur) > strongest[3] ? acc : acc + 1, 0)

  return inRange
}

// a <= b; includes a and b
const distSegment = (x: number, a: number, b: number): number =>
  (x < a) ? a - x : ((x > b) ? x - b : 0)

// x1 <= x2; includes x1 and x2
const distBox = (x: number, y: number, z: number, [x1, x2, y1, y2, z1, z2]: Box): number =>
  distSegment(x, x1, x2) + distSegment(y, y1, y2) + distSegment(z, z1, z2)

const strict = ([x1, x2, y1, y2, z1, z2]: Box): Box =>
  [x1, x2 - 1, y1, y2 - 1, z1, z2 - 1]

const inRange = ([x, y, z, r]: Bot, b: Box): boolean =>
  distBox(x, y, z, strict(b)) <= r

const botsInRange = (b: Bots, box: Box) =>
  b.reduce((acc, cur) => acc + +inRange(cur, box), 0)

function perimeter(bots: Bots): Cube {
  const mx = Math.max(...bots.map(([x, y, z, r]) => 
    Math.max(...[x - r, x + r, y - r, y + r, z - r, z + r].map(Math.abs))))
  let i = 1
  while (i <= mx)
    i *= 2

  return [-i, -i, -i, 2 * i]
}

const cube2Box = ([x, y, z, s]: Cube): Box => [x, x + s, y, y + s, z, z + s]

function splitCube([x, y, z, s]: Cube): Cube[] {
  s /= 2

  return [[x, y, z, s], [x + s, y, z, s], [x, y + s, z, s], [x, y, z + s, s], 
    [x + s, y + s, z, s], [x, y + s, z + s, s], [x + s, y, z + s, s],
    [x + s, y + s, z + s, s]]
}

class SearchSpace {
  cube: Cube
  bots: number
  dist: number

  constructor (bots: Bots, c: Cube) {
    const box = cube2Box(c)

    this.cube = c
    this.bots = botsInRange(bots, box)
    this.dist = distBox(0, 0, 0, box)
  }
}

const compSpace = (s1: SearchSpace, s2: SearchSpace) =>
  (s1.bots != s2.bots) ? s2.bots - s1.bots : 
  (s1.dist != s2.dist) ? s2.dist - s1.dist : s2.cube[3] - s1.cube[3]

function findSpace(bots: Bots): number {
  let spaces: Array<SearchSpace> = [new SearchSpace(bots, perimeter(bots))]

  while (spaces.length > 0) {
    const sp: SearchSpace = spaces.shift()

    if (sp.cube[3] == 1) 
      return sp.dist

    spaces = spaces.concat(Array.from(splitCube(sp.cube),
      a => new SearchSpace(bots, a)))
    spaces = spaces.sort(compSpace)
  }

  return -1
}

const bots = readBots()
//print(bots)
print("in range of strongest:", strongestBotRange(bots))

print("------------------")

print("d/l", distSegment(1, 2, 3), distSegment(2, 1, 3), distSegment(-1, -4, -3))
print("d/c", distBox(1, 1, 1, [1, 3, 1, 3, 1, 3]), distBox(1, 1, 1, [2, 3, 2, 3, 2, 3]))
print("i/r", inRange([1, 1, 1, 3], [2, 3, 2, 3, 2, 3]), inRange([4, 4, 4, 3], [2, 3, 2, 3, 2, 3]))
//print("p", perimeter([[251036687, 9250140, 37011551, 55675723]]))
print("perimeter", perimeter(bots), botsInRange(bots, cube2Box(perimeter(bots))), bots.length)
print("split", splitCube([1, 1, 1, 2]))

print("------------------")

print("Space", findSpace(bots))
