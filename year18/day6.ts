import fs = require('fs')

const print = console.log.bind(console)

type Coordinate = [number, number] // x, y
type Origins = Array<Coordinate>
type Grid = Array<Array<number>> // [y [x]]

function manhattan(a: Coordinate, b: Coordinate): number {
  return Math.abs(a[0] - b[0]) + Math.abs(a[1] - b[1])
}

type ComputeFunction = (Coordinate, Origins) => number

function closest(a: Coordinate, o: Origins): number {
  const dist = o.map((p)=>manhattan(a, p))
  const minDist = Math.min(...dist)

  let minOrigin = dist.indexOf(minDist)
  if (dist.indexOf(minDist, minOrigin + 1) >= 0)
    minOrigin = -1

  //print (dist, minDist, minOrigin)
  return minOrigin
}

function total(a:Coordinate, o:Origins): number {
  const dist = o.map((p)=>manhattan(a, p))
  return (dist.reduce((a, c) => a + c, 0))
}

function computeGrid(o: Origins, width: number = 10, height: number = 10, compute: ComputeFunction = closest): Grid {
  let grid = new Array(height)

  for (let y = 0; y < height; y++) {
    grid[y] = new Array(width)
    for (let x = 0; x < width; x++)
      grid[y][x] = compute([x, y], o)
  }

  //print(grid)
  return grid
}

function area(origin: number, grid: Grid): number {
  return grid.reduce((acc, cur) => acc + cur.reduce((a, c) => a + +(c == origin), 0), 0)
}

function areas(o: Origins, g: Grid) {
  const a = o.map((v, i) => area(i, g))
  //print("areas" a)

  // infinite areas
  const inf: Set<number> = new Set()
  g[0].forEach((v) => inf.add(v))
  g[g.length - 1].forEach((v) => inf.add(v))
  g.forEach((v) => {
    inf.add(v[0])
    inf.add(v[v.length - 1])
  })

  //print ("inf", inf)

  // non-infinite areas
  const nia = a.filter((v, i) => !inf.has(i))

  //print ("non-infinite", nia)

  return Math.max(...nia)
}

function areaClose(g: Grid, dist: number): number {
  //print(g)
  return g.reduce((acc, cur) => acc + cur.filter((v) => v < dist).length, 0)
}

function readOrigins(): Origins {
  return fs.readFileSync('day6.txt','utf8').split(/\r?\n/).map((s) => {
    let [x, y] = s.split(', ')
    return <Coordinate>[+x, +y]
  })
}

print("dist", manhattan([1, 1], [2, 3]))

const o1: Origins = [[1, 1], [1, 6], [8, 3], [3, 4], [5, 5], [8, 9]]

print("closest", closest([5, 0], o1))
print("total", total([4, 3], o1))

const grid1 = computeGrid(o1)
print ("area", area(3, grid1), area(4, grid1))
print("max area", areas(o1, grid1))

const tg = computeGrid(o1, 10, 10, total)
print("close area", areaClose(tg, 32))

print("------------------------------")

const o2: Origins = readOrigins()
const grid2 = computeGrid(o2, 1000, 1000)
print("max area", areas(o2, grid2))
const tg2 = computeGrid(o2, 1000, 1000, total)
print("close area", areaClose(tg2, 10000))

