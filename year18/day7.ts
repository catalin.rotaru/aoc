import fs = require('fs')

const print = console.log.bind(console)

class Node {
  order: number
  exec: number
  deps: Set<string>
  blocks: Set<string>

  constructor() {
    this.order = 0
    this.exec = 0
    this.deps = new Set<string>()
    this.blocks = new Set<string>()
  }
}

type Nodes = Map<string, Node>

function readGraph(): Nodes {
  let nodes = new Map()

  fs.readFileSync('day7.txt','utf8').split(/\r?\n/).map((s) => {
    const [ns, ms] = s.split(' must be finished before step ')
    const node1 = ns.slice(-1)
    let n1 = nodes.has(node1) ? nodes.get(node1) : new Node()
    nodes.set(node1, n1)
    const node2 = ms[0]
    let n2 = nodes.has(node2) ? nodes.get(node2) : new Node()
    nodes.set(node2, n2)

    n2.deps.add(node1)
    n1.blocks.add(node2)
  })

  return nodes
}

function getHeads(graph: Nodes): string[] {
  return [...graph.keys()].filter((v) => graph.get(v).deps.size == 0)
}

function isReady(graph: Nodes, node: string): boolean {
  const n: Node = graph.get(node)
  const deps: string[] = [...n.deps]
  //print(deps)
  
  if (deps.length == 0)
    return true
  else
    return deps.filter((el) => graph.get(el).order == 0).length == 0
}

function computeOrder(graph: Nodes, heads: string[], order = 1) {
  let next: Set<string> = new Set(heads)

  while (next.size) {
    const n = [...next].filter((v) => isReady(graph, v)).sort()[0]
    const node = graph.get(n)
    node.order = order
    next.delete(n)

    for (let n of node.blocks)
      next.add(n)

    order++
  }
}

function execute(graph: Nodes, heads: string[], workers: number = 2, delay: number = 0) {
  let next: Set<string> = new Set(heads)
  let work: Set<string> = new Set()

  for (let sec = 0; ; sec++) {
    //print("s/w/n:", sec, work, next)

    // remove finished work
    work.forEach((v) => {
      const node = graph.get(v)
      if (node.exec == 0) {
        node.order = 1
        for (let n of node.blocks)
          next.add(n)
          
        work.delete(v)
      }
    })
    //print (work)

    // assign work ready to execute to free workers
    while (work.size < workers) {
      const n = [...next].filter((v) => isReady(graph, v)).sort()[0]
      if (!n)
        break
      work.add(n)
      const node = graph.get(n)
      node.exec = delay + n.charCodeAt(0) - 'A'.charCodeAt(0) + 1
      next.delete(n)
    }
    //print("s/w/n:", sec, work, next)

    // finish if no work left
    if (work.size == 0)
      return sec

    // execute work
    work.forEach((v) => graph.get(v).exec--)
  }
}

function getOrder(graph: Nodes): string {
  let nodes = [...graph].sort((a, b) => a[1].order - b[1].order)
  return nodes.reduce((a, c) => a + c[0], '')
}

let g: Nodes = readGraph()
print("C is ready", isReady(g, 'C'))
print("E is ready", isReady(g, 'E'))
const h = getHeads(g)
print("heads", h)
computeOrder(g, h)
//print(g)
print("order", getOrder(g))

print("------------------------------")

let g1: Nodes = readGraph()
const h1 = getHeads(g1)
print("exec", execute(g1, h1, 5, 60))
