import fs = require('fs')

const print = console.log.bind(console)

type Registers = [number, number, number, number] // A = 0, B, C, D

type Sample = [Registers, Registers, number, number, number, number]
type Samples = Array<Sample>

type OpcodeFunction = (a: number, b: number, c: number) => number

type Operation = [number, number, number, number] // Opcode, A, B, C
type Code = Array<Operation>

const exec = {
  'addr': addr,
  'addi': addi,
  'mulr': mulr,
  'muli': muli,
  'banr': banr,
  'bani': bani,
  'borr': borr,
  'bori': bori,
  'setr': setr,
  'seti': seti,
  'gtir': gtir,
  'gtri': gtri,
  'gtrr': gtrr,
  'eqir': eqir,
  'eqri': eqri,
  'eqrr': eqrr
}

function addr(a: number, b: number, reg: Registers): number {
  return reg[a] + reg[b]
}

function addi(a: number, b: number, reg: Registers): number {
  return reg[a] + b
}

function mulr(a: number, b: number, reg: Registers): number {
  return reg[a] * reg[b]
}

function muli(a: number, b: number, reg: Registers): number {
  return reg[a] * b
}

function banr(a: number, b: number, reg: Registers): number {
  return reg[a] & reg[b]
}

function bani(a: number, b: number, reg: Registers): number {
  return reg[a] & b
}

function borr(a: number, b: number, reg: Registers): number {
  return reg[a] | reg[b]
}

function bori(a: number, b: number, reg: Registers): number {
  return reg[a] | b
}

function setr(a: number, b: number, reg: Registers): number {
  return reg[a]
}

function seti(a: number, b: number, reg: Registers): number {
  return a
}

function gtir(a: number, b: number, reg: Registers): number {
  return +(a > reg[b])
}

function gtri(a: number, b: number, reg: Registers): number {
  return +(reg[a] > b)
}

function gtrr(a: number, b: number, reg: Registers): number {
  return +(reg[a] > reg[b])
}

function eqir(a: number, b: number, reg: Registers): number {
  return +(a == reg[b])
}

function eqri(a: number, b: number, reg: Registers): number {
  return +(reg[a] == b)
}

function eqrr(a: number, b: number, reg: Registers): number {
  return +(reg[a] == reg[b])
}

function execute(op: string, a: number, b: number, c: number, reg: Registers): Registers {
  let rn: Registers = [reg[0], reg[1], reg[2], reg[3]]
  rn[c] = exec[op](a, b, reg)

  //print ("execute", reg, op, a, b, c, "=>", rn)

  return rn
}

function same(r1: Registers, r2: Registers): boolean {
  return r1.every((r, i) => r == r2[i])
}

// return opcode(s) who could've done before=>after registers after execution with given params
function runSample(before: Registers, after: Registers, a: number, b: number, c: number): Array<string> {
  let res = new Array()

  Object.keys(exec).forEach(op => { 
    if (same(execute(op, a, b, c, before), after)) 
      res.push(op)
  })

  return res
}

function readSamples(): [Samples, Code] {
  let samples: Samples = new Array()
  let lines = fs.readFileSync('day16.txt','utf8').split(/\r?\n/)

  let i = 0
  for (; i < lines.length; i += 4) {
    let [lb, lop, la] = lines.slice(i, i + 3)
    if (!lb.length)
      break

    let before: Registers = <Registers> lb.replace('Before: [', '').replace(']', '').split(', ').map(s => +s)
    let [opcode, a, b, c] = lop.split(' ').map(s => +s)
    let after: Registers = <Registers> la.replace('After:  [', '').replace(']', '').split(', ').map(s => +s)

    samples.push([before, after, opcode, a, b, c])
    //print(before, opcode, a, b, c, after)
  }

  let code: Code = new Array()
  for (; i < lines.length; i++) {
    let line = lines[i]
    if (line.length) 
      code.push(<Operation>[...line.split(" ").map(a => +a)])
  }
  //print ("code", code[0], code[code.length - 1], code.length)

  return [samples, code]
}

function equalSets(as: Set<string>, bs: Set<string>): boolean {
  if (as.size !== bs.size) 
    return false;
  for (let a of as) 
    if (!bs.has(a))
      return false;
  return true;
}

function intersectSets(as: Set<string>, bs: Set<string>): Set<string> {
  return new Set([...as].filter(a => bs.has(a)));
}

function getOpcodes(samples: Samples): Array<string> {
  let opcodes: Array<Set<string>> = new Array(16)

  let count = 0

  samples.forEach(s => {
    let op = s[2]
    let runResult = runSample(s[0], s[1], s[3], s[4] ,s[5])
    if (runResult.length >= 3)
      count++
    //print(op,  "=>", runResult.join(", "))

    let current = opcodes[op]
    let nop = new Set(runResult)
    if (!current) {
      opcodes[op] = nop
      //print(op,  "*>", runResult.join(", "))
    } else {
      if (!equalSets(current, nop)) {
        //print ("SETS", current, nop, equalSets(current, nop), intersectSets(current, nop))

        opcodes[op] = intersectSets(current, nop)
        //print(op, [...current.values()].join(", "), "=>", [...opcodes[op].values()].join(", "))
      }
    }
  })

  print("results 3 or more", count)

  reduceOpcodes(opcodes)
  //print(opcodes)

  return (opcodes.map(o => [...o][0]))
}

function reduceOpcodes(opcodes: Array<Set<string>>) {
  let reduced: Set<string> = new Set()

  while (reduced.size < opcodes.length) {
    let current: string = [...opcodes.find(op => (op.size == 1) && !reduced.has([...op][0]))][0]

    opcodes.forEach(op => {
      if (op.size > 1)
        op.delete(current)
    })
    reduced.add(current)
  }
}

function runCode(code: Code, op: Array<string>) {
  let reg: Registers = [0, 0, 0, 0]

  code.forEach(c => reg = execute(op[c[0]], c[1], c[2], c[3], reg))

  print("RESULT:", reg)
}

const r: Registers = [10, 20, 30, 40]
print(exec['addr'](1, 3, r))
print(exec['addi'](1, 10, r))
print(exec['mulr'](1, 2, r))
print(exec['muli'](2, 3, r))
print(exec['banr'](0, 1, r))
print(exec['bani'](0, 11, r))
print(exec['borr'](0, 1, r))
print(exec['bori'](0, 11, r))
print(exec['gtir'](21, 1, r))
print(exec['gtri'](2, 30, r))
print(exec['gtrr'](2, 1, r))
print(exec['eqir'](20, 1, r))
print(exec['eqri'](2, 30, r))
print(exec['eqrr'](0, 1, r))

print (same([1, 1, 2, 2], [1, 1, 2, 2]))
print(execute('mulr', 2, 1, 2, [3, 2, 1, 1]))
print(runSample([3, 2, 1, 1], [3, 2, 2, 1], 2, 1, 2))

print(execute('gtri', 3, 3, 0, [2, 3, 2, 2]))
print(execute('gtrr', 3, 3, 0, [2, 3, 2, 2]))
print(execute('eqir', 3, 3, 0, [2, 3, 2, 2]))
print(execute('eqri', 3, 3, 0, [2, 3, 2, 2]))
print(execute('gtri', 3, 3, 2, [3, 0, 0, 2]))
print(execute('gtrr', 3, 3, 2, [3, 0, 0, 2]))
print(execute('eqir', 3, 3, 2, [3, 0, 0, 2]))
print(execute('eqri', 3, 3, 2, [3, 0, 0, 2]))

//print("eq sets", equalSets(new Set(['gtri', 'eqir']), new Set(['gtri', 'eqir'])))

print("------------------------")

let [samples, code] = readSamples()
print("samples", samples.length)
let opcodes = getOpcodes(samples)
print("opcodes", opcodes.join(', '))
runCode(code, opcodes)
