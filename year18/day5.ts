import fs = require('fs')

const print = console.log.bind(console)

const REACT = 'a'.charCodeAt(0) - 'A'.charCodeAt(0)
const EMPTY = ' '

function getNextIdx(str: string[], idx: number): number {
  for (let i = idx + 1; i<str.length; i++) {
    if (str[i] != EMPTY)
      return i
  }

  return -1
}

function react(str1: string, str2: string): boolean {
  return Math.abs(str1.charCodeAt(0) - str2.charCodeAt(0)) == REACT
}

function same(str1: string, str2: string): boolean {
  return ((str1 == str2) || (react(str1, str2)))
}

function alchemy(str: string[]): number {

  let changed 
  do {
    changed = false
    
    for (let i = 0; i<str.length; i++) {
      const ch = str[i]
      if (ch == EMPTY)
        continue
      
      const j = getNextIdx(str, i)
      
      // if they react
      if ((j >= 0) && react(str[i], str[j])) {
        str[i] = EMPTY
        str[j] = EMPTY
        changed = true
      }
    }
  } while (changed)
  
  return str.filter((e)=> e != EMPTY).join('').length
}

function testRemoval(str: string[]): number {
  let units: Array<string> = 'abcdefghijklmnopqrstuvwxyz'.split('')

  //print (units)
  //print (units.map((u)=>str.filter((e) => !same(e, u))))

  return Math.min(...units.map((u)=>alchemy(str.filter((e) => !same(e, u)))))
}

let input = fs.readFileSync('day5.txt','utf8').split('')
print("alchemy", alchemy("dabAcCaCBAcCcaDA".split('')))
print("alchemy", alchemy(input))

print("removal", testRemoval("dabAcCaCBAcCcaDA".split('')))
print("removal", testRemoval(input))