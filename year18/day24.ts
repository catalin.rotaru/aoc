import fs = require('fs')

const print = console.log.bind(console)

const BOOST = 0 // 35 for the second part - hangs because ties between 16 and 34

/**
 * Immune System vs Infection
 * Army = Group[]
 * Group = Unit[]
 * Unit = Hit Points, Attack Damage, Attack Type, Initiative, Weaknesses & Immunities
 */

type Army = Array<Group>

class Group {
  infection: boolean
  units: number
  hp: number
  attack_damage: number
  attack_type: string
  initiative: number

  weakness: Set<string>
  immunity: Set<string>

  target: Group
  targeted: boolean

  constructor(str: string, infection: boolean) {
    this.infection = infection
    const [s1, s2] = str.split(' with an attack that does ')
    const [s3, init] = s2.split(' damage at initiative ')
    const [ad, at] = s3.split(' ')

    this.initiative = +init
    this.attack_damage = +ad + (infection ? 0 : BOOST)
    this.attack_type = at

    const [s5, s6] = s1.split(' (')
    const [u, hps] = s5.split(' units each with ')

    this.units = +u
    this.hp = +hps.replace(' hit points', '')

    this.weakness = new Set()
    this.immunity = new Set()

    if (s6) 
      s6.replace(')', '').split('; ').forEach(l => {
        if (l.startsWith('i')) {
          let s7 = l.replace("immune to ", '')
          this.immunity = new Set(s7.split(', '))
        } else {
          let s7 = l.replace("weak to ", '')
          this.weakness = new Set(s7.split(', '))
        }
      })
  }

  alive(): boolean {
    return this.units > 0
  }

  effectivePower(): number {
    return this.units * this.attack_damage
  }

  immune(attack_type: string) {
    return(this.immunity.has(attack_type))
  }

  weak(attack_type: string) {
    return(this.weakness.has(attack_type))
  }

  // damage received in a given attack
  damage(power: number, attack_type: string): number {
    if (this.immune(attack_type))
      return 0
    if (this.weak(attack_type))
      return power * 2

    return power
  }

  // damage received from given attacker
  damageFrom(attacker: Group) {
    return this.damage(attacker.effectivePower(), attacker.attack_type)
  }

  // lose units to damage
  lose(damage: number) {
    this.units -= Math.floor(damage/this.hp)
  }

  // receive attack from given attacker
  receiveAttack(attacker: Group) {
    this.lose(this.damageFrom(attacker))
  }

  // if alive and has a target selected, attack it
  attackTarget() {
    if (this.alive() && this.target)
      this.target.receiveAttack(this)
  }

  // select enemy army
  enemy(immune: Army, infection: Army) {
    return this.infection ? immune : infection
  }

  // select a target for attack by this group
  // returns undefined if it cannot deal any enemy damage
  selectTarget(targets: Army): Group {
    if (!targets.length)
      return undefined

    const t: Group = targets.reduce((a, c) => 
      (c.damageFrom(this) - a.damageFrom(this) || 
      c.effectivePower() - a.effectivePower() || 
      c.initiative - a.initiative) > 0 ? c : a)

    if (!t.damageFrom(this))
      return undefined

    this.target = t
    t.targeted = true
    return t
  }
}

// get only living units in this phase
function targetSelection(immune: Army, infection: Army) {
  // all groups, sorted by effective power and initiative
  let all: Army = immune.concat(infection).sort((a, b) => 
    b.effectivePower() - a.effectivePower() ||
    b.initiative - a.initiative)

  // reset all targets
  all.forEach(a => {
    a.target = undefined
    a.targeted = false
  })

  // each unit => select an un-targeted enemy
  all.forEach(a => 
    a.selectTarget(a.enemy(immune, infection).filter(x => !x.targeted)))
}

// commence attack
function attackPhase(immune: Army, infection: Army) {
  let all: Army = immune.concat(infection).sort((a, b) =>
    b.initiative - a.initiative)

  // each unit, attack if still alive
  all.forEach(a => a.attackTarget())
}

// fight the battle
function fightBattle(immune: Army, infection: Army) {
  for (;;) {
    let imm = immune.filter(a => a.alive())
    let inf = infection.filter(a => a.alive())

    if (!imm.length || !inf.length)
      break

    targetSelection(imm, inf)
    attackPhase(imm, inf)
  }
}

function countSurvivors(army: Army) {
  return army.reduce((a, c) => a + (c.alive() ? c.units : 0), 0)
}

function readArmies() {
  let immune: Army = []
  let infection: Army = []
  let inf: boolean = false
  fs.readFileSync('day24.txt','utf8').split(/\r?\n/).forEach(l => {
    switch (l) {
      case 'Immune System:':
        inf = false
        break
      case 'Infection:':
        inf = true
        break
      default:
        if (l)
          (inf?infection:immune).push(new Group(l, inf))
    }
  })

  return [immune, infection]
}
 
/*
print(new Group("17 units each with 5390 hit points (immune to fire; weak to " + 
"radiation, bludgeoning) with an attack that does 4507 fire damage at " + 
"initiative 2", false))

print(new Group("1383 units each with 3687 hit points with an attack that " + 
"does 26 radiation damage at initiative 15", false)) 
*/

let [imm, inf] = readArmies()
//print (imm, inf)
fightBattle(imm, inf)
print ("Survivors:", countSurvivors(imm.concat(inf)), "immunes", countSurvivors(imm))