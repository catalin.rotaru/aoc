import fs = require('fs')

const print = console.log.bind(console)
type Rules = Map<string, string>

const BUFF_SIZE = 1000

function readRules(): [string, Rules] {
  const lines = fs.readFileSync('day12.txt','utf8').split(/\r?\n/)
  //print(lines)

  const buff = new Array(BUFF_SIZE).fill('.').join('')
  const init = buff + lines[0].replace('initial state: ', '') + buff
  //print(init)

  const rl: Array<Array<string>> = lines.slice(2, lines.length).map(a => a.split(' => '))
  const r: Array<[string, string]> = rl.map(v => <[string, string]>[v[0], v[1]])
  return [init, new Map<string, string>(r)]
}

const RULE_SIZE = 5

function evolve(pots: string, rules: Rules): string {
  let res: Array<string> = new Array(pots.length).fill('.');

  for (let i = 0; i < pots.length - RULE_SIZE; i++) {
    const pattern = pots.substring(i, i + RULE_SIZE)
    const ch = rules.get(pattern)
    if (ch)
      res[i +2] = ch
  }

  return res.join('')
}

function evolution(pots: string, rules: Rules, steps: number = 20): string {
  for (let i = 0; i < steps; i++) {
    //print(pots)
    pots = evolve(pots, rules)
  }

  return pots
}

function weight(pots: string): number {
  return pots.split('').reduce((acc, cur, idx) => acc + ((cur == '#') ? idx - BUFF_SIZE : 0), 0)
}

function trimEmpty(pots: string): string {
  return pots.replace(new RegExp("^[.]+"), "").replace(new RegExp("[.]+$"), "")
}

function findRepetition(pots: string, rules: Rules): [number, number, [number, number]] {
  // stamp => [step, weight, index]
  let conf: Map<string, [number, number]> = new Map()

  for (let step = 0; ; step++) {
    const stamp = trimEmpty(pots)
    const w = weight(pots)

    if (conf.has(stamp))
      return [step, w, conf.get(stamp)]
    
    conf.set(stamp, [step, w])
    pots = evolve (pots, rules)
  }
}

const [init, rules] = readRules()
//print(rules)

//print(init)
const final = evolution(init, rules)
//print(final)
print("weight", weight(final))

//print (trimEmpty("#ABBA#"))

print("weight", weight(evolution(init, rules, 113)))
print("weight", weight(evolution(init, rules, 114)))
print("weight", weight(evolution(init, rules, 115)))

let [step, w, prev] = findRepetition(init, rules)
let res: number = prev[1] + (50000000000 - prev[0])*(w - prev[1])
print("after 50 billion years", res)