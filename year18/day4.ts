import fs = require('fs')

const print = console.log.bind(console)

const ASLEEP = -1
const AWAKE = -2
const MIH = 60 // mins in hour
const DIY = 12*31 // days in year 

type HourSleepState = Array<number> // 0 awake, 1+ asleep, [60 mins]
type YearSleepState = Array<HourSleepState> // hour for each day in year [12*31]
type GuardSleepState = Map<number, YearSleepState> // guard ID -> year's sleep state

function dayOfYear(month: number, day: number): number {
  return (month - 1) * 31 + day - 1
}

type Event = [number, number, number] // day, min, event

function parseEvent(log: string): Event {
  let [stamp, ev] = log.split('] ')
  let [date, time] = stamp.split(' ')
  let fd = date.split('-')
  let day = dayOfYear(+fd[1], +fd[2])
  let [h, m] = time.split(':')
  let min = +m
  if (+h == 23) {
    day++
    min -= MIH
  }
  //print(day, min)

  let event  = (ev == 'falls asleep') ? ASLEEP : 
    (ev == 'wakes up') ? AWAKE : +ev.match(/[0-9]+/g)
  return [day, min, event]
}

function readLogs(): GuardSleepState {
  let logs = fs.readFileSync('day4.txt','utf8').split(/\r?\n/)
  let events =  logs.map((log)=>parseEvent(log)).sort(([d1, m1], [d2, m2]) => 
    (d1 == d2) ? (m1 - m2) : (d1 - d2))
  //print(events)

  let guards: GuardSleepState = new Map()
  let year: YearSleepState
  let asleep: number
  events.forEach(([day, min, event]) => {
    switch (event) {
      case ASLEEP:
        asleep = min
        break
      case AWAKE:
        //print ("awake", year)
        for (let i = asleep; i < min; i++)
          year[day][i]++
        break
      default:
        if (guards.has(event)) {
          year = guards.get(event)
        } else {
          year = new Array(DIY)
          for (let i = 0; i < DIY; i++)
            year[i] = new Array<number>(MIH).fill(0)
          
          guards.set(event, year)
        }
    }
  })

  //print("guards", guards)
  //print(guards.get(10)[dayOfYear(11, 1)])

  return guards
}

function findMostSleeper(guards: GuardSleepState): number {
  // find guard who slept the most
  let sleeper = 0
  let sleep = 0
  guards.forEach((v, k) => {
    let cs = v.reduce((acc, cur)=> acc + cur.reduce((a, c) => a + c, 0), 0)
    if (cs > sleep) {
      sleep = cs
      sleeper = k
    }
  })

  // for that guard, find minute when most asleep
  let year = guards.get(sleeper)
  let hour: HourSleepState = year[0].map((e, i) =>
    year.reduce((acc, cur) => acc + cur[i], 0))
  
  let maxMin = hour.indexOf(Math.max(...hour))

  return sleeper*maxMin
}

function findOftenSleeper(guards: GuardSleepState): number {
  // compute total sleep for each guard per minute
  let total: Map<number, HourSleepState> = new Map()

  guards.forEach((y, g) => {
    let hour: HourSleepState = y[0].map((e, i) => 
      y.reduce((acc, cur) => acc + cur[i], 0))

    //print(hour)
    total.set(g, hour)
  })

  // find guard with minute most asleep of all guards
  let maxFreq = 0
  let maxGuard = 0
  let maxMin = 0
  total.forEach((h, g) => {
    let max = Math.max(...h)
    if (max > maxFreq) {
      maxFreq = max
      maxGuard = g
      maxMin = h.indexOf(max)
    }
  })

  return maxGuard*maxMin
}

print(parseEvent("[1518-11-01 23:55] Guard #10 begins shift"))
print(parseEvent("[1518-11-01 00:05] falls asleep"))
print(parseEvent("[1518-11-01 00:25] wakes up"))

let grid: GuardSleepState = readLogs()
//print("Guard", 571, getAsleep(571))

print("Most Sleeper", findMostSleeper(grid))
print("Often Sleeper", findOftenSleeper(grid))