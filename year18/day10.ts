import fs = require('fs')

const print = console.log.bind(console)

type Position = [number, number] // x, y
type Velocity = [number, number] // speed on x, y
type Lights = [Array<Position>, Array<Velocity>]

function readLights(): Lights {
  let pos: Array<Position> = new Array()
  let vel: Array<Velocity> = new Array()

  fs.readFileSync('day10.txt','utf8').split(/\r?\n/).map((s) => {
    let [s1, s2] = s.split('> velocity=<')
    s1 = s1.replace('position=<', '')
    let [p1, p2] = s1.split(',')
    let p:Position = [+p1.trim(), +p2.trim()]

    s2 = s2.replace('>', '')
    let [v1, v2] = s2.split(',')
    let v:Velocity = [+v1.trim(), +v2.trim()]

    //print (p, v)
    pos.push(p)
    vel.push(v)
  })

  return [pos, vel]
}


function bounds(pos: Array<Position>): [Position, Position] {
  let b = pos.reduce((a, c) => 
    [Math.min(a[0], c[0]), Math.max(a[1], c[0]), Math.min(a[2], c[1]), Math.max(a[3], c[1])], 
    [Infinity, -Infinity, Infinity, -Infinity])
  //print(b)

  return [[Math.min(b[0], b[1]), Math.max(b[0], b[1])], [Math.min(b[2], b[3]), Math.max(b[2], b[3])]]
}

function area(pos: Array<Position>): number {
  let [x, y] = bounds(pos)

  return (x[1] - x[0] + 1) * (y[1] - y[0] + 1)
}

function move(pos: Array<Position>, vel: Array<Velocity>, steps: number = 1): Array<Position> {
  return pos.map((p, i) => <Position>[p[0] + steps*vel[i][0], p[1] + steps*vel[i][1]])
}

function findMinArea(pos: Array<Position>, vel: Array<Velocity>): number {
  let oldArea = area(pos)

  for (let t = 0; ; t++) {
    pos = move(pos, vel)
    let newArea = area(pos)
    //print("area t/o/n", t, oldArea, newArea)
    if (newArea > oldArea)
      return t
    
    oldArea = newArea
  }
}

function printLights(pos: Array<Position>) {
  const [bx, by] = bounds(pos)
  const dx = bx[1] - bx[0] + 1
  const dy = by[1] - by[0] + 1

  let grid = new Array<Array<string>>()
  for (let i = 0; i < dy; i++)
    grid.push(new Array<string>(dx).fill('.'))

  pos.forEach((p) => grid[p[1] - by[0]][p[0] - bx[0]] = '#')

  grid.forEach((row) => print(row.join('')))
}

let [pos, vel] = readLights()
//print ("pos", pos)
//print ("vel", vel)
//print ("area", bounds(pos), area(pos))
//print (pos, move(pos, vel))

//printLights(pos)

const time = findMinArea(pos, vel)
print("min area at", time)
printLights(move(pos, vel, time))