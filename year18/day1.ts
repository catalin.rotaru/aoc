import fs = require('fs')

const print = console.log.bind(console)

function readFrequencies(): number[] {
  return fs.readFileSync('day1.txt','utf8').split(/\r?\n/).map((a=>+a))
}

function accumulateFrequencies(fq: number[]): number {
  return fq.reduce((acc, curr) => acc + curr)
}

function findDuplicate(fq: number[]) {
  let past: Set<Number> = new Set([])
  let acc: number = 0

  while (true) {
    for (const curr of fq) {
      past.add(acc)

      acc += curr
      
      if (past.has(acc)) 
        return acc
    }
  }
}

let puzzle: number[] = readFrequencies()

print("Accumulated:",accumulateFrequencies(puzzle))

print("Duplicate frequency:", findDuplicate([+1, -1]))
print("Duplicate frequency:", findDuplicate([+3, +3, +4, -2, -4]))
print("Duplicate frequency:", findDuplicate([-6, +3, +8, +5, -6]))
print("Duplicate frequency:", findDuplicate([+7, +7, -2, -7, -4]))
print("Duplicate frequency:", findDuplicate(puzzle))