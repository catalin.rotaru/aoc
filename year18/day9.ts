import fs = require('fs')

const print = console.log.bind(console)

class Node {
  value: number
  next: Node
  prev: Node

  constructor(val: number, next: Node = null, prev: Node = null) {
    this.value = val
    this.next = next || this
    this.prev = prev || this
  }

  insert(value: number, steps: number = 2): Node {
    if (this.next == this) {
      let nn = new Node(value, this, this)
      this.next = nn
      this.prev = nn

      return nn
    } else {
      let current: Node = this
      for (let i = 0; i < steps; i++)
        current = current.next

      //print ("ins ", current.value, current.next.value, current.next.next.value)
      const nn = new Node(value, current, current.prev)

      current.prev.next = nn
      current.prev = nn
      
      return nn
    }
  }

  remove(steps: number = 7): [Node, number] {
    let current: Node = this
    for (let i = 0; i < steps; i++)
      current = current.prev

    current.prev.next = current.next
    current.next.prev = current.prev

    return [current.next, current.value]
  }

  toString(): string {
    let res:string = ""

    let n: Node = this
    do {
      //print("s", n.value, n.next.value, n.next.next.value)
      res = res + ((res.length > 0) ? ',' : '') + n.value
      n = n.next
    } while (n != this)
    
    return res
  }
}

function game(players: number, marbles: number) {
  let p: Array<number> = new Array(players).fill(0)
  
  let current: Node = new Node(0)
  let player: number = 0
  for (let marble = 1; marble <= marbles; marble++) {
    
    if (marble % 23 == 0) {
      let [cur, val] = current.remove()
      current = cur
      p[player] += val
      p[player] += marble
    } else {
      current = current.insert(marble)
    }

    //print(marble, current.value, current.toString())

    player = (player + 1) % players
  }

  return Math.max(...p)
}

let n = new Node(0)
//print([n.value, n.next.value, n.prev.value])
n = n.insert(1)
print(n.toString())
n = n.insert(2)
print(n.toString())
n = n.insert(3)
print(n.toString())

print("high", game(9, 25))
print("high", game(10, 1618))
print("high", game(13, 7999))
print("high", game(471, 72026))
print("high", game(471, 7202600))
