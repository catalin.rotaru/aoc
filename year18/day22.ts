import fs = require('fs')
import assert = require('assert')

const print = console.log.bind(console)

type GameMap = Array<Array<number>>

const X_MUL     = 16807
const Y_MUL     = 48271
const DEPTH_DIV = 20183
const TYPE_DIV  = 3

const EMPTY   = Infinity

function newGameMap(szx: number, szy: number, fill: number = EMPTY) {
  return Array.from({length: szy}, row => new Array(szx).fill(fill))
}

// gi(0, 0) = 0
// gi(x, 0) = x * X_MUL
// gi(0, y) = y * Y_MUL
// gi(x, y) = el(x-1, y) * el(x, y-1)
// gi(Tx, Ty) = 0

// el = (gi + DEPTH) % DEPTH_DIV
// type = el % TYPE_DIV

function computeMap(depth: number, tx: number, ty: number, szx: number = tx + 1, szy: number = ty + 1): GameMap {
  let geo: GameMap = newGameMap(szx, szy)
  let ero: GameMap = newGameMap(szx, szy)
  let type: GameMap = newGameMap(szx, szy)

  for (let y = 0; y < szy; y++)
    for (let x = 0; x < szx; x++) {
      if ((x == tx) && (y == ty))
        geo[y][x] = 0
      else
        geo[y][x] = (y == 0) ? x * X_MUL : (x == 0) ? y * Y_MUL : 
          ero[y][x - 1] * ero[y-1][x]
      
      ero[y][x] = (geo[y][x] + depth) % DEPTH_DIV
      type[y][x] = ero[y][x] % TYPE_DIV
    }

  return type
}

function printMap(map: GameMap) {
  const draw = ['.', '=', '|']

  map.map(row => print(row.map(a => draw[a]).join('')))
}

function risk(map: GameMap, szx: number = map[0].length, szy: number = map.length): number {
  return map.slice(0, szy).reduce((acc, cur) => 
    cur.slice(0, szx).reduce((a, c) => a + c, acc), 0)
}

const ROCKY   = 0   // Climbing gear or Torch (not Neither)
const WET     = 1   // Climbing gear or Neither (not Torch)
const NARROW  = 2   // Torch or Neither (not Climbing Gear)
// tools: Neither = 0; Torch = 1; Climbing Gear = 2

type Move = [number, number, number, number] // x, y, plane, time

const Adjacent: Array<Array<number>> = [[0, -1], [1, 0], [0, 1], [-1, 0]] // x, y

function findTarget(map: GameMap, tx: number, ty: number) {
  // planes of search, its index being the equipped tool
  let plane = Array.from({length: 3}, p => newGameMap(map[0].length, map.length))

  // next adjacent moves
  let next_adjacent: Array<Move> = new Array()
  next_adjacent.push([0, 0, 1, 0])
  // next lateral moves to other planes
  let next_lateral: Array<Move> = new Array()

  let time: number = 0
  do {
    // get current move for current time from adjacent or lateral moves
    let move: Move
    if (next_adjacent.length && next_adjacent[0][3] == time)
      move = next_adjacent.shift()
    else if (next_lateral.length && next_lateral[0][3] == time)
      move = next_lateral.shift()
    else {
      // no move for current time - inc and continue
      time++
      continue
    }

    const [x, y, p, t] = move

    if (plane[p][y][x] <= t)
      continue
    plane[p][y][x] = t

    // target found?
    if ((x == tx) && (y == ty) && (p == 1))
      return t

    // expand into adjacent positions in current plane, if tool allowed in new region
    Adjacent.forEach(a => {
      let nx = x + a[0]
      let ny = y + a[1]

      if ((ny >= map.length) || (nx >= map[0].length))
        print("Need bigger map!!!")

      if ((nx >= 0) && (ny >= 0) && (plane[p][ny][nx] > t + 1) && (map[ny][nx] != p))
        next_adjacent.push([nx, ny, p, t + 1])
    })
    
    // expand into same position in other planes, if new tool allowed in current region
    plane.forEach((np, idx) => {
      if ((idx != p) && (np[y][x] > t + 7) && (map[y][x] != idx))
        next_lateral.push([x, y, idx, t + 7])
    })
  } while (next_adjacent.length || next_lateral.length)
}

const map1 = computeMap(510, 10, 10, 30, 30)
//printMap(map1)
print("Risk", risk(map1, 10 + 1, 10 + 1))
print("Found", findTarget(map1, 10, 10))

const map2 = computeMap(9465, 13, 704, 1000, 1000)
//printMap(map2)
print("Risk", risk(map2, 13 + 1, 704 + 1))
print("Found", findTarget(map2, 13, 704))