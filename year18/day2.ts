import fs = require('fs')

const print = console.log.bind(console)

function readIds(): string[] {
  return fs.readFileSync('day2.txt','utf8').split(/\r?\n/)
}

function categorize(id: string) {
  let app:Map<string, number> = new Map()

  id.split("").forEach((ch) => {
    let count = app.get(ch)
    app.set(ch, count ? count + 1 : 1)
  })

  //print(app)
  //print([...app.values()])

  let twice = [...app.values()].filter((val) => val == 2)
  let thrice = [...app.values()].filter((val) => val == 3)
  return [twice.length != 0, thrice.length != 0]
}

function categorizeArray(ids: string[]) {
  let cat = ids.map((id)=>categorize(id))
  let [twice, thrice] = cat.reduce((acc, cur) => [acc[0] + +cur[0], acc[1] + +cur[1]], [0, 0])

  return (twice*thrice)
}

function areAdjacent(s1: string, s2: string):boolean {
  let res = [...s1].filter((val, idx) => val != s2.charAt(idx))
  return res.length == 1
}

function findCommon(s1: string, s2: string):string {
  let res = [...s1].filter((val, idx) => val == s2.charAt(idx))
  return res.join('')
}

function findAdjacent(ids: string[]) {
  for (let id1 of ids)
    for (let id2 of ids)
      if (areAdjacent(id1, id2))
        return findCommon(id1, id2)
}

let ids = readIds()

print("Checksum:", categorizeArray(["abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"]))
print("Checksum:", categorizeArray(ids))

//print("Common:", findCommon("abcd", "axcd"))

print("Adjacent:", findAdjacent(["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"]))
print("Adjacent:", findAdjacent(ids))