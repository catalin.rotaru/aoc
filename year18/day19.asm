addi 1 16 1 b+= 16  // jmp 17
seti 1 5 5  f= 1
seti 1 2 3  d= 1
mulr 5 3 2  c= d*f
eqrr 2 4 2  c= (e == c)                 => c= (e == d*f)
addr 2 1 1  b+= c   // jr +c            => jr +(e==d*f)
addi 1 1 1  b++     // b is PC, 6 here  => jmp 8 (if (e!=d*f))
addr 5 0 0  a+= f                       => if (e==d*f) a+= f
addi 3 1 3  d++
gtrr 3 4 2  c= d>e
addr 1 2 1  b+= c   // jr +c            => jr +(d>e)
seti 2 6 1  b= 2    // jmp 3            => if (d<=e) jmp 3
addi 5 1 5  f++
gtrr 5 4 2  c= (f>e)
addr 2 1 1  b+= c   // jr +c            => jr +(f>e)
seti 1 8 1  b= 1    // jmp 2            => if (f<=e) jmp 2
mulr 1 1 1  b= b^2  // b is PC, 16 here => EXIT
addi 4 2 4  e+= 2
mulr 4 4 4  e= e^2  //                  => e= (e+2)^2
mulr 1 4 4  e*= b   // b is PC, 19 here => e= 19*(e+2)^2
muli 4 11 4 e*= 11  //                  => e= 11*19*(e+2)^2
addi 2 5 2  c+= 5
mulr 2 1 2  c*= b   // b is PC, 22 here => c= (c+5)*22
addi 2 12 2 c+= 12  //                  => c= (c+5)*22 + 12
addr 4 2 4  e+= c   //                  => e+= (c+5)*22 + 12 (=958)
addr 1 0 1  b+= a   // jr +a
seti 0 4 1  b= 0    // jmp 0            => jmp 17
setr 1 4 2  c= b    // b is PC, 27 here => c= 27
mulr 2 1 2  c= c*b  // b is PC, 28 here => c= 27*28
addr 1 2 2  c+= b   // b is PC, 29 here => c= 27*28+29
mulr 1 2 2  c*= b   // b is PC, 30 here => c= (27*28+29)*30
muli 2 14 2 c*= 14  //                  => c= (27*28+29)*30*14
mulr 2 1 2  c*=b    // b is PC, 32 here => c = (27*28+29)*30*14*32= 10550400
addr 4 2 4  e+= c   // e += 10550400
seti 0 3 0  a= 0
seti 0 7 1  b= 0    // jmp 1