import fs = require('fs')

const print = console.log.bind(console)

// get the keys for the max and min values of a Map
function getMapBounds(map: Map<number, number>): [number, number] {
  let min = map.keys().next().value
  let max = min
  map.forEach((v, k) => {
    if (v < map.get(min))
      min = k
    if (v > map.get(max))
      max = k
  })

  return [min, max]
}

function code(cycles: number) {
  let halt: Map<number, number> = new Map() // Halt value => cycle

  let f = 0

  for (let i = 0; i <= cycles; i++) {
    let c = f | 65536
    f = 10362650

    while (true) {
      let e = c & 255
      f = (((f + e) & 16777215) * 65899) & 16777215

      if (256 > c)
        break

      for (e = 0; (e + 1) * 256 <= c; e++) {}

      c = e
    }

    if (!halt.has(f))
        halt.set(f, i)
  }

  print("HALT", halt.size, getMapBounds(halt))
}

code(100000)