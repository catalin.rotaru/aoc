import fs = require('fs')

const print = console.log.bind(console)

const EAP = 3 // elf attack power (40 for second part)
const GAP = 3 // goblin attack power
const HP = 200 // health points
const UNREACHABLE = 88 // large enough
const ADJACENT: Location[] = [[0, -1], [-1, 0], [1, 0], [0, 1]] // reading order

type Units = Array<Unit>
type GameMap = Array<Array<string>> // [Y][X]
type MoveMap = Array<Array<number>>
type Location = [number, number] // X, Y
type Locations = Array<Location>

class Unit {
  x: number
  y: number
  elf: boolean
  hp: number

  constructor(x: number, y: number, elf: boolean = true) {
    this.x = x
    this.y = y
    this.elf = elf
    this.hp = HP
  }

  // set location
  move(loc: Location) {
    this.x = loc[0]
    this.y = loc[1]
  }

  // is this unit dead?
  isDead(): boolean {
    return this.hp <= 0
  }

  // is this unit alive and at the given location?
  atLocation(loc: Location): boolean {
    const [x, y] = loc
    return (!this.isDead()) && (this.x == x) && (this.y == y)
  }

  // get this unit's current location
  getLocation(): Location {
    return [this.x, this.y]
  }

  // get this unit's targets, depending of its type
  getTargets(elves: Units, goblins: Units): Units {
    return this.elf ? goblins : elves
  }

  // get array of live, adjacent targets
  adjacentTargets(targets: Units): Units {
    return ADJACENT.map(l => getUnit([this.x + l[0], this.y + l[1]], targets)).
      filter (u => Boolean(u))
  }
  
  // get a map of locations reachable from this Unit; values are nr of steps
  getReachableMap(map: GameMap, units: Units): MoveMap {
    return getReachableMap(this.getLocation(), map, units)
  }

  // select nearest reachable locations from the list
  // returns undefined if none exists
  selectNearestReachable(locs: Locations, map: GameMap, units: Units): Location {
    let reach: MoveMap = this.getReachableMap(map, units)

    let reachable =  locs.filter(loc => reach[loc[1]][loc[0]] < UNREACHABLE)
    //print("reachable", reachable)

    if (!reachable.length)
      return undefined

    let dist = reachable.map(l => reach[l[1]][l[0]])
    let minDist = Math.min(...dist)
    //print("dist", dist, minDist)

    let minReachable = reachable.filter(l => reach[l[1]][l[0]] == minDist)
    //print("minReachable", minReachable)

    return minReachable.sort((a, b) => readingOrder(a[0], a[1], b[0], b[1])) [0]
  }

  // select nearest reachable location adjacent to a target
  // returns undefined if none exists
  selectNearestReachableTargetAdjacent(targets: Units, map: GameMap, units: Units): Location {
    let liveTargets = liveUnits(targets)
    let targetAdjacent = getTargetAdjacent(liveTargets, map, units)
    //print("target adjacent", targetAdjacent)
    let nearest = this.selectNearestReachable(targetAdjacent, map, u)
    
    return nearest
  }

  // select shortest path start from this unit to location
  selectShortestPathStartTo(loc: Location, map: GameMap, units: Units): Location {
    let reach = getReachableMap(loc, map, units)
    let adj = freeAdjacent(this.getLocation(), map, units)

    let steps = adj.map(a => reach[a[1]][a[0]])

    let minSteps = Math.min(...steps)
    let closest = steps.indexOf(minSteps)

    return adj[closest]
  }

  // do this unit's turn: attack, but if can't attack, move & try attack again
  // return false if couldn't find targets alive
  doTurn(map: GameMap, elves: Units, goblins: Units): boolean {
    let targets = this.getTargets(elves, goblins)
    if (allDead(targets))
      return false

    let units = getUnits(elves, goblins)

    if (!this.doAttack(map, targets))
      if (this.doMove(map, targets, units))
        this.doAttack(map, targets)

    return true
  }

  // try to find and move towards closest accessible target
  // returns true if unit moved
  doMove(map: GameMap, targets: Units, units: Units): boolean {
    let nearest = this.selectNearestReachableTargetAdjacent(targets, map, units)

    if (!nearest)
      return false

    let start = this.selectShortestPathStartTo(nearest, map, units)

    this.move(start)
    return true
  }

  // try to attack adjacent target, the weakest in reading order one
  // returns true if attacked a target
  doAttack(map: GameMap, targets: Units): boolean {
    let adj: Units = this.adjacentTargets(targets)
    //print("adjacent targets", adj)

    if (!adj.length)
      return false

    let weak = adj.reduce((acc, cur) => acc = (cur.hp < acc.hp) ? cur : acc, adj[0])
    weak.hp -= this.elf ? EAP : GAP
    //print("attack", weak)
    return true
  }
}

function readingOrder(x1: number, y1: number, x2: number, y2: number): number {
  return (y1 == y2) ? x1 - x2 : y1 - y2
}

// get free locations adjacent to targets
function getTargetAdjacent(targets: Units, map: GameMap, units: Units): Locations {
  return targets.reduce((acc, t) => 
    acc.concat(freeAdjacent(t.getLocation(), map, units)), new Array())
}

// get a map of locations reachable from this location; values are nr of steps
function getReachableMap(loc: Location, map: GameMap, units: Units): MoveMap {
  let res = Array.from({length: map.length}, (_) => 
  new Array<number>(map[0].length).fill(UNREACHABLE))
  
  let reach: Locations = [loc]
  res[loc[1]][loc[0]] = 0
  
  for (let i = 0; i < reach.length; i++) {
    let l = reach[i]
    let [x, y] = l
    let steps = res[y][x] + 1
    let next = freeAdjacent(l, map, units)
    next.forEach(n => {
      let [nx, ny] = n
      if (res[ny][nx] == UNREACHABLE) {
        res[ny][nx] = steps
        reach.push(n)
      }
    })
  }
  
  return res
}

// get living unit at location, undefined if nothing there
function getUnit(loc: Location, units: Units): Unit {
  return units.find(u => u.atLocation(loc))
}

// is Location occupied by wall or a living unit (elf or gnome)?
function isOccupied(loc: Location, map: GameMap, units: Units): boolean {
  const [x, y] = loc
  if (map[y][x] == '#')
    return true

  return Boolean(getUnit(loc, units))
}

// get an array of free locations adjacent to the given one, in reading order
function freeAdjacent(loc: Location, map: GameMap, units: Units): Locations {
  const [x, y] = loc
  return ADJACENT.map(l => <Location>[x + l[0], y + l[1]]).
    filter(a => !isOccupied(a, map, units))
}

// do a game round: each living unit does its turn
// return false if a unit couldn't find targets
function gameRound(map: GameMap, elves: Units, goblins: Units): boolean {
  let units = getUnits(elves, goblins).sort((u1, u2) => 
    readingOrder(u1.x, u1.y, u2.x, u2.y))
  //print ("units", units)

  for (let u of units) 
    if (!u.isDead())
      if (!u.doTurn(map, elves, goblins))
        return false

  return true
}

// play the game until a unit couldn't find targets
// return step when that happened
function playGame(map: GameMap, elves: Units, goblins: Units, steps: number = Infinity): [number, number] {
  for (let step = 0; step <= steps; step++) {
    /*if (step == 23) {
      printFullMap(map, elves, goblins)
      print("Round", step, elves, goblins)
    }*/

    if (!gameRound(map, elves, goblins))
      return [step, step * getUnits(elves, goblins).reduce((acc, u) => 
        !u.isDead() ? acc + u.hp : acc, 0)]      
  }
}

function readMap(): [GameMap, Units, Units] {
  let map = fs.readFileSync('day15.txt','utf8').split(/\r?\n/).
    map(s => s.split(''))

  let e: Units = new Array()
  let g: Units = new Array()
  map.forEach((row, y) => row.forEach((s, x) => {
    if ((s == 'E') || (s == 'G')) {
      const u = new Unit(x, y)
      if (s == 'E')
        e.push(u)
      else {
        u.elf = false
        g.push(u)
      }
      row[x] = '.'
    }
  }))

  return [map, e, g]
}

function printMap(map: GameMap) {
  map.forEach(s => print(s.join('')))
}

function printFullMap(map: GameMap, elves: Units, goblins: Units) {
  map.forEach((row, y) => print(row.map((ch, x) => {
    if (getUnit([x, y], elves))
      return 'E'
    if (getUnit([x, y], goblins))
      return 'G'
    return ch
  }).join('')))
}

function getUnits(elves: Units, goblins: Units) {
  return [...elves, ...goblins]
}

function allDead(units: Units): boolean {
  return units.every(u => u.isDead())
}

function liveUnits(units: Units): Units {
  return units.filter(u => !u.isDead())
}

let [map, e, g] = readMap()
let u = getUnits(e, g)

//printFullMap(map, e, g)
print("elves", e.length)
//print("goblins", g)
//print("occ", isOccupied([0, 0], map, u))
//print("occ", isOccupied([1, 1], map, u))
//print("occ", isOccupied([2, 2], map, u))
//print("occ", isOccupied([4, 4], map, u))
//print("adj", freeAdjacent([1, 1], map, u))
//print("adj", freeAdjacent([7, 6], map, u))
//print("adj", freeAdjacent([4, 4], map, u))

//print(e[0].getReachableMap(map, u))
//print(g[0].getReachableMap(map, u))

//print("move", e[0].doMove(map, g, u))


print("---------------------")

//e[0].doTurn(map, e, g)
//print("elf", e[0])
//e[0].doAttack(map, g)
//e[0].doAttack(map, g)

print("Game", playGame(map, e, g))
//printFullMap(map, e, g)
print("Final elves", liveUnits(e).length)
//print("Final goblins", g)
