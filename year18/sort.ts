import fs = require('fs')

const print = console.log.bind(console)

type Value = [number, number]

function cmp1(a: Value, b: Value): number {
  return a[0] - b[0]
}

function cmp2(a: Value, b: Value): number {
  return a[1] - b[1]
}

let data: Value[] = [[3, 2], [2, 1], [1, 2], [2, 3], [2, 2], [3, 1]]

print(data.sort((a, b) => cmp1(a, b) || cmp2(a, b)))
print("max", data.reduce((a, c) => (cmp1(c, a) || cmp2(c, a)) > 0 ? c : a))
