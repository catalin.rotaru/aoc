import fs = require('fs')

const print = console.log.bind(console)

class Node {
  child: Array<Node>
  meta: Array<number>

  constructor(c: Array<Node>, m: number[]) {
    this.child = [...c]
    this.meta = [...m]
  }

  getLength(): number {
    return 2 + this.child.reduce((a, c) => a + c.getLength(), 0) + this.meta.length
  }

  getMetaSum(): number {
    return this.child.reduce((a, c) => a + c.getMetaSum(), 0) +
      this.meta.reduce((a, c) => a + c, 0)
  }

  getValue(): number {
    //print("val c/m", this.child.length, this.meta.length)
    if (this.child.length == 0) 
      return this.meta.reduce((a, c) => a + c, 0)

    return this.meta.reduce((a, c) => 
      a + ((c > this.child.length) ? 0 : this.child[c - 1].getValue()), 0)
  }
}

function nodeLength(nodes: Node[]): number {
  return nodes.reduce((a, c) => a + c.getLength(), 0)
}

function readNode(input: number[]): Node {
  const children = input[0]
  const child = new Array<Node>()
  //print("children", children)
  
  for (let i = 0; i < children; i++) {
    //print("sub-child", i)
    let c: Node = readNode(input.slice(2 + nodeLength(child)))
    //print("sub-child", i, c)
    child.push(c)
  } 

  const meta_start = 2 + nodeLength(child)
  const meta_data = input.slice(meta_start, meta_start + input[1])
  //print("meta", meta_data)

  return new Node(child, meta_data)
}

function readInput(): number[] {
  return fs.readFileSync('day8.txt','utf8').split(' ').map((a) => +a)
}

const in1 = [2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2]
const tree1 = readNode(in1)
//print ("A", tree1)
//print ("B", tree1.child[0], tree1.child[0].getValue())
//print ("C", tree1.child[1], tree1.child[1].getValue())
//print ("D", tree1.child[1].child[0], tree1.child[1].child[0].getValue())
print("tree l/m/v", tree1.getLength(), tree1.getMetaSum(), tree1.getValue())

print("---------------------------")
const in2 = readInput()
const tree2 = readNode(in2)
print("tree", tree2.getLength(), tree2.getMetaSum(), tree2.getValue())