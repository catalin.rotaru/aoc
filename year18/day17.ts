import fs = require('fs')

const print = console.log.bind(console)

type GameMap = Array<Array<string>>

const BUFF = 1
const LEFT = -1
const RIGHT = 1

const CLAY = '#'
const SAND = '.'
const FLOWING = '|'
const STANDING = '~'

function readInterval(str: string): [number, number] {
  const [a, b] = str.split('..')
  //print ("interval", a, b)

  return [+a, b ? +b : +a]
}

function readMap(): GameMap {
  const data = fs.readFileSync('day17.txt','utf8').split(/\r?\n/).map(l => {
    const [first, second] = l.split(', ')
    const [f1, f2] = first.split('=')
    const [s1, s2] = second.split('=')

    const i1 = readInterval(f2)
    const i2 = readInterval(s2)

    return f1 == 'x' ? [...i1, ...i2] : [...i2, ...i1]
  })
  //print(data)

  let [x1, x2, y1, y2] = data.reduce((acc, cur) => 
    [Math.min(acc[0], cur[0]), Math.max(acc[1], cur[1]), 
    Math.min(acc[2], cur[2]), Math.max(acc[3], cur[3])], 
    [Infinity, -Infinity, Infinity, -Infinity])
  print("bounds", x1, x2, y1, y2)
  x1 -= BUFF
  x2 += BUFF
  //y1 = 0

  let map: GameMap = Array.from({length: y2 - y1 + 1}, row => 
    new Array(x2 - x1 + 1).fill(SAND))

  data.forEach(d => {
    for (let x = d[0]; x <= d[1]; x++)
      for (let y = d[2]; y <= d[3]; y++)
        map[y - y1][x - x1] = CLAY
  })

  map[y1 - y1][500 - x1] = FLOWING
  
  return map
}

function setStandingWater(map: GameMap, x: number, y: number, inc: number = LEFT) {
  for (let i = x; map[y][i] == FLOWING; i += inc)
    map[y][i] = STANDING
}

function isStandingWater(map: GameMap, x: number, y: number, inc: number = LEFT) {
  let i = x
  for (; map[y][i] == FLOWING; i += inc) {
    const under = map[y+1][i]
    if ((under != CLAY) && (under != STANDING))
      return false 
  }
  if (map[y][i] != CLAY)
    return false
    
  return true
}

// overflow left or right; return true if turned into standing water
function overflow(map: GameMap, x: number, y: number, inc: number = LEFT) {
  // if sand to the side, flow there
  const side = x + inc
  if (map[y][side] == SAND)
    flow(map, side, y)

  // if clay to the side, check if in standing water
  if ((map[y][side] == CLAY) && isStandingWater(map, x, y, -inc))
    setStandingWater(map, x, y, -inc)
}

function flow(map: GameMap, x: number = Infinity, y: number = 0) {
  if (x == Infinity)
    x = map[0].indexOf(FLOWING)

  // fill current cell with flowing water
  map[y][x] = FLOWING
  
  // return if we are on the last row - there is no flowing anymore
  const down = y + 1
  if (down == map.length)
    return

  // first, try to flow down
  if (map[down][x] == SAND)
    flow(map, x, down)

  // if underneath is CLAY or STANDING: overflow left, then right
  if ((map[down][x] == CLAY) || (map[down][x] == STANDING)) {
    overflow(map, x, y)
    overflow(map, x, y, RIGHT)
  }
}

function countWater(map: GameMap, flowing: boolean = true): number {
  return map.reduce((acc, row) => acc + row.reduce((a, c) => 
    a + +(((c == FLOWING) && flowing) || (c == STANDING)), 0), 0)
}

function printMap(map: GameMap) {
  map.map(row => print(row.join('')))
}

print("interval", readInterval("20"))
print("interval", readInterval("1..66"))

let map = readMap()
//printMap(map)
//flowWater(map)
flow(map)
//printMap(map)

print("Total Water", countWater(map))
print("Standing Water", countWater(map, false))
