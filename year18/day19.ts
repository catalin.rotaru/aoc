import fs = require('fs')

const print = console.log.bind(console)

 // A = 0, B, C, D, E, F
type Registers = [number, number, number, number, number, number]
type Operation = [string, number, number, number] // Opcode, A, B, C
type Code = Array<Operation>

const exec = {
  'addr': (a: number, b: number, reg: Registers) => reg[a] + reg[b],
  'addi': (a: number, b: number, reg: Registers) => reg[a] + b,
  'mulr': (a: number, b: number, reg: Registers) => reg[a] * reg[b],
  'muli': (a: number, b: number, reg: Registers) => reg[a] * b,
  'banr': (a: number, b: number, reg: Registers) => reg[a] & reg[b],
  'bani': (a: number, b: number, reg: Registers) => reg[a] & b,
  'borr': (a: number, b: number, reg: Registers) => reg[a] | reg[b],
  'bori': (a: number, b: number, reg: Registers) => reg[a] | b,
  'setr': (a: number, b: number, reg: Registers) => reg[a],
  'seti': (a: number, b: number, reg: Registers) => a,
  'gtir': (a: number, b: number, reg: Registers) => +(a > reg[b]),
  'gtri': (a: number, b: number, reg: Registers) => +(reg[a] > b),
  'gtrr': (a: number, b: number, reg: Registers) => +(reg[a] > reg[b]),
  'eqir': (a: number, b: number, reg: Registers) => +(a == reg[b]),
  'eqri': (a: number, b: number, reg: Registers) => +(reg[a] == b),
  'eqrr': (a: number, b: number, reg: Registers) => +(reg[a] == reg[b])
}

function printInstruction(line: number, op: string, a: number, b: number, c: number, ipr: number): string {
  const reg = {0:'a', 1:'b', 2:'c', 3:'d', 4:'e', 5:'f'}
  const lc = ("0" + line).slice(-2) + ' ';
  let s1 = '='

  const jmp: boolean = (c == ipr)
  let jmp_op= 'jmp'
  
  let operation:string = exec[op].toString().split(' => ')[1]
  
  const cmp: boolean = operation.startsWith('+')
  if (cmp)
    operation = operation.slice(1)
  
  const o1 = operation.replace('reg[a]', 'x').replace('reg[b]', 'y')
  const o2 = o1.replace('a', '' + a).replace('b', '' + b)
  const o3 = o2.replace('x', reg[a]).replace('y', reg[b])
  
  let x = o3.split(' ')
  if (!cmp && (x.length == 3)) {
    if (x[0] == reg[c]) {
      x.shift()
      s1 = x.shift() + s1
    } else if (x[2] == reg[c]) {
      x.pop()
      s1 = x.pop() + s1
    }

    if (jmp)
      jmp_op = 'jr'
  }
  
  let assign: string = jmp ? jmp_op + ' ' : reg[c] + ' ' + s1 + ' '
  const inc =  (jmp ? ' + 1' : '')

  return lc + assign + x.join(' ') + inc
}

// execute single instruction from instruction set affecting registers
function execute(op: string, a: number, b: number, c: number, reg: Registers): Registers {
  let rn: Registers = <Registers>[...reg]
  rn[c] = exec[op](a, b, reg)

  //print ("execute", reg, op, a, b, c, "=>", rn)
  return rn
}

// get the keys for the max and min values of a Map
function getMapBounds(map: Map<number, number>): [number, number] {
  let min = map.keys().next().value
  let max = min
  map.forEach((v, k) => {
    if (v < map.get(min))
      min = k
    if (v > map.get(max))
      max = k
  })

  return [min, max]
}

function runCode(code: Code, ipr: number = 0, reg0: number = 0, cycles: number = Infinity) {
  let reg: Registers = [reg0, 0, 0, 0, 0, 0]
  let halt: Map<number, number> = new Map() // Halt value => cycle

  for (let ic = 0; (reg[ipr] >= 0) && (reg[ipr] < code.length) && ic < cycles; reg[ipr]++, ic++) {
    let inst = code[reg[ipr]]

    // special condition for DAY 21
    if (reg[ipr] == 28) {
      if (!halt.has(reg[5]))
        halt.set(reg[5], ic)
    }
      
    // print ("ip", reg[ipr], "R", reg, "inst", inst)
    reg = execute(inst[0], inst[1], inst[2], inst[3], reg)
  }

  print("RESULT:", reg)

  //print(halt)
  print("HALT", halt.size, getMapBounds(halt))
}

function readCode(file: string = 'day19.txt'): Code {
  let lines = fs.readFileSync(file,'utf8').split(/\r?\n/)

  let code = lines.map(line => <Operation>[...line.split(" ").
    map((a, i) => i ? +a : a)])

  return code
}

function printCode(code: Code, ipr: number = 0) {
  code.forEach((l, i) => print(printInstruction(i, l[0], l[1], l[2], l[3], ipr)))
}

print("---------- DAY 19 ----------")

let code = readCode()
//print(code)
runCode(code, 1)
//runCode(code, 1, 1)

print("---------- DAY 21 ----------")

let code2 = readCode('day21.txt')
//print(code)
printCode(code2, 3)
//runCode(code2, 3, 0, 10000000000) //10000000000