import fs = require('fs')

const print = console.log.bind(console)

const ORIGIN = '0'.charCodeAt(0)

type Recipes = Array<number>

function newRecipes(e: number, f: number): number[] {
  return (e + f).toString().split('').map(s => s.charCodeAt(0) - ORIGIN)
}

function advanceElf(elf: number, recipes: Recipes): number {
  return (elf + 1 + recipes[elf]) % recipes.length
}

function trainElves(steps: number = 20) {
  let recipes = [3, 7]
  let elf1 = 0
  let elf2 = 1

  while (recipes.length <= steps + 10) {
    const nr = newRecipes(recipes[elf1], recipes[elf2])
    recipes.push(...nr)
    elf1 = advanceElf(elf1, recipes)
    elf2 = advanceElf(elf2, recipes)

    //print(recipes.join(''), elf1, elf2)
  }

  return recipes.slice(steps, steps + 10).join('')
}

function trainElves2(pattern: string) {
  let recipes = [3, 7]
  let elf1 = 0
  let elf2 = 1

  while (true) {
    const nr = newRecipes(recipes[elf1], recipes[elf2])
    recipes.push(...nr)
    elf1 = advanceElf(elf1, recipes)
    elf2 = advanceElf(elf2, recipes)

    //print(recipes.join(''), elf1, elf2)

    let start = recipes.length - pattern.length
    if (pattern == recipes.slice(start, start + pattern.length).join(''))
      return start
    start--
    if (pattern == recipes.slice(start, start + pattern.length).join(''))
      return start
  }
}

print("new", newRecipes(3, 7))
print("train", trainElves(9))
print("train", trainElves(5))
print("train", trainElves(18))
print("train", trainElves(2018))
print("Train", trainElves(652601))
print("Train2", trainElves2("51589"))
print("Train2", trainElves2("59414"))
print("Train2", trainElves2("652601"))

