import fs = require('fs')

const print = console.log.bind(console)

const OPEN = '.'
const TREES = '|'
const LUMBERYARD = '#'

type GameMap = Array<Array<string>>
type Neighbors = Map<string, number>

function readMap(): GameMap {
  return fs.readFileSync('day18.txt','utf8').split(/\r?\n/).
    map(s => s.split(''))
}

function printMap(map: GameMap) {
  map.map(row => print(row.join('')))
}

function getNeighbors(map: GameMap, x: number, y: number): Neighbors {
  let res = new Map([[OPEN, 0], [TREES, 0], [LUMBERYARD, 0]])

  map.slice(Math.max(y - 1, 0), Math.min(y + 2, map.length)).forEach(row =>
    row.slice(Math.max(x - 1, 0), Math.min(x + 2, row.length)).forEach(acre => 
        res.set(acre, res.get(acre) +  1)))
  
  const curr = map[y][x]
  res.set(curr, res.get(curr) -  1)

  return res
}

function transformAcre(acre: string, neighbors: Neighbors): string {
  switch (acre) {
    case OPEN:
      return (neighbors.get(TREES) >= 3) ? TREES : OPEN
    case TREES:
      return (neighbors.get(LUMBERYARD) >= 3) ? LUMBERYARD : TREES
    case LUMBERYARD:
      return (neighbors.get(LUMBERYARD) >= 1) && (neighbors.get(TREES) >= 1) ? 
        LUMBERYARD : OPEN
  }
}

function transformMap(map: GameMap): GameMap {
  return map.map((row, y) => row.map((a, x) => transformAcre(a, getNeighbors(map, x, y))))
}

function applyTransforms(map: GameMap, minutes: number = 10): GameMap {
  let res: GameMap = map
  for (let i = 0; i < minutes; i++)
    res = transformMap(res)
  return res
}

function countResources(map: GameMap, res: string): number {
  return map.reduce((acc, row) => row.reduce((a, c) => a + +(c == res), acc), 0)
}

function resourceValue(map: GameMap): number {
  return countResources(map, TREES) * countResources(map, LUMBERYARD)
}

function mapDigest(map: GameMap): string {
  return map.reduce((acc, row) => acc + row.join(''), '')
}

function searchTransformCycle(map: GameMap) {
  let values: Array<number> = new Array()
  let digs: Map<string, number> = new Map()

  let prev: number
  let min: number
  for (min = 0; ; min++) {
    const dig = mapDigest(map)
    if (digs.has(dig)) {
      prev = digs.get(dig)
      break
    }

    digs.set(dig, min)
    values.push(resourceValue(map))
    map = transformMap(map)
  }

  print("Cycle current", min, "previous", prev, "value", values[prev])
  print("Future", values[prev + (1000000000 - prev) % (min - prev)])
}

let map = readMap()
//printMap(map)
//print(mapDigest(map))
print(getNeighbors(map, 0, 0))
print(getNeighbors(map, 1, 1))
print(getNeighbors(map, map[0].length - 1, map.length - 1))

let map2 = applyTransforms(map)
//printMap(map2)
print("Resources", resourceValue(map2))

searchTransformCycle(map)