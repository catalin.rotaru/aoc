import fs = require('fs')

class Claim {
  id: number
  left: number
  top: number
  right: number
  bottom: number

  constructor(claim: string) {
    let [id, sq] = claim.split(" @ ")

    this.id = +id.substring(1, id.length)

    let [loc, sz] = sq.split(": ")
    let [x, y] = loc.split(",")
    let [w, h] = sz.split("x")

    this.left = +x
    this.top = +y
    this.right = this.left + +w - 1
    this.bottom = this.top + +h - 1
  }

  intersect(claim: Claim): number {
    let left = Math.max(this.left, claim.left)   
    let right = Math.min(this.right, claim.right)
    let top = Math.max(this.top, claim.top)
    let bottom = Math.min(this.bottom, claim.bottom)

    return ((left > right) || (top > bottom)) ? null :
      (right - left + 1) * (bottom - top + 1) 
  }

  intersects(claims: Claim[]):boolean {
    return claims.some((c) => this.intersect(c) && (this.id != c.id))
  }
}

const print = console.log.bind(console)

function findNonIntersect(claims: Claim[]): number {
  return claims.find((c) => !c.intersects(claims)).id
}

function computeCommon(claims: Claim[]): number {
  let grid: Array<Array<number>> = new Array(1000)

  for (let i = 0; i < grid.length; i++) {
    grid[i] = new Array(1000)
    grid[i].fill(0)
  }

  claims.map((c)=>{
    for (let i = c.top; i <= c.bottom; i++)
      for (let j = c.left; j <=c.right; j++)
        grid[i][j]++
  })

  return grid.reduce((acc, r)=>r.reduce((a, c)=>a + +(c > 1), acc), 0)
}

function readClaims(): Claim[] {
  return fs.readFileSync('day3.txt','utf8').split(/\r?\n/).map((s) => new Claim(s))
}

let c1 = new Claim("#1 @ 1,3: 4x4")
let c2 = new Claim("#2 @ 3,1: 4x4")
let c3 = new Claim("#3 @ 5,5: 2x2")
print(c1)
print(c2)
print(c3)
print("intersect", c1.intersect(c2))
print("intersect", c2.intersect(c3))

let claims = readClaims()

print("Common fabric:", computeCommon(claims))
print("Doesn't overlap:", findNonIntersect(claims))