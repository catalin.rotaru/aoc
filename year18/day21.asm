seti 123 0 5      f= 123
bani 5 456 5      f= f&456      //72
eqri 5 72 5       f= (f==72)
addr 5 3 3        d+= f         // jr +(f==72)  => if (f==72) jmp 5
seti 0 0 3        jmp 1         // END TEST
seti 0 5 5        f= 0
bori 5 65536 2    c= f|65536    // 0b10..0
seti 10362650 3 5 f= 10362650
bani 2 255 4      e= c&255      // lowest byte of C set to 1
addr 5 4 5        f+= e
bani 5 16777215 5 f&= 16777215  // 0b1..1
muli 5 65899 5    f*= 65899
bani 5 16777215 5 f&= 16777215  // 0b1..1
gtir 256 2 4      e= (256>c)
addr 4 3 3        d+= e         // jr +(256>c)  => if (256>c) jmp 16 => 28
addi 3 1 3        d++           // jr +1        => jmp 17
seti 27 4 3       d= 27                         => jmp 28
seti 0 3 4        e= 0
addi 4 1 1        b= e+1
muli 1 256 1      b= b*256
gtrr 1 2 1        b= (b>c)
addr 1 3 3        d+= b         // jr +(b>c)    => if (b>c) jmp 23
addi 3 1 3        d++           // jr +1        => jmp 24
seti 25 2 3       d= 25                         => jmp 26
addi 4 1 4        e++
seti 17 7 3       d= 17         // jmp 18
setr 4 0 2        c= e
seti 7 8 3        d= 7          // jmp 8
eqrr 5 0 4        e= (a == f)
addr 4 3 3        d+= e         // jr +(a == f)  => if (a == f) jmp 31 EXIT
seti 5 1 3        d = 5         // jmp 6