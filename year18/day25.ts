import fs = require('fs')

const print = console.log.bind(console)

const NONE = -1
const MIN = 3

type Location = [number, number, number, number]
type Points = Array<Point>

class Point {
  loc: Location
  const: number

  constructor(x: number, y: number, z: number, t: number) {
    this.const = NONE

    this.loc = [x, y, z, t]
  }

  free(): boolean {
    return this.const == NONE
  }
}

function distance(l1: Location, l2: Location) {
  return Math.abs(l2[0] - l1[0]) + Math.abs(l2[1] - l1[1]) + 
    Math.abs(l2[2] - l1[2]) + Math.abs(l2[3] - l1[3])
}

function readPoints(): Points {
  return fs.readFileSync('day25.txt','utf8').split(/\r?\n/).map(l => {
    let p: number[] = l.trim().split(',').map(s=>+s)
    return new Point(p[0], p[1], p[2], p[3])
  })
}

function seekConstellations(points: Points) {
  // for each point in set
  for (let i = 0; i < points.length; i++) {
    let n: Point = points[i]
    // for each previous point
    for (let j = 0; j < i; j++) {
      let prev: Point = points[j]
      // same constellation?
      let dist: number = distance(prev.loc, n.loc)
      if (dist <= MIN) {
        // not in a constellation already?
        if (n.free())
          // join the previous one
          n.const = prev.const
        else {
          // set all previous to mine
          let c = prev.const
          points.forEach(p => {
            if (p.const == c)
              p.const = n.const
          })
        }
      }
    }
    // no constellation found => new constellation!
    if (n.free())
      n.const = i
  }
}

function countConstellations(points: Points) {
  return new Set(points.map(p => p.const)).size
}

print("dist", distance([1, 2, 3, 4], [2, 3, 4, 5]))

let p: Points = readPoints()
seekConstellations(p)
print("Constellations:", countConstellations(p))
//print(p)