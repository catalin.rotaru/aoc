import fs = require('fs')

const print = console.log.bind(console)

type Path = Array<string|PathArray>
interface PathArray extends Array<Path> {}

function printTypes(path: Path) {
  path.forEach(p => print(typeof(p), typeof(p) == 'string'))
}

function printPath(path: Path, result: string = '', index: number = 0) {
  // end of the path?
  if (index >= path.length) {
    print(result)
    return
  }

  let current = path[index]
  index++
  if (typeof(current) == 'string')
    printPath(path, result + current, index)
  else
    current.forEach(a => {})
}

let p:Path = ["abc", [["x"], ["y"]], "ddd"]

print(p)
printTypes(p)