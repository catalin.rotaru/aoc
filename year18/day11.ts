import fs = require('fs')

const print = console.log.bind(console)

function hundredDigit(a: number): number {
  const s: string = a.toString()

  return (s.length < 3) ? 0 : +s.slice(-3, -2)
}

function powerLevel(x: number, y: number, serial: number): number {
  const rackId = x + 10
  const level = (rackId * y + serial) * rackId

  return hundredDigit(level) - 5
}

type Grid = Array<Array<number>>

const SIZE = 300
const SQUARE = 3

function getGrid(serial: number): Grid {
  let grid: Grid = Array.from({length: SIZE}, (_, y) => 
    Array.from({length: SIZE}, (_, x) => powerLevel(x + 1, y + 1, serial)))

  return grid
}

// get the value (sum) of a square
function sumSquare(g: Grid, x: number, y: number, size: number): number {
  return g.slice(y, y + size).reduce((acc, cur) => 
    acc + cur.slice(x, x + size).reduce((a,c)=>a+c), 0)
}

// faster way to get the value (sum) of a square by starting from a previously
// computed square
function sumSquare2(g: Grid, x: number, y: number, size: number, prev: number): number {
  const col = x + size - 1
  let res = g[y + size - 1].slice(x, col).reduce((a, c) => a + c,  prev)
  for (let i = y; i < y + size; i++)
    res += g[i][col]
  
  // ASSERT res == sumSquare(g, x, y, size)
  return res
}

// get a grid of values of squares of a given size
function getSquares(g: Grid, square: number = SQUARE): Grid {
  //print("get squares", square)

  let sq: Grid = Array.from({length: SIZE - square + 1}, (_, y) =>
    Array.from({length: SIZE - square + 1}, (_, x) => 
      sumSquare(g, x, y, square)))

  return sq
}

// faster way to get a grid of values of squares of a given size by starting
// with a grid of  previously computed smaller size squares
function getSquares2(g: Grid, prev: Grid, square: number = SQUARE): Grid {
  //print("get squares", square)

  let sq: Grid = Array.from({length: SIZE - square + 1}, (_, y) =>
    Array.from({length: SIZE - square + 1}, (_, x) => 
      sumSquare2(g, x, y, square, prev ? prev[y][x] : 0)))

  return sq
}

// get array grids of values of squares of a all sizes
function getAllSquares(g: Grid): Array<Grid> {
  let sq: Array<Grid> = Array.from({length: SIZE}, (_, sz) =>
    getSquares(g, sz + 1))

  return sq
}

// faster way to get array grids of values of squares of a all sizes
function getAllSquares2(g: Grid): Array<Grid> {
  let prev: Grid = null
  let sq: Array<Grid> = Array.from({length: SIZE}, (_, sz) => 
    prev = getSquares2(g, prev, sz + 1))

  return sq
}

function largestSquare(sq: Grid): [number, number, number] { // X, Y, Value
  const res = sq.reduce((acc, row, y) => 
    row.reduce((a, v, x) => (v > a[2]) ? [x, y, v] : a, acc), [0, 0, -Infinity])

  return [res[0] + 1, res[1] + 1, res[2]]
}

function largestAllSquare(sq: Array<Grid>): [number, number, number] {  // X, Y, Size
  const largest = sq.map(g => largestSquare(g))
  //print("all largest", largest)

  const res = largest.reduce((acc, cur, idx) =>
    (cur[2] > acc[2]) ? [cur[0], cur[1], cur[2], idx] : acc, 
    [0, 0, -Infinity, 0])

  return [res[0], res[1], res[3] + 1]
}

print("Hello 2018!")
print("hundred", hundredDigit(1), hundredDigit(12), hundredDigit(123), hundredDigit(1234))
print("level", powerLevel(3, 5, 8))
print("level", powerLevel(122,79, 57))
print("level", powerLevel(217,196, 39))
print("level", powerLevel(101,153, 71))

print ("largest", largestSquare(getSquares(getGrid(18))))
print ("largest", largestSquare(getSquares(getGrid(42))))
print ("largest", largestSquare(getSquares(getGrid(8561))))

print("-------------------------")

//let g = getGrid(18)
//print(g)
//print(largestSquare(getSquares(g, 1)))
//print(largestSquare(getSquares(g, 2)))
//print(getSquares(g, 3))
//print(largestSquare(getSquares(g, 3)))
//print(largestSquare(getSquares(g, 4)))

print ("largest all", largestAllSquare(getAllSquares2(getGrid(18))))
print ("largest all", largestAllSquare(getAllSquares2(getGrid(42))))
print ("largest all", largestAllSquare(getAllSquares2(getGrid(8561))))