import fs = require('fs')

const print = console.log.bind(console)

type Point = [number, number] // x, y
type Vector = [Point, Point] // origin, destination

// direction => [x, y]
const dir = {'N': [0, -1], 'S': [0, 1], 'E': [1, 0], 'W': [-1, 0]}

function readNavigation(): string[] {
  return fs.readFileSync('day12.txt','utf8').split(/\r?\n/)
}

const rotateOnce = ([x, y]: Point, right: boolean = true): Point =>
  right ? [-y, x] : [y, -x]

function rotate(p: Point, steps: number, right: boolean = true): Point {
  let res = p
  for (let i = 0; i < steps; i++)
    res = rotateOnce(res, right)
  return res
}

const translate = ([x, y]: Point, [dx, dy]: Point, steps: number = 1): Point =>
  [x + dx * steps, y + dy * steps]

const manhattan = ([x, y]: Point) =>
  Math.abs(x) + Math.abs(y)

function move([ship, way]: Vector, act: string, v2: boolean = false): Vector {
  const op = act.charAt(0)
  const steps = +act.slice(1)
  switch (op) {
    case 'F':
      return [translate(ship, way, steps), way]
    case 'R':
    case 'L':
      return [ship, rotate(way, steps/90, op === 'R')]
    default:
      return v2 ? [ship, translate(way, dir[op], steps)] : 
        [translate(ship, dir[op], steps), way]
  }
}

const navigate = (op: string[]) =>
  op.reduce((a, c) => move(a, c), <Vector>[[0, 0], [1, 0]])

const navigate2 = (op: string[]) =>
  op.reduce((a, c) => move(a, c, true), <Vector>[[0, 0], [10, -1]])

print('N', move([[5, 5], [1, 0]], "N10"))
print('E', move([[5, 5], [0, -1]], "E5"))
print('R', move([[1, 1], [0, -1]], "R90"))
print('L', move([[1, 1], [1, 0]], "L180"))
print('F', move([[1, 1], [1, 0]], "F5"))

const navigation  = readNavigation()

print("answer", manhattan(navigate(navigation)[0]))

print("------------------")

print('r', rotateOnce([1, 1]))
print('R270', rotate([1, 1], 3))
print('t', translate([5, 5], [1, 1], 10))

print('F10', move([[0, 0], [10, -1]], 'F10', true))
print('N3', move([[100, -10], [10, -1]], 'N3', true))
print('F7', move([[100, -10], [10, -4]], 'F7', true))
print('R90', move([[170, -38], [10, -4]], 'R90', true))
print('F11', move([[170, -38], [4, 10]], 'F11', true))

print("answer2", manhattan(navigate2(navigation)[0]))