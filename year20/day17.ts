import fs = require('fs')

const print = console.log.bind(console)

enum Tile {OFF = '.', ACTIVE = '#'}
type Grid = Array<Array<Array<Tile>>> // z, y, x
type Cell = [number, number, number] // x, y, z
type Cells = Array<Cell>

type Grid4 = Array<Array<Array<Array<Tile>>>> // w, z, y, x
type Cell4 = [number, number, number, number] // x, y, z, w
type Cell4s = Array<Cell4>

// sz is even
function createGrid(init: string[][], sz: number = 100): Grid {
  let res = Array.from({length: sz}, () => 
    Array.from({length: sz}, () => new Array<Tile>(sz).fill(Tile.OFF)))

  const h = Math.floor(init.length/2)
  const w = Math.floor(init[0].length/2)
  init.forEach((l, y) => l.forEach((v, x) => 
    res[sz/2][y - h + sz/2][x - w + sz/2] = <Tile>v))

  return res
}

function neighbors([x, y, z]: Cell): Cells {
  let res: Cells = new Array()
  for (let i = -1; i < 2; i++)
    for (let j = -1; j < 2; j++)
      for (let k = -1; k < 2; k++)
        if (i || j || k)
          res.push([x + i, y + j, z + k])
  return res
}

const getCell = (g: Grid, [x, y, z]: Cell): Tile =>
  ((x >= 0) && (x < g[0][0].length) && (y >= 0) && (y < g[0].length) && 
    (z >= 0) && (z < g.length)) ? g[z][y][x] : Tile.OFF

const actives = (g: Grid, a: Cell): number =>
  neighbors(a).filter(c => getCell(g, c) == Tile.ACTIVE).length

const cycle = (g: Grid): Grid =>
  g.map((p, z) => p.map((l, y) => l.map((c, x) => {
    const a = actives(g, [x, y, z])
    return (c === Tile.ACTIVE) ? 
      ((a == 2) || (a == 3) ? Tile.ACTIVE : Tile.OFF) :
      ((a == 3) ? Tile.ACTIVE : Tile.OFF)
  })))

// total actives
const total = (g: Grid): number =>
  g.reduce((a2, z) => z.reduce((a, y) => 
    a + y.filter(v => v === Tile.ACTIVE).length, a2), 0)

function cycles(g: Grid, steps: number = 6): number {
  for (let i = 0; i < steps; g = cycle(g), i++);
  return total(g)
}

function readSlice(): string[][] {
  return fs.readFileSync('day17.txt','utf8').split(/\r?\n/).map(l => l.split(''))
}

const printGrid = (g: Grid) => g.forEach((p, i) => {
  print("plane", i)
  p.forEach(r => print(r.join('')))
})

// sz is even
function createGrid4(init: string[][], sz: number = 100): Grid4 {
  let res = Array.from({length: sz}, () => Array.from({length: sz}, () => 
    Array.from({length: sz}, () => new Array<Tile>(sz).fill(Tile.OFF))))

  const h = Math.floor(init.length/2)
  const w = Math.floor(init[0].length/2)
  init.forEach((l, y) => l.forEach((v, x) => 
    res[sz/2][sz/2][y - h + sz/2][x - w + sz/2] = <Tile>v))

  return res
}

function neighbors4([x, y, z, w]: Cell4): Cell4s {
  let res: Cell4s = new Array()
  for (let i = -1; i < 2; i++)
    for (let j = -1; j < 2; j++)
      for (let k = -1; k < 2; k++)
        for (let l = -1; l < 2; l++)
          if (i || j || k || l)
            res.push([x + i, y + j, z + k, w + l])
  return res
}

const getCell4 = (g: Grid4, [x, y, z, w]: Cell4): Tile =>
  ((x >= 0) && (x < g[0][0][0].length) && (y >= 0) && (y < g[0][0].length) && 
    (z >= 0) && (z < g[0].length) && (w >= 0) && (w < g.length)) ? 
      g[w][z][y][x] : Tile.OFF

const actives4 = (g: Grid4, a: Cell4): number =>
  neighbors4(a).filter(c => getCell4(g, c) == Tile.ACTIVE).length

const cycle4 = (g: Grid4): Grid4 =>
  g.map((s, w) => s.map((p, z) => p.map((l, y) => l.map((c, x) => {
    const a = actives4(g, [x, y, z, w])
    return (c === Tile.ACTIVE) ? 
      ((a == 2) || (a == 3) ? Tile.ACTIVE : Tile.OFF) :
      ((a == 3) ? Tile.ACTIVE : Tile.OFF)
  }))))

// total actives
const total4 = (g: Grid4): number =>
  g.reduce((a3, w) => w.reduce((a2, z) => z.reduce((a, y) =>
    a + y.filter(v => v === Tile.ACTIVE).length, a2), a3), 0)

function cycles4(g: Grid4, steps: number = 6): number {
  for (let i = 0; i < steps; g = cycle4(g), i++);
  return total4(g)
}

print("n", neighbors([0, 0, 0]).length)

const init = readSlice()
//print(init)

let grid = createGrid(init, 20)
print(actives(grid, [0, 0, 0]), actives(grid, [-5, -5, -5]), actives(grid, [6, 6, 6]))

print(total(grid))
print(total(cycle(grid)))

print("a", cycles(grid))

print("------------------")

let grid4 = createGrid4(init, 20)
print("a2", cycles4(grid4))
