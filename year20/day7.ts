import fs = require('fs')

const print = console.log.bind(console)

type Inside = Map<string, Map<string, number>> // outer bag => bags, counts
type Outside = Map<string, Set<string>> // inner bag => bags

const union = (s1: Set<any>, s2: Set<any>): Set<any> =>
  new Set([...s1, ...s2])

function setOuterBag(o: Outside, bag: string, outer: string) {
  const outers = o.get(bag) || new Set()
  outers.add(outer)
  o.set(bag, outers)
}

function readBags(): [Inside, Outside] {
  let outside = new Map()
  
  let inside = fs.readFileSync('day7.txt','utf8').split(/\r?\n/).reduce((a, l) => {
    const [outer, s2]  = l.split(" bags contain ")
    let inner = new Map<string, number>()
    if (s2 !== "no other bags.")
      s2.split(', ').map(s => {
        const space = s.indexOf(' ')
        const name = s.slice(space + 1, s.lastIndexOf(' '))
        inner.set(name, +s.slice(0, space))
        setOuterBag(outside, name, outer)
      })
    a.set(outer, inner)

    return a
  }, new Map())

  return [inside, outside]
}

function getOuterBags (o: Outside, bag: string): Set<string> {
  const outers = o.get(bag) || new Set<string>()
  return [...outers.values()].reduce((a, c) => 
    union(a, getOuterBags(o, c)), outers)
}

function countInnerBags (i: Inside, bag: string): number {
  const inners = i.get(bag) || new Map<string, number>()
  return [...inners.entries()].reduce((a, [k, v]) => 
    a + v * countInnerBags(i, k), 1)
}

const [inside, outside] = readBags()

//print(inside)
//print(outside)
print("answer", getOuterBags(outside, "shiny gold").size)
print("answer", countInnerBags(inside, "shiny gold") - 1)

print("------------------")
