import fs = require('fs')

const print = console.log.bind(console)

type Pixel = string
type Tile = Array<Array<Pixel>> // y, x
type ID = number
type Border = string
type Tiles = Map<ID, [Border[], Border[]]> // id => [borders, borders]
type Edges = Map<Border, Set<ID>> // border => ids
type Position = [boolean, number] // flipped, rotations
type Neighbors = Map<ID, [ID[], ID[]]> // id => [ids, ids]
type Image = Array<Array<[ID, Position]>> // y, x
type Content = Map<ID, Tile>

const loadTile = (str: string[]): Tile =>
  str.map(l => <Pixel[]>l.split(''))

const printTile = (t: Tile) =>
  t.forEach(l => print(toString(l)))

const toString = (p: Pixel[]): string => p.join('')

const tileString = (t: Tile): string =>
  t.map(toString).join('')

const tileBorders = (t: Tile): Border[] =>
  [toString(t[0]), toString(t.map(l => l[l.length - 1])), 
  toString([...t[t.length - 1]].reverse()), 
  toString([...t.map(l => l[0])].reverse())]

const flipBorders = (b: Border[]): Border[] =>
  b.map(s => s.split('').reverse().join('')).reverse()

function setEdges(id: ID, bs: Border[], e: Edges = new Map()): Edges {
  bs.forEach(b => {
    const ts = e.get(b) || new Set()
    ts.add(id)
    e.set(b, ts)
  })

  return e
}

function readTiles(): [Tiles, Edges, Content] {
  let e = new Map()
  let c = new Map()
  const ts: Tiles = new Map(fs.readFileSync('day20.txt','utf8')
  .split(/\r?\n\r?\n/).map(tt => {
    const [title, grid] = tt.split(/:\r?\n/)
    const id = +title.slice(5)
    const t = loadTile(grid.split(/\r?\n/))
    const b = tileBorders(t)
    const rb = flipBorders(b)
    setEdges(id, b, e)
    setEdges(id, rb, e)
    c.set(id, tileContent(t))
    return [id, [b, rb]]
  }))

  return [ts, e, c]
}

// return neighbor of 'b' border
const neighbor = (b: Border, id: ID, e: Edges): ID => 
  [...e.get(b).values()].filter(v => v != id)[0]

// return neighbors for 't' tile set when normal or flipped
const tileNeighbors = (t: Tiles, e: Edges): Neighbors =>
  new Map([...t.entries()].map(([k, [b, fb]]) => 
    [k, [b.map(v => neighbor(v, k, e)), fb.map(v => neighbor(v, k, e))]]))

// return count of connecting neighbors in 'n' neighbor set
const connections = (n: ID[]): number => 
  n.filter(v => v).length

// return counts of connecting neighbors for 'n' tile set when normal or flipped
const tileConnCount = (n: Neighbors): [ID, number][] =>
  [...n.entries()].map(([k, v]) => [k, connections(v[0].concat(v[1]))])

// tiles with only 4 connections (normal + flipped) are corners
const corners = (tc: [ID, number][]): ID[] =>
  tc.map(([k, v]) => (v === 4) ? k: undefined).filter(v => v)

// return rotation of the tile to match Up and Left, undefined if can't
function findRotation(up: ID, left: ID, n: ID[]): number {
  for (const [i, e] of n.entries())
    if (e == up) {
      const res = (4 - i) % 4
      if (n[3 - res] === left) 
        return res
    }
  
  return undefined
}

// return tile flip state and rotation to achieve Up and Left neighbors
function findPosition(up: ID, left: ID, [n, fn]: [ID[], ID[]]): Position {
  const off = findRotation(up, left, n)
  return (off != undefined) ? [false, off] : [true, findRotation(up, left, fn)]
}

// only used to get Up or Left neighbor tile from Image
const imageTile = (img: Image, x: number, y: number): ID =>
  (y < 0) ? undefined : (x < 0) ? undefined : img[y][x][0]

// get tile directional neighbor from list, considering tile Position
function getNeighbor([f, r]: Position, [n, fn]: [ID[], ID[]], dir: number = 1): ID {
  const rot = (dir - r + 4) % 4
  return f ? fn[rot] : n[rot]
}

function layTiles(n: Neighbors, start: ID): Image {
  const sz = Math.sqrt(n.size)
  let res = Array.from({length: sz}, () =>
     new Array<[ID, Position]>(sz).fill(undefined))
  let next = start

  res.forEach((l, j) => {
    l.forEach((_, i) => {
      const up = imageTile(res, i, j - 1)
      const left = imageTile(res, i - 1, j)
      const ns = n.get(next) // neighbors
      const pos = findPosition(up, left, ns)
      res[j][i] = [next, pos]
      next = getNeighbor(pos, ns)
    })
    const [tid, tp] = l[0]
    next = getNeighbor(tp, n.get(tid), 2)
  })

  return res
}

const tileContent = (t: Tile): Tile =>
  t.filter((l, j) => (j > 0) && (j < t.length - 1)).map(l =>
    l.filter((_, i) => (i > 0) && (i < l.length - 1)))

const flip = (t: Tile): Tile =>
  t[0].map((_, i) => t.map(r => r[i]))

const rotateOnce = (t: Tile): Tile =>
  t[0].map((_, i) => t.map(r => r[i]).reverse())

function rotate(t: Tile, steps: number = 1): Tile {
  let res = t
  for (let i = 0; i < steps; i++)
    res = rotateOnce(res)
  return res
}

const positionContent = (t: Tile, [f, r]: Position): Tile =>
  rotate(f ? flip(t) : t, r)

function stitch(img: Image, c: Content): Tile {
  const res = img.map(r => r.map(([id, p]) => positionContent(c.get(id), p)))
  return res.reduce((acc, row) => acc.concat(row.reduce((a, c) => 
    a.map((r, i) => r.concat(c[i])))), new Array())
}

function countMonsters(map: Tile): number {
  const d = 1 + map[0].length - 20
  const re = new RegExp("(?=(#.{" + d + "}#.{4}##.{4}##.{4}###.{" + d + 
    "}#.{2}#.{2}#.{2}#.{2}#.{2}#)).", 'g')
  const str = tileString(map)

  let res = 0
  while ((re.exec(str)) != null)
    res++
  return res
}

function findMonsters(map: Tile): number {
  for (let r = 0; r < 2; r++) {
    let m = r ? flip(map) : map
    
    for (let i = 0; i < 3; i++, m = rotate(m)) {
      const res = countMonsters(m)
      if (res)
        return res
    }
  }

  return 0
}

const roughness = (t: Tile, m: number): number =>
  t.reduce((acc, cur) => acc + cur.filter(v => v == '#').length, 0) - m * 15

const tile = loadTile(["123", "456", "789"])
printTile(tile)
printTile(tileContent(tile))

const brd = tileBorders(tile)
print("b", brd)
const fbr = flipBorders(brd)
print("fb", fbr)

print("-----rotate1")
printTile(rotate(tile))
print("-----rotate2")
printTile(rotate(tile, 2))
print("-----flip")
printTile(flip(tile))

print(setEdges(1, brd))
const [tiles, edges, content] = readTiles()
//print(tiles)
//print(edges)

const nbs = tileNeighbors(tiles, edges)
//print(nbs)

const conns = tileConnCount(nbs)
//print(conns)
const corn = corners(conns)
print(corn)
print("A1", corn.reduce((a, c) => a * c))

print("------------------")

print("r", findRotation(0, 3, [0, 1, 2, 3]))
print("r", findRotation(3, 2, [0, 1, 2, 3]))
print("p", findPosition(1, 2, [[0, 1, 2, 3], [3, 2, 1, 0]]))

const img = layTiles(nbs, corn[0])
//print(img)

const map = stitch(img, content)
//printTile(map)
//print(countMonsters(flip(map)))

const monsters = findMonsters(map)
print("monsters:", monsters)
print("A2: ", roughness(map, monsters))