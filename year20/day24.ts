import fs = require("fs")

const print = console.log.bind(console)

type Hex = [number, number] // x, z
type Grid = Set<string>
type Neighbor = Map<string, number>
const neighbor = {
  'nw': [0, -1],
  'ne': [1, -1],
  'e': [1, 0],
  'se': [0, 1],
  'sw': [-1, 1],
  'w': [-1, 0]
}
const SEP = ':'

const h2s = (h: Hex): string =>
  h[0] + SEP + h[1]

const s2h = (s: string): Hex =>
  s.split(SEP).map(v => +v) as Hex

function flip(g: Grid, h: Hex) {
  const s = h2s(h)

  if (g.has(s))
    g.delete(s)
  else
    g.add(s)
}

function dir(s: string, offset: number = 0): [string, number] {
  let ch = s.charAt(offset)
  let off = offset + 1

  if ((ch === 's') || (ch === 'n')) {
    ch += s.charAt(off)
    off++
  }

  return [ch, off]
}

function travel(s: string): Hex {
  let [x, z] = [0, 0]
  let [d, off] = ['', 0]

  do {
    [d, off] = dir(s, off)
    const n = neighbor[d]
    x += n[0]
    z += n[1]
    //print(d, n, [x, z])
  } while (off < s.length)

  return [x, z]
}

function pattern(s: string[]): Grid {
  let g: Grid = new Set()

  s.forEach(v => flip(g, travel(v)))

  return g
}

function readPaths(): string[] {
  return fs.readFileSync('day24.txt', 'utf8').split(/\r?\n/)
}

function blackNeighbors(g: Grid): Neighbor {
  let n = new Map()

  g.forEach(h => {
    Object.entries(neighbor).forEach(([k, v]) => {
      let [x, z] = s2h(h)
      x += v[0]
      z += v[1]
      const s = h2s([x, z])
      const c = (n.get(s) || 0) + 1
      n.set(s, c)
    })
  })

  return n
}

function evolve(g: Grid): Grid {
    const n = blackNeighbors(g)
    return new Set([...n.keys()].filter(k => {
      const c = n.get(k)
      return g.has(k) ? (c == 1) || (c == 2) : (c == 2)
    }))
}

function days(grid: Grid, n: number = 100): number {
  let g = grid
  for (let i = 0; i < n; i++)
    g = evolve(g)

  return g.size
}

let g: Grid = new Set()
g.add(h2s([2, 1]))
g.add(h2s([2, 1]))
print(g, "has", g.has(h2s([2, 1])))

print(dir('see'), dir('see', 2))
print("travel", travel("esew"))
print("travel", travel("nwwswee"))

const paths = readPaths()
const grid = pattern(paths)
print("A1:", grid.size)

print("------------------")

print(s2h("3:-1"))

print("A2:", days(grid))
