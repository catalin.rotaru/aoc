import fs = require('fs')

const print = console.log.bind(console)

const LOOP_MAX = 1000
enum OP {ACC = "acc", JMP = "jmp", NOP = "nop"}

// instruction => [pc, arg]
const op = {
  [OP.ACC]: (arg: number) => [1, arg],
  [OP.JMP]: (arg: number) => [arg, 0],
  [OP.NOP]: (_arg: number) => [1, 0]
}

type Memory = string[]

function loop(m: Memory): number {
  let acc = 0
  let done: Array<boolean> = new Array(m.length).fill(false);

  for (let pc = 0; !done[pc];) {
    const [dpc, dac] = op[m[pc]](+m[pc + 1])
    done[pc] = true
    acc += dac
    pc += 2 * dpc
  }

  return acc
}

function loop2(m: Memory): number {
  let acc = 0
  let count = 0

  for (let pc = 0; (pc < m.length) && (count < LOOP_MAX); count++) {
    const [dpc, dac] = op[m[pc]](+m[pc + 1])
    acc += dac
    pc += 2 * dpc
  }

  return count < LOOP_MAX ? acc : undefined
}

function repair(mem: Memory) {
  let res: number = undefined

  for (let i = 0; i < mem.length; i += 2) {
    const m = Array.from(mem)
    switch (m[i]) {
      case OP.JMP:
        m[i] = OP.NOP
        break
      case OP.NOP:
        m[i] = OP.JMP
        break  
    }

    if (res = loop2(m))
      break
  }

  return res
}

function readProgram(): Memory {
  return fs.readFileSync('day8.txt','utf8').split(/\r?\n| /)
}

const mem = readProgram()
//print(mem)
print(loop(mem))

print("------------------")

print(repair(mem))