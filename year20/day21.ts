import fs = require('fs')

const print = console.log.bind(console)

type Allergen = string
type Ingredient = string
type Food = Array<Ingredient>
type Allergic = Map<Ingredient, Allergen> // allergic ingredients map
type Contained = Map<Allergen, Set<Ingredient>> // allergens possibly contained in listed ingredients

const intersect = (a: Set<any>, b: Set<any>): Set<any> =>
  new Set([...a].filter(v => b.has(v)))

function processFood(ing: Ingredient[], al: Allergen[], c: Contained = new Map()): Contained {
  al.forEach(a => {
    const i = new Set(ing)
    const e = c.get(a)
    c.set(a, e ? intersect(e, i) : i)
  })
  return c
}

function findAllergens(c: Contained) {
  const all = [...c.keys()].sort((a, b) => c.get(a).size - c.get(b).size)
  //print(all)
  all.forEach((v, i) => {
    const a = c.get(v).values().next().value
    for (let j = i + 1; j < all.length; j++)
      c.get(all[j]).delete(a)
  })
}

const getAllergic = (c: Contained): Allergic =>
  new Map([...c.entries()].map(([k, v]) => [v.values().next().value, k]))

function readFood(): [Food[], Contained] {
  const res =  fs.readFileSync('day21.txt','utf8').split(/\r?\n/).
    map(l => l.slice(0, -1).split(' (contains '))

  const c = new Map()
  const food = res.map(([i, a]) => {
    const f = i.split(' ')
    processFood(f, a.split(', '), c)
    return f
  })
  findAllergens(c)

  return [food, c]
}

const [food, cont] = readFood()
//print("food", food)
//print("contained", cont)

const alle = getAllergic(cont)
//print("allergic", alle)

print("A1", food.reduce((acc, cur) => acc + 
  cur.reduce((a, c) => a + +(!alle.has(c)), 0), 0))

print("------------------")

print("A2", [...alle.entries()].sort((a, b) => 
  a[1].localeCompare(b[1])).map(v => v[0]).join(','))