import fs = require('fs')

const print = console.log.bind(console)

type Passport = Map<string, string>
type Validator = (p: Passport) => boolean

const test = {
  'byr': (s: string): boolean => /^\d+$/.test(s) && (+s >= 1920) && (+s <= 2002),
  'iyr': (s: string): boolean => /^\d+$/.test(s) && (+s >= 2010) && (+s <= 2020),
  'eyr': (s: string): boolean => /^\d+$/.test(s) && (+s >= 2020) && (+s <= 2030),
  'hgt': (s: string): boolean => {
    const height = +s.slice(0, -2)
    if (s.endsWith('cm')) 
      return (height >= 150) && (height <= 193)
    if (s.endsWith('in')) 
      return (height >= 59) && (height <= 76)
    return false
  },
  'hcl': (s: string): boolean => /^#(\d|[a-f]){6}$/.test(s),
  'ecl': (s: string): boolean => /^(amb|blu|brn|gry|grn|hzl|oth)$/.test(s),
  'pid': (s: string): boolean => /^\d{9}$/.test(s)
}

function parsePassport(str: string): Passport {
  return str.split(/\r?\n| /).reduce((a, c) => {
    const [n, v] = c.split(':')
    a.set(n, v)
    return a
  }, new Map())
}

const isValid = (p: Passport): boolean =>
  (p.size == 8) || ((p.size == 7) && !p.has('cid'))

const isValid2 = (p: Passport): boolean =>  
  isValid(p) && Object.keys(test).every(k => test[k](p.get(k)))

const countValid = (p: Passport[], v: Validator = isValid): number =>
  p.filter(v).length

function readPassports(): Passport[] {
  return fs.readFileSync('day4.txt','utf8').split(/\r?\n\r?\n/).
    map(l => parsePassport(l))
}

print(parsePassport("hcl:#cfa07d eyr:2025 pid:166559648"))

const p = readPassports()
print(p.length, "passports")

print("valid:", countValid(p))

print("------------------")

print("byr", test['byr']("abcd"), test['byr']("1919"), test['byr']("2002"), test['byr']("2003"))
print("123cm".slice(0, -2))
print("hgt", test['hgt']("60in"), test['hgt']("190cm"), test['hgt']("190in"), test['hgt']("190"))
print("hgt", test['hgt']("12ain"), test['hgt']("1"), test['hgt']("59in"))
print("hcl", test['hcl']("#123abc"), test['hcl']("#123abz"), test['hcl']("123abc"), test['hcl']("#123abcd"))
print("ecl", test['ecl']("brn"), test['ecl']("wat"), test['ecl']("blue"))
print(test['pid']("000000001"), test['pid']("0123456789"))

print("valid2:", countValid(p, isValid2))