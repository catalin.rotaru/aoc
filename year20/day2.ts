import fs = require('fs')

const print = console.log.bind(console)

type Policy = [number, number, string, string] // min, max, char, password
type Validator = (p: Policy) => boolean

function valid([min, max, ch, pass]: Policy): boolean {
  const count = pass.split('').filter(c => (c === ch)).length
  return (count >= min) && (count <= max)
}

function valid2([min, max, ch, pass]: Policy): boolean {
  const second = pass.charAt(max - 1) === ch
  return pass.charAt(min - 1) === ch ? !second : second
}

function readEntries(): Policy[] {
  return fs.readFileSync('day2.txt','utf8').split(/\r?\n/).map(l => {
    const [s1, pass] = l.split(': ')
    const [s2, ch] = s1.split(' ')
    const [min, max] = s2.split('-')

    return [+min, +max, ch, pass]
  })
}

const countValid = (p: Policy[], v: Validator = valid): number =>
  p.filter(v).length

print(valid([1, 3, 'a', "abcde"]))
print(valid([1, 3, 'b', "cdefg"]))
print(valid([2, 9, 'c', "ccccccccc"]))

const ent = readEntries()
print(ent[0])

print("valid:", countValid(ent))

print("------------------")

print(valid2([1, 3, 'a', "abcde"]))
print(valid2([1, 3, 'b', "cdefg"]))
print(valid2([2, 9, 'c', "ccccccccc"]))

print("valid2:", countValid(ent, valid2))