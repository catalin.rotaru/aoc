import fs = require('fs')

const print = console.log.bind(console)

function apply(val: number, mask: string): number {
  const str = val.toString(2)
  const arr = str.padStart(mask.length, '0').split('')
  const res = arr.map((v, i) => {
    const m = mask.charAt(i)
    return (m === 'X') ? v : m
  })
  return parseInt(res.join(''), 2)
}

function apply2(val: number, mask: string): string {
  const str = val.toString(2)
  const arr = str.padStart(mask.length, '0').split('')
  return arr.map((v, i) => {
    const m = mask.charAt(i)
    return (m === '0') ? v : (m === '1') ? '1' : m
  }).join('')
}

function readProgram(): string[][] {
  return fs.readFileSync('day14.txt','utf8').split(/\r?\n/).map(l => l.split(' = '))
}

function execute(p: string[][]): number {
  let mask: string
  let mem: Map<number, number> = new Map()

  p.forEach(([a, b]) => {
    if (a === 'mask')
      mask = b
    else
      mem.set(+a.slice(4, -1), apply(+b, mask))
  })

  return [...mem.values()].reduce((a, c) => a + c, 0)
}

function decode(mask: string, res: number[] = [], idx: number = 0, begin: string = ''): number[] {
  if (idx == mask.length)
    res.push(parseInt(begin, 2))
  else {
    const ch = mask.charAt(idx)
    idx++
    if (ch === 'X') {
      decode(mask, res, idx, begin + '0')
      decode(mask, res, idx, begin + '1')
    } else
      decode(mask, res, idx, begin + ch)
  }

  return res
}

function execute2(p: string[][]): number {
  let mask: string
  let mem: Map<number, number> = new Map()

  p.forEach(([a, b]) => {
    if (a === 'mask')
      mask = b
    else {
      const addr = decode(apply2(+a.slice(4, -1), mask))
      addr.forEach(v => mem.set(v, +b))
    }
  })

  return [...mem.values()].reduce((a, c) => a + c, 0)
}

print(apply(11, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"))

const prog = readProgram()
print("a1", execute(prog))

print("------------------")

print(apply2(42, "000000000000000000000000000000X1001X"))
print(decode("000000000000000000000000000000X1101X"))
print(apply2(26, "00000000000000000000000000000000X0XX"))
print(decode("00000000000000000000000000000001X0XX"))

print("a2", execute2(prog))