import fs = require('fs')

const print = console.log.bind(console)

type Answers = Array<Array<Set<string>>> // group, person, answer
type Operation = (s1: Set<any>, s2: Set<any>) => Set<any>

const union = (s1: Set<any>, s2: Set<any>): Set<any> =>
  new Set([...s1, ...s2])

const intersect = (s1: Set<any>, s2: Set<any>): Set<any> =>
  new Set([...s1].filter(x => s2.has(x)))

function readAnswers(): Answers {
  return fs.readFileSync('day6.txt','utf8').split(/\r?\n\r?\n/).
    map(l => l.split(/\r?\n/).map(s => new Set(s.split(''))))
}

function countAnswers(all: Answers, op: Operation = union): number {
  const groups = all.map(g => g.reduce((a, c) => op(a, c), g[0]))
  return groups.reduce((a, c) => a + c.size, 0)
}

const answers = readAnswers()
//print(answers)
//print(answers[1], answers[2])
print(answers[0][0], answers[1][0], answers[2][0])

print("answers", countAnswers(answers))

print("------------------")

print("answers2", countAnswers(answers, intersect))