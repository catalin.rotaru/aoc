import fs = require('fs')

const print = console.log.bind(console)

function readAdaptors(): number[] {
  const res = fs.readFileSync('day10.txt','utf8').
    split(/\r?\n/).map(l => +l).sort((a, b) => a - b)

  return [0, ...res, res[res.length - 1] + 3]
}

function distribution(adaptors: number[]): number {
  const res = adaptors.reduce((a, c, i) => {
    if (i > 0) {
      const d = c - adaptors[i - 1]
      a.set(d, (a.get(d) || 0) + 1)
    }
    return a
  }, new Map())
  //print(res)

  return res.get(1) * res.get(3)
}

function countChains(adaptors: number[]): number[] {
  const ads = new Set([...adaptors])
  let n = [0, 0, 1, 1]

  for (let i = Math.min(...adaptors) + 2; i <= Math.max(...adaptors); i++) {
    let m = [n[2], 0, n[1] + n[3], 0]

    if (ads.has(i)) {
      m[1] = n[0] + n[2]
      m[3] = n[1] + n[3]
    }
    n = Array.from(m)
  }

  return n
}

const adaptors = readAdaptors()
//print(adaptors)
print(distribution(adaptors))

print("------------------")

print(countChains(adaptors))
