import fs = require('fs')

const print = console.log.bind(console)

enum Tile {TREE = '#', OPEN = '.', OUT = '@'}
type Grid = Array<Array<string>> // y, x

function readGrid(): Grid {
  return fs.readFileSync('day3.txt','utf8').split(/\r?\n/).map(l => l.split(''))
}

const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

function getTile(x: number, y: number, grid: Grid): Tile {
  const len = grid[0].length
  return (y < grid.length) ? <Tile> grid[y][x % len] : Tile.OUT
}

function slide (grid: Grid, dx: number = 3, dy: number = 1): number {
  let trees = 0
  for (let x = 0, y = 0; ;x += dx, y += dy) {
    const ch = getTile(x, y, grid)
    if (ch === Tile.OUT)
      break
    
    if (ch === Tile.TREE)
      trees++
  }

  return trees
}

const grid = readGrid()
//printGrid(grid)

print(getTile(0, 0, grid))
print(getTile(1, 0, grid))
print(getTile(29, 0, grid))
print(getTile(30, 0, grid))
print(getTile(31, 0, grid))
print(getTile(32, 0, grid))
print(getTile(0, 323, grid))

print("answer", slide(grid))

print("------------------")

print("answer", slide(grid) * slide(grid, 1, 1) * slide(grid, 5) * 
  slide(grid, 7) * slide(grid, 1, 2))