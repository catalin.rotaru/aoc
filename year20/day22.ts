import fs = require('fs')

const print = console.log.bind(console)

type Deck = Array<number>
type Config = Set<string>

function round(d1: Deck, d2: Deck) {
  const a = d1.pop() || 0
  const b = d2.pop() || 0

  let res = (a > b) ? d1 : d2
  res.unshift(Math.min(a, b), Math.max(a, b))
}

function game(d1: Deck, d2: Deck) {
  while ((d1.length > 0) && (d2.length > 0))
    round(d1, d2)
}

function round2(d1: Deck, d2: Deck) {
  const a = d1.pop() || 0
  const b = d2.pop() || 0
  const win = ((d1.length >= a) && (d2.length >= b)) ?
    game2(d1.slice(-a), d2.slice(-b)) : (a > b)

  if (win)
    d1.unshift(b, a)
  else
    d2.unshift(a, b)
}

// return true if d1 won
function game2(d1: Deck, d2: Deck): boolean {
  let c: Config = new Set()

  while ((d1.length > 0) && (d2.length > 0)) {
    const s = d1.join() + '|' + d2.join()
    if (c.has(s))
      return true
    c.add(s)

    round2(d1, d2)
  }

  return (d1.length > d2.length)
}

const score = (d: Deck):number =>
  d.reduce((acc, cur, idx) => acc + cur * (idx + 1), 0)

function readGame(): [Deck, Deck] {
  const [s1, s2] = fs.readFileSync('day22.txt','utf8').split(/\r?\n\r?\nPlayer 2:\r?\n/)
  
  const d1 = s1.split(/\r?\n/).map(l => +l)
  const d2 = s2.split(/\r?\n/).map(l => +l)
  d1.shift()

  return [d1.reverse(), d2.reverse()]
}

const od1 = [1, 3, 6, 2, 9]
const od2 = [10, 7, 4, 8, 5]
let deck1 = od1.slice()
let deck2 = od2.slice()
game(deck1, deck2)

print(deck1, deck2)
print(score(deck1), score(deck2))

const [odd1, odd2] = readGame()
let dd1 = odd1.slice()
let dd2 = odd2.slice()
game(dd1, dd2)
print("A1:", score(dd1), score(dd2))

print("------------------")

let dd3 = od1.slice()
let dd4 = od2.slice()
game2(dd3, dd4)
print(dd3, dd4)
print(score(dd3), score(dd4))

let dd5 = odd1.slice()
let dd6 = odd2.slice()
game2(dd5, dd6)
print("A2:", score(dd5), score(dd6))
