import fs = require('fs')

const print = console.log.bind(console)

enum Token {SPACE = ' ', ADD = '+', MUL = '*', OPEN = '(', CLOSE = ')'}

const isDigit = (ch: string) =>
  /^\d$/.test(ch)

// parse single token, return [token, length parsed]
function token(s: string): [string, number] {
  let i = 0
  let ch: string

  if (s.length <= 0)
    return ['', 0]

  // skip starting spaces
  while ((ch = s.charAt(i)) === Token.SPACE) {
    i++
    // reached end of string?
    if (i >= s.length)
      return ['', s.length]
  }

  let begin = i
  i++
  
  // just an operator?
  if (!isDigit(ch))
    return [ch, i]

  // must be a number - add each digit
  while ((i < s.length) && (isDigit(s.charAt(i))))
    i++

  return [s.slice(begin, i), i]
}

// parse expression, return [value, length parsed]
// cannot start or end with operator or parentheses
function parse(s: string): [number, number] {
  let res = 0
  let op = Token.ADD
  let idx = 0
  for (;;) {
    let [t, i] = token(s.slice(idx))
    idx += i
    
    // stop of end of string or closing parentheses
    if (!t || (t === Token.CLOSE)) 
      break
    
      
    if ((t === Token.ADD) || (t === Token.MUL)) {
      op = t
      continue
    }
      
    let val = 0
    // recurse on open parentheses
    if (t === Token.OPEN) {
      [val, i] = parse(s.slice(idx))
      idx += i
    } else // second token is number
      val = +t

    res = (op === Token.ADD) ? res + val : res * val
  }

  return [res, idx]
}

function parseMath(s: string): string {
  let ns: string, os = s
  while ((ns = os.replace(/\d+\+\d+/g, w => eval(w))) !== os)
    os = ns
  return eval(ns)
}

function parseParens(s: string): string {
  let ns: string, os = s
  while ((ns = os.replace(/\(([^()]*)\)/g, (_, p1) => parseMath(p1))) !== os)
    os = ns
  return ns
}

const parse2 = (s: string): string =>
  parseMath(parseParens(s.split(' ').join('')))

function readExpr(): string[] {
  return fs.readFileSync('day18.txt','utf8').split(/\r?\n/)
}

print ("t", token("   "), token(" +- "), token(" 123"), token(" 123) "))

print("12 =>", parse("12"))
print("11 + 22 =>", parse("11 + 22"))
print("(1 + 2) =>", parse("(1 + 2)"))
print("(1 + (2 * 3)) =>", parse("(1 + (2 * 3))"))

print("1 + 2 * 3 + 4 * 5 + 6 =>", parse("1 + 2 * 3 + 4 * 5 + 6"))
print("1 + (2 * 3) + (4 * (5 + 6)) =>", parse("1 + (2 * 3) + (4 * (5 + 6))"))
print("2 * 3 + (4 * 5) =>", parse("2 * 3 + (4 * 5)"))
print("5 + (8 * 3 + 9 + 3 * 4 * 3) =>", parse("5 + (8 * 3 + 9 + 3 * 4 * 3)"))
print("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) =>", parse("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"))
print("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 =>", parse("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"))

const expr = readExpr()
print("A", expr.reduce((a, c) => a + parse(c)[0], 0))

print("------------------")

print("1*2+3*4+5 =", parseMath("1*2+3*4+5"))
print("1 + 2 * 3 + 4 * 5 + 6 =", parseMath("1+2*3+4*5+6"))
print("1 + (2 * 3) + (4 * (5 + 6)) =", parse2("1+(2*3)+(4*(5+6))"))
print("2 * 3 + (4 * 5) =", parse2("2 * 3 + (4 * 5)"))
print("5 + (8 * 3 + 9 + 3 * 4 * 3) =", parse2("5 + (8 * 3 + 9 + 3 * 4 * 3)"))
print("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) =", parse2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"))
print("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 =", parse2("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"))

print("A2", expr.reduce((a, c) => a + +parse2(c), 0))