import fs = require('fs')

const print = console.log.bind(console)

const PREAMBLE = 25

const isSum = (pre: number[], sum: number): boolean =>
  pre.some((a, i) => pre.slice(i + 1).includes(sum - a))

const verify = (ent: number[]): number =>
  ent.slice(PREAMBLE).find((v, i) => !isSum(ent.slice(i, i + PREAMBLE), v))

function findWeakness(ent: number[], sum: number): number {
  for (let i = 0; i < ent.length; i++) {
    let add = ent[i]
    for (let j = i + 1; j < ent.length; j++) {
      add += ent[j]
      if (add == sum) {
        const range = ent.slice(i, j + 1)
        return Math.min(...range) + Math.max(...range)
      } else if (add > sum)
        break
    }
  }
}

function readEntries(): number[] {
  return fs.readFileSync('day9.txt','utf8').split(/\r?\n/).map(l => +l)
}

const entries = readEntries()
//print(ent)
print(isSum([1, 2, 3, 4, 5], 9))

const special = verify(entries)
print(special)

print("------------------")

print(findWeakness(entries, special))
