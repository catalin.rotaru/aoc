import fs = require('fs')

const print = console.log.bind(console)

const departure = (bus: number, time: number): number =>
  time + ((bus - (time % bus)) % bus)

function earliest(time: number, buses: string): number {
  const b = buses.split(',').map(v => +v).filter(v => v)
  const d = b.map(v => departure(v, time))
  const res = Math.min(...d)

  return (res - time) * b[d.indexOf(res)]
}

// a&b must be primes; returns [cycle, offset]
function sync(a: number, k: number, b: number, l: number): [number, number] {
  let i = 0
  for (; (i * a - k + l) % b ; i++);
  return [a * b, i * a - k]
}

function find(buses: string): number {
  const b = buses.split(',').map(v => +v)

  return b.reduce(([c, o], v, i) => v ? sync(c, -o, v, i) : [c, o], [1, 0])[1]
}

print(departure(7, 939), departure(13, 939), departure(59, 939))
print(departure(7, 938))

print(earliest(939, "7,13,x,x,59,x,31,19"))

const buses = "41,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,557,x,29,x,x,x,x,x,x,x,x,x,x,13,x,x,x,17,x,x,x,x,x,23,x,x,x,x,x,x,x,419,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19"
print("a1", earliest(1005595, buses))

print("------------------")

print(sync(3, 0, 5, 1))

print(sync(2, 0, 3, 1))
print(sync(6, -2, 5, 2))
print(sync(30, -8, 7, 3))

print('f', find("2, 3, 5, 7"))
print('f', find("17,x,13,19"))
print('f', find("1789,37,47,1889"))
print('a2', find(buses))

