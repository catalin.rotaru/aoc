import fs = require('fs')

const print = console.log.bind(console)

type Rule = [string, string]      // id, content
type Rules = Map<string, string>  // id => content

function readInput(): [Rules, string[]] {
  const [r, m] = fs.readFileSync('day19.txt','utf8').split(/\r?\n\r?\n/)
  return [new Map(r.split(/\r?\n/).map(l =><Rule>l.split(": "))), 
    m.split(/\r?\n/)]
}

// rule for space-separated ids
const idsRule = (ids: string, raw: Rules, cache: Rules): string =>
  ids.split(' ').map(v => regexRule(v, raw, cache)).join('')

function regexRule(id: string, raw: Rules, cache: Rules = new Map(), v2: boolean = false): string {
  if (cache.has(id))
    return cache.get(id)

  const r = raw.get(id)
  let res = r.startsWith('"') ? r.slice(1, -1) : 
    r.includes('|') ? '(' + r.split(' | ').map(v => 
        idsRule(v, raw, cache)).join('|') + ')' : 
    idsRule(r, raw, cache)

  if (v2 && (id === '0'))
    res = regexRule('42', raw, cache) + '+' + regexRule('31', raw, cache) + '+'

  cache.set(id, res)
  return res
}

const testRule = (r: string, s: string): boolean =>
  new RegExp('^' + r + '$').test(s)

const allValid = (r: string, msg: string[]): string[] =>
  msg.filter(m => testRule(r, m))

function allValid2(r: string, cache: Rules, msg: string[]): string[] {
  const b42 = new RegExp('^' + cache.get('42') + '+', 'g')
  const r42 = new RegExp(cache.get('42'), 'g')
  const b31 = new RegExp(cache.get('31') + '+$', 'g')
  const r31 = new RegExp(cache.get('31'), 'g')
  const valid = allValid(r, msg).filter(m => 
    m.match(b42)[0].match(r42).length > m.match(b31)[0].match(r31).length)
  
  return valid
}

print(/^ab$/.test("abb"))
print(/^(a|b)$/.test("a"))
print(/^a(ab|ba)$/.test("abb"))
print("4 1 | 5".split(' | ').join('|'))

const [rules, msg] = readInput()
//print(rules)
const rule0 = regexRule('0', rules)
//print("r0", rule0)
print(testRule(rule0, 'aaaabb'))
print("A1: ", allValid(rule0, msg).length)

print("------------------")

const ruleCache = new Map()
const rule02 = regexRule('0', rules, ruleCache, true)
print("A2: ", allValid2(rule02, ruleCache, msg).length)