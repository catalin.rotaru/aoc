import fs = require("fs")

const print = console.log.bind(console)

const MAX_CUPS = 1000000

type Next = Array<number> // for each index labeled cup store next cup's label

// increment modulo array length
const inc = (idx: number, arr: number[]): number =>
  (idx + 1) % arr.length

function remove(cups: number[], val: number): number {
  const pos = inc(cups.indexOf(val), cups)
  return cups.splice(pos, 1)[0]
}

function destination(label: number, rem: number[], min: number, max: number): number {
  let l = label
  do {
    l--
    if (l < min)
      l = max
  } while (rem.includes(l))

  return l
}

function move(cups: number[], cur: number = 0): number {
  const min = Math.min(...cups)
  const max = Math.max(...cups)
  const val = cups[cur]

  let rem = []
  for (let i = 0; i < 3; i++)
    rem.push(remove(cups, val))

  const dest = destination(val, rem, min, max)
  const di = cups.indexOf(dest)
  cups.splice(di + 1, 0, ...rem)

  return inc(cups.indexOf(val), cups)
}

function order(cups: number[]): string {
  let res: number[] = []
  const start = cups.indexOf(1)
  for (let i = inc(start, cups); i != start; i = inc(i, cups))
    res.push(cups[i])

  return res.join('')
}

function play(cups: number[], steps: number = 100): string {
  let cur = 0
  for (let i = 0; i < steps; i++)
    cur = move(cups, cur)

  return(order(cups))
}

function init(seed: string, size: number = undefined): [Next, number] {
  const sz: number = size || seed.length
  let res = Array.from({length: sz}, (_, i) => i + 2)

  const sd = seed.split('').map(v => +v)
  res[res.length - 1] = sd[0]

  sd.forEach((v, i) => 
    res[v - 1] = sd[i + 1] || (size ? i + 2 : sd[0]))

  return [res, sd[0]]
}

function remove2(cups: Next, val: number): number[] {
  let res = new Array()
  let  l = val
  for (let i = 0; i < 3; i++) {
    l = cups[l - 1]
    res.push(l)
  }

  cups[val - 1] = cups[l - 1]

  return res
}

function move2(cups: number[], cur: number): number {
  let rem = remove2(cups, cur)
  const dest = destination(cur, rem, 1, cups.length)
  
  const last = rem[rem.length - 1] - 1
  cups[last] = cups[dest - 1]
  cups[dest - 1] = rem[0]

  return cups[cur - 1]
}

function order2(cups: number[], first: number = 1): string {
  let res: number[] = []
  for (let cur = cups[first - 1]; cur != first; cur = cups[cur - 1])
    res.push(cur)

  return res.join('')
}

function play2(cups: number[], first: number, steps: number = 10000000): string {
  let cur = first
  for (let i = 0; i < steps; i++)
    cur = move2(cups, cur)

  if (cups.length == MAX_CUPS) {
    const a = cups[0]
    const b = cups[a - 1]
    return (a * b).toString()
  }

  return order2(cups)
}

print(play([3, 8, 9, 1, 2, 5, 4, 6, 7], 10))
print(play([3, 8, 9, 1, 2, 5, 4, 6, 7]))

print("A1:", play([5, 8, 9, 1, 7, 4, 2, 6, 3]))

print("------------------")

print(play2(...init("389125467"), 10))
print(play2(...init("389125467"), 100))
print("A1:", play2(...init("589174263"), 100))

print(play2(...init("389125467", MAX_CUPS)))
print("A2:", play2(...init("589174263", MAX_CUPS)))
