import fs = require('fs')

const print = console.log.bind(console)

const decode = (s: string): number =>
  parseInt(s.replace(/F|L/g, '0').replace(/B|R/g, '1'), 2)

function readPasses(): number[] {
  return fs.readFileSync('day5.txt','utf8').split(/\r?\n/).map(decode)
}

const findPass = (pass: number[]): number =>
  pass.sort((a, b) => a - b).find((e, i) => e - pass[i - 1] == 2) - 1

print(decode("FBFBBFF"), decode("RLR"), decode("FBFBBFFRLR"))
print(decode("BFFFBBFRRR"))
print(decode("FFFBBBFRRR"))
print(decode("BBFFBBFRLL"))

const pass = readPasses()
//print(pass)
print("max", Math.max(...pass))

print("------------------")

print(pass[0], pass[-1], pass[0] - pass[-1], pass[0] - pass[-1] == 2)
print("pass", findPass(pass))
