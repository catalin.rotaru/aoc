import fs = require('fs')

const print = console.log.bind(console)

type Rule = [[number, number], [number, number]]
type Rules = Map<string, Rule>
type Ticket = Array<number>
type Labels = Map<string, number>

const START = "departure"

function readTickets(): [Rules, Ticket, Ticket[]] {
  const [r, y, n] = fs.readFileSync('day16.txt','utf8').split(/\r?\n\r?\n/)

  const rules = r.split(/\r?\n/).reduce((a, c) => {
    const [field, rule] = c.split(': ')
    a.set(field, rule.split(' or ').map(s =>  s.split('-').map(v => +v)))
    return a
  }, new Map())

  const yours = y.split(/\r?\n/)[1].split(',').map(v => +v)
  const near = n.split(/\r?\n/).slice(1).map(s => s.split(',').map(v => +v))

  return [rules, yours, near]
}

const valid = (val: number, rule: Rule): boolean =>
  ((rule[0][0] <= val) && (rule[0][1] >= val)) || 
  ((rule[1][0] <= val) && (rule[1][1] >= val))

// val has a rule for which it is valid
const hasValid = (val: number, rules: Rule[]): boolean =>
  rules.some(r => valid(val, r))

// all values obey this rule
const allObey = (val: number[], rule: Rule): boolean => 
  val.every(v => valid(v, rule))

function errorRate(tix: Ticket[], r: Rule[]): number {
  const invalid = tix.flat().filter(v => !hasValid(v, r))
  return invalid.reduce((a, c) => a + c, 0)
}

function decode(tix: Ticket[], rules: Rules): Labels {
  const rls = [...rules.values()]
  tix = tix.filter(t => t.every(v => hasValid(v, rls)))
  //print(tix)

  const keys = [...rules.keys()]
  const fields = tix[0].map((_, i) => 
    new Set(keys.filter(k => allObey(tix.map(t => t[i]), rules.get(k)))))
  //print(fields)

  const labels: Labels = new Map()
  let idx: number
  while ((idx = fields.findIndex(v => v.size == 1)) >= 0) {
    const name = fields[idx].values().next().value
    labels.set(name, idx)
    fields.forEach(v => v.delete(name))
  }
  return labels
}

const signature = (l: Labels, yt: Ticket) =>
  [...l.entries()].filter(([k, _]) => k.startsWith(START))
    .reduce((a, c) => a * yt[c[1]], 1)

print(valid(4, [[1, 3], [5, 7]]))

const [rules, yourT, nearT] = readTickets()
//print(rules)
//print(yourT)
//print(nearT)

print("a1", errorRate(nearT, [...rules.values()]))

print("------------------")

const labels = decode(nearT, rules)
//print(labels)

print("a2", signature(labels, yourT))

