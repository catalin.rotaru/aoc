import fs = require("fs")

const print = console.log.bind(console)

const DIV = 20201227
const START = 7

function modExp(a: number, b: number, n:number) {
  a = a % n;
  var result = 1;
  var x = a;

  while(b > 0){
    var leastSignificantBit = b % 2;
    b = Math.floor(b / 2);

    if (leastSignificantBit == 1) {
      result = result * x;
      result = result % n;
    }

    x = x * x;
    x = x % n;
  }
  return result;
}

const crypt = (ls: number, sn: number = START): number =>
  modExp(sn, ls, DIV)

function find(pk: number): number {
  let i = 0
  for (let val = 1; val != pk; i++)
    val = (val * START) % DIV

  return i
}

print(crypt(8))
print(crypt(11))
print(crypt(8, 17807724))
print(crypt(11, 5764801))

print('f', find(5764801))
print('f', find(17807724))

print('a', find(1965712), find(19072108))
print('A', crypt(find(1965712), 19072108))

print("------------------")
