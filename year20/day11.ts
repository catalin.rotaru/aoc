import fs = require('fs')

const print = console.log.bind(console)

enum Tile {FLOOR = '.', EMPTY = 'L', TAKEN = '#'}
type Grid = Array<Array<string>> // y, x
const neighbor = [[0, 1], [0, -1], [1, 0], [-1, 0], [1, 1], [-1, -1], [1, -1], [-1, 1]] // array of [x, y]

function readGrid(): Grid {
  return fs.readFileSync('day11.txt','utf8').split(/\r?\n/).map(l => l.split(''))
}

const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

const countGrid = (g: Grid, k: string = Tile.TAKEN) =>
  g.reduce((a, l) => a + l.filter(v => v === k).length, 0)

// return first visible seat in the given direction
function look(g: Grid, x: number, y: number, dx: number, dy: number, steps: number): Tile {
  for (let i = 0; i < steps; i++) {
    let nx = x + dx * (i + 1)
    let ny = y + dy * (i + 1)

    if ((ny < 0) || (ny >= g.length) || (nx < 0) || (nx >= g[0].length))
      break

    let v = g[ny][nx]
    if (v != Tile.FLOOR)
      return <Tile> v
  }

  return undefined
}

// return on-grid neighbors
const neighbors = (g: Grid, x: number, y: number, steps: number = 1): Tile[] =>
  neighbor.map(([dx, dy]) => look(g, x, y, dx, dy, steps)).filter(v => v)

function occupy(g: Grid, steps: number = 1, level: number = 4): [Grid, boolean] {
  let changed = false

  let res = g.map((l, y) => l.map((v, x) => {
    if (v === Tile.FLOOR)
      return Tile.FLOOR
    const n = neighbors(g, x, y, steps).filter(v => v == Tile.TAKEN).length
    if ((v == Tile.EMPTY) && (n === 0)) {
      changed = true
      return Tile.TAKEN
    } 
    if ((v == Tile.TAKEN) && (n >= level)) {
      changed = true
      return Tile.EMPTY
    }
    return v
  }))

  return [res, changed]
}

function iterate(g: Grid): Grid {
  let changed: boolean
  do {
    [g, changed] = occupy(g)
  } while (changed)
  return g
}

function iterate2(g: Grid): Grid {
  let changed: boolean
  do {
    [g, changed] = occupy(g, Number.POSITIVE_INFINITY, 5)
  } while (changed)
  return g
}

let grid: Grid = readGrid()
//printGrid(grid)
print(neighbors(grid, 0, 0).length, neighbors(grid, 1, 1).length, 
  neighbors(grid, grid[0].length - 1, grid.length - 1).length)

//printGrid(occupy(grid)[0])
//printGrid(occupy(occupy(grid)[0])[0])

print(countGrid(iterate(grid)))

print("------------------")

print(countGrid(iterate2(grid)))