import fs = require('fs')

const print = console.log.bind(console)

function game(list: string, steps: number = 2020): number {
  let n: Map<number, number> = list.split(',').reduce((a, c, i) => {
    a.set(+c, i + 1)
    return a
  }, new Map())

  let last = [...n.keys()][n.size - 1]
  for (let i = n.size + 1; i <= steps; i++) {
    const current = n.has(last) ? i - 1 - n.get(last) : 0
    n.set(last, i - 1) 
    last = current
  }

  return last
}

print(game("0,3,6", 10))
print(game("1,3,2"))

print("a1", game("0,13,1,16,6,17"))

print("------------------")

print(game("0,3,6", 30000000))
print("a2", game("0,13,1,16,6,17", 30000000))