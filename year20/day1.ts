import fs = require('fs')

const print = console.log.bind(console)

function search2(e: number[]) {
  e.forEach((a, i) => {
    const b = 2020 - a
    if (e.slice(i + 1).includes(b))
      print("a:", a, b, a * b)
  })
}

function search3(e: number[]) {
  e.forEach((a, i) =>
    e.slice(i + 1).forEach((b, j) => {
      const c = 2020 - a - b
      if (e.slice(i + j + 1).includes(c))
        print("a2:", a, b, c, a * b * c)
    }))
}

function readEntries(): number[] {
  return fs.readFileSync('day1.txt','utf8').split(/\r?\n/).map(l => +l)
}

const ent = readEntries()
search2(ent)

print("------------------")

search3(ent)
