import fs = require('fs')

const print = console.log.bind(console)

type Vector = Array<number> // x, y, z
type Moons = [Array<Vector>, Array<Vector>] // positions, velocities
type LinearMoons = [Array<number>, Array<number>] // positions, velocities

// [a, b] positions => velocity deltas
function gravity(a: number, b: number): [number, number] {
  return (a == b) ? [0, 0] : (a < b) ? [1, -1] : [-1, 1]
}

function applyGravity(a: number, b: number, va: number, vb: number): [number, number] {
  let [dva, dvb] = gravity(a, b)
  return [va + dva, vb + dvb]
}

// return new Velocities for the two moons
function vectorGravity(p1: Vector, p2: Vector, v1: Vector, v2: Vector): [Vector, Vector] {
  let r1: Vector = new Array()
  let r2: Vector = new Array()

  p1.forEach((p, i) => {
    let [va, vb] = applyGravity(p, p2[i], v1[i], v2[i])
    r1.push(va)
    r2.push(vb)
  })

  return [r1, r2]
}

function applyVelocity(a: number, v: number): number {
    return a + v
}

// return new Position
function vectorVelocity(p: Vector, v: Vector): Vector {
  return p.map((a, i) => applyVelocity(a, v[i]))
}

function run(m: Moons, steps: number = 10): Moons {
  let [p, v] = m
  for (let s = 0; s < steps; s++) {
    // apply gravities to each moon pair
    for (let i = 0; i < p.length; i++)
      for (let j = i + 1; j < p.length; j++) {
        let [va, vb] = vectorGravity(p[i], p[j], v[i], v[j])
        v[i] = va
        v[j] = vb
      }

    // apply velocities
    p = p.map((a, i) => vectorVelocity(a, v[i]))

    //p.map((a, i) => print("p", a, "v", v[i]))
    //print('')
  }

  return [p, v]
}

function step(m: LinearMoons): LinearMoons {
  let [p, v] = m

  // apply gravities to each moon pair
  for (let i = 0; i < p.length; i++)
    for (let j = i + 1; j < p.length; j++) {
      let [va, vb] = applyGravity(p[i], p[j], v[i], v[j])
      v[i] = va
      v[j] = vb
    }

  // apply velocities
  p = p.map((a, i) => applyVelocity(a, v[i]))

  return [p, v]
}

function hash(m: LinearMoons): string {
  let [p, v] = m

  return p.reduce((acc, cur) => acc += cur + ' ', '') + ':' + 
    v.reduce((acc, cur) => acc += cur + ' ', '')
}

function run2(m: Moons): Array<number> {
  let [p, v] = m
  let res: Array<number> = new Array()

  // for each dimension
  for (let d = 0; d < p[0].length; d++) {
    let poz: Array<number> = p.map(val => val[d])
    let vel: Array<number> = v.map(val => val[d])
    //print([poz, vel])
    
    let state: Set<string> = new Set()
    let now: string = hash([poz, vel])

    do {
      [poz, vel] = step([poz, vel])
      state.add(now)
      now = hash([poz, vel])
    } while (!state.has(now))

    res.push(state.size)
  }

  return res
}

function vEnergy(v: Vector): number {
  return v.reduce((acc, cur) => acc + Math.abs(cur), 0)
}

function mEnergy(p: Vector, v: Vector): number {
  return vEnergy(p) * vEnergy(v)
}

function energy(m: Moons): number {
  let [p, v] = m

  return p.reduce((acc, cur, i) => acc + mEnergy(cur, v[i]), 0)
}

const gcd = (a: number, b: number) => a ? gcd(b % a, a) : b;
const lcm = (a: number, b: number) => a * b / gcd(a, b);

print(gravity(3, 5))
print(vectorGravity([3, 3, 3], [5, 5, 5], [1, 1, 1], [2, 2, 2]))
print(vectorVelocity([1, 2, 3], [-2, 0, 3]))

print("------------------")

let m1: Moons = [[[-1, 0, 2], [2, -10, -7], [4, -8, 8], [3, 5, -1]], 
  [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]]

print("energy", energy(run(m1)))

let m2: Moons = [[[-3, 15, -11], [3, 13, -19], [-13, 18, -2], [6, 0, -1]], 
  [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]]

print("energy", energy(run(m2, 1000)))

print("------------------")

let ma: Moons = [[[-1, 0, 2], [2, -10, -7], [4, -8, 8], [3, 5, -1]], 
  [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]]

let cycles = run2(ma)
print ("cycles", cycles, cycles.reduce(lcm))

let mb: Moons = [[[-3, 15, -11], [3, 13, -19], [-13, 18, -2], [6, 0, -1]], 
  [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]]

print ("Repeating:", run2(mb).reduce(lcm))