import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = number[]

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, input: number[], out: number[], base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      write(m, input.shift(), a, rb, p1)
      break
  
    case OP.OUT:
      out.push(read(m, a, rb, p1))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

enum Tile {STATIONARY = 0, PULLED}
type Grid = Array<Array<Tile>> // y, x

class Drone {
  mem: Memory

  grid: Grid

  constructor(mem: Memory, len: number = 10) {
    this.mem = mem

    this.grid = Array.from({length: len}, () => new Array<Tile>(len).fill(Tile.STATIONARY))
  }

  scan(x: number, y: number): number {
    let input = [x, y]
    let out: number[] = []

    let m = this.mem.concat(new Array(100000).fill(0))
    let rb: number = 0
    for (let pc = 0; out.length == 0;)
      [pc, rb] = execute(m, input, out, rb, pc)

    return out[0]
  }

  scanCorners(ox: number, oy: number, len: number) {
    return [[this.scan(ox, oy), this.scan(ox + len - 1, oy)], 
      [this.scan(ox, oy + len - 1), this.scan(ox + len - 1, oy + len - 1)]]
  }

  searchGrid(ox: number, oy: number, len: number) {
    let x = ox
    let y = oy

    while ((this.scan(x, y) != Tile.PULLED) || 
    (this.scan(x + len - 1, y) != Tile.PULLED) || 
    (this.scan(x, y + len - 1) != Tile.PULLED) || 
    (this.scan(x + len - 1, y + len - 1) != Tile.PULLED)) {
      if (this.scan(x + len - 1, y) != Tile.PULLED)
        if (this.scan(x, y + 1)) 
          y++
        else if (this.scan(x + 1, y + 1)) {
          x++
          y++
        }

      if (this.scan(x, y + len - 1) != Tile.PULLED)
        if (this.scan(x + 1, y)) 
          x++
        else if (this.scan(x + 1, y + 1)) {
          x++
          y++
        }
    }

    return [x, y]
  }

  scanGrid() {
    this.grid.forEach((r, y) => r.forEach((_, x) => this.grid[y][x] = this.scan(x, y)))
  }

  printGrid(g: Grid = this.grid) {
    const sh = {[Tile.STATIONARY]: '.', [Tile.PULLED]: '#'}
    g.forEach(r => print(r.map(v => sh[v]).join('')))
  }
}

// program execution loop, ends on halt
function run(mem: Memory) {
  let drone = new Drone(mem, 50)

  drone.scanGrid()
  drone.printGrid()

  print("Pulled", drone.grid.reduce((acc, cur) => acc + cur.filter(v => v == Tile.PULLED).length, 0))
  print("------------------")

  drone.printGrid(drone.scanCorners(3, 4, 2))

  print("Square", drone.searchGrid(3, 4, 100))
}

function readProgram(): Memory {
  return fs.readFileSync('day19.txt','utf8').split(',').map(v => +v)
}

let prog = readProgram()
run(prog)