import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = Array<number>

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, input: Memory, out: Memory, base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      write(m, input.shift(), a, rb, p1)
      break
  
    case OP.OUT:
      let ch = read(m, a, rb, p1)
      out.push(ch)
      process.stdout.write(String.fromCharCode(ch))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

class Droid {
  mem: Memory

  constructor(mem: Memory, size: number = 50) {
    this.mem = mem.concat(new Array(100000).fill(0))
  }

  run(code: string[], end: string = "WALK") {
    let input: Memory = compile(code.concat([end]))
    let out: Memory = []

    let rb: number = 0
    for (let pc = 0; OP.END !== this.mem[pc];)
      [pc, rb] = execute(this.mem, input, out, rb, pc)

    return out[out.length - 1]
  }
}

function readProgram(): Memory {
  return fs.readFileSync('day21.txt','utf8').split(',').map(v => +v)
}

const compile = (code: string[]): Memory => {
  let lines = code.reduce((acc, cur) => acc.concat([cur, String.fromCharCode(10)]), [])
  let res: string[] = lines.reduce((acc, cur) => acc.concat(cur.split('')), [])
  return res.map(v => v.charCodeAt(0))
}

const prog = readProgram()
const droid = new Droid(prog)
const code = [
  "NOT A J",
  "NOT B T",
  "OR T J",
  "NOT C T",
  "OR T J",
  "NOT D T",
  "NOT T T",
  "AND T J"]

print("damage", droid.run(code))

const droid2 = new Droid(prog)
const code2 = [
  "NOT A J",
  "NOT B T",
  "OR T J",
  "NOT C T",
  "OR T J",
  "NOT D T",
  "NOT T T",
  "AND T J",

  "NOT E T",
  "NOT T T",
  "OR H T",
  "AND T J"]

print("damage", droid2.run(code2, "RUN"))