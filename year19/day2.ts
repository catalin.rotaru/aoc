import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, END = 99}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b
}

// returns true while program not finished, false when OP_END was encountered
function execute(m: number[], ip: number) {
  const inst = m[ip]

  m[m[ip + 3]] = op[inst](m[m[ip + 1]], m[m[ip + 2]])
}

function loop(m: number[]) {
  for (let ip = 0; OP.END !== m[ip]; ip += 4)
    execute(m, ip)
}

function run(m: number[]) {
  print(m)
  loop(m)
  print(m)
}

// run with a noun and verb as inputs
function run2(m: number[], n: number, v: number) {
  let mem = Array.from(m)

  mem[1] = n
  mem[2] = v

  loop(mem)

  return mem[0]
}

// run all searching for an output
function search(m: number[], out: number) {
  for (let i = 0; i < 100; i++)
    for (let j = 0; j < 100; j++) {
      if (out === run2(m, i, j)) {
        print(100 * i + j)
        return
      }
    }

}

function readProgram(): number[] {
  return fs.readFileSync('day2.txt','utf8').split(',').map(v => +v)
}

run([1,9,10,3,2,3,11,0,99,30,40,50])
run([2,4,4,5,99,0])
run([1,1,1,4,99,5,6,0,99])

print("------------------")

let m = readProgram()
print(run2(m, 12, 2))

search(m, 19690720)