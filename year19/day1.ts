import fs = require('fs')

const print = console.log.bind(console)

function fuel(mass: number): number {
  return Math.max(0, Math.floor(mass / 3) - 2)
}

// recursively compute the required fuel
function fuel2(mass: number): number {
  let f: number = fuel(mass)
  return f > 0 ? f + fuel2(f) : 0
}

function totalFuel(masses: number[], f: (m: number) => number = fuel): number {
  return masses.map(m => f(m)).reduce((acc, cur) => acc + cur)
}

function readMasses(): number[] {
  return fs.readFileSync('day1.txt','utf8').split(/\r?\n/).map(l => +l)
}

print(fuel(12))
print(fuel(14))
print(fuel(1969))
print(fuel(100756))

const mods = readMasses()
print(totalFuel(mods))

print("------------------")

print(fuel2(14))
print(fuel2(1969))
print(fuel2(100756))

print(totalFuel(mods, fuel2))

