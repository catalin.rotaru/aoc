import fs = require('fs')

const print = console.log.bind(console)

const SIZE = 10007
type Operation = [number, number] // a, b

const mulMod = (a: number, b: number, m: number): number  =>
  Number((BigInt(a % m) * BigInt(b % m)) % BigInt(m))

const divMod = (a: number, b: number, m: number): number  =>
  mulMod(a, invMod(b, m), m)

function powMod(a: number, n: number, m: number): number {
  if (n == 0)
    return 1
  const b = powMod(a, Math.floor(n / 2), m)
  const res = mulMod(b, b, m)
  return (n % 2 == 0) ? res : mulMod(res, a, m)
}

// m must be Prime
const invMod = (a: number, m: number): number => powMod(a, m - 2, m)

const linear = (x: number, a: number, b: number, n: number = SIZE): number =>
  (mulMod(a, x, n) + b + n) % n

const invLinear = (x: number, a: number, b: number, n: number = SIZE): number =>
  divMod(x - b + n, a, n)

function process(x: number, op: Operation[], n: number = SIZE): number {
  return op.reduce((acc, cur) => acc = linear(acc, cur[0], cur[1], n), x)
}

function compose(op: Operation[], n: number = SIZE): Operation {
  // ax + b = v1 => b = v1 for x = 0
  const b = process(0, op, n)
  // ax + b = v2 => a = v2 - b for a = 1
  const a = (process(1, op, n) - b + n) % n

  return [a, b]
}

function repeat(op: Operation, r: number, n: number): Operation {
  const [a, b] = op
  const ra = powMod(a, r, n)
  const rb = divMod(mulMod(b, 1 - ra + n, n), 1 - a + n, n)

  return [ra, rb]
}

function readShuffle(): Operation[] {
  return fs.readFileSync('day22.txt','utf8').split(/\r?\n/).map(l => {
    if (l === 'deal into new stack')
      return [-1, -1]
    const k = +l.split(' ').slice(-1)
    return l.charAt(0) == 'c' ? [1, -k]: [k, 0]
  })
}

print("deal", linear(0, -1, -1, 10), linear(9, -1, -1, 10))
print("cut", linear(0, 1, -3, 10), linear(8, 1, -3, 10))
print("cut", linear(0, 1, 4, 10), linear(8, 1, 4, 10))
print("inc", linear(3, 3, 0, 10), linear(8, 3, 0, 10))

print("------------------")

let shuffle = readShuffle()
const [a, b] = compose(shuffle)
print("a b =" , a, b)

const v = 2019
print(process(v, shuffle))
const p = linear(v, a, b)
print("Position", p, invLinear(p, a, b))

print("------------------")

const n2 = 119315717514047
const sh2 = compose(shuffle, n2)
const [a2, b2] = repeat(sh2, 101741582076661, n2)
const p2 = 2020

print("pow", powMod(7, 2, 5), powMod(101, 67, 13))
print("mod invert", invMod(p2, n2))
print("repeat a b", a2, b2)

const v2 = invLinear(p2, a2, b2, n2)
print("Value", v2, linear(v2, a2, b2, n2))