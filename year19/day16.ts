import fs = require('fs')

const print = console.log.bind(console)

type List = Array<number>

function pattern(base: List, count: number = 1): List {
  let a = Array.from(base, v => new Array(count).fill(v))
  return [].concat.apply([], a)
}

function phaseElement(input: List, p: List): number {
  let res = input.reduce((acc, cur, idx) => acc + cur * p[(idx + 1) % p.length], 0) 
  return Math.abs(res) % 10
}

function phase(input: List, p: List): List {
  return input.map((v, i) => phaseElement(input, pattern(p, i + 1)))
}

function fft(input: List, steps: number = 100): string {
  for (let i = 0; i < steps; i++)
    input = phase(input, [0, 1, 0, -1])

  return input.slice(0, 8).join('')
}

function amplifySignal(o: List, repeat: number = 10000): List {
  const len = o.length
  return Array.from({length: repeat * len}, (_, i) => o[i % len])
}

// second-half-phase computation optimization
function halfPhase(o: List, steps: number = 100): List {
  const len = o.length
  for (let j = 0; j < steps; j++) {
    let sum = 0
    for (let i = len - 1; i >= 0; i--) {
      sum = (sum + o[i]) % 10
      o[i] = sum
    }
  }

  return o
}

function decode(o: List): string {
  const signal = amplifySignal(o)
  let half = signal.slice(signal.length / 2)
  halfPhase(half)
  
  const off = +o.slice(0, 7).join('') - half.length
  return half.slice(off, off + 8).join('')
}

print(pattern([0, 1, 0, -1], 3))
print(phaseElement([9, 8, 7, 6, 5], [1, 2, 3]))
print(phase([1,2,3,4,5,6,7,8], [0, 1, 0, -1]))

print("------------------")

print("fft", fft([1,2,3,4,5,6,7,8], 4))
print("fft", fft("80871224585914546619083218645595".split('').map(v => +v)))
print("fft", fft("19617804207202209144916044189917".split('').map(v => +v)))
print("fft", fft("69317163492948606335995924319873".split('').map(v => +v)))

print("------------------")

const signal = "59773419794631560412886746550049210714854107066028081032096591759575145680294995770741204955183395640103527371801225795364363411455113236683168088750631442993123053909358252440339859092431844641600092736006758954422097244486920945182483159023820538645717611051770509314159895220529097322723261391627686997403783043710213655074108451646685558064317469095295303320622883691266307865809481566214524686422834824930414730886697237161697731339757655485312568793531202988525963494119232351266908405705634244498096660057021101738706453735025060225814133166491989584616948876879383198021336484629381888934600383957019607807995278899293254143523702000576897358".split('').map(v => +v)
print("size", signal.length)
print("FFT", fft(signal))

print("------------------")

print("half-process", halfPhase([5, 6, 7, 8], 4))

print("dec", decode("03036732577212944063491565474664".split('').map(v => +v)))
print("dec", decode("02935109699940807407585447034323".split('').map(v => +v)))
print("dec", decode("03081770884921959731165446850517".split('').map(v => +v)))

print("------------------")

print("Dec", decode(signal))

