import fs = require('fs')

const print = console.log.bind(console)

// map from object to set of its orbiters
type Planets = Set<string>
type Orbits = Map<string, Planets>
type Depths = Map<string, number>

const CENTER = 'COM'

function setOrbit(o: Orbits, planet: string, satellite: string) {
  const p: Planets = o.has(planet) ? o.get(planet) : new Set()
  o.set(planet, p)
  p.add(satellite)
}

function readOrbit(o: Orbits, s: string) {
  let [a, b] = s.split(')')
  setOrbit(o, a, b)
}

function readMap(): Orbits {
  let o: Orbits = new Map()

  fs.readFileSync('day6.txt','utf8').split(/\r?\n/).forEach(v => readOrbit(o, v))

  return o
}

function setDepths(o: Orbits, d: Depths = null, r: string = CENTER, l: number = 0): Depths {
  let dd: Depths = d ? d : new Map()

  dd.set(r, l)
  if (o.has(r))
    o.get(r).forEach(p => setDepths(o, dd, p, l + 1))

  return dd
}


function getNears(o: Orbits): Orbits {
  let n: Orbits = new Map();

  o.forEach((v, k) => v.forEach(p => {
    setOrbit(n, k, p)
    setOrbit(n, p, k)
  }))

  return n
}

function distances(n: Orbits, start: string = "YOU"): Depths {
  let d: Depths = new Map()
  d.set(start, 0)  
  let next: string[] = [start]

  while (next.length > 0) {
    let p = next.shift()

    let near: string[] = [...n.get(p).values()]
    near.forEach(v => {
      const dist: number = d.get(p) + 1
      if (!d.has(v)) {
          d.set(v, dist)
          next.push(v)
      }
    })
  }

  return d
}

const m: Orbits = readMap()
//print(m)
print([...setDepths(m).values()].reduce((acc, cur) => acc + cur))

print("------------------")

const n = getNears(m)
//print(n)
//print(distances(n))
print(distances(n).get("SAN") - 2)

