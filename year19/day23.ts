import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = Array<number>

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, input: Memory, ins: Array<Memory>, out: Memory, nat: Memory, base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      const val = (input.length > 0) ? input.shift() : -1
      write(m, val, a, rb, p1)
      break
  
    case OP.OUT:
      out.push(read(m, a, rb, p1))
      if (out.length >= 3) {
        let [d, x, y] = out

        if (d == 255) {
          if (nat.length == 0)
            print("Sent [dest, x, y]:", out)
          nat[0] = x
          nat[1] = y
        } else {
          const it = ins[d]
          it.push(x)
          it.push(y)
        }

        out.length = 0
      }
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

class Network {
  mem: Array<Memory>
  pc: Array<number>
  rb: Array<number>

  input: Array<Memory>
  out: Array<Memory>
  nat: Memory
  sent: Set<number>

  constructor(mem: Memory, size: number = 50) {
    this.mem = Array.from({length: size}, () => mem.concat(new Array(100000).fill(0)))
    this.pc = new Array(size).fill(0)
    this.rb = new Array(size).fill(0)

    this.input = Array.from({length: size}, (_, i) => [i])
    this.out = Array.from({length: size}, () => new Array())
    this.nat = []
    this.sent = new Set()
  }

  run() {
    for (;;) {
      for (let j = 0; j < 50000; j++) {
        for (let i = 0; i < this.mem.length; i++) {
          [this.pc[i], this.rb[i]] = 
            execute(this.mem[i], this.input[i], this.input, this.out[i], this.nat, this.rb[i], this.pc[i])
        }
      }
      
      let [x, y] = this.nat
      this.input[0].push(x)
      this.input[0].push(y)
      if (this.sent.has(y)) {
        print("Sent again", y)
        break
      } else
        this.sent.add(y)
    }
  }
}

function readProgram(): Memory {
  return fs.readFileSync('day23.txt','utf8').split(',').map(v => +v)
}

const prog = readProgram()
const net = new Network(prog)
net.run()

print("------------------")