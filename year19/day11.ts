import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = number[]

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, io: Io, base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      write(m, io.input(), a, rb, p1)
      break
  
    case OP.OUT:
      io.output(read(m, a, rb, p1))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

enum Paint {DEFAULT = 0, BLACK, WHITE}
enum Dir {UP = 0, RIGHT, DOWN, LEFT}
const MOVE = {
  [Dir.RIGHT]: [1, 0],
  [Dir.LEFT]: [-1, 0],
  [Dir.DOWN]: [0, 1],
  [Dir.UP]: [0, -1]
}

type Grid = Array<Array<Paint>> // y, x

class Io {
  grid: Grid
  x: number
  y: number
  dir: Dir

  outPaint: boolean

  constructor(len: number = 1000, start: Paint = Paint.DEFAULT) {
    this.grid = Array.from({length: len}, () => new Array<Paint>(len).fill(Paint.DEFAULT))

    this.x = len/2
    this.y = len/2
    this.grid[this.y][this.x] = start

    this.dir = Dir.UP

    this.outPaint = true
  }

  input(): number {
    return +(this.grid[this.y][this.x] == Paint.WHITE)
  }

  output(v: number) {
    if (this.outPaint) 
      this.grid[this.y][this.x] = v ? Paint.WHITE : Paint.BLACK
    else {
      this.dir = (this.dir + (v ? 1 : -1) + 4) % 4
      this.x += MOVE[this.dir][0]
      this.y += MOVE[this.dir][1]
    }
    
    this.outPaint = !this.outPaint
  }

  painted(): number {
    return this.grid.reduce((acc, cur) => acc + cur.filter(v => v != Paint.DEFAULT).length, 0)
  }

  printGrid() {
    const sh = {[Paint.DEFAULT]: ' ', [Paint.BLACK]: ' ', [Paint.WHITE]: '█'}
    this.grid.forEach(r => print(r.map(v => sh[v]).join('')))
  }
}

// program execution loop, ends on halt
function run(mem: Memory) {
  let rb = 0
  let io = new Io()
  let m: Memory = Array.from(mem)

  for (let pc = 0; OP.END !== m[pc];)
    [pc, rb] = execute(m, io, rb, pc)

  print("Painted", io.painted())
}

// program execution loop, ends on halt
function run2(mem: Memory) {
  let rb = 0
  let io = new Io(100, Paint.WHITE)
  let m: Memory = Array.from(mem)

  for (let pc = 0; OP.END !== m[pc];)
    [pc, rb] = execute(m, io, rb, pc)

  io.printGrid()
}

function readProgram(): Memory {
  return fs.readFileSync('day11.txt','utf8').split(',').map(v => +v)
}

const io = new Io(4)
print(io.grid)
print(io.input())
;
[1,0, 0,0, 1,0, 1,0, 0,1, 1,0, 1,0].forEach(v => io.output(v))
io.printGrid()
print("painted", io.painted())

print("------------------")

let m = readProgram()
run(m)
run2(m)