import fs = require('fs')

const print = console.log.bind(console)

enum Tile {WALL = '#', SPACE = '.', ENTRY = '@'} // lowercase -> key, upper -> door
type Grid = Array<Array<string>> // y, x
type Keys = Map<string, Cell>
type Cell = [number, number] // x, y
type Cells = Array<Cell>
type Move = [string, number] // key, steps
type Moves = Array<Move>
const FAR = -1
const neighbor = [[0, 1], [0, -1], [1, 0], [-1, 0]] // Cells

const isKey = (s: string) => s.match(/[a-z]/g)
//const isDoor = (s: string) => s.match(/[A-Z]/g)

function readGrid(): Grid {
  return fs.readFileSync('day18.txt','utf8').split(/\r?\n/).map(l => l.split(''))
}

function splitGrid(g: Grid, o: Cell) {
  const mask = ['@#@', '###', '@#@']
  const [y, x] = o

  mask.forEach((r, j) => 
    r.split('').forEach((v, i) => g[y + j - 1][x + i - 1] = v))
}

const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

function getKeys(g: Grid): Keys {
  return g.reduce((acc, r, y) => r.reduce((a, v, x) => {
    if (isKey(v))
      a.set(v, [x, y])
    return a
  }, acc), new Map())
}

function openDoor(g: Grid, k: string): Grid {
  const d = k.toUpperCase()
  return g.map(r => r.map(v => ((v == k) || (v == d)) ? Tile.SPACE : v))
}

const getEntry = (g: Grid): Cell => getEntries(g)[0]

function getEntries(g: Grid): Cells {
  return g.reduce((acc: Cells, r, y) => r.reduce((a, v, x) => {
    if (v == Tile.ENTRY) {
      a.push([x, y])
      g[y][x] = Tile.SPACE
    }
    return a
  }, acc), [])
}

// return on-grid, space/key neighbor cells
function neighbors(g: Grid, x: number, y: number): Cells {
  const res: Cells = neighbor.map(dn => {
    const nx = x + dn[0]
    const ny = y + dn[1]

    const v = g[ny][nx]
    if ((v == Tile.SPACE) || (isKey(v)))
      return [nx, ny]
    
    return undefined
  })

  return res.filter(v => v)
}

function getMoves(g: Grid, kc: Keys, o: Cell): Moves {
  let dist = Array.from({length: g.length}, () => new Array<number>(g[0].length).fill(FAR))
  let next: Cells = [o]
  dist[o[1]][o[0]] = 0
  
  while (next.length > 0) {
    const [x, y] = next.shift() // current cell
    if (g[y][x] != Tile.SPACE)
      continue

    const n = neighbors(g, x, y).filter(([a, b]) => dist[b][a] == FAR)
    const d = dist[y][x] + 1
    n.forEach(([a, b]) => dist[b][a] = d)
    next.push(...n)
  }
  
  let res: Moves = Array.from(kc.entries(), 
    ([k, [x, y]]) => [k, isKey(g[y][x]) ? dist[y][x] : FAR])
  return res.filter(([_, v]) => (v > FAR))
}

const getMoves4 = (g: Grid, kc: Keys, o: Cells): Moves[] =>
  o.map(v => getMoves(g, kc, v))

const hash = (g: Grid, o: Cell|Cells): string => o + g.reduce((acc, cur) => 
  acc.concat(cur.filter(v => isKey(v))), []).sort().join('')
type Cache = Map<string, number>
let cache: Cache = new Map()
let cacheHits = 0

function report(desc: string, count: number, gran: number = 1000): number {
  count++
  if (count % gran == 0)
      print(desc, count)
  return count
}

function collect(g: Grid, kc: Keys, o: Cell): number {
  let h = hash(g, o)
  if (cache.has(h)) {
    cacheHits = report("cache hits", cacheHits, 10000)
    return cache.get(h)
  }

  let m = getMoves(g, kc, o)
  let steps = m.map(([k, s]) => collect(openDoor(g, k), kc, kc.get(k)) + s)
  const res = (steps.length > 0) ? Math.min(...steps) : 0
  
  cache.set(h, res)
  report("cache size", cache.size)
  return res
}

function collect4(g: Grid, kc: Keys, o: Cells): number {
  let h = hash(g, o)
  if (cache.has(h)) {
    cacheHits = report("cache hits", cacheHits, 10000)
    return cache.get(h)
  }

  let m = getMoves4(g, kc, o)
  let steps = m.map((v, i) => v.map(([k, s]) => 
    collect4(openDoor(g, k), kc, Array.from(o, (v, idx) => 
      (i == idx) ? kc.get(k) : v)) + s))
  let fSteps = [].concat(...steps)
  const res = (fSteps.length > 0) ? Math.min(...fSteps) : 0

  cache.set(h, res)
  report("cache size", cache.size)
  return res
}

let grid: Grid = readGrid()
//printGrid(grid)
//printGrid(openDoor(grid, 'a'))

const keys: Keys = getKeys(grid)
//print("keys", keys)

const entry = getEntry(grid)
print("entry", entry)
print("hash", hash(grid, entry))
print("neighbors", neighbors(grid, 1, 1))
print("neighbors", neighbors(grid, 4, 1))
print("moves", getMoves(grid, keys, entry))
print("moves", getMoves(openDoor(grid, 'a'), keys, [7, 1]))

print("------------------")

print("Collect", collect(grid, keys, entry))

print("------------------")

splitGrid(grid, entry)
//printGrid(grid)

const entries = getEntries(grid)
print("entries", entries)
print("hash", hash(grid, entries))
//printGrid(grid)
print("moves4", getMoves4(grid, keys, entries))

print("------------------")

cache.clear()
print("Collect4", collect4(grid, keys, entries))