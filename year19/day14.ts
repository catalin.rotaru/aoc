import fs = require('fs')

const print = console.log.bind(console)

type Ingredient = Map<string, number> // chemical => quantity
type Lab = Map<string, Chemical> // name -> chemical

const ORE: string = "ORE"
const FUEL: string = "FUEL"

class Chemical {
  name: string
  quantity: number
  level: number
  ingredient: Ingredient

  constructor (recipe: string) {
    const [ls, rs] = recipe.split(' => ')
    const [cq, cn] = splitRecipe(rs)
    this.name = cn
    this.quantity = cq;

    this.ingredient = new Map()
    ls.split(', ').forEach(v => {
      let [q, n] = splitRecipe(v)
      if (n)
        this.ingredient.set(n, q)
    })
  }

  getLevel(l: Lab): number {    
    if (!this.level) {
      if (this.name == ORE)
        this.level = 0
      else {
        // max level of ingredients + 1
        let levels = Array.from(this.ingredient.keys(), k => level(l, k))
        this.level = Math.max(...levels) + 1
      }
    }

    return this.level
  }
}

function level(l: Lab, name: string = FUEL): number {
  return l.get(name).getLevel(l)
}

function splitRecipe(s: string): [number, string] {
  let [q, n] = s.split(' ')
  return [+q, n]
}

function readRecipes(): Lab {
  let lab = new Map()
  const str = fs.readFileSync('day14.txt','utf8').split(/\r?\n/)
  str.push(" => 1 ORE")
  str.forEach(s => {
    let c = new Chemical(s)
    lab.set(c.name, c)
  })
  return lab
}

function oreCost(l: Lab, q: number = 1, s: string = FUEL): number {
  let ing: Ingredient = new Map()
  ing.set(s, q)

  do {
    // pick highest level ingredient
    let ms = [...ing.keys()].reduce((acc, cur) => level(l, cur) > level(l, acc) ? cur : acc)
    // reduce it
    let ni = reduce(l, ms, ing.get(ms))
    // merge reduction into ingredients
    ni.forEach((v, k) => ing.set(k, v + (ing.get(k) || 0)))
    // delete it
    ing.delete(ms)
  } while ((ing.size > 1) || (!ing.has(ORE)))

  return ing.get(ORE)
}

// reduce chemical into list of simpler ingredients in needed quantity
// can't be ORE!
function reduce(l: Lab, s: string = FUEL, q: number = 1): Ingredient {
  let c: Chemical = l.get(s)
  let blocks: number = Math.ceil(q / c.quantity)

  let res: Ingredient = new Map(c.ingredient)
  res.forEach((v, k) => res.set(k, v * blocks))

  return res
}

// binary search the amount of fuel doable with <= 1 trillion ore
function searchFuel(l: Lab, q: number = 1000000000000) {
  let min = 1
  let max = 2

  while (oreCost(l, max) < q) {
    min = max
    max *= 2
  }
  print ("interval ", min, max)

  while (max - min > 1) {
    let med = Math.floor((min + max) / 2)
    if (oreCost(l, med) < q)
      min = med
    else
      max = med
  }
  print ("interval ", min, max)
}

let c = new Chemical("1 A, 2 B, 3 C => 2 D")
print(c)
let o = new Chemical(" => 1 ORE")
print(o)

print("------------------")

let l = readRecipes()
print("level", level(l))
//print(l)

//print(reduce(l, "A", 11))
//print(reduce(l, "C", 2))
print("Ore\t", oreCost(l))
print("t ore\t", oreCost(l, 82892753))

print("------------------")

searchFuel(l)