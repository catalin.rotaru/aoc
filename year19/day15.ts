import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = number[]

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, out: number[], input: number, base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      write(m, input, a, rb, p1)
      break
  
    case OP.OUT:
      out.push(read(m, a, rb, p1))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

type Grid = Array<Array<Tile>> // y, x
enum Tile {UNKNOWN = -1, WALL, EMPTY, OXYGEN}
enum Move {NORTH = 1, SOUTH, WEST, EAST}
const MOVE = {
  [Move.EAST]: [1, 0],
  [Move.WEST]: [-1, 0],
  [Move.SOUTH]: [0, 1],
  [Move.NORTH]: [0, -1]
}

class Droid {
  m: Memory
  pc: number
  rb: number
  grid: Grid
  x: number
  y: number

  constructor(mem: Memory, len: number = 1000) {
    this.m = mem.concat(new Array(10000).fill(0))
    this.pc = 0
    this.rb = 0

    this.grid = Array.from({length: len}, () => new Array<Tile>(len).fill(Tile.UNKNOWN))
    this.x = Math.floor(len/2)
    this.y = Math.floor(len/2)
    this.grid[this.y][this.x] = Tile.EMPTY
  }

  // u: move only if unexplored
  // returns if actually moved
  move(dir: Move, u: boolean = false): boolean {
    if (u) {
      let nx = this.x + MOVE[dir][0]
      let ny = this.y + MOVE[dir][1]

      if (this.grid[ny][nx] != Tile.UNKNOWN)
        return false
    }

    let out: number[] = []

    while (out.length == 0)
      [this.pc, this.rb] = execute(this.m, out, dir, this.rb, this.pc)

    let status = out[0]
    let nx = this.x + MOVE[dir][0]
    let ny = this.y + MOVE[dir][1]

    this.grid[ny][nx] = status

    if (status == Tile.WALL)
      return false

    this.x = nx
    this.y = ny
    return true
  }

  explore() {
    let dirs: Move[] = [Move.NORTH, Move.EAST, Move.SOUTH, Move.WEST]

    dirs.forEach(d => {
      if (this.move(d, true)) {
        this.explore()
        this.move(d < 3 ? 3 - d : 7 - d)
      }
    })
  }

  printGrid() {
    const sh = {[Tile.UNKNOWN]: ' ', [Tile.EMPTY]: '.', [Tile.WALL]: '#', [Tile.OXYGEN]: 'O'}
    this.grid.forEach(r => print(r.map(v => sh[v]).join('')))
  }
}

type Distances = Array<Array<number>>

const FAR = -1

function distances(g: Grid, x: number = -1, y: number = -1, dist: Distances = null, d: number = 0): Distances {
  if (x < 0)
    x = Math.floor(g[0].length / 2)
  if (y < 0)
    y = Math.floor(g.length / 2)

  if (g[y][x] == Tile.WALL)
    return

  dist = dist || Array.from({length: g.length}, () => new Array<Tile>(g[0].length).fill(FAR))
  if (dist[y][x] == FAR) 
    dist[y][x] = d

  let visit: Array<[number, number]> = []
  d++

  for (let k in MOVE) {
    let [dx, dy] = MOVE[k]
    let nx = x + dx
    let ny = y + dy

    if (dist[ny][nx] == FAR) {
      dist[ny][nx] = d
      visit.push([nx, ny])
    }
  }

  visit.forEach(([nx, ny]) => distances(g, nx, ny, dist, d))

  return dist
}

function readProgram(): Memory {
  return fs.readFileSync('day15.txt','utf8').split(',').map(v => +v)
}

function findOxygen(g: Grid): [number, number] {
  let dist: Distances = distances(g)

  let x: number
  let y = g.findIndex(r => (x = r.findIndex(v => v == Tile.OXYGEN)) != -1)

  print ("Oxy", [x, y], dist[y][x])

  return [x, y]
}

function fillOxygen(g: Grid, x: number, y: number): number {
  let dist: Distances = distances(g, x, y)
  
  return Math.max(...[].concat.apply([], dist))
}

let m = readProgram()
let d = new Droid(m, 100)
d.explore()
//d.printGrid()

print("------------------")

let [ox, oy] = findOxygen(d.grid)
print("Full", fillOxygen(d.grid, ox, oy))