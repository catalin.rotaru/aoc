import fs = require('fs')

const print = console.log.bind(console)

function gen(start: number = 1, len: number = 6, base: number = 0): number {
  let count = 0

  for (let i = start; i <= 9; i++) {
    let pass: number = base + i

    if (len > 1)
      count += gen(i, len - 1, pass * 10)
    else if (valid(pass))
      count++
  }

  return count
}

function valid(pass: number): boolean {
  if ((pass < 153517) || (pass > 630395))
    return false
  
  return double2(pass)
}

function double(n: number, p: number = 0): boolean {
  const cur: number = n % 10

  if (cur == p)
    return true
  else if (n >= 10)
    return double(Math.floor(n / 10), cur)
  else
    return false
}


function double2(n: number, p: number = 0, pp: number = 0): boolean {
  const cur: number = n % 10

  if ((cur == p) && (p != pp)) {
    if (n < 10)
      return true
    else
      if (cur != Math.floor(n / 10) % 10)
        return true
      else
        return double2(Math.floor(n / 10), cur, p)
  } else if (n >= 10)
    return double2(Math.floor(n / 10), cur, p)
  else
    return false
}

print(double(1234))
print(double(1134))
print(double(1224))
print(double(1233))

print("------------------")

print("count", gen())

print("------------------")

print(double2(112233))
print(double2(123444))
print(double2(111122))
print(double2(112222))
