import fs = require('fs')

const print = console.log.bind(console)

type Point = [number, number] // x, y
type Segment = [Point, Point] // a, b
type Wire = Array<Segment>    // first segment starts from origin 0,0

// map from direction to (x, y)
// origin in top left
const DIR = {
  'R': [1, 0],
  'L': [-1, 0],
  'D': [0, 1],
  'U': [0, -1]
}

function computeSegment(path: string, origin: Point = [0, 0]): Segment {
  let d = DIR[path.charAt(0)]
  let l = +path.slice(1)

  return [origin, [origin[0] + l * d[0], origin[1] + l * d[1]]]
}

function computeWire(path: string): Wire {
  let w: Wire = new Array()
  let o: Point = [0, 0]

  path.split(',').forEach(l => {
    let s = computeSegment(l, o)
    w.push(s)
    o = s[1]
  })

  return w
}

function traceSegment(s: Segment): Point[] {
  let [a, b] = s
  let dx = b[0] - a[0]
  dx = dx == 0 ? 0 : Math.sign(dx)
  let dy = b[1] - a[1]
  dy = dy == 0 ? 0 : Math.sign(dy)
  //print("deltas", dx, dy)

  let res: Array<Point> = new Array<Point>() 

  for (let i = a[0], j = a[1]; (i != b[0]) || (j != b[1]); i += dx, j += dy)
    res.push([i + dx, j + dy])

  return res
}

function traceWire(w: Wire): Point[] {
  return [].concat.apply([], w.map(traceSegment))
}

function onSegment(p: Point, s: Segment): boolean {
  let a: Point = s[0]
  let b: Point = s[1]

  // vertical segment
  if (a[0] === b[0])
    return (a[0] === p[0]) && 
      (Math.min(a[1], b[1]) <= p[1]) && (Math.max(a[1], b[1]) >= p[1])

  // horizontal segment, a[1] === b[1]
  return (a[1] === p[1]) && 
    (Math.min(a[0], b[0]) <= p[0]) && (Math.max(a[0], b[0]) >= p[0])
}

function onWire(p: Point, w: Wire): boolean {
  return w.some(v => onSegment(p, v))
}

function manhattan(a: Point, b: Point = [0, 0]): number {
  return Math.abs(a[0] - b[0]) + Math.abs(a[1] - b[1])
}

function closestIntersection(w1: Wire, w2: Wire): Point {
  const is: Point[] = traceWire(w1).filter(p => onWire(p, w2))

  return is.sort((a, b) => manhattan(a) - manhattan(b))[0]
}

const OUTSIDE: number = -1

function stepsOnSegment(p: Point, s: Segment): number {
  if (onSegment(p, s))
    return manhattan(s[0], p)
  else return OUTSIDE
}

function stepsOnWire(p: Point, w: Wire): number {
  let res: number = 0;

  for (const s of w) {
    let d = stepsOnSegment(p, s)
    if (d == OUTSIDE)
      res += manhattan(s[0], s[1])
    else
      return res + d
  }

  return OUTSIDE
}

function steps(p: Point, w1: Wire, w2: Wire): number {
  const s1 = stepsOnWire(p, w1)
  const s2 = stepsOnWire(p, w2)

  return (s1 == OUTSIDE) || (s2 == OUTSIDE) ? OUTSIDE : s1 + s2
}

function shortestIntersection(w1: Wire, w2: Wire): Point {
  const is: Point[] = traceWire(w1).filter(p => onWire(p, w2))

  return is.sort((a, b) => steps(a, w1, w2) - steps(b, w1, w2))[0]
}

function readWires(): Wire[] {
  return fs.readFileSync('day3.txt','utf8').split(/\r?\n/).map(computeWire)
}

print("U4", computeSegment("U4"))
print("D5", computeSegment("D5"))
print("L2", computeSegment("L2"))
print("R3", computeSegment("R3"))

print("on", onSegment([1, 3], [[1, 2], [1, 1]]))
print("on", onSegment([1, 3], [[2, 3], [0, 3]]))

print("trace", traceSegment(computeSegment("D7")))
print("t wire", traceWire(computeWire("R3,D2,L5,U5")))

print("------------------")

const w1 = computeWire("R8,U5,L5,D3")
print("w1", w1)
const w2 = computeWire("U7,R6,D4,L4")
print("w2", w2)
const is: Point[] = traceWire(w1).filter(p => onWire(p, w2))

print("intersect", is)
print("closest", is.sort((a, b) => manhattan(a) - manhattan(b))[0])

print("------------------")

const w3 = computeWire("R75,D30,R83,U83,L12,D49,R71,U7,L72")
const w4 = computeWire("U62,R66,U55,R34,D71,R55,D58,R83")
print("closest", manhattan(closestIntersection(w3, w4)))

const w5 = computeWire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51")
const w6 = computeWire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
print("Closest", manhattan(closestIntersection(w5, w6)))

const [wa, wb] = readWires()
//print("wa len", traceWire(wa).length)
//print("wb len", traceWire(wb).length)
print("closest", manhattan(closestIntersection(wa, wb)))

print("------------------")

print("steps", stepsOnSegment([0, -2], computeSegment("U4")))
print("steps", steps([3, -3], w1, w2))

print("shortest", steps(shortestIntersection(w1, w2), w1, w2))
print("shortest", steps(shortestIntersection(w3, w4), w3, w4))
print("shortest", steps(shortestIntersection(w5, w6), w5, w6))
print("Shortest", steps(shortestIntersection(wa, wb), wa, wb))
