import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = number[]

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, out: number[], input: number[], base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      if (input.length == 0)
        return [pc, rb]
      write(m, input.shift(), a, rb, p1)
      break
  
    case OP.OUT:
      out.push(read(m, a, rb, p1))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

// program execution loop, ends on halt
function loop(m: Memory, input: number[]): number[] {
  let rb = 0
  let out = new Array<number>()
  for (let pc = 0; OP.END !== m[pc];)
    [pc, rb] = execute(m, out, input, rb, pc)

  return out
}

function run(m: Memory, input: number[] = []): number[] {
  //print(m)
  let mem = m.concat(new Array(1000000).fill(0))
  let out = loop(mem, input)
  //print(m)
  return out
}

function sP(p: number, m: Mode): string {
  switch (m) {
    case Mode.POS: return "[" + p + "]"
    case Mode.REL: return "[RB " + (p>=0 ? '+ ' : '- ') + Math.abs(p) + "]"
    default: return '' + p
  }
}

function codeString(m: Memory, pc: number): [string, number] {
  const [inst, p1, p2, p3] = decode(m[pc])

  let res = OP[inst]

  switch (inst) {
    case OP.ADD:
      res =  sP(m[pc + 3], p3) + ' = ' + sP(m[pc + 1], p1) + ' + ' + sP(m[pc + 2], p2)
      break
    case OP.MUL:
      res =  sP(m[pc + 3], p3) + ' = ' + sP(m[pc + 1], p1) + ' * ' + sP(m[pc + 2], p2)
      break
    case OP.IN:
      res = sP(m[pc + 1], p1) + " = INPUT"
      break
    case OP.OUT:
      res = sP(m[pc + 1], p1) + " ==> OUTPUT"
      break
    case OP.JT:
      res = "if (" + sP(m[pc + 1], p1) + ") goto " + sP(m[pc + 2], p2)
      break
    case OP.JF:
      res = "if (" + sP(m[pc + 1], p1) + " == 0) goto " + sP(m[pc + 2], p2)
      break
    case OP.LT:
      res =  sP(m[pc + 3], p3) + ' = (' + sP(m[pc + 1], p1) + ' < ' + sP(m[pc + 2], p2) + ')'
      break
    case OP.EQ:
      res =  sP(m[pc + 3], p3) + ' = (' + sP(m[pc + 1], p1) + ' == ' + sP(m[pc + 2], p2) + ')'
      break
    case OP.RBO:
      res = "RB += " + sP(m[pc + 1], p1)
      break
    case OP.END:
      res = "EXIT"
      break
    default:
      res = '' + inst
  }

  return [res, op_len[inst] || 1]
}

function codePrint(m: Memory) {
  for (let i = 0; i < m.length;) {
    const [s, l] = codeString(m, i)
    print(i + "\t" + s)
    i += l
  }
}

function readProgram(): Memory {
  return fs.readFileSync('day9.txt','utf8').split(',').map(v => +v)
}

print(run([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]))
print(run([1102,34915192,34915192,7,4,7,99,0]))
print(run([104,1125899906842624,99]))

print("------------------")

codePrint([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99])
print('')
codePrint([1102,34915192,34915192,7,4,7,99,0])
print('')
codePrint([104,1125899906842624,99])

print("------------------")

let m = readProgram()
print("BOOST", run(m, [1]))
print("COORD", run(m, [2]))

print("------------------")

//codePrint(m)