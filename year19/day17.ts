import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = number[]

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, io: Io, base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      write(m, io.input(), a, rb, p1)
      break
  
    case OP.OUT:
      io.output(read(m, a, rb, p1))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

type Grid = Array<Array<number>> // y, x

enum Tile {NEWLINE = 10, SCAFFOLD = 35, SPACE = 46, INTERSECTION = 79, RL = 60, RR = 62, RU = 94, RD = 118}
const DIR = [Tile.RU, Tile.RR, Tile.RD, Tile.RL]
const STEP = [[0, -1], [1, 0], [0, 1], [-1, 0]]

class Io {
  grid: Grid
  cmd:  string

  constructor() {
    this.grid = new Array()
    this.grid.push(new Array())
  }

  input(): number {
    process.stdout.write(this.cmd[0])
    let res = this.cmd.charCodeAt(0)
    this.cmd = this.cmd.substring(1)
    return res
  }

  output(v: number) {
    if (v > 255)
      print(v)
    else
      process.stdout.write(String.fromCharCode(v))

    if (v == Tile.NEWLINE)
      this.grid.push(new Array())
    else
      this.grid[this.grid.length - 1].push(v)
  }

  printGrid() {
    let g = this.grid.map((r, y) => r.map((v, x) => this.isIntersection(x, y) ? Tile.INTERSECTION : v))

    g.forEach(r => print(r.map(v => String.fromCharCode(v)).join('')))
  }

  isIntersection(x: number, y: number): boolean {
    for (let i = -1; i <= 1; i++)
      if ((this.get(x, y + i) == Tile.SPACE) || (this.get(x + i, y) == Tile.SPACE))
        return false

    return true
  }

  calibrate(): number {
    let g: Grid = this.grid.map((r, y) => r.map((v, x) => this.isIntersection(x, y) ? x*y : 0))
    return g.reduce((acc, cur) => acc + cur.reduce((a, c) => a + c, 0), 0)
  }

  findRobot() {
    const bot: number[] = [Tile.RL, Tile.RR, Tile.RU, Tile.RD]
    let x: number
    let y: number = this.grid.findIndex(r => {
      x = r. findIndex(v => bot.includes(v))
      return x > 0
    })

    return [x, y]
  }

  get(x: number, y: number): Tile {
    if ((y < 0) || (y >= this.grid.length) || (x < 0) || (x >= this.grid[0].length))
      return Tile.SPACE

    return this.grid[y][x]
  }

  advance(x: number, y: number, d: number): [number, number, number] {
    let res = 0
    let [dx, dy] = STEP[d]

    while (this.get(x + dx, y + dy) != Tile.SPACE) {
      x += dx
      y += dy
      res++
    }

    return [x, y, res]
  }

  turn(x: number, y: number, d: number): [number, string] {
    let nd: number[] = [-1, 1].map(v => (d + v + DIR.length) % DIR.length)
    let res = nd.findIndex(v => {
      let [dx, dy] = STEP[v]
      if (this.get(x + dx, y + dy) != Tile.SPACE) 
        return true
    })

    if (res >= 0)
      return [nd[res], (res > 0) ? 'R' : 'L']

    return [-1, '']
  }

  travel(x: number, y: number, d: number = 0): string {
    let res = ''
    do {
      let steps: number
      [x, y, steps] = this.advance(x, y, d)

      if (steps)
        res += steps + ','

      let s: string
      [d, s] = this.turn(x, y, d)
      if (d >= 0)
        res += s + ','
    } while (d >= 0)

    return res.slice(0, res.length - 1)
  }

  visit():string {
    let [x, y] = this.findRobot()
    let dir = DIR.findIndex(v => v == this.grid[y][x])
    print("bot", String.fromCharCode(DIR[dir]), [x, y])

    return this.travel(x, y, 0)
  }
}

function prepare(s: string): string {
  const a = 'R,6,L,10,R,8,R,8'
  const b = 'R,12,L,8,L,10'
  const c = 'R,12,L,10,R,6,L,10'
  var ra = new RegExp(a, 'g');
  var rb = new RegExp(b, 'g');
  var rc = new RegExp(c, 'g');

  let res = s.replace(ra, 'A').replace(rb, 'B').replace(rc, 'C')

  return res + '\n' + a + '\n' + b + '\n' + c + '\n' + 'n\n'
}

function loop(mem: Memory, io: Io) {
  let rb = 0
  let m: Memory = mem.concat(new Array(10000).fill(0))

  for (let pc = 0; OP.END !== m[pc];)
    [pc, rb] = execute(m, io, rb, pc)
}

// program execution loop, ends on halt
function run(mem: Memory) {
  let io = new Io()
  loop(mem, io)

  io.grid = io.grid.filter(r => r.length > 0)

  //io.printGrid()
  print("C", io.calibrate())

  print("------------------")
  
  let path: string = io.visit()
  //print("path:", path)
  io.cmd = prepare(path)
  //print(io.cmd)

  mem[0] = 2
  loop(mem, io)
}

function readProgram(): Memory {
  return fs.readFileSync('day17.txt','utf8').split(',').map(v => +v)
}

let m = readProgram()
run(m)
