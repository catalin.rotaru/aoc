import fs = require('fs')

const print = console.log.bind(console)

enum Color {Black = 0, White, Transparent}
type Layer = Color[]
type Image = Layer[]

function readLayers(w: number, h: number, data: number[]): Image {
  const l: number = w * h

  var r = Array(Math.ceil(data.length / l)).fill(0);

  return r.map((_, i) => data.slice(i * l, l * (i + 1)))
}

function count(layer: Layer, digit: number): number {
  return layer.filter(v => v == digit).length
}

function fewest(img: Image, digit: number = 0): Layer {
  return [...img].sort((a, b) => count(a, digit) - count(b, digit))[0]
}

function checksum(img: Image): number {
  const layer: Layer = fewest(img)
  //print(layer)
  return count(layer, 1) * count(layer, 2)
}

function readImage(): Image {
  return readLayers(25, 6, 
    fs.readFileSync('day8.txt','utf8').split('').map(v => +v))
}

function color(px: number[]): Color {
  return px.find(v => (v == Color.Black) || (v == Color.White))
}

function render(img: Image): Layer {
  return img[0].map((_, i) => color(img.map(l => l[i])))
}

function printImage(l: Layer, w: number): string[] {
  var r = Array(l.length / w).fill(0)
  var rows = r.map((_, i) => l.slice(i * w, w * (i + 1)))
  //print (rows)

  return rows.map(r => r.map(c => c ? '█' : ' ').join(''))
}

const img = readLayers(3, 2, "123456789012".split('').map(v => +v))
print (img)
print (checksum(img))

const image = readImage()
print ("crc", checksum(image))

print("------------------")

print("color", color([2, 2, 0, 1, 2]))
const img0 = readLayers(2, 2, "0222112222120000".split('').map(v => +v))

printImage(render(img0), 2).forEach(v => print(v))

print("------------------")
printImage(render(image), 25).forEach(v => print(v))