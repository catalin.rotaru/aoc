import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, END = 99}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.END]: 1
}

// returns next PC
function execute(m: number[], out: number[], pc: number = 0, input: number = 0) {
  const [inst, p1, p2, p3] = decode(m[pc])
  const a = m[pc + 1]

  if (inst == OP.IN)
    m[a] = input
  else if (inst == OP.OUT)
    out.push(p1 ? a : m[a])
  else if (inst == OP.JT) {
    const b = m[pc + 2]
    if ((p1 ? a : m[a]) != 0)
      return p2 ? b : m[b]
  } else if (inst == OP.JF) {
    const b = m[pc + 2]
    if ((p1 ? a : m[a]) == 0)
      return p2 ? b : m[b]
  } else {
    const b = m[pc + 2]
    const c = m[pc + 3]

    m[c] = op[inst](p1 ? a : m[a], p2 ? b : m[b])
  }

  return pc + op_len[inst]
}

// decode opcode => inst, p1, p2, p3 (true if immediate)
function decode(op: number): [number, boolean, boolean, boolean] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1 != 0, p2 != 0, p3 != 0]
}

function loop(m: number[], input: number = 0): number[] {
  let out = new Array<number>()
  for (let pc = 0; OP.END !== m[pc];)
    pc = execute(m, out, pc, input)

  return out
}

function run(m: number[], input: number = 0): number {
  //print(m)
  let mem = Array.from(m)
  let out = loop(mem, input)
  //print(m)
  return out[out.length - 1]
}

function readProgram(): number[] {
  return fs.readFileSync('day5.txt','utf8').split(',').map(v => +v)
}

print(op_len[99])
print("dec", decode(1002))
print("dec", decode(1))
print("dec", decode(11103))

print("------------------")

//run([1,9,10,3,2,3,11,0,99,30,40,50])
//run([2,4,4,5,99,0])
//run([1,1,1,4,99,5,6,0,99])
//print("------------------")

let m = readProgram()
print("diag", run(m, 1))

print("------------------")

print("test", run([3,9,8,9,10,9,4,9,99,-1,8], 8))
print("test", run([3,9,7,9,10,9,4,9,99,-1,8], 7))
print("test", run([3,3,1108,-1,8,3,4,3,99], 8))
print("test", run([3,3,1107,-1,8,3,4,3,99], 7))
print("test", run([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], -1))
print("test", run([3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 1))
print("test", run([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
  1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
  999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], 8))

print("diag", run(m, 5))
