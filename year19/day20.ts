import fs = require('fs')

const print = console.log.bind(console)

enum Tile {WALL = '#', OPEN = '.'}
type Grid = Array<Array<string>> // y, x
type Portals = Map<string, [string, string, number]> // "x,y" => [label, "x,y", level]
type Cell = [number, number] // x, y
type Cells = Array<Cell>
type Cell3 = [number, number, number] // x, y, z
type Cells3 = Array<Cell3>
const FAR = -1
const neighbor = [[0, 1], [0, -1], [1, 0], [-1, 0]] // array of [x, y]

// return on-grid neighbors
function neighbors(g: Grid, x: number, y: number): Cells {
  const res: Cells = neighbor.map(v => {
    const nx = x + v[0]
    const ny = y + v[1]
    if ((nx < 0) || (nx >= g[0].length) || (ny < 0) || (ny >= g.length))
      return undefined
    return [nx, ny]
  })
  return res.filter(v => v)
}

function readGrid(): Grid {
  return fs.readFileSync('day20.txt','utf8').split(/\r?\n/).map(l => l.split(''))
}

function readLabel(g: Grid, x: number, y: number, x1: number, y1: number): string {
  let x2 = 2 * x1 - x
  let y2 = 2 * y1 - y

  return ((x1 <= x2) && (y1 <= y2)) ? g[y1][x1] + g[y2][x2] : g[y2][x2] + g[y1][x1]
}

const hash = (a: number, b: number): string => a + ',' + b
const unHash = (s: string): number[] => s.split(',').map(v => +v)

function findEntry(p: Portals, l: string): string {
  return [...p.entries()].find(([_, v]) => v[0] == l)[0]
}

function findExit(p: Portals, entry: string): string {
  const l = p.get(entry)[0]
  const res = [...p.entries()].filter(([k, v]) => (v[0] == l) && (k != entry))
  
  return (res.length > 0) ? res[0][0] : undefined
}

function portalLevel(g: Grid, x: number, y: number): number {
  if ((x == 2) || (x == g[0].length - 3) || (y == 2) || (y == g.length - 3))
    return -1
  return 1
}

function readPortals(g: Grid): Portals {
  let res: Portals = new Map()

  g.forEach((r, y) => r.forEach((v, x) => {
    if (v == Tile.OPEN) {
      const n = neighbors(g, x, y)

      // letter neighbor?
      const la = n.find(([a, b]) => g[b][a].match(/[A-Z]/i))
      if (la) {
        const label = readLabel(g, x, y, la[0], la[1])
        const level = portalLevel(g, x, y)
        res.set(hash(x, y), [label, undefined, level])
      }
    }
  }))

  res.forEach((v, k) => res.set(k, [v[0], findExit(res, k), v[2]]))
  return res
}

function getSteps(g: Grid, p: Portals, ox: number, oy: number): Array<Array<number>> {
  let s = Array.from({length: g.length}, () => new Array<number>(g[0].length).fill(FAR))
  s[oy][ox] = 0
  let v: Cells = []
  v.push([ox, oy])

  while (v.length > 0) {
    const [x, y] = v.shift() // current cell
    
    let n: Cells = neighbors(g, x, y).filter(([a, b]) => g[b][a] == Tile.OPEN)
    
    const l = hash(x, y)
    // portal?
    if (p.has(l)) {
      const t = p.get(l)[1] // exit
      if (t) {
        const tc: number[] = unHash(t)
        n.push([tc[0], tc[1]])
      }
    }
    
    n = n.filter(([a, b]) => s[b][a] == FAR)    
    const d = s[y][x] + 1
    n.forEach(([a, b]) => s[b][a] = d)

    v = v.concat(n)
  }

  return s
}

function getSteps3(g: Grid, p: Portals, ox: number, oy: number): Array<Array<number>> {
  let s: Array<Array<Array<number>>> = Array.from({length: 10000}, () => 
    Array.from({length: g.length}, () => new Array<number>(g[0].length).fill(FAR)))

  s[0][oy][ox] = 0
  let v: Cells3 = []
  v.push([ox, oy, 0])

  for (let i = 0; (i < 1000000) && (v.length > 0); i++) {
    const [x, y, z] = v.shift() // current cell
    
    let n: Cells3 = neighbors(g, x, y).filter(([a, b]) => g[b][a] == Tile.OPEN).map(v => [v[0], v[1], z])
    const l = hash(x, y)
    // portal?
    if (p.has(l)) {
      const [_, t, pl] = p.get(l) // portal exit
      if (t && (z + pl >= 0)) {
        const tc: number[] = unHash(t)
        n.push([tc[0], tc[1], z + pl])
      }
    }

    n = n.filter(([a, b, c]) => s[c][b][a] == FAR)
    const d = s[z][y][x] + 1
    n.forEach(([a, b, c]) => s[c][b][a] = d)

    v = v.concat(n)
  }

  return s[0]
}

let grid: Grid = readGrid()

//print(grid)
print(neighbors(grid, 5, 5))
print(neighbors(grid, 0, 0))
print(neighbors(grid, grid[0].length - 1, grid.length - 1))
let ports: Portals = readPortals(grid)
//print(ports)
//print(findExit(ports, hash(6, 10)))
let entry = unHash(findEntry(ports, 'AA'))
let exit = unHash(findEntry(ports, 'ZZ'))
print("entries", entry, exit)

print("------------------")

let steps = getSteps(grid, ports, entry[0], entry[1])
//print(steps)
print("steps", steps[exit[1]][exit[0]])

print("------------------")

let steps3 = getSteps3(grid, ports, entry[0], entry[1])
//print(steps)
print("steps3", steps3[exit[1]][exit[0]])