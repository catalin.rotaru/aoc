import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, END = 99}
enum Mode {POS = 0, IMM}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.END]: 1
}

type Memory = number[]

function read(mem: Memory, addr: number, mode: Mode = Mode.IMM): number {
  return (mode == Mode.IMM) ? addr : mem[addr]
}

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns next PC; same PC if waiting on an input
function execute(m: Memory, out: number[], input: number[], pc: number = 0): number {
  const [inst, p1, p2, _] = decode(m[pc])
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      if (input.length == 0)
        return pc
      m[a] = input.shift()
      break
  
    case OP.OUT:
      out.push(read(m, a, p1))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, p1) != 0)
        return read(m, b, p2)
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, p1) == 0)
        return read(m, b, p2)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      m[c] = op[inst](read(m, a, p1), read(m, b, p2))
  }

  return pc + op_len[inst]
}

// program execution loop, ends on halt
function loop(m: Memory, input: number[]): number[] {
  let out = new Array<number>()
  for (let pc = 0; OP.END !== m[pc];)
    pc = execute(m, out, input, pc)

  return out
}

function run(m: Memory, input: number[]): number {
  //print(m)
  let mem = Array.from(m)
  let out = loop(mem, input)
  //print(m)
  return out[out.length - 1]
}

function phaseRun(m: Memory, ph: number[]): number {
  let input = 0

  for (let p of ph)
    input = run(m, [p, input])

  return input
}

// runs a phased feedback loop
function feedRun(m: Memory, ph: number[]): number {
  const n: number = ph.length

  let mem: Memory[] = Array.from(ph, _ => Array.from(m))
  let io: Array<number>[] = Array.from(ph, v => [v])
  io[0].push(0)
  let pc: Array<number> = new Array(n).fill(0);

  // RUN loop
  do {
    // loop amps
    for (let i: number = 0; i < n; i++) {
      // loop i amp's program
      while (OP.END !== mem[i][pc[i]]) {
        let npc: number = execute(mem[i], io[(i + 1) % n], io[i], pc[i])
        // waiting for input?
        if (npc == pc[i])
          break
        pc[i] = npc
      }
    }
  } while (OP.END !== mem[n - 1][pc[n - 1]])

  return io[0][0]
}

function selectRun(m: Memory, depth: number = 5, ph: number[] = []): number {
  if (ph.length >= depth) {
    let res =  phaseRun(m, ph)
    return res
  }
  
  let best: number = -1
  for (let p: number =  0; p <= 4; p++)
    if (!ph.includes(p))
      best = Math.max(best, selectRun(m, depth, ph.concat([p])))
  return best
}

function selectFeedRun(m: Memory, depth: number = 5, ph: number[] = []): number {
  if (ph.length >= depth) {
    let res =  feedRun(m, ph)
    return res
  }
  
  let best: number = -1
  for (let p: number =  5; p <= 9; p++)
    if (!ph.includes(p))
      best = Math.max(best, selectFeedRun(m, depth, ph.concat([p])))
  return best
}

function readProgram(): Memory {
  return fs.readFileSync('day7.txt','utf8').split(',').map(v => +v)
}

print(phaseRun([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], 
  [4, 3, 2, 1, 0]))
print(phaseRun([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], 
  [4, 4, 4, 4, 4]))
print(phaseRun([3,23,3,24,1002,24,10,24,1002,23,-1,23,
  101,5,23,23,1,24,23,23,4,23,99,0,0], 
  [0,1,2,3,4]))
print(phaseRun([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
  1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0], 
  [1,0,4,3,2]))  

print("------------------")

print(selectRun([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]))
print(selectRun([3,23,3,24,1002,24,10,24,1002,23,-1,23,
  101,5,23,23,1,24,23,23,4,23,99,0,0]))
print(selectRun([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
  1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]))

  
let m = readProgram()
print("best", selectRun(m))

print("------------------")

print(feedRun([3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
  27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5], [9,8,7,6,5]))
print(feedRun([3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
  -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
  53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10], [9,7,8,5,6]))

print("best", selectFeedRun(m))