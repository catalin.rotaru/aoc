import fs = require('fs')

const print = console.log.bind(console)

enum Dot {EMPTY = 0, ASTEROID, HIDDEN, ORIGIN}

const EMPTY = '.'
const ASTEROID = '#'
const HIDDEN = 'o'
const ORIGIN = 'X'

type Point = [number, number] // x, y

// map of dots stored in a one-dimensional array
class Map {
  width: number
  height: number
  data: Array<Dot>

  constructor(width: number, str: string = '') {
    this.width = width
    this.height = str.length / width

    this.data = [...str].map(v => (v == ASTEROID) ? Dot.ASTEROID : Dot.EMPTY)
  }

  copy(): Map {
    let res = new Map(this.width)
    res.height = this.height
    res.data = Array.from(this.data)

    return res
  }

  printDot(d: Dot): string {
    switch (d) {
      case Dot.ASTEROID: return ASTEROID
      case Dot.HIDDEN: return HIDDEN
      case Dot.ORIGIN: return ORIGIN
      default: return EMPTY
    }
  }

  print(ox: number = -1, oy: number = -1) {
    if (this.inside(ox, oy))
      this.set(ox, oy, Dot.ORIGIN)

    for (let i = 0; i < this.data.length; i += this.width)
      print(this.data.slice(i, i + this.width).map(v => this.printDot(v)).join(''))

    if (this.inside(ox, oy))
      this.set(ox, oy, Dot.ASTEROID)
  }

  toPoint(idx: number): Point {
    return [idx % this.width, Math.floor(idx/this.width)]
  }

  get(x: number, y: number): Dot {
    return this.data[x + y * this.width]
  }

  inside(x: number, y: number): boolean {
    return (x >= 0) && (x < this.width) && (y >= 0) && (y < this.height)
  }

  set(x: number, y: number, val: Dot): boolean {
    if (!this.inside(x, y))
      return false

    this.data[x + y * this.width] = val
    return true
  }

  // hide on map asteroids shaded by Asteroid (a) from Origin (o)
  // return false if there is no Asteroid actually there
  shade(o: Point, a: Point): boolean {
    let [x, y] = a
    if (!this.inside(x, y) || (this.get(x, y) != Dot.ASTEROID))
      return false

    let dx = a[0] - o[0]
    let dy = a[1] - o[1]
    let dvz = gcd(Math.abs(dx), Math.abs(dy))
    dx /= dvz
    dy /= dvz

    for (let i = 1; this.set(a[0] + dx * i, a[1] + dy * i, Dot.HIDDEN); i++);
    return true
  }

  // visible asteroids from origin (o) on this map
  // changes map!
  visible(o: Point): number {
    if (this.get(...o) != Dot.ASTEROID)
      return 0

    let res = 0

    for (let i = 1; i < Math.max(this.width, this.height); i++)
      for (let j = 0; j < i * 2; j++) {
        res += +this.shade(o, [o[0] + i - j, o[1] + i])
        res += +this.shade(o, [o[0] - i, o[1] + i - j])
        res += +this.shade(o, [o[0] - i + j, o[1] - i])
        res += +this.shade(o, [o[0] + i, o[1] - i + j])
      }

    return res
  }

  // returns number of visible asteroids
  locScore(): number {
    let loc = this.data.map((v, i) => this.copy().visible(this.toPoint(i)))
    return Math.max(...loc)
  }

  // returns location with most visible asteroids
  location(): Point {
    let loc = this.data.map((v, i) => this.copy().visible(this.toPoint(i)))
    return this.toPoint(loc.reduce((max, x, i, arr) => x > arr[max] ? i : max, 0))
  }
}

function gcd(a: number, b: number): number {
  if (!b)
    return a

  return gcd(b, a % b)
}

// Quadrants are:
// 4 | 1
// -----
// 3 | 2
function quadrant(a: Point, o: Point = [0, 0]): number {
  let x  = a[0] - o[0]
  let y  = a[1] - o[1]

  if (y >= 0)
    return (x >= 0) ? 2 : 3
  else
    return (x >= 0) ? 1 : 4
}

function cmpPoint(a: Point, b: Point, o: Point = [0, 0]) {
  let qd = quadrant(a, o) - quadrant(b, o)

  if (qd)
    return qd
  
   return (b[0] - o[0])/(b[1] - o[1]) - (a[0] - o[0])/(a[1] - o[1])
}

function readMap(): Map {
  const str = fs.readFileSync('day10.txt','utf8').split(/\r?\n/)
  return new Map(str[0].length, str.join(''))
}

const m1: Map = new Map(5, ".#..#.....#####....#...##")
m1.print()
print()
let m2 = m1.copy()
m2.shade([1, 0], [2, 2])
m2.print(1, 0)

print(m1.copy().visible([3, 4]))
print(m1.locScore())

print("------------------")

print(new Map(10, "......#.#.#..#.#......#######..#.#.###...#..#.......#....#.##..#....#..##.#..#####...#..#..#....####").locScore())

print("------------------")

const mm = readMap()
const oo =  mm.location()
print("A1:", oo)
print("A1:", mm.locScore())

print("------------------")
print("q", quadrant([2, 2], [3, 3]))
print("q", quadrant([5, 2], [3, 3]))
print("q", quadrant([2, 5], [3, 3]))

print("cmp", cmpPoint([1, -2], [1, 2]))
print("cmp", cmpPoint([2, 1], [1, 2]))
print("cmp", cmpPoint([-1, 2], [-2, 1]))
print("cmp", cmpPoint([-2, -1], [-1, -2]))
print("cmp", cmpPoint([1, -2], [2, -1]))

const m3 = new Map(17, ".#....#####...#..##...##.#####..####...#...#.#####...#.....#...###....#.#.....#....##")
m3.print()
print()

m3.visible([8, 3])
m3.print(8,3)

m3.set(8, 3, Dot.ORIGIN)
print(m3.data.map((_, i) => m3.toPoint(i)). filter(v => m3.get(...v) == Dot.ASTEROID).sort((a, b) => cmpPoint(a, b, [8, 3])))

print("------------------")

mm.visible(oo)
mm.set(oo[0], oo[1], Dot.ORIGIN)
print(mm.data.map((_, i) => mm.toPoint(i)). filter(v => mm.get(...v) == Dot.ASTEROID).sort((a, b) => cmpPoint(a, b, oo))[199])