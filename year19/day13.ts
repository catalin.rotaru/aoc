import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

type Memory = number[]

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, io: Io, base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      write(m, io.input(), a, rb, p1)
      break
  
    case OP.OUT:
      io.output(read(m, a, rb, p1))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

enum Tile {EMPTY = 0, WALL, BLOCK, PADDLE, BALL}
enum Joystick {LEFT = -1, NEUTRAL, RIGHT}
type Grid = Array<Array<Tile>> // y, x


class Io {
  grid: Grid
  out: Array<number>
  joy: Joystick
  score: number
  paddle: number

  constructor(len: number = 1000) {
    this.grid = Array.from({length: len}, () => new Array<Tile>(len).fill(Tile.EMPTY))
    this.out = new Array()
    this.joy = Joystick.NEUTRAL
    this.score = 0
    this.paddle = 0
  }

  input(): number {
    return this.joy
  }

  output(v: number) {
    this.out.push(v)

    if (this.out.length == 3) {
      let [x, y, val] = this.out

      if ((x == -1) && (y == 0))
        this.score = val
      else {
        this.grid[y][x] = val
        
        switch (val) {
          case Tile.PADDLE:
            this.paddle = x
            break
          case Tile.BALL:
            this.joy = Math.sign(x - this.paddle)
            break
        }
      }

      this.out.length = 0
    }
  }

  printGrid() {
    const sh = {[Tile.EMPTY]: ' ', [Tile.WALL]: '#', [Tile.BLOCK]: '█', [Tile.PADDLE] : '=', [Tile.BALL] : '*'}
    this.grid.forEach(r => print(r.map(v => sh[v]).join('')))
  }
}

// program execution loop, ends on halt
function run(mem: Memory, coins: number = 1) {
  let rb = 0
  let io = new Io(50)
  let m: Memory = Array.from(mem)
  m[0] = coins

  for (let pc = 0; OP.END !== m[pc];)
    [pc, rb] = execute(m, io, rb, pc)

  //io.printGrid()
  print("Blocks:", io.grid.reduce((acc, cur) => acc + cur.filter(v => v == Tile.BLOCK).length, 0))
  print("Score", io.score)
}

function readProgram(): Memory {
  return fs.readFileSync('day13.txt','utf8').split(',').map(v => +v)
}

let m = readProgram()
run(m)

print("------------------")

run(m, 2)