import fs = require('fs')

const print = console.log.bind(console)

enum Tile {BUG = '#', SPACE = '.'}
type Grid = Array<Array<string>> // y, x
type Cell = [number, number] // x, y
type Cells = Array<Cell>
const ADJACENT = [[0, 1], [0, -1], [1, 0], [-1, 0]] // array of [x, y]

type Cell3d = [number, number, number] // x, y, z
type Cells3d = Array<Cell3d>
type Grid3d = Array<Array<Array<string>>> // z, y, x

// return on-grid neighbors
function neighbors(g: Grid, x: number, y: number): Cells {
  const res: Cells = ADJACENT.map(v => {
    const nx = x + v[0]
    const ny = y + v[1]
    if ((nx < 0) || (nx >= g[0].length) || (ny < 0) || (ny >= g.length))
      return undefined
    return [nx, ny]
  })
  return res.filter(v => v)
}

function neighbors3d(g: Grid, x: number, y: number, z: number): Cells3d {
  if ((x == 2) && (y == 2))
    return []

  let extra: Cells3d
  let res: Cells3d = ADJACENT.map(v => {
    const nx = x + v[0]
    const ny = y + v[1]

    if (nx < 0)
      return [1, 2, z - 1]
    if (nx >= g[0].length)
      return [3, 2, z - 1]
    if (ny < 0)
      return [2, 1, z - 1]
    if (ny >= g.length)
      return [2, 3, z - 1]

    if ((nx == 2) && (ny == 2)) {
      if (y == ny) 
        extra = (x < nx) ? 
          [[0, 0, z + 1], [0, 1, z + 1], [0, 2, z + 1], [0, 3, z + 1], [0, 4, z + 1]] : 
          [[4, 0, z + 1], [4, 1, z + 1], [4, 2, z + 1], [4, 3, z + 1], [4, 4, z + 1]]
      else if (x == nx)
        extra = (y < ny) ? 
          [[0, 0, z + 1], [1, 0, z + 1], [2, 0, z + 1], [3, 0, z + 1], [4, 0, z + 1]] : 
          [[0, 4, z + 1], [1, 4, z + 1], [2, 4, z + 1], [3, 4, z + 1], [4, 4, z + 1]]

      return undefined
    }
    
    return [nx, ny, z]
  })

  return (extra ? res.concat(extra) : res).filter(v => v)
}

function nextState(current: string, bugs: number): string {
  if (current == Tile.BUG)
    return (bugs != 1) ? Tile.SPACE : current
  else
    return ((bugs == 1) || (bugs == 2)) ? Tile.BUG : current
}

function evolve(g: Grid, x: number, y: number): string {
  const n: Cells = neighbors(g, x, y)
  const bugs: number = n.map(([x, y]) => g[y][x]).filter(v => v == Tile.BUG).length
  return nextState(g[y][x], bugs)
}

function evolve3d(g: Grid3d, x: number, y: number, z: number): string {
  const grid: Grid = g[z]
  const n: Cells3d = neighbors3d(grid, x, y, z)
  const bugs: number = n.map(([x, y, z]) => ((z <= 0) || (z >= g.length)) ? 0 : g[z][y][x]).filter(v => v == Tile.BUG).length
  return nextState(g[z][y][x], bugs)
}

const evolveGrid = (g: Grid): Grid => g.map((r, y) => r.map((_, x) => evolve(g, x, y)))

const evolveGrid3d = (g: Grid3d): Grid3d => g.map((l, z) => l.map((r, y) => r.map((_, x) => evolve3d(g, x, y, z))))

const printGrid = (g: Grid) => g.forEach(r => print(r.join('')))

const hash = (g: Grid): string => g.map(r => r.join('')).join('')

const score = (g: Grid): number => g.reduce((acc, r, y) => acc + r.reduce((a, v, x) => 
  a + ((v === Tile.BUG) ? Math.pow(2, x + y * r.length) : 0), 0), 0)

const count = (g: Grid3d): number => g.reduce((acc, l) => acc + l.reduce((a, r) => a + r.filter((v) => v == Tile.BUG).length, 0), 0)

function timeEvolve(g: Grid, steps: number = 10000) {
  let prev: Set<string> = new Set()

  for (let i = 0; i < steps; i++) {
    g = evolveGrid(g)
    //printGrid(g)
    //print()
    const h = hash(g)
    if (prev.has(h)) {
      print("meet again at", i, "score", score(g))
      //printGrid(g)
      //print()

      break
    }

    prev.add(h)
  }
}

function timeEvolve3d(grid: Grid, steps: number = 10) {
  const height = steps * 2
  const o = Math.floor(height/2)

  let g: Grid3d = Array.from({length: height}, (_) => Array.from({length: grid.length}, (_) => new Array(grid[0].length).fill(Tile.SPACE)))
  g[o] = grid
  //printGrid(g[o])

  for (let i = 0; i < steps; i++)
    g = evolveGrid3d(g)

  print("BUGS", count(g))
}

function readGrid(): Grid {
  return fs.readFileSync('day24.txt','utf8').split(/\r?\n/).map(l => l.split(''))
}

let grid: Grid = readGrid()
print(neighbors(grid, 3, 3))
print(neighbors(grid, 0, 0))
print(neighbors(grid, grid[0].length - 1, grid.length - 1))
//printGrid(grid)
//print()
timeEvolve(grid, 4)

print("------------------")

timeEvolve(grid)

print("------------------")

print(neighbors3d(grid, 3, 3, 3))
print(neighbors3d(grid, 1, 1, 1))
print(neighbors3d(grid, 3, 0, 3))
print(neighbors3d(grid, 4, 0, 3))
print(neighbors3d(grid, 3, 2, 3))
print(neighbors3d(grid, 2, 1, 3))

timeEvolve3d(grid, 200)
