import fs = require('fs')

const print = console.log.bind(console)

enum OP {ADD = 1, MUL, IN, OUT, JT, JF, LT, EQ, RBO, END = 99}
enum Mode {POS = 0, IMM, REL}

const op = {
  [OP.ADD]: (a: number, b: number): number => a + b,
  [OP.MUL]: (a: number, b: number): number => a * b,
  [OP.LT]: (a: number, b: number): number => a < b ? 1 : 0,
  [OP.EQ]: (a: number, b: number): number => a == b ? 1 : 0
}

const op_len = {
  [OP.ADD]: 4,
  [OP.MUL]: 4,
  [OP.IN]: 2,
  [OP.OUT]: 2,
  [OP.JT]: 3,
  [OP.JF]: 3,
  [OP.LT]: 4,
  [OP.EQ]: 4,
  [OP.RBO]: 2,
  [OP.END]: 1
}

const NEWLINE = 10

type Memory = Array<number>

function read(mem: Memory, addr: number, base: number = 0, mode: Mode = Mode.IMM): number {
  switch (mode) {
    case Mode.POS: return mem[addr]
    case Mode.REL: return mem[base + addr]
    default: return addr
  }
}

function write(mem: Memory, val: number, addr: number, base: number = 0, mode: Mode = Mode.POS) {
    mem[((mode == Mode.REL) ? base : 0) + addr] = val
  }

// decode opcode => inst, p1, p2, p3 (read mode)
function decode(op: number): [number, Mode, Mode, Mode] {
  const inst = op % 100
  const r1 = Math.floor(op/100)

  const p1 = r1 % 10
  const r2 = Math.floor(r1/10)

  const p2 = r2 % 10
  const r3 = Math.floor(r2/10)

  const p3 = r3 % 10

  return [inst, p1, p2, p3]
}

// returns [next PC , relative base]; same PC if waiting on an input
function execute(m: Memory, input: Memory, out: string[], base: number = 0, pc: number = 0): [number, number] {
  const [inst, p1, p2, p3] = decode(m[pc])
  let rb = base
  const a: number = m[pc + 1]
  let b: number

  switch (inst) {
    case OP.IN:
      write(m, input.shift(), a, rb, p1)
      break
  
    case OP.OUT:
      let ch = read(m, a, rb, p1)
      let l = (ch == NEWLINE) || (out.length == 0) ? '' : out.pop() + String.fromCharCode(ch)
      out.push(l)
      process.stdout.write(String.fromCharCode(ch))
      break

    case OP.JT:
      b = m[pc + 2]
      if (read(m, a, rb, p1) != 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.JF:
      b = m[pc + 2]
      if (read(m, a, rb, p1) == 0)
        return [read(m, b, rb, p2), rb]
      break

    case OP.RBO:
      rb += read(m, a, rb, p1)
      break
  
    default:
      b = m[pc + 2]
      const c = m[pc + 3]
      let res = op[inst](read(m, a, rb, p1), read(m, b, rb, p2))
      write(m, res, c, rb, p3)
  }

  return [pc + op_len[inst], rb]
}

class Droid {
  mem: Memory

  constructor(mem: Memory) {
    this.mem = mem.concat(new Array(100000).fill(0))
  }

  run(code: string[]) {
    let input: Memory = compile(code)
    let out: string[] = []

    let rb: number = 0
    for (let pc = 0; (OP.END != this.mem[pc]) && ((input.length > 0) || (out[out.length - 1] != 'Command?'));)
      [pc, rb] = execute(this.mem, input, out, rb, pc)
  }
}

function readProgram(): Memory {
  return fs.readFileSync('day25.txt','utf8').split(',').map(v => +v)
}

const compile = (code: string[]): Memory => {
  let res: string[] = code.reduce((acc, cur) => 
    acc.concat(cur.split('').concat([String.fromCharCode(NEWLINE)])), [])
  return res.map(v => v.charCodeAt(0))
}

const prog = readProgram()
const droid = new Droid(prog)
const code = [ // hull breach
  'south', // Engineering 
    'take fuel cell',
    'north', // return

  'west', // Holodeck
    'take mouse',
    'north', // Science Lab
      //'take photons',
      'east', // Arcade 
        'take klein bottle',
        'west', // return
      'south', // return
    'west', // Passages
      //'take infinite loop',
      'south', // Observatory
        'take dark matter',
        'north', // return
      'west', // Stables 
        //'take escape pod',
        'east', // return
      'east', // return
    'east', // return

  'north', // Hallway 
    'north', // Storage 
      'south', // return
    'west', // Sick Bay
      'south', // Kitchen 
        'take planetoid',
        'west', // Warp Drive Maintenance
          'take antenna',
          'east', // return
        'east', // Navigation 
          'take mutex',
          'east', // Gift Wrapping Center
            'west', // return
          'south', // Hot Chocolate Fountain
            'take whirled peas',
            'west', // Crew Quarters
              //'take molten lava',
              'east', // return
            'south', // Corridor
              // 'take giant electromagnet',
              'east', // Security Checkpoint <----------------
                
                'drop mouse', // too heavy
                //'drop planetoid', // too light without
                'drop whirled peas', // too heavy
                //'drop fuel cell', // too light without
                'drop klein bottle', // too heavy
                //'drop antenna', // too light without
                
                'drop dark matter',
                //'drop mutex',

                'inv',
                'north',
                /*'west', // return
              'north', // return
            'north', // return
          'west', // return
        'north', // return
      'east', // return
    'south', // return*/

]
droid.run(code)