import fs = require("fs")

const print = console.log.bind(console)

function findMarker(s: string, size: number = 4): number {
  for (let i = 0; i <= s.length - size; i++) {
    const str = s.slice(i, i + size)
    const m = new Set(str.split(''))
    if (m.size == size)
      return i + size
  }

  return 0
}

function readBuffer(): string {
  return fs.readFileSync('day6.txt','utf8')
}

print(findMarker("aaaabcd"))
print(findMarker("mjqjpqmgbljsphdztnvjfqwrcgsmlb"))
print(findMarker("bvwbjplbgvbhsrlpgdmjqwftvncz"))
print(findMarker("nppdvjthqldpwncqszvftbrmjlhg"))
print(findMarker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"))
print(findMarker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"))

const buff = readBuffer()

print("A1:", findMarker(buff))

print("------------------")

print("A2:", findMarker(buff, 14))
