import fs = require("fs")

const print = console.log.bind(console)

type Move = [string, string]  // opponent move, my move
type Moves = Array<Move>

const same = {'X': 'A', 'Y': 'B', 'Z': 'C'} // Rock, Paper, Scissors
const wins = {'A': 'C', 'C': 'B', 'B': 'A'} // wins : loses
const loses = {'A': 'B', 'B': 'C', 'C': 'A'} // loses : wins
const shape = {'A': 1, 'B': 2, 'C': 3}

function readGuide(): Moves {
  return fs.readFileSync('day2.txt','utf8').split(/\r?\n/).map(l => 
    <Move>l.split(' '))
}

const sameGuide = (moves: Moves): Moves =>
  moves.map(m => [m[0], same[m[1]]])

const score = (m: Move): number =>
  shape[m[1]] + ((wins[m[1]] == m[0]) ? 6 : ((m[1] == m[0]) ? 3 : 0))

const gameScore = (moves: Moves): number =>
  moves.reduce((acc, cur) => acc + score(cur), 0)

function recommend(m: Move): string {
  switch (m[1]) {
    case 'X':
      return wins[m[0]]
    case 'Z':
      return loses[m[0]]
    default:  // Y
      return m[0]
  }
}

const recommendGuide = (moves: Moves): Moves =>
  moves.map(m => [m[0], recommend(m)])

const guide = readGuide()
//print(guide)

//print(score(['A', 'B']))
//print(score(['B', 'A']))

const sGuide = sameGuide(guide)
//print(sGuide)
print("A1:", gameScore(sGuide))

print("------------------")

const rGuide = recommendGuide(guide)
//print(rGuide)
print("A2:", gameScore(rGuide))
