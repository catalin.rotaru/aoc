import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Cell = [number, number] // [x, y]
type Cells = Array<Cell>
type Grid = Array<Array<string>> // [y] [x]

enum Tile {NONE = ' ', OPEN = '.', SOLID = '#'}
const Dir: Cells = [[0, 1], [-1, 0], [0, -1], [1, 0]] // array of [x, y]

function readBoard(): [Grid, Array<string>] {
  const [m, p] = fs.readFileSync('day22.txt','utf8').split(/\r?\n\r?\n/)
  const map = m.split(/\r?\n/).map(l => l.split(''))
  return [map, p.match(/\d+|\D+/g)]
}

function move(m: Grid, o: Cell, d: Cell): Cell {
  return o
}

const [map, path] = readBoard()
//printGrid(map)
//print(path)

print(move(map, [0, 0], Dir[0]))

print("------------------")
