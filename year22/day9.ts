import fs = require("fs")

const print = console.log.bind(console)

type Moves = Array<[string, number]> // [direction, steps]
type Cell = [number, number] // x, y
type Visited = Set<string>

const move = {  // direction -> [x, y]
  'R': [1, 0],
  'L': [-1, 0],
  'U': [0, -1],
  'D': [0, 1]
}

const doMove = (c: Cell, d: string): Cell =>
  [c[0] + move[d][0], c[1] + move[d][1]]

// head, tail => new tail
function follow(h: Cell, t: Cell): Cell {
  const dx = h[0] - t[0]
  const dy = h[1] - t[1]
  
  const [nx, ny] = ((Math.abs(dx) > 1) || (Math.abs(dy) > 1)) ?
    [Math.sign(dx), Math.sign(dy)] : [0, 0]
  return [t[0] + nx, t[1] + ny]
}

function readMoves(): Moves {
  return fs.readFileSync('day9.txt','utf8').split(/\r?\n/).map(l => {
    const [d, s] = l.split(' ')
    return [d, +s]
  })
}

const hash = (c: Cell): string =>
  c[0].toString() + ':' + c[1].toString()

function doMoves(moves: Moves, rope: number = 2) {
  let k: Array<Cell> = new Array(rope).fill([0, 0])

  let v: Visited = new Set()
  v.add(hash(k[0]))

  moves.forEach(m => {
    for (let i = 0; i < m[1]; i++) {
      k[0] = doMove(k[0], m[0])
      for (let j = 1; j < rope; j++)
        k[j] = follow(k[j - 1], k[j])

      v.add(hash(k[rope - 1]))
    }
  })

  return v.size
}

print(doMove([5, 5], 'D'))
print(follow([7, 5], [5, 5]))
print(follow([5, 7], [5, 5]))
print(follow([6, 3], [5, 5]))
print(follow([7, 5], [5, 5]))

const moves = readMoves()
//print(moves)

print("A1:", doMoves(moves))

print("------------------")

print("A2:", doMoves(moves, 10))