import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Operation = [string, number] // [op, arg]
type Program = Array<Operation>
type State = Array<[number, number]> // [after cycle, register]
type Grid = Array<Array<string>>  // Array[y][x]

const interest = [20, 60, 100, 140, 180, 220]

function readProgram(): Program {
  return fs.readFileSync('day10.txt','utf8').split(/\r?\n/).map(l => {
    const [o, a] = l.split(' ')
    return [o, a ? +a : 0]
  })
}

function execute(prog: Program): State {
  let cycle = 0
  let x = 1

  let res: State = new Array()
  res.push([cycle, x])

  prog.forEach(([o, a]) => {
    cycle += (o == 'noop') ? 1 : 2
    x += a

    res.push([cycle, x])
  })

  return res
}

function reg(state: State, cycle: number): number {
  const n = state.findIndex(s => s[0] > cycle - 1)
  return state[n - 1][1]
}

const score = (state: State, cycle: number): number =>
  reg(state, cycle) * cycle

const draw = (state: State): Grid =>
  Array.from({length: 6}, (_, y) => Array.from({length: 40}, (_, x) => {
    const c = 40 * y + x + 1
    const r = reg(state, c)
    return (r - 1 <= x) && (r + 1 >= x) ? '█' : ' '
  }))

const prog = readProgram()
//print(prog)

const state = execute(prog)
//print(state)

print(interest.map(e => score(state, e)))
print("A1:", interest.reduce((acc, cur) => acc + score(state, cur), 0))

print("------------------")

printGrid(draw(state))