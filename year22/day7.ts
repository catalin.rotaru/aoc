import fs = require("fs")

const print = console.log.bind(console)

const UNKNOWN = -1
type Nodes = Array<Node>
type Files = Array<File>
type Dirs = Map<string, Dir>  // name -> directory

class Node {
  readonly name: string
  protected size: number
  readonly parent: Dir

  constructor(n: string, s: number, p: Dir) {
    this.name = n
    this.size = s
    this.parent = p
  }

  getSize(): number {
    return this.size
  }
}

class File extends Node {
  constructor(n: string, s: number, p: Dir) {
    super(n, s, p)
  }
}

class Dir extends Node {
  protected dirs: Dirs    // sub-directories
  protected files: Files  // files included

  constructor(n: string, p: Dir = null) {
    super(n, UNKNOWN, p)
    this.dirs = new Map()
    this.files = new Array()
  }

  private computeSize(): number {
    const nodes: Nodes = this.files
    return nodes.concat([...this.dirs.values()]).
      reduce((acc, cur) => acc + cur.getSize(), 0)
  }

  getSize(): number {
    if (this.size == UNKNOWN) 
      this.size = this.computeSize() 
      
    return this.size
  }

  getDir(n: string) {
    let res = this.dirs.get(n)
    if (!res) {
      res = new Dir(n, this)
      this.dirs.set(n, res)
    }
    
    return res
  }

  addDir(n: string) {
    this.dirs.set(n, new Dir(n, this))
  }

  addFile(n: string, s: number) {
    this.files.push(new File(n, s, this))
  }

  getSizes(): Array<number> {
    let res = [...this.dirs.values()].reduce((acc, cur) => 
      acc.concat(cur.getSizes()), [])
    res.push(this.getSize())
    return res
  }

  sumSmaller(): number {
    return this.getSizes().reduce((acc, cur) => 
      acc + ((cur <= 100000) ? cur : 0), 0)
  }

  smallestOver(): number {
    const need = 30000000 - (70000000 - this.getSize())
    return Math.min(...this.getSizes().filter(v => v >= need))
  }
}

function readListing(): Array<string> {
  return fs.readFileSync('day7.txt','utf8').split(/\r?\n/)
}

function processListing(lines: Array<string>): Dir {
  const root = new Dir('/')
  let current = root

  lines.forEach(l => {
    switch (l.slice(0, 4)) {
      case ('$ cd'):
        const d = l.slice(5)
        current = (d == '/') ? root : 
          ((d == '..') ? current.parent : current.getDir(d))
        break
      case ('$ ls'):
        break
      default:
        let [s, n] = l.split(' ')
        if (s == 'dir')
          current.addDir(n)
        else
          current.addFile(n, +s)
    }
  })

  return root
}

const list = readListing()
//print(list)

const root = processListing(list)
print(root.getSize())
//print(root.getSizes())

print("A1:", root.sumSmaller())

print("------------------")

print("A2:", root.smallestOver())
