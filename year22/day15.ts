import fs = require("fs")

const print = console.log.bind(console)

type Cell = [number, number] // x, y
type Sensor = [Cell, number] // location, radius
type Sensors = Array<Sensor>
type Beacons = Array<Cell>
type Segment = [number, number] // x1, x2
type Segments = Array<Segment>

const ALL: Segment = [Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER]

const dist = (a: Cell, b: Cell): number =>
  Math.abs(b[0] - a[0]) + Math.abs(b[1] - a[1])

function dedupe(a: Array<any>): Array<any> {
  let seen = new Set()
  return a.filter(v => {
    const s = v.toString()
    if (seen.has(s))
      return false
    seen.add(s)
    return true
  })
}

function readLocations(): [Sensors, Beacons] {
  const a =  fs.readFileSync('day15.txt','utf8').split(/\r?\n/).
    map(l => l.slice(12).split(': closest beacon is at x=').map(h => 
      <Cell>h.split(', y=').map(v => +v)))
  const bs = a.map(v => v[1])
  
  return [a.map(v => [v[0], dist(v[0], v[1])]), dedupe(bs)]
}

const intersect = (a: Segment, b: Segment): Segment =>
  (b[1] < a[0]) || (a[1] < b[0]) ? undefined :
    [Math.max(b[0], a[0]), Math.min(b[1], a[1])]

// segment in range of sensor s on row
function inRange(s: Sensor, row: number, limit: Segment): Segment {
  const [[x, y], r] = s
  if ((row < y - r) || (row > y + r))
    return undefined

  const d = r - Math.abs(row - y)
  return intersect([x - d, x + d], limit)
}

// segment a - segment b
function remove(a: Segment, b: Segment): Segments {
  const [a1, a2] = a
  const [b1, b2] = b

  if ((b2 < a1) || (a2 < b1))
    return [a]

  if (b1 <= a1)
    return (b2 >= a2) ? [] : [[b2 + 1, a2]]
  
  // b1 <= a2
  let res: Segments = [[a1, b1 - 1]]
  return (b2 < a2) ? res.concat([[b2 + 1, a2]]) : res
}

// segments [a] + segment b - common areas
const add = (a: Segments, b: Segment): Segments =>
  a.map(e => remove(e, b)).flat().concat([b])

const rowCoverage = (sens: Sensors, row: number, lm: Segment = ALL): Segments =>
  sens.map(s => inRange(s, row, lm)).filter(v => v).
  reduce((acc, cur) => add(acc, cur), [])

function coverage(sens: Sensors, b: Beacons, row: number = 10) {
  const seg = rowCoverage(sens, row)
  //print(seg)

  let res =  seg.reduce((acc, cur) => acc + cur[1] - cur[0] + 1, 0)
  res -= b.filter(e => (row == e[1]) && inOne(seg, e[0])).length
  res -= sens.filter(e => (row == e[0][1]) && inOne(seg, e[0][0])).length

  return res
}

const inside = (a: Segment, x: number): boolean =>
  (a[0] <= x) && (x <= a[1])

const inOne = (a: Segments, x: number): boolean =>
  a.find(s => inside(s, x)) != undefined

function search(sens: Sensors) {
  for (let row = 4000000; row >= 0; row--) {
    let seg = rowCoverage(sens, row, [0, 4000000])
    seg.sort((a, b) => a[1] - b[0])
    //print(seg)

    for (let i = 1; i < seg.length; i++)
      if (seg[i - 1][1] < seg[i][0] - 1)
        return row + (seg[i][0] - 1) * 4000000
  }
}

const [sens, b] = readLocations()
//print(sens, b)

//print(inRange(sens[6], 7), inRange(sens[6], 10), inRange(sens[6], 16))
//print(sens.map(s => intersect(s, 10)))
print("remove", remove([1, 4], [2, 2]))
print("remove", remove([1, 4], [3, 5]))
print("remove", remove([1, 4], [0, 1]))
print("add", add([[1, 4], [6, 8]], [3, 7]))

//print(coverage(sens, b))
print("A1:", coverage(sens, b, 2000000))

print("------------------")

print("A2:", search(sens))
