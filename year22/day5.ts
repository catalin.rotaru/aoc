import fs = require("fs")

const print = console.log.bind(console)

type Stack = Array<string>
type Stacks = Array<Stack>
type Move = [number, number, number] // quantity, source, destination
type Moves = Array<Move>

function parseLine(line: string, stacks: Stacks = null): Stacks {
  let res = stacks ? stacks : 
    Array.from({length: (1 + line.length) / 4}, () => [])

  for (let i = 1; i < line.length; i += 4) {
    const ch = line.charAt(i);
    if (ch != ' ')
      res[(i - 1) / 4].unshift(ch)
  }

  return res
}

function parseMove(line: string): Move {
  const [q, sd] = line.split(' from ')
  const [s, d] = sd.split(' to ').map(v => +v)
  return [+q.slice('move '.length), s, d]
}

function readCranes(): [Stacks, Moves]  {
  const [s, m] = fs.readFileSync('day5.txt','utf8').split(/\r?\n\r?\n/).
    map(v => v.split(/\r?\n/))

  const stacks = 
    s.slice(0, -1).reduce((acc, cur) => parseLine(cur, acc), <Stacks>null)
  const moves = m.map(l => parseMove(l))

  return [stacks, moves]
}

function moveCrates(s: Stacks, m: Move, r: boolean = true): void {
  let crates = s[m[1] - 1].splice(-m[0])
  if (r)
    crates = crates.reverse()
  s[m[2] - 1].push(...crates)
}

function rearrange(s: Stacks, moves: Moves, r: boolean = true): string {
  moves.forEach(m => moveCrates(s, m, r))

  return s.reduce((acc, cur) => acc += cur[cur.length - 1], '')
}

const dupe = (s: Stacks): Stacks =>
  s.map(a => Array.from(a))

const s = parseLine("    [D]    ")
parseLine("[N] [C]    ", s)
print(parseLine("[Z] [M] [P]", s))
moveCrates(s, [1, 2, 1])
print(s)
moveCrates(s, [3, 1, 3])
print(s)

print(parseMove("move 10 from 2 to 1"))

const [stacks, moves] = readCranes()
//print(stacks)
//print(moves)
print("A1:", rearrange(dupe(stacks), moves))

print("------------------")

print("A2:", rearrange(stacks, moves, false))
