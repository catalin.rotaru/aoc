import fs = require("fs")

const print = console.log.bind(console)

type Calories = Array<Array<number>>

function readCalories(): Calories {
  return fs.readFileSync('day1.txt','utf8').split(/\r?\n\r?\n/).map(l => 
    l.split(/\r?\n/).map(v => +v))
}

const sumCalories = (c: Calories): Array<number> =>
  c.map(e => e.reduce((acc, cur) => acc + cur)).sort((a, b) => b - a)

const calories = readCalories()
const elves = sumCalories(calories)

print("A1:", elves[0])

print("------------------")

print("A2:", elves[0] + elves[1] + elves[2])