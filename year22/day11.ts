import fs = require("fs")

const print = console.log.bind(console)

type Items = Array<Array<number>>
type Op = [string, number] // op, argument
type Ops = Array<Op>
type Test = [number, number, number] // test, true option, false option
type Tests = Array<Test>
type Inspected = Array<number>

const operation = {
  '+': (old: number, arg: number) => old + (arg ? arg : old),
  '*': (old: number, arg: number) => old * (arg ? arg : old)
}

function readMonkeys(): [Items, Ops, Tests] {
  let it: Items = new Array()
  let op: Ops = new Array()
  let t: Tests = new Array()

  fs.readFileSync('day11.txt','utf8').split(/\r?\n\r?\n/).forEach(m => {
    const l = m.split(/\r?\n/)

    it.push(l[1].slice("  Starting items: ".length).split(', ').map(v => +v))
    op.push([l[2].charAt("  Operation: new = old ".length), 
      +l[2].slice("  Operation: new = old * ".length)])
    t.push([+l[3].slice("  Test: divisible by ".length),
      +l[4].slice("    If true: throw to monkey ".length),
      +l[5].slice("    If false: throw to monkey ".length)])
  })

  return [it, op, t]
}

function inspect(it: Items, op: Ops, t: Tests, m: number, f: number = 0): void {
  while (it[m].length > 0) {
    const item = it[m].shift()
    const stress = operation[op[m][0]](item, op[m][1])
    const relief = f ? (stress % f) : Math.floor(stress / 3)
    const dest = t[m][(relief % t[m][0]) ? 2 : 1]
    it[dest].push(relief)
  }
}

function round(it: Items, op: Ops, t: Tests, count: number = 1, f: boolean = false): Inspected {
  let done = new Array(it.length).fill(0)
  let floor = f ? t.reduce((acc, [cur, _t, _f]) => acc * cur, 1) : 0

  for (let i = 0; i < count; i++)
    it.forEach((e, m) => {
      done[m] = done[m] + e.length
      inspect(it, op, t, m, floor)
    })

  return done
}

function business(d: Inspected): number {
  d.sort((a, b) => b - a)
  return d[0] * d[1]
}

const [items, ops, tests] = readMonkeys()
//print(items)
const done = round(items.map(e => Array.from(e)), ops, tests, 20)
//print (items)
//print(done)

print("A1:", business(done))

print("------------------")

const done2 = round(items, ops, tests, 10000, true)
print("A2:", business(done2))