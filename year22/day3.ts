import fs = require("fs")

const print = console.log.bind(console)

type Bags = Array<string>

const intersect = (s1: Set<any>, s2: Set<any>): Set<any> =>
  new Set([...s1].filter(x => s2.has(x)))

function findCommon(bag: string): string {
  const mid = bag.length / 2
  const a = bag.substring(0, mid)
  const b = bag.substring(mid)
  //print(a, b)

  const s1 = new Set(a.split(''))
  const s2 = new Set(b.split(''))

  const [res] = intersect(s1, s2)
  return res
}

const priority = (s: string): number =>
  1 + s.charCodeAt(0) - 
    ((s == s.toLowerCase()) ? 'a'.charCodeAt(0) : ('A'.charCodeAt(0) - 26))

const sumPriority = (bags: Bags): number =>
  bags.reduce((acc, cur) => acc + priority(findCommon(cur)), 0)

function readBags(): Bags {
  return fs.readFileSync('day3.txt','utf8').split(/\r?\n/)
}

const groupBags = (bags: Bags, n: number = 3): Array<Bags> =>
Array.from({ length: bags.length / n}).map((_, i) => 
  bags.slice(i * n, (i + 1) * n))

function badge(bags: Bags): string {
  const s1 = new Set(bags[0].split(''))
  const s2 = new Set(bags[1].split(''))
  const s3 = new Set(bags[2].split(''))

  const [res] = intersect(intersect(s1, s2), s3)
  return res
}

const sumBadges = (bags: Array<Bags>): number =>
  bags.map(badge).reduce((acc, cur) => acc + priority(cur), 0)

//print(priority('a'), priority('z'), priority('A'), priority('Z'))
//print(findCommon('vJrwpWtwJgWrhcsFMMfFFhFp'))

const b = readBags()
print("A1:", sumPriority(b))

print("------------------")

//print(groupBags(b).map(badge))

print("A2:", sumBadges(groupBags(b)))