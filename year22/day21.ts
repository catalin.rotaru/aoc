import fs = require("fs")

const print = console.log.bind(console)

type Recipes = Map<string, string>
type Monkeys = Map<string, Monkey>

class Monkey {
  readonly name: string
  private count: number
  private readonly operand: string
  private readonly left: Monkey
  private readonly right: Monkey
  private variable: boolean

  constructor(r: Recipes, m: Monkeys, n: string) {
    this.name = n

    const s = r.get(n)
    if (/^\d/.test(s)) {
      this.count = +s
      this.variable = (n == 'humn')
    } else {
      //this.count = undefined
      const op = s.split(' ')
      this.operand = op[1]
      this.left = new Monkey(r, m, op[0])
      this.right = new Monkey(r, m, op[2])
    }

    m.set(n, this)
  }

  getCount(): number {
    if (this.count == undefined)
      this.count = eval(this.left.getCount() + this.operand + 
        this.right.getCount())
    return this.count
  }

  isVar(): boolean {
    if (this.variable == undefined)
      this.variable = (this.left.isVar() || this.right.isVar())
    return this.variable
  }

  equalize(val: number = 0) {
    if (this.name == 'root') {
      if (this.left.isVar())
        this.left.equalize(this.right.getCount())
      else
        this.right.equalize(this.left.getCount())
    } else if (this.operand == undefined) {
      print("A2:", val)
    } else {
      switch (this.operand) {
        case '+':
          if (this.left.isVar())
            this.left.equalize(val - this.right.getCount())
          else
            this.right.equalize(val - this.left.getCount())
          break

        case '-':
          if (this.left.isVar())
            this.left.equalize(val + this.right.getCount())
          else
            this.right.equalize(this.left.getCount() - val)
          break

        case '*':
          if (this.left.isVar())
            this.left.equalize(val / this.right.getCount())
          else
            this.right.equalize(val / this.left.getCount())
          break
        
        case '/':
          if (this.left.isVar())
            this.left.equalize(val * this.right.getCount())
          else
            this.right.equalize(this.left.getCount() / val)
          break
      }
    }
  }
}

function readJobs(): Recipes {
  const res = new Map()
  fs.readFileSync('day21.txt','utf8').split(/\r?\n/).forEach(l => {
    const [k, v] = l.split(': ')
    res.set(k, v)
  })

  return res
}

const rec = readJobs()
//print(rec)

let mk = new Map()
const root = new Monkey(rec, mk, "root")
print("A1:", root.getCount())

print("------------------")

root.equalize()
