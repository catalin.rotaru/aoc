import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Grid = Array<Array<number>>  // Array[y][x]
type Visible = Array<Array<boolean>>

function readTrees(): Grid {
  return fs.readFileSync('day8.txt','utf8').split(/\r?\n/).map(l => l.split('').
    map(v => +v))
}

function visibility(t: Grid): Visible {
  let ml = t.map(l => l.map(_ => -1))
  let mr = t.map(l => l.map(_ => -1))
  let mt = t.map(l => l.map(_ => -1))
  let mb = t.map(l => l.map(_ => -1))

  for (let y = 0; y < t.length; y++) { // top to bottom
    let cm = -1
    for (let x = 0; x < t[y].length; x++) { // left to right
      ml[y][x] = cm
      cm = Math.max(cm, t[y][x])
    }

    cm = -1
    for (let x = t[y].length - 1; x >= 0; x--) { // right to left
      mr[y][x] = cm
      cm = Math.max(cm, t[y][x])
    }
  }

  for (let x = 0; x < t[0].length; x++) { // left to right
    let cm = -1
    for (let y = 0; y < t.length; y++) { // top to bottom
      mt[y][x] = cm
      cm = Math.max(cm, t[y][x])
    }

    cm = -1
    for (let y = t.length - 1; y >= 0; y--) { // bottom to top
      mb[y][x] = cm
      cm = Math.max(cm, t[y][x])
    }
  }

  //printGrid(ml)
  //printGrid(mr)  
  //printGrid(mt)
  //printGrid(mb)

  return t.map((l, y) => l.map((v, x) => 
    v > Math.min(ml[y][x], mr[y][x], mt[y][x], mb[y][x], )))
}

function score(t: Grid, x: number, y: number, h: number): number {
  let i = x - 1
  for (; (i >= 0) && (t[y][i] < h); i--);
  const left = x - i - +(i < 0)

  i = x + 1
  for (; (i < t[y].length) && (t[y][i] < h); i++);
  const right = i - x - +(i == t[y].length)

  let j = y - 1
  for (; (j >= 0) && (t[j][x] < h); j--);
  const top = y - j - +(j < 0)

  j = y + 1
  for (; (j < t.length) && (t[j][x] < h); j++);
  const bottom = j - y - +(j == t.length)

  //print([left, right, top, bottom])
  return left * right * top * bottom
}

function maxScore(t: Grid): number {
  const scores  = t.map((l, y) => l.map((v, x) => score(t, x, y, v)))
  //printGrid(scores)

  return Math.max(...scores.map(l => Math.max(...l)))
}

const trees = readTrees()
//printGrid(trees)

const vis = visibility(trees)
//print(vis)

print("A1:", vis.reduce((acc, cur) => acc + cur.filter(v => v).length, 0))

print("------------------")

print (score(trees, 2, 1, 5))
//print (score(trees, 2, 0, 3))
//print (score(trees, 1, 4, 5))
//print (score(trees, 4, 1, 2))
print (score(trees, 3, 2, 5))

print("A2:", maxScore(trees))