import fs = require("fs")

const print = console.log.bind(console)

type Packet = Array<Value>
type Value = number | Packet
type Pair = [Packet, Packet]
type Pairs = Array<Pair>
type Packets = Array<Packet>

const numberEnd = (s: string): number =>
  s.search(/\D/)

function parse(s: string): [Value, number] {
  const ch = s[0]
  let res: Value
  let off: number

  if (ch == '[') { // is array
    res = new Array()
    off = 1
    while (s.charAt(off) != ']') {
      let [n, o] = parse(s.slice(off))
      res.push(n)
      off += o
      if (s.charAt(off) == ',')
        off++
    } 
    off++
  } else { // is number
    off = numberEnd(s)
    res = +s.slice(0, off)
  }

  return [res, off]
}

function compare(a: Value, b: Value): number {
  if ((!Array.isArray(a)) && (!Array.isArray(b)))
    return a - b

  const va = Array.isArray(a) ? a : [a]
  const vb = Array.isArray(b) ? b : [b]
  
  for (let i = 0; i < Math.min(va.length, vb.length); i++) {
    const r = compare(va[i], vb[i])
    if (r)  
      return r
  }
  
  return va.length - vb.length
}

function readPairs(): Pairs {
  return fs.readFileSync('day13.txt','utf8').split(/\r?\n\r?\n/).
    map(p => <Pair>p.split(/\r?\n/).map(s => parse(s)[0]))
}

const sumOrdered = (p: Pairs) =>
  p.reduce((acc, cur, idx) => acc + 
  ((compare(cur[0], cur[1]) < 0) ? idx + 1 : 0), 0)

const findValue = (v: Value, n: number): boolean =>
  Array.isArray(v) && Array.isArray(v[0]) && v[0][0] == n

function organize(p: Packets): number {
  p.push(<Packet>parse("[[2]]")[0])
  p.push(<Packet>parse("[[6]]")[0])
  p.sort(compare)

  const i = p.findIndex(e => findValue(e, 2))
  const j = p.findIndex(e => findValue(e, 6))

  return (i + 1) * (j + 1)
}

print(parse("123,456"))
print(parse("[1,22,333]")[0])
print(parse("[[1],[2,3,4]]")[0])
print(parse("[[1],4]")[0])
print(parse("[9]")[0])
print(parse("[[8,7,6]]")[0])
print(parse("[[]]")[0])
print(parse("[1,[2,[3,[4,[5,6,7]]]],8,9]")[0])

print(compare(parse("[1,1,3,1,1]")[0], parse("[1,1,5,1,1]")[0]))
print(compare(parse("[[1],[2,3,4]]")[0], parse("[[1],4]")[0]))
print(compare(parse("[9]")[0], parse("[[8,7,6]]")[0]))
print(compare(parse("[[4,4],4,4]")[0], parse("[[4,4],4,4,4]")[0]))
print(compare(parse("[7,7,7,7]")[0], parse("[7,7,7]")[0]))
print(compare(parse("[]")[0], parse("[3]")[0]))
print(compare(parse("[[[]]]")[0], parse("[[]]")[0]))
print(compare(parse("[1,[2,[3,[4,[5,6,7]]]],8,9]")[0],
  parse("[1,[2,[3,[4,[5,6,0]]]],8,9]")[0]))

const ps = readPairs()
//print(ps)

//print(ps.map(p => compare(p[0], p[1])))

print("A1:", sumOrdered(ps))

print("------------------")

print("A2:", organize(ps.flat()))