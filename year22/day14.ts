import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Cell = [number, number] // x, y
type Cells = Array<Cell>
type Walls = Array<Cells>
type Grid = Array<Array<string>> // y, x

enum Tile {ROCK = '#', AIR = '.', SAND = 'o'}
const fall = [0, -1, 1] // array of x

function readWalls(): Walls {
  return fs.readFileSync('day14.txt','utf8').split(/\r?\n/).
    map(l => l.split(' -> ').map(p => <Cell>p.split(',').map(v => +v)))
}

function bounds(w: Walls): [number, number, number, number] {
  const c = w.flat()
  const xs = c.map(v => v[0])
  const mnx = Math.min(...xs)
  const max = Math.max(...xs)
  const may = Math.max(...c.map(v => v[1]))

  return [mnx, max, 1, may]
}

function drawLine(g: Grid, o: Cell, a: Cell, b: Cell): void {
  const dx = Math.sign(b[0] - a[0])
  const dy = Math.sign(b[1] - a[1])
  const [ax, ay] = translate(a, o)
  const [bx, by] = translate(b, o)

  for (let i = ax, j = ay; (i != bx + dx) || (j != by + dy); i += dx, j += dy)
    g[j][i] = Tile.ROCK
}

const translate = (c: Cell, o: Cell): Cell =>
  [c[0] - o[0], c[1] - o[1]]

function drawGrid(walls: Walls, pad: number = 1): [Grid, Cell] {
  const [mnx, max, mny, may] = bounds(walls)
  const o: Cell = [mnx - pad, mny - 1]
  print("Bounds, Origin: ", [mnx, max, mny, may], o)

  let g = Array.from({length: may - mny + 3}, v => 
    new Array(max - mnx + 1 + pad * 2).fill(Tile.AIR))

  walls.forEach(w => {
    for (let i = 0; i < w.length - 1; i ++)
      drawLine(g, o, w[i], w[i + 1])
  })

  return [g, o]
}

function grain(g: Grid, o: Cell): boolean {
  let [x, y] = translate([500, o[1]], o)
  if (g[y][x] != Tile.AIR)
    return false
  
  for (; y < g.length - 1; y++ ) {
    g[y][x] = Tile.SAND

    const n = fall.find(dx => g[y + 1][x + dx] == Tile.AIR)
    if (n == undefined)
      break

    g[y][x] = Tile.AIR
    x += n
  }

  return y < g.length - 1
}

function sand(g: Grid, o: Cell): number {
  let res = 0
  for (;grain(g, o); res++);
  return res
}

const walls = readWalls()
//print(walls)

const [grid, origin] = drawGrid(walls)
print("A1:", sand(grid, origin))
//printGrid(grid)

print("------------------")

const [grid2, origin2] = drawGrid(walls, 1000)
grid2.push(new Array(grid2[0].length).fill(Tile.ROCK))
print("A2:", sand(grid2, origin2))
//printGrid(grid2)
