import fs = require("fs")

const print = console.log.bind(console)

type Grid = Array<Array<number>> // y, x
type Cell = [number, number] // x, y
type Cells = Array<Cell>
type Dist = Array<Array<number>>

const neighbor = [[0, 1], [0, -1], [1, 0], [-1, 0]] // array of [x, y]
const UNVISITED = Number.MAX_SAFE_INTEGER
const START = 'a'.charCodeAt(0)

function readGrid(): Grid {
  return fs.readFileSync('day12.txt','utf8').split(/\r?\n/).
    map(l => l.split('').map(s => s.charCodeAt(0)))
}

function getLimits(grid: Grid): [Cell, Cell] {
  const b = 'S'.charCodeAt(0)
  const e = 'E'.charCodeAt(0)

  let begin: Cell
  let end: Cell

  grid.forEach((l, y) => l.forEach((v, x) => {
    if (v == b) {
      begin = [x, y]
      grid[y][x] = START
    }
    if (v == e) {
      end = [x, y]
      grid[y][x] = 'z'.charCodeAt(0)
    }
  }))

  return [begin, end]
}

const onGrid = (g: Grid, x: number, y: number): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, x: number, y: number): Cells =>
  neighbor.map(([dx, dy]) => <Cell>[x + dx, y + dy]).
    filter(([x, y]) => onGrid(g, x, y))

function climb(grid: Grid, dist: Dist, x: number, y: number, steps: number = 0) {
  dist[y][x] = steps
  const ns = steps + 1
  neighbors(grid, x, y).filter(([i, j]) => 
    (grid[j][i] <= grid[y][x] + 1) && (dist[j][i] > ns))
    .forEach(([nx, ny]) => climb(grid, dist, nx, ny, ns))
}

function search(grid: Grid, begin: Cell, end: Cell): number {
  let dist = grid.map(l => new Array(l.length).fill(UNVISITED))
  climb(grid, dist, ...begin)
  
  return dist[end[1]][end[0]]
}

function hikes(grid: Grid, end: Cell): number {
  const d: Dist = grid.map((l, y) => l.map((v, x) => 
    (v == START) ? search(grid, [x, y], end) : Number.MAX_SAFE_INTEGER))

  return Math.min(...d.map(l => Math.min(...l)))
}

const grid = readGrid()
//print(grid)
const [begin, end] = getLimits(grid)
print(begin, end)
//print(grid)
print(neighbors(grid, 0, 0))

print("A1:", search(grid, begin, end))

print("------------------")

print("A2:", hikes(grid, end))