import fs = require("fs")

const print = console.log.bind(console)

type List = Array<number>

// l: list of numbers, p: numbers at positions, idx: of number to be mixed
function mix2(l: List, p: List, i: number): void {
  const poz = p.indexOf(i)
  const v = l[i]
  const n = (poz + v) % (l.length - 1)
  p.splice(poz, 1)
  p.splice(n, 0, i)
  //print(p.map(v => l[v]).join(','))
}

function mixAll2(l: List, count: number = 1): List {
  let p = l.map((_, i) => i)
  for (let j = 0; j < count; j++)
    for (let i = 0; i < l.length; i++)
      mix2(l, p, i)
  return p.map(v => l[v])
}

function score(l: List): number {
  const o = l.indexOf(0)
  return l[(o + 1000) % l.length] + l[(o + 2000) % l.length] + 
    l[(o + 3000) % l.length]
}

function readList(): List {
  return fs.readFileSync('day20.txt','utf8').split(/\r?\n/).map(l => +l)
}

const decrypt = (l: List): List =>
  l.map(v => v * 811589153) 

const l0 = [1, 2, -3, 3, -2, 0, 4]
print(score(mixAll2(l0)))

const list = readList()
print("list:", list.length, "set:", new Set(list).size)

print("A1:", score(mixAll2(list)))

print("------------------")

print(score(mixAll2(decrypt(l0), 10)))

print("A2:", score(mixAll2(decrypt(list), 10)))
