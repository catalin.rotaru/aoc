import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Cell = [number, number] // x, y
type Cells = Array<Cell>
type Grid = Array<Array<string>> // y, x

enum Dir {LEFT = -1, DOWN = 0, RIGHT = 1}
const Push = {'<': Dir.LEFT, '>': Dir.RIGHT}
enum Tile {ROCK = '#', AIR = '.'}
const Size: Cells = [[4, 1], [3, 3], [3, 3], [1, 4], [2, 2]]
const Shape: Array<Cells> = [[[0,0], [1,0], [2,0], [3,0]], 
  [[1,0], [0,1], [1,1], [2,1], [1,2]], 
  [[2,0], [2,1], [0,2], [1,2], [2,2]], 
  [[0,0], [0,1], [0,2], [0,3]], 
  [[0,0], [1,0], [0,1], [1,1]]]

const initGrid = (width: number = 7) =>
  new Array(new Array(width).fill(Tile.AIR))

function freeRows(grid: Grid): number {
  const h = grid.findIndex(row => row.includes(Tile.ROCK))
  return h < 0 ? grid.length : h
}

function makeSpace(grid: Grid, height: number): number {
  const a = freeRows(grid)
  const need = height + 3 - a
  for (let i = 0; i < need; i++ )
    grid.unshift(new Array(grid[0].length).fill(Tile.AIR))

  return (need < 0) ? a : a + need
}

const canPlace = (grid: Grid, o: Cell, piece: Cells, size: Cell): boolean =>
  (o[0] >= 0) && (o[0] + size[0] <= grid[0].length) && 
  (o[1] + size[1] <= grid.length) && 
  piece.find(([x, y]) => grid[o[1] + y][o[0] + x] == Tile.ROCK) == undefined

const placePiece = (grid: Grid, o: Cell, piece: Cells): void =>
  piece.forEach(([x, y]) => grid[o[1] + y][o[0] + x] = Tile.ROCK)

const partial = (a: Array<number>, s: number, l: number): number =>
  a.slice(s, s + l).reduce((acc, cur) => acc + cur)


function play(g: Grid, wind: string, count: number = 2022 ): number {
  let cache: Array<number> = new Array()
  let seen: Map<string, number> = new Map() // hash => piece count seen at

  let w = 0 // wind index 
  let i = 0 // piece count
  let cycle: number // cycle start

  for (; i < count; i++) {
    const p = i % Shape.length // piece index
    const piece = Shape[p]
    const size = Size[p]

    let x = 2
    let free = makeSpace(g, size[1])
    const tower = g.length - free
    let y = free - 3 - size[1]

    for (;;) {
      const dx = Push[wind[w]]
      w = (w + 1) % wind.length

      if (canPlace(g, [x + dx, y], piece, size))
        x += dx

      if (!canPlace(g, [x, y + 1], piece, size))
        break
      
      y++
    }

    placePiece(g, [x, y], piece)

    free = freeRows(g)
    const added = g.length - free - tower
    const hash = p + ':' + w + ':' + g.slice(free, free + 20).flat().join('')
    if (seen.has(hash)) {
      cycle = seen.get(hash)
      //print("Seen", i, "at", cycle_start)
      break
    } else {
      cache.push(added)
      seen.set(hash, i)
    }
  }

  if (i == count)
    return g.length - freeRows(g)

  const cycle_len = i - cycle
  const pre_cycle = partial(cache, 0, cycle)
  const in_cycle = partial(cache, cycle, cycle_len)
  const cycles = Math.floor((count - cycle) / cycle_len)
  const after_cycle = partial(cache, cycle, (count - cycle) % cycle_len)
  return pre_cycle + in_cycle * cycles + after_cycle
}

let g0 = initGrid()
//printGrid(g0)
print(freeRows(g0))
makeSpace(g0, 1)
//printGrid(g0)
print(canPlace(g0, [3, 3], Shape[0], Size[0]))
placePiece(g0, [0, 0], Shape[4])
//printGrid(g0)

const g = initGrid()
print(play(g, ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>"))
//printGrid(g)

const wnd =  fs.readFileSync('day17.txt','utf8')
print("A1:", play(initGrid(), wnd))

print("------------------")

print("A2:", play(initGrid(), wnd, 1000000000000))
