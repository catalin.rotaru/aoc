import fs = require("fs")

const print = console.log.bind(console)

type Cell = [number, number, number] // [x, y, z]
type Cells = Array<Cell>
type Grid = Array<Array<Array<number>>> // [z] [y] [x]

enum Tile {AIR, LAVA, STEAM}
const Neighbor: Cells = // array of [x, y, z]
  [[0, 1, 0], [0, -1, 0], [1, 0, 0], [-1, 0, 0], [0, 0, 1], [0, 0, -1]]

function readDroplets(): Cells {
  return fs.readFileSync('day18.txt','utf8').split(/\r?\n/).
    map(l => <Cell>l.split(',').map(v => +v))
}

const bounds = (a: Array<number>): [number, number] =>
  [Math.min(...a), Math.max(...a)]

const translate = (c: Cell, o: Cell): Cell =>
  [c[0] - o[0], c[1] - o[1], c[2] - o[2]]

function initGrid(drops: Cells): Grid {
  const [mix, max] = bounds(drops.map(v => v[0]))
  const [miy, may] = bounds(drops.map(v => v[1]))
  const [miz, maz] = bounds(drops.map(v => v[2]))
  const o: Cell = [mix, miy, miz]
  let res = Array.from({length: maz - miz + 1}, _ => 
    Array.from({length: may - miy + 1}, _ =>
      new Array(max - mix + 1).fill(Tile.AIR)))

  drops.forEach(d => {
    const p = translate(d, o)
    res[p[2]][p[1]][p[0]] = Tile.LAVA
  })

  return res
}

const onGrid = (g: Grid, x: number, y: number, z: number): boolean =>
  ((z >= 0) && (z < g.length) && (y >= 0) && (y < g[0].length) && 
    (x >= 0) && (x < g[0][0].length))

const neighbors = (g: Grid, x: number, y: number, z: number): Cells =>
  Neighbor.map(([dx, dy, dz]) => <Cell>[x + dx, y + dy, z + dz]).
    filter(([x, y, z]) => onGrid(g, x, y, z))

const sides = (g: Grid, valid: Tile = Tile.AIR): number =>
  g.reduce((ap, p, z) => ap + p.reduce((ar, r, y) => 
    ar + r.reduce((acc, cur, x) => 
      acc + (cur == Tile.LAVA ? 6 - neighbors(g, x, y, z).filter(n => 
        g[n[2]][n[1]][n[0]] != valid).length : 0), 0), 0), 0)

function steam(g: Grid): void {
  let next: Cells = new Array()

  for (let z = 0; z < g.length; z++)
    for (let y = 0; y < g[0].length; y++)
      for (let x = 0; x < g[0][0].length; x++)
        if ((z == 0) || (z == g.length - 1) ||
          (y == 0) || (y == g[0].length - 1) ||
          (x == 0) || (x == g[0][0].length - 1))
          next.push([x, y, z])

  while (next.length > 0) {
    const [x, y, z] = next.pop()
    if (g[z][y][x] != Tile.AIR)
      continue

    g[z][y][x] = Tile.STEAM
    next.push(...neighbors(g, x, y, z))
  }
}

const lava = readDroplets()
//print(lava)
const grid = initGrid(lava)
//print(grid)
print("sizes:", grid.length, grid[0].length, grid[0][0].length)

print("A1:", sides(grid))

print("------------------")

steam(grid)
//print(grid)

print("A2:", sides(grid, Tile.STEAM))
