import fs = require("fs")

const print = console.log.bind(console)

type Range = [number, number]
type Pair = [Range, Range]
type Pairs = Array<Pair>
type CountTest = (r1: Range, r2: Range) => boolean

const contains = (r1: Range, r2: Range): boolean =>
  ((r1[0] >= r2[0]) && (r1[1] <= r2[1])) || 
  ((r1[0] <= r2[0]) && (r1[1] >= r2[1]))

const countPairs = (p: Pairs, test: CountTest = contains): number =>
  p.reduce((acc, cur) => acc + +test(...cur), 0)

function readPairs(): Pairs {
  return fs.readFileSync('day4.txt','utf8').split(/\r?\n/).
    map(l => <Pair>l.split(',').map(r => r.split('-').map(v => +v)))
}

const overlaps = (r1: Range, r2: Range): boolean =>
  ((r1[0] <= r2[0]) && (r1[1] >= r2[0])) || 
  ((r1[0] <= r2[1]) && (r1[1] >= r2[1])) ||
  ((r2[0] <= r1[0]) && (r2[1] >= r1[0]))

print(contains([2, 4], [6, 8]))
print(contains([2, 8], [3, 7]))
print(contains([6, 6], [4, 6]))

const pairs = readPairs()
//print(pairs)

print("A1:", countPairs(pairs))

print("------------------")

print("A2:", countPairs(pairs, overlaps))
