import fs = require("fs")

const print = console.log.bind(console)

const readMoves = (): string =>
  fs.readFileSync('day1.txt','utf8')

function navigate(s: string): number {
  const move = s.split('')
  const up = move.filter(v => v === '(').length
  return up - (move.length - up)
}

function navigate2(s: string): number {
  const move = s.split('')
  let pos = 0
  return 1 + move.findIndex(v => ((pos += (v == '(') ? 1 : -1) == -1))
}

print(navigate("(()(()("))

const moves = readMoves()

print("A1:", navigate(moves))

print("------------------")

print("A2:", navigate2(moves))
