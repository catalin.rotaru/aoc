import fs = require("fs")

const print = console.log.bind(console)

const move = {'^': [0, -1], 'v': [0, 1], '<': [-1, 0], '>': [1, 0]}

type Location = [number, number]

const readMoves = (): string =>
  fs.readFileSync('day3.txt','utf8')

const navigate=(o: Location, d: string): Location =>
  [o[0] + move[d][0], o[1] + move[d][1]]

function deliver(m: string): number {
  let h: Set<string> = new Set()
  moves.split('').reduce((acc, cur) => {
    h.add(acc[0] + 'x' + acc[1])
    return navigate(acc as Location, cur)
  }, [0, 0])

  return h.size
}

function deliver2(m: string): number {
  let h: Set<string> = new Set()
  let bot: Array<Array<string>> = [new Array(), new Array()]

  moves.split('').forEach((e, i) => bot[i % 2].push(e))
  bot.forEach(b => b.reduce((acc, cur) => {
    h.add(acc[0] + 'x' + acc[1])
    return navigate(acc as Location, cur)
  }, [0, 0]))

  return h.size 
}

print(navigate([0, 0], '>'))

const moves = readMoves()

print("A1:", deliver(moves))

print("------------------")

print("A2:", deliver2(moves))
