import fs = require("fs")

const print = console.log.bind(console)

type Box = [number, number, number] // [length, width, height]

const readBoxes = (): Array<Box> =>
  fs.readFileSync('day2.txt','utf8').split(/\r?\n/).map(v => parse(v))

const parse = (s: String): Box =>
  s.split('x').map(s => +s) as Box

function area(p: Box): number {
  const s1 = p[0] * p[1]
  const s2 = p[1] * p[2]
  const s3 = p[2] * p[0]

  return 2 * (s1 + s2 + s3) + Math.min(s1, s2, s3)
}

function ribbon(p: Box): number {
  const p1 = p[0] + p[1]
  const p2 = p[1] + p[2]
  const p3 = p[2] + p[0]

  return 2 * Math.min(p1, p2, p3) + p[0] * p[1] * p[2]
}

print(area(parse("2x3x4")))

const boxes = readBoxes()

print("A1:", boxes.reduce((acc, cur) => acc + area(cur), 0))

print("------------------")

print(ribbon(parse("2x3x4")))
print(ribbon(parse("1x1x10")))

print("A2:", boxes.reduce((acc, cur) => acc + ribbon(cur), 0))
