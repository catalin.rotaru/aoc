import fs = require("fs")

const print = console.log.bind(console)

type Subset = [number, number, number] // [red, green, blue]
type Game = Array<Subset>
type Games = Array<Game>

const cube = {'red': 0, 'green': 1, 'blue': 2}

function readSubset(s: string): Subset {
  let res: Subset = [0, 0, 0]

  s.split(', ').forEach(v => {
    const [count, color] = v.split(' ')
    res[cube[color]] = +count
  })
  return res
}

const readGame = (s: string): Game =>
  s.split(': ')[1].split('; ').map(readSubset)

const readGames = (): Games =>
  fs.readFileSync('day2.txt','utf8').split(/\r?\n/).map(readGame)

const possible = (g: Subset): boolean =>
  (g[0] <= 12) && (g[1] <= 13) && (g[2] <= 14)

const sumPossibles = (games: Games): number =>
  games.reduce((acc, cur, idx) => acc + (cur.every(possible) ? idx + 1 : 0), 0)

const power = (game: Game): number =>
  Math.max(...game.map(g => g[0])) * Math.max(...game.map(g => g[1])) *
  Math.max(...game.map(g => g[2]))

print(readSubset("1 red, 2 green, 6 blue"))
print(readSubset("5 blue, 4 red, 13 green"))

print(readGame("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"))

const games = readGames()

print ("A1:", sumPossibles(games))

print("------------------")

print(power(readGame("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red")))

print ("A12", games.reduce((acc, cur) => acc + power(cur), 0))
