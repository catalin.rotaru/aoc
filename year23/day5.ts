import fs = require("fs")

type Seeds = Array<number>
type Mapping = [number, number, number] // [src start, src end, offset]
type Mappings = Array<Array<Mapping>>
type Range = [number, number] // [start, end]
type Ranges = Array<Range>

const print = console.log.bind(console)

function readMapping(s: string): Mapping {
  const [dest, src, len] = s.split(' ').map(v => +v)
  return [src, src + len, dest - src]
}

function readMappings(): [Seeds, Mappings] {
  const r = fs.readFileSync('day5.txt','utf8').split(/\r?\n\r?\n/)
  const s = r[0].split(' ').slice(1).map(v => +v)
  const m = r.slice(1).map(l => l.split(/\r?\n/).slice(1).map(readMapping))

  return [s, m]
}

function remap(seed: number, map: Array<Mapping>): number {
  const m = map.find(([start, end, _]) => 
    (seed >= start) && (seed < end))

  return m ? seed + m[2] : seed
}

const translate = (seed: number, map: Mappings): number =>
  map.reduce((acc, cur) => remap(acc, cur), seed)

// return [[common], [remainder]]
function intersect(r: Range, m: Range): [Ranges, Ranges] {
  const [a, b] = r
  const [s, e] = m

  if ((b <= s) || (a >= e))
    return [[], [r]]

  if (a < s) {
    if (b <= e) // s < b <= e
      return [[[s, b]], [[a, s]]]
    else // a < s < e < b
      return [[[s, e]], [[a, s], [e, b]]]
  } else {  // s <= a < e
    if (b <= e)
      return [[[a, b]], []]
    else // e < b
      return [[[a, e]], [[e, b]]]
  }    
}

// return [[common], [remainder]]
const intersectRanges = (r: Ranges, m: Range): [Ranges, Ranges] =>
  r.reduce((acc, cur) => {
    const [com, rem] = intersect(cur, m)
    return [acc[0].concat(com), acc[1].concat(rem)]
  }, [[], []])

// return [[remapped], [remainder]]
function remapRanges(r: Ranges, m: Mapping): [Ranges, Ranges] {
  const [s, e, o] = m
  const [com, rem] = intersectRanges(r, [s, e])
  return [com.map(v => [v[0] + o, v[1] + o]), rem]
}

function translateRanges(r: Ranges, m: Array<Mapping>): Ranges {
  const [rl, mpd] = m.reduce((acc, cur) => {
    const [com, rem] = remapRanges(acc[0], cur)
    return [rem, acc[1].concat(com)]
  }, [r, []])

  return rl.concat(mpd)
}

const processRanges = (r: Ranges, m: Mappings): Ranges =>
  m.reduce((acc, cur) => translateRanges(acc, cur), r)

const seeds2ranges = (seeds: Seeds): Ranges =>
  seeds.map((s, i) => 
    (i % 2 == 0) ? [s, s + seeds[i + 1]] : undefined).filter(v => v) as Ranges

print(readMapping("50 98 2"))
print(remap(100, [readMapping("50 98 2")]))

const [seeds, maps] = readMappings()
//print(maps)
//print(translate(79, maps))
//print(translate(14, maps))
//print(translate(55, maps))
//print(translate(13, maps))

//print(seeds.map(s => translate(s, maps)))

print("A1:", Math.min(...seeds.map(s => translate(s, maps))))

print("------------------")

print(intersect([5, 25], [10, 20]))
//print(remapRanges([[0, 10], [20, 30], [40, 50]], [5, 45, 1000]))
//print(translateRanges([[0, 10], [20, 30], [40, 50]], [[5, 22, 1000], [28, 45, 2000]]))

const ranges = seeds2ranges(seeds)
//print (ranges)

const loc = processRanges(ranges, maps)
//print(loc)

print("A2:", Math.min(...loc.map(v => v[0])))
