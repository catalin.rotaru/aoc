import fs = require("fs")

const print = console.log.bind(console)

const digit = ["zero", "one", "two", "three", "four", "five", "six", "seven", 
  "eight", "nine"]

const readLines = (): Array<string> =>
  fs.readFileSync('day1.txt','utf8').split(/\r?\n/)

const getValues = (lines: Array<string>): Array<number> =>
  lines.map(l => +(l.charAt(l.search(/\d/)) + l.charAt(l.search(/\d(?=\D*$)/))))

function firstDigit(s: string): string {
  let v1 = ''
  const p1 = digit.reduce((acc, cur, idx) => {
    const n = s.indexOf(cur)
    if ((n >= 0) && (n < acc)) {
      v1 = idx.toString()
      return n
    }
    return acc
  }, s.length)

  const p2 = s.search(/\d/)

  if (p2 < 0)
    return v1

  const v2 = s.charAt(p2)

  if (p1 == s.length)
    return v2

  return (p1 < p2) ? v1 : v2
}

function lastDigit(s: string): string {
  let v1 = ''
  const p1 = digit.reduce((acc, cur, idx) => {
    const n = s.lastIndexOf(cur)
    if (n > acc) {
      v1 = idx.toString()
      return n
    }
    return acc
  }, -1)

  const p2 = s.search(/\d(?=\D*$)/)

  if (p2 < 0)
    return v1

  const v2 = s.charAt(p2)

  if (p1 < 0)
    return v2

  return (p1 > p2) ? v1 : v2
}

const getValues2 = (lines: Array<string>): Array<number> =>
  lines.map(l => +(firstDigit(l) + lastDigit(l)))

const lines = readLines()
const val = getValues(lines)

//print(val)

print("A1:", val.reduce((acc, cur) =>  acc + cur))

print("------------------")

print(firstDigit("15"))
print(lastDigit("45"))

const val2 = getValues2(lines)

//print(val2)

print("A2:", val2.reduce((acc, cur) =>  acc + cur))
