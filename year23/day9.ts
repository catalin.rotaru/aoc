import fs = require("fs")

type Series = Array<number>
type Multi = Array<Series>

const print = console.log.bind(console)
const s2a = (s: string): Array<number> => s.split(" ").map(Number)

const diff = (s: Series): Series =>
  s.slice(0, -1).map((v, i) => s[i + 1] - v)

function diffs(s: Series): Multi {
  let res: Multi = new Array()

  for (let cur = s; cur.some(v => v != 0); cur = diff(cur))
    res.push(cur)

  return res
}

const predict = (m: Multi): number =>
  m.reduceRight((acc, cur) => acc + cur.at(-1), 0)

const predict2 = (m: Multi): number =>
  m.reduceRight((acc, cur) => cur[0] - acc, 0)

const readHist = (): Multi =>
  fs.readFileSync('day9.txt','utf8').split(/\r?\n/).map(s2a)

//print(diffs(s2a("0 3 6 9 12 15")))
print(predict(diffs(s2a("0 3 6 9 12 15"))))
print(predict(diffs(s2a("1 3 6 10 15 21"))))
print(predict(diffs(s2a("10 13 16 21 30 45"))))

const hist = readHist()
//print(hist)

print("A1:", hist.reduce((acc, cur) => acc + predict(diffs(cur)), 0))

print("------------------")

print("A2:", hist.reduce((acc, cur) => acc + predict2(diffs(cur)), 0))
