import fs = require("fs")

type Race = [number, number] // [time, distance]
type Races = Array<Race>

const print = console.log.bind(console)

const dist = (bt: number, tt: number): number =>
  (tt - bt) * bt

const sim = (t: number): Array<number> =>
  Array.from({length: t + 1}, (_, i) => dist(i, t))

const win = (t: number, d: number): Array<number> =>
  sim(t).filter(v => v > d)

const margin = (r: Races): number =>
  r.reduce((acc, cur) => acc * win(...cur).length, 1)

print(dist(3, 7))
print(sim(7))
print(win(7, 9))
print(margin([[7, 9], [15, 40], [30, 200]]))

print("A1:", margin([[57, 291], [72, 1172], [69, 1176], [92, 2026]]))

print("------------------")

print("A2:", win(57726992, 291117211762026).length)
