import fs = require("fs")

type Node = string
type Neighbors = [Node, Node]
type Network = Map<Node, Neighbors>

const neighbor = {'L': 0, 'R': 1}

const print = console.log.bind(console)

function readNode(s: string, n: Network = new Map()) {
  const [l, r] = s.split(' = (')
  const [nl, nr] = r.slice(0, -1).split(', ')
  //print(l + ': ' + nl + ', ' + nr)

  n.set(l, [nl, nr])
  return n
}

function readMap(): [string, Network] {
  const [p, n] = fs.readFileSync('day8.txt','utf8').split(/\r?\n\r?\n/)
  let res = new Map()
  const m = n.split(/\r?\n/).forEach(l => readNode(l, res))

  return [p, res]
}

function navigate(path: string, n: Network) {
  let steps = 0
  let cur = 'AAA'

  for (let i = 0; ; i = (i + 1) % path.length) {
    if (cur == 'ZZZ')
      break
    cur = n.get(cur)[neighbor[path.charAt(i)]]

    steps++
  }

  return steps
}

const gcd = (a: number, b: number): number => a ? gcd(b % a, a) : b
const lcm = (a: number, b: number): number => a * b / gcd(a, b)

function navigate2(path: string, n: Network) {
  let steps = 0
  let cur = [...n.keys()].filter(e => e.slice(-1) == 'A')
  let cycle = new Array(cur.length).fill(0)

  for (let i = 0, done = false; !done ; i = (i + 1) % path.length) {
    cur.forEach((e, idx) => {
      if (e.endsWith('Z')) {
        cycle[idx] = steps
        if (cycle.every(v => v))
          done = true
      }
    })
    cur = cur.map(e => n.get(e)[neighbor[path.charAt(i)]])
    steps++
  }

  return cycle.reduce(lcm)
}

readNode("AAA = (BBB, CCC)")

const [path, network] = readMap()

print("A1:", navigate(path, network))

print("------------------")

print("A2:", navigate2(path, network))
