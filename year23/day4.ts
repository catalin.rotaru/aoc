import fs = require("fs")

const print = console.log.bind(console)

type Card = [Array<number>, Array<number>] // [winning numbers, my numbers]
type Cards = Array<Card>

const readCard = (s: string): Card =>
  s.slice(8).split(' | ').map(l =>l.split(/\s+/).map(v => +v)) as Card

function readCards(): Cards {
  return fs.readFileSync('day4.txt','utf8').split(/\r?\n/).map(readCard)
}

function countHits(c: Card) {
  const w = new Set(c[0])
  return c[1].reduce((acc, cur) => acc + (w.has(cur) ? 1 : 0), 0)
}

function value(c: Card): number {
  const hits = countHits(c)
  return hits ? 2 ** (hits - 1) : 0
}

function play(cards: Cards) {
  let n = new Array(cards.length).fill(1)
  cards.forEach((c, i) => {
    const h = countHits(c)
    for (let j = i + 1; j <= i + h; j++)
      n[j] += n[i]
  })

  //print(n)
  return n.reduce((acc, cur) => acc + cur)
}

print(value(readCard("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53")))

const cards = readCards()
//print(cards)

print("A1:", cards.reduce((acc, cur) => acc + value(cur), 0))

print("------------------")

print("A2:", play(cards))
