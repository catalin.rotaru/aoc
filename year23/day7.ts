import fs = require("fs")

type Hand = Array<string>  // 5 one-char strings
type Game = Map<string, number>

enum HandType {FIVE, FOUR, FULL, THREE, TWO, ONE, HIGH}

const card = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2']
const cardOrder = new Map(card.map((e, i) => [e, i]))
const JOKER = 'J'

const print = console.log.bind(console)

function type(h: Hand, joker: boolean = false): HandType {
  const l = h.reduce((acc, cur) => {
    acc.set(cur, (acc.get(cur) || 0) + 1)
    return acc
  }, new Map())
  const f = [...l.values()].sort((a, b) => b - a)
  const j = (joker && l.has(JOKER)) ? l.get(JOKER) : 0

  let jo = 0
  let res = HandType.HIGH
  switch (f.length) {  // number of kinds
    case 1:
      res = HandType.FIVE
      break

    case 2:
      res = (f[0] == 4) ? HandType.FOUR : HandType.FULL
      jo = [0, 1, 2, 2, 1][j]
      break

    case 3:
      res = (f[0] == 3) ? HandType.THREE : HandType.TWO
      jo = [0, 2, 3, 2][j]
      break

    case 4:
      res = HandType.ONE
      jo = (j > 0) ? 2 : 0
      break

    default:
      res = HandType.HIGH
      jo = (j > 0) ? 1 : 0
  }
  res -= jo

  return res
}

function compHands(h1: Hand, h2: Hand, joker: boolean = false): number {
  const t1 = type(h1, joker)
  const t2 = type(h2, joker)

  if (t1 != t2) 
    return t1 - t2

  const idx = h1.findIndex((h, i) => h != h2[i])
  return cardOrder.get(h1[idx]) - cardOrder.get(h2[idx])
}

function readHands(): Game {
  const m: Array<[string, number]> = fs.readFileSync('day7.txt','utf8')
    .split(/\r?\n/).map(l => {
      const [h, b] = l.split(' ')
      return [h, +b]
    })

  return new Map(m)
}

const score = (g: Game, joker: boolean = false): number =>
  [...g.keys()].map(e => e.split('')).sort((a, b) => 
    compHands(a, b, joker)).reduce((acc, cur, idx) =>
      acc + g.get(cur.join('')) * (g.size - idx), 0)

print(type(Array.from("ac1db")))
print(compHands(Array.from("77888"), Array.from("77788")))

const game = readHands()
//print(game)

print("A1:", score(game))

print("------------------")

cardOrder.set(JOKER, card.length)
//print(cardOrder)

print(type(Array.from("QQJJJ"), true))

print("A2:", score(game, true))
