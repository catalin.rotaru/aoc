import fs = require("fs")

const print = console.log.bind(console)
const printGrid = (grid: Grid) => grid.forEach(g => print(g.join('')))

type Cell = [number, number] // [x, y]
type Cells = Array<Cell>
type Row = Array<string> // [x]
type Grid = Array<Row> // [y] [x]

const neighbor = [[-1, -1], [0, -1], [1, -1], [-1, 0], // array of [x, y]
  [1, 0], [-1, 1], [0, 1], [1, 1]]

function readGrid(): Grid {
  return fs.readFileSync('day3.txt','utf8').split(/\r?\n/).map(l => l.split(''))
}

const onGrid = (g: Grid, x: number, y: number): boolean =>
  ((y >= 0) && (y < g.length) && (x >= 0) && (x < g[0].length))

const neighbors = (g: Grid, x: number, y: number): Cells =>
  neighbor.map(([dx, dy]) => <Cell>[x + dx, y + dy]).
    filter(([x, y]) => onGrid(g, x, y))

const isNotNumber = (n: string): boolean =>
  isNaN(+n)

// return [start, value]
function findNumber(r: Row, x: number): [number, number] {
  if (isNotNumber(r[x]))
    return undefined

  const start = r.slice(0, x).findLastIndex(isNotNumber) + 1
  let end = r.slice(start).findIndex(isNotNumber)
  if (end < 0)
    end = r.length

  const val = r.slice(start, start + end).join('')
  return [start, +val]
}

// return array of ["x:y", value]
function partsAround(g: Grid, x: number, y: number): Array<[string, number]> {
  const n = neighbors(g, x, y)
  const np = n.map(e => [e[1], findNumber(g[e[1]], e[0])])
  return  np.filter(e => e[1]).map(e => [e[0] + ':' + e[1][0], e[1][1]])
}

function partSum(g: Grid): number {
  const p = g.map((r, y) => r.map((v, x) => 
    (isNotNumber(v) && (v != '.') ? partsAround(g, x, y) : undefined)))
  const fp = p.flat(2).filter(e => e)
  const v = [...new Map(fp).values()]
  return v.reduce((acc, cur) => acc + cur)
}

function gearProd(g: Grid, x: number, y: number): number {
  const fp = partsAround(g, x, y)
  const v = [...new Map(fp).values()]
  return (v.length == 2) ? v[0] * v[1] : 0
}

const gearSum = (g: Grid): number =>
  g.reduce((acc, r, y) => acc + r.reduce((a, e, x) => 
    a + ((e === '*') ? gearProd(g, x, y) : 0), 0), 0)

const grid = readGrid()
//printGrid(grid)

//print(findNumber(grid[0]))
//print(findNumber(grid[7]))
//print(findNumber(grid[8]))

//print(getNumbers(grid[0]))
//print(getNumbers(grid[7]))
//print(getNumbers(grid[8]))

//print(isPart(grid, 0, 0))
//print(isPart(grid, 2, 0))

//print(partNumber(grid, 0, 0, 3))
//print(partNumber(grid, 5, 0, 3))

print ("A1:", partSum(grid))

print("------------------")

print(findNumber(grid[0], 2))
print(findNumber(grid[1], 1))
print(findNumber(grid[2], 2))
print(findNumber(grid[7], 8))
print(findNumber(grid[4], 2))

print(gearProd(grid, 3, 1))
print(gearProd(grid, 3, 3))

print ("A2:", gearSum(grid))
