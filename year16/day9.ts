import fs = require('fs')

const print = console.log.bind(console)

type Marker = [number, number] // size, count
enum Mark {BEGIN = '(', DIV = 'x', END = ')'}

// returns [marker, location]
function parseMarker(str: string, loc: number = 0): [Marker, number] {
  let begin: number = loc
  while (str.charAt(begin) !== Mark.BEGIN)
    begin++
  begin++

  let end: number = begin
  while (str.charAt(end) !== Mark.END)
    end++
  
  let mark: string = str.substring(begin, end)
  //print("mark", mark)

  let [size, count] = mark.split(Mark.DIV).map(v => +v)
  return [[size, count], end + 1]
}

// returns [data size, location]
function parseData(size: number, str: string, recursive: boolean = false, loc: number = 0): [number, number] {
  const end = loc + size
  const data = str.substring(loc, end)
  const dsz = recursive ? parseStream(data, recursive) : data.length

  return [dsz, end]
}

function parseStream(str: string, recursive: boolean = false): number {
  let res: number = 0

  for (let i: number = 0; i < str.length; i++) {
    let ch: string = str.charAt(i)

    if (ch == Mark.BEGIN) {
      let [[size, count], l1] = parseMarker(str, i)
      let [dsz, l2] = parseData(size, str, recursive, l1)
      res += count * dsz

      i = l2 - 1
      continue
    }

    if (ch != ' ')
      res++
  }

  return res++
}

print('marker', parseMarker("aaa(2x6)bbc"))
print('data', parseData(2, "aaa(2x6)bbc", false, 8))
print('stream', parseStream("aaa(2x6)bbc"))

print("------------------")

print('p', parseStream("ADVENT"))
print('p', parseStream("A(1x5)BC"))
print('p', parseStream("(3x3)XYZ"))
print('p', parseStream("A(2x2)BCD(2x2)EFG"))
print('p', parseStream("(6x1)(1x3)A"))
print('p', parseStream("X(8x2)(3x3)ABCY"))

print("------------------")

const stream: string = fs.readFileSync('day9.txt','utf8')
print('Parse', parseStream(stream))
print('Parse2', parseStream(stream, true))
