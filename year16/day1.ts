import fs = require('fs');

type Coordinate = [number, number];
type Move = [string, number]; 

let vector: Coordinate[] = [[0, 1], [1, 0], [0, -1], [-1, 0]];

function readMoves(): Move[] {
  let moves:string = fs.readFileSync('day1.txt','utf8').toString();
  let res = moves.split(", ").map(function(step: string): Move {
    return [step[0], +step.slice(1)]
  })

  return res
}

function turn(direction: number, side: string): number {
  return ((direction + ((side === "R") ? 1 : -1) % vector.length) + vector.length) % vector.length
}

function travelSteps(moves: Move[]): Coordinate {
  let x = 0;
  let y = 0;
  let d = 0;

  for (let step of moves) {
    //console.log(step)
    d = turn(d, step[0])
    x += step[1] * vector[d][0]
    y += step[1] * vector[d][1]
    //console.log([x, y])
  }

  return [x,y]
}

function distance(loc: Coordinate) {
  return Math.abs(loc[0]) + Math.abs(loc[1])
}

function traceSteps(moves: Move[]): Coordinate {
  let x = 0;
  let y = 0;
  let d = 0;

  let trace: Coordinate[] = [[x,y]];
  
  for (let step of moves) {
    //console.log(step)
    d = turn(d, step[0])

    if (vector[d][0] === 0) {
      for (let j = 1; j <= step[1]; j++)
      {
        let ny =  y + j * vector[d][1];
        for (let poz of trace) {
          if ((poz[0] === x) && (poz[1] === ny))
            return poz;
        }
        //console.log([x, ny]);
        trace.push([x, ny]);
      }
      y += step[1] * vector[d][1]
    } else {
      for (let i = 1; i <= step[1]; i++)
      {
        let nx =  x + i * vector[d][0];
        for (let poz of trace) {
          if ((poz[0] === nx) && (poz[1] === y))
            return poz;
        }
        //console.log([nx, y]);
        trace.push([nx, y]);
      }
      x += step[1] * vector[d][0]
    }
  }

  throw Error("No location visited twice!")
}

//console.log(turn(0, "R"));
//console.log(turn(3, "R"));
//console.log(turn(0, "L"));
//console.log(turn(1, "L"));
let moves = readMoves();
//console.log(moves);
console.log(distance(travelSteps(moves)));
console.log(distance(traceSteps(moves)));
