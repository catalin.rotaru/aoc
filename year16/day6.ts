import fs = require('fs')

const print = console.log.bind(console)

type Messages = Array<string>
type Frequencies = Map<string, number>

function mostFrequent(letter: string[], least: boolean = false): string {
  const f: Frequencies = new Map()

  letter.forEach((l) => f.set(l, (f.get(l) || 0) + 1))
  //print(f)

  return [...f.keys()].reduce((a, c) => (f.get(a) > f.get(c)) == least ? c : a)
}

function recoverLetter(msg: Messages, col: number, least: boolean = false): string {
  const letters = msg.map((a) => a.charAt(col))
  return mostFrequent(letters, least)
}

function recoverMessage(msg: Messages, least: boolean = false): string {
  let res:string = ''
  for (let i = 0; i < msg[0].length; i++)
    res += recoverLetter(msg, i, least)

  return res
}

function readMessages(): Messages {
  return fs.readFileSync('day6.txt','utf8').split(/\r?\n/)
}

const ltr = ['a', 'b', 'c', 'b', 'c', 'b']
print("fq", mostFrequent(ltr), mostFrequent(ltr, true))

const msg = readMessages()
print("letter", recoverLetter(msg, 0), recoverLetter(msg, 0, true))
print("recover", recoverMessage(msg), recoverMessage(msg, true))