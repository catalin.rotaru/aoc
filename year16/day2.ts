import fs = require('fs');

type Coordinate = [number, number];

let keypad: string[][] = [["1", "2", "3"], ["4", "5", "6"], ["7", "8", "9"]];
let keypad2: string[][] = [[" ", " ", "1", " ", " "], [" ", "2", "3", "4", " "], ["5", "6", "7", "8", "9"], [" ", "A", "B", "C", " "], [" ", " ", "D", " ", " "]];

function readMoves(): string[] {
  let data: string = fs.readFileSync('day2.txt','utf8').toString();
  return data.split(/\r?\n/)
}

function move(position: Coordinate, dirs: String, kp: string[][]): Coordinate {
  
  let row:number = position[0]
  let col:number = position[1]

  //console.log(dirs)
  for (let i:number = 0; i < dirs.length; i++) {
    //console.log("row", row, "col", col);

    switch (dirs[i]) {
      case "L":
        if (col > 0)
          if (kp[row][col - 1] != " ")
            col -= 1;
        break;
  
      case "R":
        if (col < kp.length - 1)
          if (kp[row][col + 1] != " ")
            col += 1;
        break;
  
       case "U":
        if (row > 0)
          if (kp[row - 1][col] != " ")
            row -= 1;
        break;
  
      case "D":
        if (row < kp[0].length - 1)
          if (kp[row + 1][col] != " ")
            row += 1;
        break;
  
      default:
        throw "Unknown move " + dirs[i] + " in " + dirs;
    }  
  }

  return [row, col]
}

function processMoves(moves: string[], kp: string[][], start: Coordinate) {
  let position: Coordinate = start;

  for (let dirs of moves) {
    position = move(position, dirs, kp);
    console.log(position, "=>", kp[position[0]][position[1]]);
  }
}

//console.log(move([2, 2], "LURDL"))

let moves = readMoves();
//console.log(moves);
processMoves(moves, keypad, [1, 1]);
console.log("---")
processMoves(moves, keypad2, [2, 0]);