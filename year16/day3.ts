import fs = require('fs');

type Triangle = [number, number, number]

function readTriangles(): Triangle[] {
  let data = fs.readFileSync('day3.txt','utf8')
  
  return data.split(/\r?\n/).map(function(s:string): Triangle {
    //console.log(s)
    let sides = s.trim().split(/ +/).map((v) => +v)
    //console.log(sides)
    return [sides[0], sides[1], sides[2]]
  });
}

function isValid(t: Triangle): boolean {
  //console.log(t)
  return ((t[0] + t[1] > t[2]) && (t[2] + t[1] > t[0]) && (t[0] + t[2] > t[1]))
}

function countValid(triangles: Triangle[]): number {
  return triangles.reduce((total, triangle) => total + +isValid(triangle), 0)
}

function transposeTriangles(triangles: Triangle[]): Triangle[] {
  let res: Triangle[] = []

  for (let i = 0; i < triangles.length; i += 3) {
    for (let j = 0; j < 3; j++)
      res.push([triangles[i][j], triangles[i + 1][j], triangles[i + 2][j]])
  }
  return res;
}

//console.log(isValid([25, 10, 5]))
let t = readTriangles()
console.log(countValid(t))

let tt = transposeTriangles(t)
console.log(countValid(tt))