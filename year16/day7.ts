import fs = require('fs')

const print = console.log.bind(console)

const SIZE = 4
const SIZE2 = 3

function isAbba(str: string): boolean {
  return (str.length == SIZE) && (str.charAt(0) != str.charAt(1)) &&
    (str.charAt(0) == str.charAt(3)) && (str.charAt(1) == str.charAt(2))
}

function hasAbba(str: string): boolean {
  for (let i = 0; i <= str.length - SIZE; i++)
    if (isAbba(str.slice(i, i + SIZE)))
      return true

  return false
}

function isAba(str: string): boolean {
  return (str.length == SIZE2) && (str.charAt(0) != str.charAt(1)) &&
    (str.charAt(0) == str.charAt(2))
}

function getAba(str: string): Array<string> {
  let res = new Array<string>()

  for (let i = 0; i <= str.length - SIZE2; i++) {
    const aba = str.slice(i, i + SIZE2)
    if (isAba(aba))
      res.push(aba)
  }

  return res
}

function hasBab(str: string[], aba: string): boolean {
  let bab = aba.charAt(1) + aba.charAt(0) + aba.charAt(1)
  return str.some(s => s.includes(bab))
}

function pushIfNonEmpty(arr: Array<string>, str: string) {
  if (str.length > 0)
    arr.push(str)
}

function splitAddress(str: string): [Array<string>, Array<string>] {
  let regular = new Array<string>()
  let hyper = new Array<string>()

  str.split('[').forEach((s) => {
      const [s1, s2] = s.split(']')
      //print('s]', [s1, s2])
      if (s2 != undefined) {
        pushIfNonEmpty(hyper, s1)
        pushIfNonEmpty(regular,s2)
      } else 
        pushIfNonEmpty(regular, s1)
  })

  return [regular, hyper]
}

function addressTLS(str: string): boolean {
  const [reg, hyp] = splitAddress(str)

  return (reg.some(s => hasAbba(s))) && (!hyp.some(s => hasAbba(s)))
}

function addressSSL(str: string): boolean {
  const [reg, hyp] = splitAddress(str)
  const aba = reg.map(v => getAba(v)).reduce((acc, cur) => acc.concat(cur), [])

  return aba.some(v => hasBab(hyp, v))
}

function readAddresses(): string[] {
  return fs.readFileSync('day7.txt','utf8').split(/\r?\n/)
}

print("abba", isAbba("abbaa"))
print("abba", isAbba("aaaa"))
print("abba", isAbba("abbc"))
print("abba", isAbba("xyyx"))
print("has abba", hasAbba("iodxoj"))
print("has abba", hasAbba("ioxxoj"))
print("has abba", hasAbba("iooioj"))
print("has abba", hasAbba("abjooj"))

//print(splitAddress("[abcde]sad"))
//print(splitAddress("[abba][mnop]"))

print("supports", addressTLS("abba[mnop]qrst"))
print("supports", addressTLS("abcd[bddb]xyyx"))
print("supports", addressTLS("aaaa[qwer]tyui"))
print("supports", addressTLS("ioxxoj[asdfgh]zxcvbn"))

print("------------------")

const addr = readAddresses()
print("RESULT", addr.filter(addressTLS).length)

print("------------------")

print("aba", isAba("abcd"))
print("aba", isAba("xyz"))
print("aba", isAba("yxy"))
print("get aba", getAba("zazbz"))
print("has bab", hasBab(["zazbz", "abcb"], "cbc"))

print("ssl", addressSSL("aba[bab]xyz"))
print("ssl", addressSSL("xyx[xyx]xyx"))
print("ssl", addressSSL("aaa[kek]eke"))
print("ssl", addressSSL("zazbz[bzb]cdb"))

print("------------------")

print("RESULT", addr.filter(addressSSL).length)