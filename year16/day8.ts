import fs = require('fs')

const print = console.log.bind(console)

const ON = 1
const OFF = 0
type Row = Array<number>
type Matrix = Array<Row> // y, x => ON/OFF

const rotate = (row: Row, shift: number): Row => 
  row.splice(-shift).concat(row)

const sum = (m: Matrix) =>
  m.reduce((acc, row) => acc + row.reduce((a, c) => a + c), 0)

class Screen {
  pix: Matrix

  constructor(width: number = 50, height: number = 6) {
    this.pix = Array.from({length: height}, () => new Array(width).fill(OFF))
  }

  print(reader: boolean = false) {
    const ch = reader ? [' ', '█'] : ['.', '#']
    this.pix.forEach(r => print(r.map(v => ch[v]).join('')))
  }

  turnRect(width: number, height: number) {
    this.pix.slice(0, height).forEach(r => r.fill(ON, 0, width))
  }

  rotRow(row: number, shift: number) {
    this.pix[row] = rotate(this.pix[row], shift)
  }

  rotCol(col: number, shift: number) {
    let a = this.pix.map(r => r[col])
    a = rotate(a, shift)
    a.forEach((v, i) => this.pix[i][col] = v)
  }
}

function readScreen(): Screen {
  let scr = new Screen()
  const str = fs.readFileSync('day8.txt','utf8').split(/\r?\n/)

  str.forEach(l => {
    const w = l.split(' ')
    
    if (w[0] == 'rect') {
      const [a, b] = w[1].split('x')
      scr.turnRect(+a, +b)
    } else {
       const a = +w[2].slice(2)
       const b = +w[4]
      if (w[1] == 'row')
        scr.rotRow(a, b)
      else
        scr.rotCol(a, b)
    }
  })

  return scr
}

print(rotate([1, 2, 3, 4, 5, 6, 7], 3))

print("------------------")

let s = new Screen(7, 3)
s.turnRect(3, 2)
s.rotCol(1, 1)
s.rotRow(0, 4)
s.rotCol(1, 1)
s.print()

print("------------------")

let screen = readScreen()
print("Lit", sum(screen.pix))
screen.print(true)
