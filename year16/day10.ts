import fs = require('fs')

const print = console.log.bind(console)

type Chip = number
type Instr = [boolean, number, boolean] // take highest, destination, to output
type Bots = Map<number, Bot> // id => bot
type Output = Map<number, number>

class Bot {
  id: number
  chips: Array<Chip>
  instr: Array<Instr>
  bots: Bots
  out: Output

  constructor(id: number, bots: Bots, out: Output) {
    this.id = id
    this.bots = bots
    this.out = out

    this.chips = new Array()
    this.instr = new Array()

    bots.set(id, this)
  }

  addChip(chip: Chip) {
    this.chips.push(chip)
    this.execute()
  }

  addInstruction(high: boolean, dest: number, out: boolean) {
    this.instr.push([high, dest, out])
    this.execute()
  }

  execute() {
    if ((this.chips.length < 2) || (this.instr.length < 2))
      return

    this.instr.forEach(([high, dest, too]) => {
      //print("cmp", this.id, this.chips)
      const v = this.chips.sort((a, b) => a - b)
      if ((v[0] == 17) && (v[1] == 61))
        print("Cmp", this.id)
        
      const val = high ? v[1] : v[0]

      if (too)
        this.out.set(dest, val)
      else 
        getBot(dest, this.bots, this.out).addChip(val)
    })

    this.chips.length = 0
  }
}

const getBot = (id: number, bots: Bots, out: Output) =>
  bots.has(id) ? bots.get(id) : new Bot(id, bots, out)

function readInstructions(bots: Bots, out: Output) {
  return fs.readFileSync('day10.txt','utf8').split(/\r?\n/).forEach(l => {
    const w = l.split(' ')
    if (w[0] == 'value') {
      //print('value', w[1], '=>', w[4], w[5])
      getBot(+w[5], bots, out).addChip(+w[1], )
    } else {
      //print('instr bot', w[1], '=>', w[5], w[6], w[10], w[11])
      const b = getBot(+w[1], bots, out)
      b.addInstruction(false, +w[6], w[5] == 'output')
      b.addInstruction(true, +w[11], w[10] == 'output')
    }
  })
}

const b = new Map()
const o = new Map()
getBot(1, b, o).addChip(5)
getBot(1, b, o).addInstruction(true, 2, false)
print("bots", b)

print("------------------")

const bots = new Map()
const out = new Map()
readInstructions(bots, out)
//print("Bots", bots)
print("Out", out.get(0) * out.get(1) * out.get(2))
