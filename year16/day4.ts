import fs = require('fs')

const print = console.log.bind(console)

class Room {
  name: string
  sector: number
  checksum: string


  constructor(room: string) {
    let parts = room.split('-')
  
    let [id, cs] = parts.pop().split('[')
  
    this.name = parts.join('')
    this.sector = +id
    this.checksum  = cs.slice(0, -1)
  }

  computeChecksum(): string {
    let fq: Map<string, number> = new Map()

    this.name.split('').forEach((ch) => {
      let count = fq.get(ch)
      fq.set(ch, count ? count + 1 : 1)
    })
    //print(fq)

    let sorted = [...fq].sort(([k1, v1], [k2, v2]) => 
      (v1 == v2) ? (k1 > k2 ? 1 : -1) : v2 - v1)
    //print(sorted)

    return sorted.map((v)=>v[0]).slice(0, 5).join('')
  }

  isReal(): boolean {
    return this.checksum == this.computeChecksum()
  }
}

function readRooms(): Room[] {
  return fs.readFileSync('day4.txt','utf8').split(/\r?\n/).map((s) => new Room(s))
}

function computeReals(rooms: Room[]): number {
  return rooms.reduce((acc, cur) => cur.isReal() ? acc + cur.sector : acc, 0)
}

function rotateChar(char: string, offset: number): string {
  let origin = "a".charCodeAt(0)
  let length = "z".charCodeAt(0) - origin + 1

  let res = origin + (((char.charCodeAt(0) - origin) + offset) % length)
  //print(char, origin, length, res)
  return String.fromCharCode(res)
}

function cipher(str: string, key: number): string {
  return [...str].map((c) => rotateChar(c, key)).join('')
}

function findRoom(rooms: Room[], name: string): Room {
  return rooms.find((r) => cipher(r.name, r.sector).startsWith(name))
}

let room1 = new Room("aaaaa-bbb-z-y-x-123[abxyz]")
print(room1, room1.isReal())
let room2 = new Room("a-b-c-d-e-f-g-h-987[abcde]")
print(room2, room2.isReal())
let room3 = new Room("not-a-real-room-404[oarel]")
print(room3, room3.isReal())
let room4 = new Room("totally-real-room-200[decoy]")
print(room4, room4.isReal())
print("Real Sum", computeReals([room1, room2, room3, room4]))

let rooms = readRooms()
print("Real Sum", computeReals(rooms))

print("rotate", rotateChar("a", 1))
print("rotate", rotateChar("z", 1))
print("cipher", cipher("qzmtzixmtkozyivhz", 343))

print("special room sector", findRoom(rooms, "northpoleobjects").sector)
