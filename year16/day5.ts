import crypto = require('crypto')

const print = console.log.bind(console)

function findPassword(door: string, zeroes: number = 5, size: number = 8): string {
  let password: string = ""

  for (let i = 0 ; password.length < size ; i++) {
    const root: string = door + i
    const md5: string = crypto.createHash('md5').update(root).digest('hex')
    //print (root, md5)
    if (md5.substring(0, zeroes) == '00000')
      password += md5.charAt(zeroes)
  }

  return password
}

function findPassword2(door: string, zeroes: number = 5, size: number = 8): string {
  let password: string[] = new Array<string>(size)
  let count = 0

  for (let i = 0 ; count < size; i++) {
    const root: string = door + i
    const md5: string = crypto.createHash('md5').update(root).digest('hex')
    //print (root, md5)
    if (md5.substring(0, zeroes) == '00000') {
      let pos: number = md5.charCodeAt(zeroes) - '0'.charCodeAt(0)
      //print("md5 match", md5, pos)

      if ((pos < size) && (!password[pos])) {
        password[pos] = md5.charAt(zeroes + 1)
        count++
        //print("pass char", password)
      }
    }
  }

  return password.join('')
}

print ("password", findPassword("abc"))
print ("password", findPassword("cxdnnyjw"))
print ("password2", findPassword2("abc"))
print ("password2", findPassword2("cxdnnyjw"))